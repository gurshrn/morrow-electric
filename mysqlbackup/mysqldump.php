<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'aws/vendor/autoload.php';
	
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

ini_set('memory_limit', '-1');
ini_set('upload_max_filesize ', '1G');
ini_set('post_max_size ', '1G');
ini_set('max_execution_time', 30000); //300 seconds = 5 minutes
ini_set('max_input_time', 30000); //300 seconds = 5 minutes


$dt = new DateTime();
$date = $dt->format('Y-m-d H:i:s');


$database = 'morrowel_morrow';
$user = 'morrowel_morrow';
$pass = 'morrow123';
$host = 'localhost';
$filename = 'dump'.time().'.sql';
$dir = dirname(__FILE__) . '/'.$filename;

echo "<h3>Backing up database to `<code>{$dir}</code>`</h3>";

exec("mysqldump --user={$user} --password={$pass} --host={$host} {$database} --result-file={$dir} 2>&1", $output);

$bucket = "sqlbackupmodule";
    
$sdk = new Aws\Sdk([
	'region'   => 'us-east-2',
	'version'  => 'latest',
	'DynamoDb' => [
		'region' => 'us-east-2'
	],
  'credentials' => array(
	'key' => 'AKIAJFBA5DL6IX32MEUA',
	'secret'  => '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',	
)
]);


// Use an Aws\Sdk class to create the S3Client object.
$s3Client = $sdk->createS3();
 
$uploader = new MultipartUploader($s3Client, $dir, [
'bucket' => $bucket,
'ACL'    => 'public-read',
'key'    => 'morrowelectric/'.$filename,
]);

try {
	$result = $uploader->upload();
	//echo "Upload complete: {$result['ObjectURL']}\n";
	$msg = "S3 Upload Successful.";	
	$s3file=$result['ObjectURL'];
	
	// send email
	
	$to = "sandeep.soni@imarkinfotech.com, daniel@creativeone.ca";
	$subject = "My Sql Backup - morrowelectric - ".$date;

	$message = "
	<html>
	<head>
	<title>My Sql Backup</title>
	</head>
	<body>
	<p>This email contains sql backup link!</p>
	<a href='".$s3file."'>Download</a>
	</body>
	</html>
	";

	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

	// More headers
	$headers .= 'From: <kj@creativeone.ca>' . "\r\n";
	$headers .= 'Cc: aman.kalra@imarkinfotech.com' . "\r\n";

	//mail($to,$subject,$message,$headers);

	
} catch (MultipartUploadException $e) {
	echo $e->getMessage();
}
