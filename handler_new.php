<?php 
require_once   'setup.php';
date_default_timezone_set("US/Eastern"); 

function ShowCustomerGen()
{
	$db = get_connection();	
	get_mysqlconnection();	
	
	$sql= "SELECT * FROM service_region"; 
	$result=mysql_query($sql);
	
	
	$e=$_REQUEST['e'];
	$cust_id=$_REQUEST['cust_id']; 
?>
<?php
if($e==1)
{
?>
<script type="text/javascript">


	jQuery( ".datepicker" ).datepicker({
        changeMonth: false,
        changeYear: false
		
    });
	
	jQuery('#edit_cust_gen').validate({
		rules: {
			label: {
				required: true
			},
			service_reg: {
				required: true
			},
			sn: {
				required: true
			} 
		}, 
		submitHandler: function(form) {
			var cust_id=jQuery('#cust_idd').val();
			jQuery('#cust_gen_loader').show(); 
			jQuery('.cust_gen_result').empty().show();
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: '<?php echo SITE_URL; ?>handler_new.php', 
				success: function(data) 
				{
					//alert(data);
					if(data==1)
					{
						jQuery('#cust_gen_loader').hide(); 
						jQuery('.cust_gen_result').empty().append('<div class="msg_res green">Generator has been updated</div>');  
						jQuery('.cust_gen_result').fadeOut(3000); 
						jQuery('#cust_gen_area_1').slideUp();  
						reload_gen_section(cust_id);  
						
					}  
					if(data==2)
					{
						jQuery('#cust_gen_loader').hide(); 
						jQuery('.cust_gen_result').empty().append('<div class="msg_res red">Check enter Dates</div>');  
						jQuery('.cust_gen_result').fadeOut(3000); 
											
					}  
				}
			});
		}
		
	});

	function show_inventory(e)
	{
		var inv1=jQuery('input[name=inventory_1]').val();
		var inv2=jQuery('input[name=inventory_2]').val();
		var inv3=jQuery('input[name=inventory_3]').val();
		jQuery.ajax({type: "POST",
		url: "handler_new.php",
		data: "e="+e+"&inv1="+inv1+"&inv2="+inv2+"&inv3="+inv3+"&action=viewInventory",
		success:function(result)
		{
			if(e==1)
			{
				jQuery('.oil_filter').empty().append(result); 
			}	
			else if(e==2)
			{
				jQuery('.air_filter').empty().append(result); 
			}	
			else if(e==3)
			{
				jQuery('.spark_plug').empty().append(result); 
			}	 
		},
		error:function(e){
		console.log(e);
		}	
		});
	}	

function reload_gen_section(row_id)
{
	
	jQuery.ajax({type: "POST",
		url: "handler_new.php", 
		data: "row_id="+row_id+"&action=ReloadGenSec",
		success:function(result)   
		{
			jQuery('.gen_section').empty().append(result);
		},
		error:function(e){
		console.log(e);
		}	
		});
}
	
</script>
<div class="new-gen-area">
        
		<h3 class="section-hdr">Edit Generator <a href="javascript:void(0);" onclick="jQuery('.cust_gen_area').empty().slideUp();" class="close"><i class="fa fa-plus"></i></a></h3>
        <div class="gen-ins-area clearfix">
        <form role="form" class="cs-form" name="edit_cust_gen" id="edit_cust_gen" action="" method="post">
			<input type="hidden" name="cust_id"  value="<?php echo $cust_id; ?>" > 
			<input type="hidden" name="action" value="EditCustomerGenerator" > 
			<input name="oil_filter_id" id="oil_filter_id" type="hidden" value="<?php echo get_cust_gen_info($cust_id,'oil_filter'); ?>"> 
			<input name="air_filter_id" id="air_filter_id" type="hidden" value="<?php echo get_cust_gen_info($cust_id,'air_filter'); ?>"> 
			<input name="spark_plug_id" id="spark_plug_id" type="hidden" value="<?php echo get_cust_gen_info($cust_id,'spark_plug'); ?>">    
			<div class="row">  
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="label" class="is-filled" value="<?php echo get_cust_gen_info($cust_id,'label'); ?>">
                <label class="im-label">Label</label>
              </div> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4"> 
              <div class="group">
			   <select name="service_reg" >
                  <option value="">Service Region</option>
					<?php
					$service_reg=get_cust_gen_info($cust_id,'service_reg');
					while($row=mysql_fetch_array($result))
					{					
					?>
						<option value="<?php echo $row['name']; ?>" <?php if($service_reg==$row['name']) { echo 'selected'; } ?>><?php echo $row['name']; ?></option>	
					<?php
					}
					?>
                  </select>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="model" class="is-filled" value="<?php echo get_cust_gen_info($cust_id,'model'); ?>">
                <label class="im-label">Model</label>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="group">
				<?php $type=get_cust_gen_info($cust_id,'type'); ?>
               <p class="radio-btns"><strong>Type</strong> 
                 <label>
                   <input type="radio" name="type" value="Liquid" <?php if($type=="Liquid") { echo 'checked'; } ?>>
                   Liquid Cooled</label>
                 <label>
                   <input type="radio" name="type"  value="Air" <?php if($type=="Air") { echo 'checked'; } ?>>
                   Air Cooled</label>  
                 <br>
               </p>  
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6">
              <div class="group">
                <input type="text" name="sn" class="is-filled" value="<?php echo get_cust_gen_info($cust_id,'sn'); ?>">
                <label class="im-label">SN</label>
              </div>
            </div>
          </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4">
				<?php 
				$install_date=get_cust_gen_info($cust_id,'install_date'); 
				$warr_exp=get_cust_gen_info($cust_id,'warr_exp'); 

				$install_date=date('m/d/Y', strtotime($install_date));
				$warr_exp=date('m/d/Y', strtotime($warr_exp));
				?>
              <div class="group">
                <input type="text" name="install_date" class="datepicker is-filled" value="<?php echo $install_date; ?>" readonly>
                <label class="im-label">Installing Date</label>
              </div> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
			    <input type="text" name="warr_exp" class="datepicker is-filled" value="<?php echo $warr_exp; ?>" readonly>   
                <label class="im-label">Warranty Date</label>  
              </div> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="oil_qty" class="is-filled" value="<?php echo get_cust_gen_info($cust_id,'oil_qty'); ?>">
                <label class="im-label">Oil Qty</label>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="row"><div class="col-sm-6"><div class="group">
				<?php 
				$oil_filter=get_cust_gen_info($cust_id,'oil_filter'); 
				?>
                <input type="text" name="inventory_1" class="is-filled" value="<?php echo get_part_no($oil_filter); ?>" onclick="show_inventory(1);">
                <label class="im-label">Oil Filter</label>
				<div class="oil_filter"></div>
              </div></div>
              <div class="col-sm-6"><div class="group">
				<?php 
				$air_filter=get_cust_gen_info($cust_id,'air_filter'); 
				?>
                <input type="text" name="inventory_2" class="is-filled" value="<?php echo get_part_no($air_filter); ?>" onclick="show_inventory(2);">  
                <label class="im-label">Air Filter</label>  
				<div class="air_filter"></div> 
              </div></div></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6">
              <div class="group">
				<?php 
				$spark_plug=get_cust_gen_info($cust_id,'spark_plug'); 
				?>
                <input type="text" name="inventory_3" class="is-filled" value="<?php echo get_part_no($spark_plug); ?>" onclick="show_inventory(3);">    
                <label class="im-label">Spark Plugs</label> 
				<div class="spark_plug"></div> 
              </div> 
            </div>
          </div>
          <div class="text-right"> 
			<?php $wint=get_cust_gen_info($cust_id,'winterize'); ?>
          <p class="ng-btns"><label><input name="winter" type="checkbox" value="Yes" <?php if($wint=="Yes") { echo 'checked'; } ?>> Winterize</label>   
		  <button type="submit" class="cs-btn btn-green" name="submit ">Save Generator</button>
		  <img src="<?php echo SITE_URL; ?>assets/images/loader.gif" id="cust_gen_loader" style="display:none;"/>	
        </div> 
        </form>
		<div class="cust_gen_result"></div>
        <div class="clr"></div> 
         
      </div> 
        </div>
<?php
}
else
{
?>
<script type="text/javascript">
jQuery(document).ready(function()
{
	jQuery( ".datepicker" ).datepicker({
        changeMonth: false,
        changeYear: false,
		//minDate:0 
		
    });
	
	function reload_gen_section(row_id)
	{
		
		jQuery.ajax({type: "POST",
		url: "handler_new.php", 
		data: "row_id="+row_id+"&action=ReloadGenSec",
		success:function(result)   
		{
			
			jQuery('.gen_section').empty().append(result);
		},
		error:function(e){
		console.log(e);
		}	
		}); 
	}
	
	jQuery('#cust_gen').validate({ 
		rules: {
			label: {
				required: true
			},
			service_reg: {
				required: true
			},
			sn: {
				required: true
			} 
		},
		submitHandler: function(form) {
			jQuery('#cust_gen_loader').show();
			jQuery('.cust_gen_result').empty().show();
			var cust_id=jQuery('#cust_idd').val();
			var ram=jQuery('#ram').val();  
			/* var typee=jQuery('input[name=type]:checked').length;
			if(typee=="0")
			{
				alert('Select Type');
				return false;
			} */ 
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: '<?php echo SITE_URL; ?>handler_new.php', 
				success: function(data) 
				{
					if(data==1) 
					{
						jQuery('#cust_gen_loader').hide(); 
						jQuery('.cust_gen_result1').empty().append('<div class="msg_res green">Generator has been added</div>'); 
						jQuery('.cust_gen_result1').fadeOut(3000);  
						jQuery('#cust_gen').trigger('reset'); 
						reload_gen_section(cust_id);  
						if(ram=="0")  
						{	
							reload_gen_section(cust_id);  
						}
						else
						{
							location.reload(); 
						}	
					}
					if(data==2)
					{
						jQuery('#cust_gen_loader').hide(); 
						jQuery('.cust_gen_result1').empty().append('<div class="msg_res red">Check enter dates</div>'); 
						jQuery('.cust_gen_result1').fadeOut(3000);  
						
					}
				}
			});
		}
		
	});
});
	
	
	
	function show_inventory(e)
	{
		var inv1=jQuery('input[name=inventory_1]').val(); 
		var inv2=jQuery('input[name=inventory_2]').val();
		var inv3=jQuery('input[name=inventory_3]').val();
		var loader='<img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" style="padding: 30px;" />';
		
		if(e==1) 
		{
			jQuery('.oil_filter').empty().show(); 
			jQuery('.oil_filter').empty().append(loader); 
		}	
		else if(e==2) 
		{
			jQuery('.air_filter').empty().show(); 
			jQuery('.air_filter').empty().append(loader); 
		}	
		else if(e==3)
		{
			jQuery('.spark_plug').empty().show();;      
			jQuery('.spark_plug').empty().append(loader);      
		}	
		 
		jQuery.ajax({type: "POST",
		url: "handler_new.php",
		data: "e="+e+"&inv1="+inv1+"&inv2="+inv2+"&inv3="+inv3+"&action=viewInventory",
		success:function(result)
		{
			if(e==1)
			{
				jQuery('.oil_filter').empty().append(result); 
			}	
			else if(e==2)
			{
				jQuery('.air_filter').empty().append(result); 
			}	
			else if(e==3)
			{
				jQuery('.spark_plug').empty().append(result); 
			}	 
		},
		error:function(e){
		console.log(e);
		}	
		});
	}
	
</script>
<div class="new-gen-area">
        
		<h3 class="section-hdr">New Generator</h3>
        <div class="gen-ins-area clearfix">
        <form role="form" class="cs-form" name="cust_gen" id="cust_gen" action="" method="post">
			<input type="hidden" name="cust_id" value="<?php echo $cust_id; ?>" > 
			<input type="hidden" name="action" value="AddCustomerGenerator" > 
			<input type="hidden" name="oil_filter_id" id="oil_filter_id" > 
			<input type="hidden" name="air_filter_id" id="air_filter_id" > 
			<input type="hidden" name="spark_plug_id" id="spark_plug_id" >   
			<div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="label"> 
                <label class="im-label">Label</label>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
			   <select name="service_reg" >
                  <option value="">Service Region</option>
					<?php
					while($row=mysql_fetch_array($result))
					{					
					?>
						<option value="<?php echo $row['name']; ?>"><?php echo $row['name']; ?></option>	
					<?php
					}
					?>
                  </select>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="model">
                <label class="im-label">Model</label>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="group">
               <p class="radio-btns"><strong>Type</strong> 
                 <label>
                   <input type="radio" name="type" value="Liquid">
                   Liquid Cooled</label>
                 <label>
                   <input type="radio" name="type" value="Air">
                   Air Cooled</label>
                 <br>
               </p>  
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6">
              <div class="group">
                <input type="text" name="sn">
                <label class="im-label">SN</label>
              </div>
            </div>
          </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="install_date" class="datepicker is-filled" value="<?php echo date('m/d/Y') ?>" readonly>
                <label class="im-label">Installation Date</label> 
              </div> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
			    <input type="text" name="warr_exp" class="datepicker is-filled" value="<?php echo date('m/d/Y') ?>" readonly> 
                <label class="im-label">Warranty Expiration</label> 
              </div> 
            </div>
            <div class="col-lg-2 col-md-2 col-sm-4">
              <div class="group">
                <input type="text" name="oil_qty">
                <label class="im-label">Oil Qty</label>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
              <div class="row"><div class="col-sm-6"><div class="group"> 
                <input type="text" name="inventory_1" class="is-filled" onkeyup="show_inventory(1);">
                <label class="im-label">Oil Filter</label>
				 <div class="oil_filter" style="display:none;"></div> 
              </div></div>  
              <div class="col-sm-6"><div class="group">
                <input type="text" name="inventory_2" class="is-filled" onkeyup="show_inventory(2);">
                <label class="im-label">Air Filter</label> 
				 <div class="air_filter" style="display:none;"></div>
              </div></div></div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-6">
              <div class="group">
                <input type="text" name="inventory_3" class="is-filled" onkeyup="show_inventory(3);"> 
                <label class="im-label">Spark Plugs</label>    
				 <div class="spark_plug" style="display:none;"></div>
              </div> 
            </div>
          </div>
          <div class="text-right"> 
          <p class="ng-btns"><label><input name="winter" type="checkbox" value="Yes"> Winterize</label> 
		  <button type="submit" class="cs-btn btn-green" name="submit ">Create Generator</button> 
		  <img src="<?php echo SITE_URL; ?>assets/images/loader.gif" id="cust_gen_loader" style="display:none;"/>	
        </div> 
        </form>
		<div class="cust_gen_result1"></div> 
        <div class="clr"></div> 
         
      </div> 
        </div>
<?php
}
}

function AddCustomerGenerator()
{
	$db = get_connection();	
	get_mysqlconnection();	
	$today=date('Y-m-d'); 
	$cust_id=$_REQUEST['cust_id']; 
	$label=$_REQUEST['label'];
	$service_reg=$_REQUEST['service_reg'];
	$model=$_REQUEST['model'];
	$type=$_REQUEST['type'];
	$sn=$_REQUEST['sn'];
	
	$install_date=$_REQUEST['install_date'];
	$warr_exp=$_REQUEST['warr_exp'];
	
	$oil_qty=$_REQUEST['oil_qty'];
	$inventory_1=$_REQUEST['oil_filter_id'];
	$inventory_2=$_REQUEST['air_filter_id'];
	$inventory_3=$_REQUEST['spark_plug_id'];
	$winterize=$_REQUEST['winter']; 
	
	$install_date=date('Y/m/d', strtotime($install_date));
	$warr_exp=date('Y/m/d', strtotime($warr_exp)); 
	if($install_date > $warr_exp)
	{
		echo "2";
		die;
	}	
	
	
	$sql="insert into customer_generator (cust_id,label,service_reg,model,type,sn,install_date,warr_exp,oil_qty,oil_filter,air_filter,spark_plug,winterize,status,cr_date) values('".$cust_id."','".$label."','".$service_reg."','".$model."','".$type."','".$sn."','".$install_date."','".$warr_exp."','".$oil_qty."','".$inventory_1."','".$inventory_2."','".$inventory_3."','".$winterize."','1','".$today."')";  
	$res=mysql_query($sql);    
	echo "1"; 
}

function EditCustomerGenerator()
{
	$db = get_connection();	
	get_mysqlconnection();	 
	$cust_id=$_REQUEST['cust_id']; 
	$label=$_REQUEST['label'];
	$service_reg=$_REQUEST['service_reg'];
	$model=$_REQUEST['model'];
	$type=$_REQUEST['type'];
	$sn=$_REQUEST['sn'];
	$install_date=$_REQUEST['install_date'];
	$warr_exp=$_REQUEST['warr_exp'];
	$oil_qty=$_REQUEST['oil_qty'];
	$inventory_1=$_REQUEST['oil_filter_id'];
	$inventory_2=$_REQUEST['air_filter_id'];
	$inventory_3=$_REQUEST['spark_plug_id'];
	$winterize=$_REQUEST['winter']; 
	$install_date=date('Y/m/d', strtotime($install_date));
	$warr_exp=date('Y/m/d', strtotime($warr_exp)); 
	
	$install_date=date('Y/m/d', strtotime($install_date));
	$warr_exp=date('Y/m/d', strtotime($warr_exp)); 
	if($install_date > $warr_exp)
	{
		echo "2";
		die;
	}	 
		
	update_cust_gen_info($cust_id,'label',$label);
	update_cust_gen_info($cust_id,'service_reg',$service_reg);
	update_cust_gen_info($cust_id,'model',$model);
	update_cust_gen_info($cust_id,'type',$type);
	update_cust_gen_info($cust_id,'sn',$sn);
	update_cust_gen_info($cust_id,'install_date',$install_date);
	update_cust_gen_info($cust_id,'warr_exp',$warr_exp);
	update_cust_gen_info($cust_id,'oil_qty',$oil_qty);
	update_cust_gen_info($cust_id,'oil_filter',$inventory_1);
	update_cust_gen_info($cust_id,'air_filter',$inventory_2);
	update_cust_gen_info($cust_id,'spark_plug',$inventory_3); 
	update_cust_gen_info($cust_id,'winterize',$winterize);  
	echo "1";
}


function viewInventory()
{
	$db = get_connection();	
	get_mysqlconnection();	 
	$idd=$_REQUEST['e']; 
	$inv1=$_REQUEST['inv1']; 
	$inv2=$_REQUEST['inv2']; 
	$inv3=$_REQUEST['inv3']; 
	if($idd==1)
	{
		$sql="select * from inventory where part_no like '%".$inv1."%' and category='Oil Filters'";
	}	
	else if($idd==2)
	{
		$sql="select * from inventory where part_no like '%".$inv2."%' and category='Air Filters'";
	}	
	else 
	{
		$sql="select * from inventory where part_no like '%".$inv3."%' and category='Spark Plug'";
	}
	$res=mysql_query($sql);
	?>
	<ul class="filter_list">
	<?php
	while($row=mysql_fetch_array($res))
	{
		$row['id'];
		$row['part_no'];
		?>
		<li><a href="javascript:void(0);" onclick="sel_item('<?php echo $idd; ?>','<?php echo $row['id']; ?>','<?php echo $row['part_no']; ?>');"><?php echo $row['part_no']; ?></a></li>   
		<?php
	}	
	?>
	</ul>
	<?php
} 


function ReloadGenSec()
{
	$db = get_connection();	
	get_mysqlconnection();	 
	$cust_id=$_REQUEST['row_id'];    
?>
<h2 class="cust_work">Generators</h2>
	<table id="" class="display table data-tbl" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th></th>
        <th>Label</th>
        <th>Type </th>
        <th>Model </th>       
        <th>Installation Date </th>
        <th>SN</th> 
        <th></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th></th>
        <th>Label</th>
        <th>Type </th>
        <th>Model </th>       
        <th>Installation Date </th>
        <th>SN</th>
        <th></th>
      </tr>
    </tfoot>
    <tbody>
	<?php
	$sql="select * from customer_generator where cust_id='".$cust_id."' order by install_date desc";
	$res=mysql_query($sql); 
	while($row=mysql_fetch_array($res)) 
	{	 
	?>
	<tr>	
        <td></td>
        <td><?php echo $row['label']; ?></td>
        <td><?php echo $row['type']; ?> </td>
        <td><?php echo $row['model']; ?> </td>        
        <td><?php $install_date=$row['install_date']; echo date('F d, Y', strtotime($install_date)); ?></td>
        <td><?php echo $row['sn']; ?></th>  
        <td><a href="javascript:void(0)" class="btn-edit" onclick="show_cust_gen('1','<?php echo $row['id']; ?>');">Edit</a></td>   
    </tr> 
	<?php
	}
	?>
	</tbody>
	</table>
<?php	
} 

function CheckCustomerGenrator()
{
	$db = get_connection();	
	get_mysqlconnection();	 
	$cust_id=$_REQUEST['cust_id'];  
	$count=cust_has_genrator($cust_id); 
	$sql="select * from customer_generator where cust_id='".$cust_id."'";
	if($count > 0)  
	{ 
	$res=mysql_query($sql); 
	?>
	<p class="cs-radio">Choose a generator    
		<?php
		while($row=mysql_fetch_array($res))
		{	
			$id=$row['id'];
			$label=$row['label'];
			$sn=$row['sn'];
		?> 
		<label>   
			<input type="radio" name="genrator" value="<?php echo $id; ?>">   
			<span class="custm-radio"></span> <?php echo $label; ?> /  <?php echo $sn; ?> 
		</label>    
		<?php
		}
		?> 
	</p>
	<?php	
	}
	else 
	{
	?>
	<p>No generators were found  <a href="javascript:void(0);" class="cs-btn btn-blue" onclick="show_cust_gen('0','<?php echo $cust_id; ?>');">New Generator</a> </p>
	<?php       
	}		
	
}

function AddAssignedService()
{
	$db = get_connection();	
	get_mysqlconnection();	 
	$cr_date=date('Y-m-d');
	$gen_id=$_REQUEST['gen_id'];   
	$staff_id=$_REQUEST['staff_id'];   
	$assign_date=$_REQUEST['assign_date'];    
	
	$service_type=$_REQUEST['service_type'];
	$notes=mysql_escape_string($_REQUEST['notes']); 
	
	$assign_date=date('Y-m-d', strtotime($assign_date)); 
	
	$del_sql="delete from genassignservices where gen_id='".$gen_id."'";
	$res_del=mysql_query($del_sql);
	
	$sql="insert into genassignservices (gen_id,staff_id,service_date,cr_date,type,notes,status) values ('".$gen_id."','".$staff_id."','".$assign_date."','".$cr_date."','".$service_type."','".$notes."','1')"; 
	$res=mysql_query($sql);    
	echo "1";   
} 

?>