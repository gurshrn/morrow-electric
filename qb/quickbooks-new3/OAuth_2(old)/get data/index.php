<?php
$session_id = session_id();
if (empty($session_id))
{
    session_start();
}
include('src/config.php');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Data\IPPReferenceType;
use QuickBooksOnline\API\Data\IPPAttachableRef;
use QuickBooksOnline\API\Data\IPPAttachable;
use QuickBooksOnline\API\Facades\Bill;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Data\IPPPurchase;

use QuickBooksOnline\API\Facades\PurchaseOrder;
use QuickBooksOnline\API\Facades\Vendor;



// use QuickBooksOnline\API\Data\IPPPurchase;
// use QuickBooksOnline\API\Data\IPPPurchaseOrder;

/*require "vendor/autoload.php";*/

/*use QuickBooksOnline\API\DataService\DataService;*/

// $dataService = DataService::Configure(array(
         // 'auth_mode'        => "oauth2",
         // 'ClientID'         => "Q0zH9nlzo11UMf3wIvwCE7f8OC3nxmQKMCHoR3CwrXepGHflRO",
         // 'ClientSecret'     => "hfVIXsdRVxCri9A8J346NJjoT5KJVo3ZPu71kAwm",
         // 'accessTokenKey'   => $_SESSION['access_token'],
         // 'refreshTokenKey'  => $_SESSION['refresh_token'],
         // 'QBORealmID'       => "123145866267839",
         // 'baseUrl'          => "Development"
// ));
$dataService = DataService::Configure(array(
         'auth_mode'        => "oauth2",
         'ClientID'         => "Q0uhh3ysYcNzrQD2X54zbrdQbSWqvuNseeMbEEzT63pkadLYNj",
         'ClientSecret'     => "H8keudf1pQBuadDNRLo2Ony9GP2JR4AAdEmfMEOb",
         'accessTokenKey'   => $_SESSION['access_token'],
         'refreshTokenKey'  => $_SESSION['refresh_token'],
         'QBORealmID'       => "123146152299339",
         //'QBORealmID'       => "123146149916654",
         'baseUrl'          => "Development"
));

/*var_dump($dataService);
echo "string"*/;

$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");


$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();

$accessToken = $OAuth2LoginHelper->refreshToken();

$dataService->updateOAuth2Token($accessToken);

$dataService->throwExceptionOnError(true);


$error = $OAuth2LoginHelper->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    return;
}
$dataService->updateOAuth2Token($accessToken);

// Run a query
/*$entities = $dataService->Query("SELECT * FROM Customer");
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}
// Echo some formatted output
$i = 0;
foreach ($entities as $oneCustomer)
{
	echo "<br>";
    echo "Customer[$i] DisplayName: {$oneCustomer->DisplayName}	(Created at {$oneCustomer->MetaData->CreateTime})\n";
    echo "<br>";
    $i++;
} */



// Run a query
/*$entities = $dataService->Query("DELETE FROM Invoice where Id='257'");
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}
// Echo some formatted output
// $i = 0;
// foreach ($entities as $oneCustomer){
//     echo "Customer[$i] DisplayName: {$oneCustomer->DisplayName}	(Created at {$oneCustomer->MetaData->CreateTime})\n";
//     $i++;
// }

echo "<pre>";
//print_r($entities);
foreach ($entities as $Invoice)
{
	echo "InvoiceDocNumber => ".$Invoice->DocNumber;
	echo "<br>";
	echo "string";
	echo "<pre>";
	print_r($Invoice);
	echo "<br>";
	echo "<br>";

	// print('Invoice # ' . $Invoice->getDocNumber() . ' has a total of $' . $Invoice->getTotalAmt() . "\n");
	// print('    First line item: ' . $Invoice->getLine(0)->getDescription() . "\n");
	// print('    Internal Id value: ' . $Invoice->getId() . "\n");
	// print("\n");

	//print_r($Invoice);
	//$Line = $Invoice->getLine(0);
	//print_r($Line);
}*/



//Add a new Invoice
/*$theResourceObj = Invoice::create([
  "DocNumber" => "37311sbf",
  "LinkedTxn" => [],
  "Line" => [[
      "Id" => "1",
      "LineNum" => 1,
      "Amount" => 566.0,
      "DetailType" => "SalesItemLineDetail",
      "SalesItemLineDetail" => [
          "ItemRef" => [
              "value" => "15",
              "name" => "Apple"
          ],
          "TaxCodeRef" => [
              "value" => "NON",
              "name" => "10"
          ],
          "Qty" => "5"
      ]
  ], [
      "Amount" => 586.0,
      "DetailType" => "SubTotalLineDetail",
      "SubTotalLineDetail" => []
  ]],
  "CustomerRef" => [
      "value" => "59",
      "name" => "Sunil Bhatt new"
  ]
]);
$resultingObj = $dataService->Add($theResourceObj);


$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else {
	echo "<br>";
    echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
    echo "<br>";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
    echo "<br>";
    echo $xmlBody . "\n";
    echo "<br>";
    echo "<br>";
    echo "<br>";
} */




/*$theResourceObj = Invoice::create([
     "Line" => [
   [
     "Amount" => 100.00,
     "DetailType" => "SalesItemLineDetail",
     "SalesItemLineDetail" => [
       "ItemRef" => [
         "value" => 20,
         "name" => "Hours"
        ]
      ]
      ]
    ],
"CustomerRef"=> [
  "value"=> 59
],
      "BillEmail" => [
            "Address" => "Familiystore@intuit.com"
      ],
      "BillEmailCc" => [
            "Address" => "a@intuit.com"
      ],
      "BillEmailBcc" => [
            "Address" => "v@intuit.com"
      ]
]);
$resultingObj = $dataService->Add($theResourceObj);


$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else {
    echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
    echo $xmlBody . "\n";
}*/


// Add a customer
/*$customerObj = Customer::create([
  "BillAddr" => [
     "Line1"=>  "main road",
     "City"=>  "Noida",
     "Country"=>  "",
     "CountrySubDivisionCode"=>  "",
     "PostalCode"=>  "201301"
 ],
 "Notes" =>  "Here are other details.",
 "Title"=>  "",
 "GivenName"=>  "Sunil",
 "MiddleName"=>  "Bhatt",
 "FamilyName"=>  "FamilyName",
 "Suffix"=>  "",
 "FullyQualifiedName"=>  "Sunilf Bhattf",
 "CompanyName"=>  "CompanyName Ok",
 "DisplayName"=>  "List name",
 "PrimaryPhone"=>  [
     "FreeFormNumber"=>  "11111"
 ],
 "PrimaryEmailAddr"=>  [
     "Address" => "sunil@gmail.com"
 ]
]);

$resultingCustomerObj = $dataService->Add($customerObj);
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else
{
	echo "<pre>";
    var_dump($resultingCustomerObj);
    echo "</pre>";
}*/


// ADD iTEM
/*$dateTime = new \DateTime('NOW');
var_dump($dateTime);
$Item = Item::create([
      "Name" => "TATAsdfsad 1111Office Sudfsfasdfpplies",
      "Description" => "TATA This is the sales description.",
      "Active" => true,
      "FullyQualifiedName" => "Office Supplies",
      "Taxable" => true,
      "UnitPrice" => 25,
      "Type" => "Inventory",
      "IncomeAccountRef"=> [
        "value"=> 79,
        "name" => "Landscaping Services:Job Materials:Fountains and Garden Lighting"
      ],
      "PurchaseDesc"=> "TATA This is the purchasing description.",
      "PurchaseCost"=> 35,
      "ExpenseAccountRef"=> [
        "value"=> 80,
        "name"=> "TATA Cost of Goods Sold"
      ],
      "AssetAccountRef"=> [
        "value"=> 81,
        "name"=> "TATA Inventory Asset"
      ],
      "TrackQtyOnHand" => true,
      "QtyOnHand"=> 100,
      "InvStartDate"=> $dateTime
]);


$resultingObj = $dataService->Add($Item);
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else {
    echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
    echo $xmlBody . "\n";
}*/







// use site
/*$customerObj = Customer::create([
  "BillAddr" => [
     "Line1"=>  "main road",
     "City"=>  "Noida",
     "Country"=>  "",
     "CountrySubDivisionCode"=>  "",
     "PostalCode"=>  "201301"
 ],
 "Notes" =>  "Here are other details.",
 "Title"=>  "",
 "GivenName"=>  "Sunil",
 "MiddleName"=>  "Bhatt",
 "FamilyName"=>  "FamilyName",
 "Suffix"=>  "",
 "FullyQualifiedName"=>  "Sunilf Bhattf",
 "CompanyName"=>  "CompanyName Ok",
 "DisplayName"=>  "List name Sunil",
 "PrimaryPhone"=>  [
     "FreeFormNumber"=>  "11111"
 ],
 "PrimaryEmailAddr"=>  [
     "Address" => "sunil@gmail.com"
 ]
]);



$resultCustomer = $dataService->Add($customerObj);
$resultCustomererror = $dataService->getLastError();
if ($resultCustomererror != null)
{
    $quserID = "59";

    echo "resultCustomererror The Status code is: " . $resultCustomererror->getHttpStatusCode() . "\n";
    echo "<br>";
    echo "The Helper message is: " . $resultCustomererror->getOAuthHelperError() . "\n";
    echo "<br>";
    echo "The Response message is: " . $resultCustomererror->getResponseBody() . "\n";
    echo "<br>";
    echo "<br>";
}
else
{
    $quserID = $resultCustomer->Id;
}
echo $quserID; */


/*$addInvoiceObj = Invoice::create([
  "DocNumber" => "78600-1wasday",
  "LinkedTxn" => [],
  "Line" => [[
      "Id" => "1",
      "LineNum" => 1,
      "Amount" => 420.0,
      "DetailType" => "SalesItemLineDetail",
      "SalesItemLineDetail" => [
          "ItemRef" => [
              "value" => "25",
          ],
          "TaxCodeRef" => [
              "value" => "NON"
          ]
      ]
  ], [
      "DetailType" => "SubTotalLineDetail",
      "SubTotalLineDetail" => []
  ]],
  "CustomerRef" => [
      "value" => "1"
  ]
]);
$resultInvoice = $dataService->Add($addInvoiceObj);


$resultInvoicerror = $dataService->getLastError();
if ($resultInvoicerror != null) {
    echo "The Status code is: " . $resultInvoicerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $resultInvoicerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $resultInvoicerror->getResponseBody() . "\n";
}
else {
  echo "<br>";
    echo "Created Id={$resultInvoice->Id}. Reconstructed response body:\n\n";
    echo "<br>";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultInvoice, $urlResource);
    echo "<br>";
    echo $xmlBody . "\n";
    echo "<br>";
    echo "<br>";
    echo "<br>";
}*/

/*$checkInvoice = $dataService->Query("select * from Invoice where id='639'");
$checkInvoiceerror = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $checkInvoiceerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $checkInvoiceerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $checkInvoiceerror->getResponseBody() . "\n";
    exit();
}
else
{
    if(!empty($checkInvoice) && sizeof($checkInvoice) == 1)
    {
        $theInvoice = current($checkInvoice);
        echo "<pre>";
        var_dump($theInvoice);
    }
}*/


/*$checkCustomer = $dataService->Query("SELECT * FROM Customer where DisplayName='List name Sunil1'");
$checkCustomererror = $dataService->getLastError();
if ($checkCustomererror != null) {
    echo "The Status code is: " . $checkCustomererror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $checkCustomererror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $checkCustomererror->getResponseBody() . "\n";
    exit();
}
else
{
    if(!empty($checkCustomer) && sizeof($checkCustomer) == 1)
    {
        $theCustomer = current($checkCustomer);
        var_dump($theCustomer->Id);
    }
}*/






//add in site code
// Check Invoice in Quick Book by sb 1-December-2017
/*$checkInvoice = $dataService->Query("select * from Invoice where docNumber='37311sbf'");
$checkInvoiceerror = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $checkInvoiceerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $checkInvoiceerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $checkInvoiceerror->getResponseBody() . "\n";
    exit();
}
else
{
    if(!empty($checkInvoice) && sizeof($checkInvoice) == 1)
    {
        $theInvoice = current($checkInvoice);
        var_dump($theInvoice);
    }
}*/


// Check customer
/*$checkqbcustomer = $dataService->Query("SELECT * FROM Customer where DisplayName='List name Sunil'");
$checkqbcustomererror = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $checkqbcustomererror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $checkqbcustomererror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $checkqbcustomererror->getResponseBody() . "\n";
    exit();
}
else
{
    if(!empty($checkqbcustomer) && sizeof($checkqbcustomer) == 1)
    {
        $theCustomer = current($checkqbcustomer);
        var_dump($theCustomer);
    }
}*/


// Create member in Quickbook
/*$customerObj = Customer::create([
  "BillAddr" => [
     "Line1"=>  "main road",
     "City"=>  "Noida",
     "Country"=>  "",
     "CountrySubDivisionCode"=>  "",
     "PostalCode"=>  "201301"
 ],
 "Notes" =>  "Here are other details.",
 "Title"=>  "",
 "GivenName"=>  "Sunil",
 "MiddleName"=>  "Bhatt",
 "FamilyName"=>  "FamilyName",
 "Suffix"=>  "",
 "FullyQualifiedName"=>  "Sunilf Bhattf",
 "CompanyName"=>  "CompanyName Ok",
 "DisplayName"=>  "List name Sunil",
 "PrimaryPhone"=>  [
     "FreeFormNumber"=>  "11111"
 ],
 "PrimaryEmailAddr"=>  [
     "Address" => "sunil@gmail.com"
 ]
]);



$qbcustomeresult = $dataService->Add($customerObj);
$qbcustomeresulterror = $dataService->getLastError();
if ($qbcustomeresulterror != null)
{
    $quserID = "59";

    echo "resultCustomererror The Status code is: " . $qbcustomeresulterror->getHttpStatusCode() . "\n";
    echo "<br>";
    echo "The Helper message is: " . $qbcustomeresulterror->getOAuthHelperError() . "\n";
    echo "<br>";
    echo "The Response message is: " . $qbcustomeresulterror->getResponseBody() . "\n";
    echo "<br>";
    echo "<br>";
}
else
{
    $quserID = $qbcustomeresult->Id;
}*/
//$qbcustomeresultObj->Id;



//Add a new Invoice
/*$addInvoiceObj = Invoice::create([
  "DocNumber" => "78600-1wasday",
  "LinkedTxn" => [],
  "Line" => [[
      "Id" => "1",
      "LineNum" => 1,
      "Amount" => 420.0,
      "DetailType" => "SalesItemLineDetail",
      "SalesItemLineDetail" => [
          "ItemRef" => [
              "value" => "25",
          ],
          "TaxCodeRef" => [
              "value" => "NON"
          ]
      ]
  ], [
      "DetailType" => "SubTotalLineDetail",
      "SubTotalLineDetail" => []
  ]],
  "CustomerRef" => [
      "value" => "1"
  ]
]);
$resultInvoice = $dataService->Add($addInvoiceObj);


$resultInvoicerror = $dataService->getLastError();
if ($resultInvoicerror != null) {
    echo "The Status code is: " . $resultInvoicerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $resultInvoicerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $resultInvoicerror->getResponseBody() . "\n";
}
else {
  echo "<br>";
    echo "Created Id={$resultInvoice->Id}. Reconstructed response body:\n\n";
    echo "<br>";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultInvoice, $urlResource);
    echo "<br>";
    echo $xmlBody . "\n";
    echo "<br>";
    echo "<br>";
    echo "<br>";
}*/


/*$entities = $dataService->Query("SELECT * FROM Payment");
echo "<pre>";
print_r($entities);
echo "</pre>";*/
/*$CompanyInfo = $dataService->getCompanyInfo();
$error = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else
{
    $nameOfCompany = $CompanyInfo->CompanyName;
    echo "Test for OAuth Complete. Company Name is {$nameOfCompany}. Returned response body:\n\n";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($CompanyInfo, $somevalue);
    echo $xmlBody . "\n";
}
echo "<pre>";
var_dump($_SESSION);*/




/*$theResourceObj = Invoice::create([
  "DocNumber" => "asd",
  "LinkedTxn" => [],
  "Line" => [[
      "Id" => "1",
      "LineNum" => 1,
      "Amount" => 566.0,
      "DetailType" => "SalesItemLineDetail",
      "SalesItemLineDetail" => [
          "ItemRef" => [
              "value" => "15",
              "name" => "Apple"
          ],
          "TaxCodeRef" => [
              "value" => "NON",
              "name" => "10"
          ],
          "Qty" => "5"
      ]
  ], [
      "Amount" => 586.0,
      "DetailType" => "SubTotalLineDetail",
      "SubTotalLineDetail" => []
  ]],
  "CustomerRef" => [
      "value" => "59",
      "name" => "Sunil Bhatt new"
  ]
]);
$resultingObj = $dataService->Add($theResourceObj);*/



// add invoice with attachment
/*$imageBase64 = array();
$filename = "http://www.calihomefunerals.com/cash-receipt-form.pdf";


$imageBase64['application/pdf'] = echoBase64("https://www.antennahouse.com/XSLsample/pdf/sample-link_1.pdf");

$sendMimeType = "application/pdf";

$addInvoiceObj = Invoice::create([
                          "DocNumber" => "987456",
                          "LinkedTxn" => [],
                          "Line" => [[
                              "Id" => "1",
                              "LineNum" => 1,
                              "Amount" => 830,
                              "DetailType" => "SalesItemLineDetail",
                              "SalesItemLineDetail" => [
                                  "ItemRef" => [
                                      "value" => "26",
                                  ],
                                  "TaxCodeRef" => [
                                      "value" => "NON"
                                  ]
                              ]
                          ], [
                              "DetailType" => "SubTotalLineDetail",
                              "SubTotalLineDetail" => []
                          ]],
                          "CustomerRef" => [
                              "value" => 132
                          ]
                        ]);
$resultInvoice = $dataService->Add($addInvoiceObj);

$resultInvoicerror = $dataService->getLastError();

// Create a new IPPAttachable
$randId = rand();
$entityRef = new IPPReferenceType(array('value'=>611, 'type'=>'Invoice'));
$attachableRef = new IPPAttachableRef(array('EntityRef'=>$entityRef));
$objAttachable = new IPPAttachable();
$objAttachable->FileName = "37345.pdf";
$objAttachable->AttachableRef = $attachableRef;
$objAttachable->Category = 'Image';
$objAttachable->Tag = 'Tag_37345';

// Upload the attachment to the Bill
$resultObj = $dataService->Upload(base64_decode($imageBase64[$sendMimeType]),
                                  $objAttachable->FileName,
                                  $sendMimeType,
                                  $objAttachable);

echo "<pre>";
print_r($resultObj);
echo "</pre>";

//echoBase64("/your/file/here");

function echoBase64($filename)
{
    $contents = file_get_contents($filename);
    return $base64_contents = base64_encode($contents);
    //  $base64_contents_split = str_split($base64_contents, 80);
    // echo "\t\$imageBase64 = \"\" . \n";
    // foreach ($base64_contents_split as $one_line) {
    //     echo "\t\t\"{$one_line}\" . \n";
    // }
    // echo "\t\t\"\";\n";
}*/




// Paid invoice
/*$customerObj = Payment::create([
  "CustomerRef" => [
      "value" => "185"
   ],
   "TotalAmt"=> 150.00,
   "TxnDate" => "2018-02-16",
   "Line" => [[
         "Amount" => 150.00,
         "LinkedTxn" => [
               "TxnId" => "639",
               "TxnType" => "Invoice"
               ],
            ]]
]);



$qbcustomeresult = $dataService->Add($customerObj);
$qbcustomeresulterror = $dataService->getLastError();
if ($qbcustomeresulterror != null)
{
    $quserID = "59";

    echo "resultCustomererror The Status code is: " . $qbcustomeresulterror->getHttpStatusCode() . "\n";
    echo "<br>";
    echo "The Helper message is: " . $qbcustomeresulterror->getOAuthHelperError() . "\n";
    echo "<br>";
    echo "The Response message is: " . $qbcustomeresulterror->getResponseBody() . "\n";
    echo "<br>";
    echo "<br>";
}
else
{
    echo "<pre>";
    print_r($qbcustomeresult);
}*/




// create item
//$dateTime = new \DateTime('NOW');
/*$createqbItemdata = Item::create([
      "Name" => "Dummy Product",
      "Description" => "This is the sales description.",
      "Active" => true,
      "FullyQualifiedName" => "Office Supplies",
      "Taxable" => true,
      "Type" => "Service",
      "IncomeAccountRef"=> [
        "value"=> 79,
        "name" => "Sales of Product Income"
      ]
]);


$createqbItemdataObj = $dataService->Add($createqbItemdata);
$createqbItemdataerror = $dataService->getLastError();
if ($createqbItemdataerror != null) {
    echo "The Status code is: " . $createqbItemdataerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $createqbItemdataerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $createqbItemdataerror->getResponseBody() . "\n";
}
else {
    echo "Created Id={$createqbItemdataObj->Id}. Reconstructed response body:\n\n";
    $xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($createqbItemdataObj, $urlResource);
    echo $xmlBody . "\n";
}*/





// Get item data by name
/*$getItemData = $dataService->Query("select * from Item where SKU='452612'");
$getItemDataerror = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $getItemDataerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $getItemDataerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $getItemDataerror->getResponseBody() . "\n";
    exit();
}
else
{
    $getItemData = current($getItemData);
    echo $getItemData->Id;
    echo "<pre>";
    print_r($getItemData);
    echo "</pre>";
    // if(!empty($checkInvoice) && sizeof($checkInvoice) == 1)
    // {
    //     $checkItems = current($checkItem);
    //     var_dump($checkItems);
    // }
}

//$theCustomer = reset($getItemData);

$updateItem = Item::update($getItemData, [
                                    "Name" => "Dummy Product",
                                    "Description" => "Dummy Product Description.",
                                    "Active" => true,
                              ]);

$resultingCustomerUpdatedObj = $dataService->Update($updateItem);

echo "<pre>";
var_dump($resultingCustomerUpdatedObj);*/

// $checkInvoice = $dataService->Query("select * from Payment Where Id='631'");
/*$checkInvoice = $dataService->Query("select * from Payment");

echo "<pre>";
print_r($checkInvoice);*/

/*$deleteInvoiceQb =  current($checkInvoice);
$dataService->Delete($deleteInvoiceQb);
$checkInvoiceerror = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $checkInvoiceerror->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $checkInvoiceerror->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $checkInvoiceerror->getResponseBody() . "\n";
    exit();
}
else
{
    $theInvoice = current($checkInvoice);
    echo "<pre>";
    echo $theInvoice->Id;
    print_r($theInvoice);
}*/






//update invoice




















/*$entities = $dataService->Query("select * from Invoice where docNumber='25640'");
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}

echo "<pre>";
//print_r($entities);
foreach ($entities as $Invoice)
{
  echo "InvoiceDocNumber => ".$Invoice->DocNumber;
  echo "<br>";
  echo "string";
  echo "<pre>";
  print_r($Invoice);
  echo "<br>";
  echo "<br>";
}

if(empty($entities)) exit();
$theCustomer = reset($entities);

$updateCustomer = Invoice::update($theCustomer, [
                          "DocNumber" => "25640",
                          "Line" => [[
                              "Id" => "1",
                              "LineNum" => 1,
                              "Amount" => 638,
                              "DetailType" => "SalesItemLineDetail",
                              "SalesItemLineDetail" => [
                                  "ItemRef" => [
                                      "value" => 26,
                                  ]
                              ]
                          ]]
                        ]);

$resultingCustomerUpdatedObj = $dataService->Update($updateCustomer);
$xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingCustomerUpdatedObj, $urlResource);
echo "Completed a Sparse Update on {$resultingCustomerUpdatedObj->Id} - updated object state is:\n{$xmlBody}\n\n";*/


// Run a query
// $entities = $dataService->Query("DELETE FROM attachable where AttachableRef.EntityRef.value = '557'");
// $entities = $dataService->Query("select * FROM attachable where AttachableRef.EntityRef = '557' ORDER BY MetaData.CreateTime DESC MAXRESULTS 300");
//$entities = $dataService->Query("select * FROM attachable where AttachableRef.EntityRef.value = '557'");

/*$checkInvoice  = $dataService->Query("select * from attachable where AttachableRef.EntityRef.Type = 'Invoice' and AttachableRef.EntityRef.value = '636'");
// $checkInvoice =  current($checkInvoice);
//$crudResultObj = $dataService->Delete($checkInvoice);
$error = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}

echo "<pre>";
var_dump($checkInvoice);
echo "</pre>";*/








// Get Purchase order
// $entities = $dataService->Query("select * from PurchaseOrder where Id = '114'");

/*$entities = $dataService->Query("SELECT * FROM vendor");
$error = $dataService->getLastError();
if ($error != null) {
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}
// Echo some formatted output
echo "<pre>";
print_r($entities);
echo "</pre>";
die();*/










// Code for create Vendor For Purchase order
/*$randomPurchaseObj = Vendor::create([
                                        // "BillAddr" =>   [
                                        //                     "Line1" => "Dianne's Auto Shop",
                                        //                     "Line2" => "Dianne Bradley",
                                        //                     "Line3" => "29834 Mustang Ave.",
                                        //                     "City" => "Millbrae",
                                        //                     "Country" => "U.S.A",
                                        //                     "CountrySubDivisionCode" => "CA",
                                        //                     "PostalCode" => "94030"
                                        //                 ],
                                        // "TaxIdentifier" => "99-5688293",
                                        // "AcctNum" => "35372649",
                                        // "Title" => "Ms.",
                                        "GivenName" => "Test",
                                        "FamilyName" => "Test Data",
                                        // "Suffix" => "Sr.",
                                        // "CompanyName" => "Dianne's Auto Shop",
                                        "DisplayName" => "Test Data",
                                        // "PrintOnCheckName" => "Dianne's Auto Shop",
                                        // "PrimaryPhone" =>   [
                                        //                         "FreeFormNumber" => "(650) 555-2342"
                                        //                     ],
                                        // "Mobile" => [
                                        //                 "FreeFormNumber" => "(650) 555-2000"
                                        //             ],
                                        // "PrimaryEmailAddr" =>   [
                                        //                             "Address" => "dbradley@myemail.com"
                                        //                         ],
                                        // "WebAddr" =>    [
                                        //                     "URI" => "http =>//DiannesAutoShop.com"
                                        //                 ]
                                ]);

$purchaseObjConfirmation = $dataService->Add($randomPurchaseObj);
echo "Created Vendor object, and received Id={$purchaseObjConfirmation->Id}\n";

$error = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "<br>";
    echo "<br>";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "<br>";
    echo "<br>";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}*/




// Code for create Item For Purchase order
/*$createqbItemdata = Item::create([
                                    "Name" => "Test Purchase Order Item5",
                                    "Sku"  => "Pu-order5",
                                    "Description" => "Test Purchase Order Item3",
                                    "Active" => true,
                                    "FullyQualifiedName" => "Office Supplies",
                                    "Taxable" => true,
                                    "Type" => "Service",
                                    // "PurchaseDesc" => "Purchase Desc Test",
                                    // "PurchaseCost" => 35,
                                    "ExpenseAccountRef" => [
                                      "value" => 80,
                                      "name" => "Cost of Goods Sold"
                                    ],
                                    "IncomeAccountRef"=> [
                                        "value"=> 79,
                                        "name" => "Sales of Product Income"
                                    ]
                                ]);
$createqbItemdataObj = $dataService->Add($createqbItemdata);
$createqbItemdataerror = $dataService->getLastError();
if ($createqbItemdataerror != null)
{
    // $_SESSION["alerttype"] = "danger";
    // $_SESSION["alertmsg"] = "Unable to create a dummy Item due to ".$createqbItemdataerror->getResponseBody()."<br>Customer Reference : ".$last_name."<br>Order Reference : ".$order_id;
    break;
}
else
{
    echo $dummyProductId = $createqbItemdataObj->Id;
}*/







// Code for create Purchase order
/*$randomPurchaseObj = PurchaseOrder::create([
                                        "Line"=> [[
                                            "Id"=> "1",
                                            "Amount"=> 1500.0,
                                            "DetailType"=> "ItemBasedExpenseLineDetail",
                                            "ItemBasedExpenseLineDetail"=> [
                                                // "CustomerRef"=> [
                                                //     "value"=> "178"
                                                //     // "name"=> "Cool Cars"
                                                // ],
                                                "BillableStatus"=> "NotBillable",
                                                "ItemRef"=> [
                                                    "value"=> "65"
                                                    // "name"=> "Garden Supplies"
                                                ],
                                                "UnitPrice"=> 786.0,
                                                "Qty"=> 5,
                                                "TaxCodeRef"=> [
                                                    "value"=> "NON"
                                                ]
                                            ]
                                        ]],
                                        "VendorRef" => [
                                                        "value"=> "187"
                                                        // "name"=> "Hicks Hardware"
                                                        ]

                                        // "APAccountRef" => [
                                        //                 "value"=> "33"
                                        //                 // "name"=> "Accounts Payable (A/P)"
                                        //                 ],
                                        // "TotalAmt"=> "15.0"
                                    ]);

$purchaseObjConfirmation = $dataService->Add($randomPurchaseObj);
echo "Created Purchase object, and received Id={$purchaseObjConfirmation->Id}\n";

$error = $dataService->getLastError();
if ($error != null)
{
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "<br>";
    echo "<br>";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "<br>";
    echo "<br>";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
    exit();
}*/



// Get vendor Data
$checkVendor = $dataService->Query("select * from vendor where DisplayName='Test test test Test'");
$checkVendorError = $dataService->getLastError();
if ($checkVendorError != null)
{
    $_SESSION["tpa_purchase_quickbook_error"] = 1;
    exit();
}
else
{

    $checkVendor = current($checkVendor);
    if (empty($checkVendor))
    {
        echo "Not";
        var_dump($checkVendor);
    }
    else
    {
        echo "get<pre>";
        var_dump($checkVendor);
    }
}

?>
