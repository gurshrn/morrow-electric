<?php
	$session_id = session_id();
	if (empty($session_id))
	{
		session_start();
	}
	include('src/config.php');
	use QuickBooksOnline\API\DataService\DataService;
	use QuickBooksOnline\API\PlatformService\PlatformService;
	use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
	use QuickBooksOnline\API\Facades\Customer;
	use QuickBooksOnline\API\Facades\Invoice;
	use QuickBooksOnline\API\Facades\Item;
	use QuickBooksOnline\API\Data\IPPReferenceType;
	use QuickBooksOnline\API\Data\IPPAttachableRef;
	use QuickBooksOnline\API\Data\IPPAttachable;
	use QuickBooksOnline\API\Facades\Bill;
	use QuickBooksOnline\API\Facades\Payment;
	use QuickBooksOnline\API\Facades\Purchase;
	use QuickBooksOnline\API\Data\IPPPurchase;

	use QuickBooksOnline\API\Facades\PurchaseOrder;
	use QuickBooksOnline\API\Facades\Vendor;

	$dataService = DataService::Configure(array(
         'auth_mode'        => "oauth2",
         'ClientID'         => "Q02O5nKwqCw2E8565Ei1IxfUKQjE3GZX5jyUVkjo7jipMSH5o8",
         'ClientSecret'     => "2vBLy0cOpeIsYIKG6yZBz8y0CTLEz2EBPDmdkwL8",
         'accessTokenKey'   => $_SESSION['access_token'],
         'refreshTokenKey'  => $_SESSION['refresh_token'],
         'QBORealmID'       => "193514836765544",
         'baseUrl'          => "Production"
	));
 
	$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

	$dataService->throwExceptionOnError(true);
	
	

	$customerInfo = $cus_result2[0];
	
	
	
	if($customerInfo['qb_id'] == 0)
	{
		
		if($customerInfo['company_name'] != '')
		{
			$firstname = $customerInfo['company_name'];
			$lastname = '';
		}
		else
		{
			$firstname = $customerInfo['first_name'];
			$lastname = $customerInfo['last_name'];
		}
		
		
		
		$customersObj = Customer::create([
			"BillAddr" => [
				"Line1"=>  $customerInfo['home_address'],
				"City"=>  $customerInfo['home_city'],
				"Country"=>  "",
				"CountrySubDivisionCode"=>  "",
				"PostalCode"=>  $customerInfo['home_postal_code']
			],
			"Notes" =>  $customerInfo['notes'],
			"Title"=>  "",
			"GivenName"=>  $firstname,
			"MiddleName"=>  $lastname,
			"FamilyName"=>  "",
			"Suffix"=>  "",
			"FullyQualifiedName"=>  '',
			"CompanyName"=>  '',
			"DisplayName"=>  '',
			"PrimaryPhone"=>  [
			"FreeFormNumber"=>  $customerInfo['phone']
			],
			"PrimaryEmailAddr"=>  [
				"Address" => $customerInfo['email']
			]
		]);

		$resultingCustomersObj = $dataService->Add($customersObj);
		
		
		$error = $dataService->getLastError();
		if ($error) 
		{
			echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
			echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			echo "The Response message is: " . $error->getResponseBody() . "\n";
		}
		else 
		{
			$ids = $resultingCustomersObj->Id;
			updateCustomerQbId($ids,$customerInfo['id']);
		}
	}
	
	