<?php

	$session_id = session_id();
	if (empty($session_id))
	{
		session_start();
	}
	include('src/config.php');
	use QuickBooksOnline\API\DataService\DataService;
	use QuickBooksOnline\API\PlatformService\PlatformService;
	use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
	use QuickBooksOnline\API\Facades\Customer;
	use QuickBooksOnline\API\Facades\Invoice;
	use QuickBooksOnline\API\Facades\Item;
	use QuickBooksOnline\API\Data\IPPReferenceType;
	use QuickBooksOnline\API\Data\IPPAttachableRef;
	use QuickBooksOnline\API\Data\IPPAttachable;
	use QuickBooksOnline\API\Facades\Bill;
	use QuickBooksOnline\API\Facades\Payment;
	use QuickBooksOnline\API\Facades\Purchase;
	use QuickBooksOnline\API\Data\IPPPurchase;

	use QuickBooksOnline\API\Facades\PurchaseOrder;
	use QuickBooksOnline\API\Facades\Vendor;

	$dataService = DataService::Configure(array(
         'auth_mode'        => "oauth2",
         'ClientID'         => "Q02O5nKwqCw2E8565Ei1IxfUKQjE3GZX5jyUVkjo7jipMSH5o8",
         'ClientSecret'     => "2vBLy0cOpeIsYIKG6yZBz8y0CTLEz2EBPDmdkwL8",
         'accessTokenKey'   => $_SESSION['access_token'],
         'refreshTokenKey'  => $_SESSION['refresh_token'],
         'QBORealmID'       => "193514836765544",
         'baseUrl'          => "Production"
	));
 
	$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

	$dataService->throwExceptionOnError(true);
	$dateTime = new \DateTime('NOW');
	
	
	
	
	if(isset($inventoryArray) && !empty($inventoryArray))
	{
		
		foreach($inventoryArray as $row1)
		{
			if(!empty($row1['name']))
			{
				
				$name = $row1['name'];
			
				$data1 = getInventoryItems($name);
				
				if(empty($data1))
				{
					$dateTime = new \DateTime('NOW');
					
					$Item = Item::create([
						  "Name" => $name,
						  "Description" => "TATA This is the sales description.",
						  "Active" => true,
						  "FullyQualifiedName" => "Office Supplies",
						  "Taxable" => true,
						  "UnitPrice" => 0,
						  "Type" => "Inventory",
						  "IncomeAccountRef"=> [
							"value"=> 52,
							"name" => "Landscaping Services:Job Materials:Fountains and Garden Lighting"
						  ],
						  "ExpenseAccountRef"=> [
							"value"=> 53,
							"name"=> "TATA Cost of Goods Sold"
						  ],
						  "AssetAccountRef"=> [
							"value"=> 54,
							"name"=> "TATA Inventory Asset"
						  ],
						  "TrackQtyOnHand" => true,
						  "QtyOnHand"=> 100,
						  "InvStartDate"=> $dateTime
					]);
					

					$resultingCustomerObj = $dataService->Add($Item);
					$error = $dataService->getLastError();
					if ($error) 
					{
						echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
						echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
						echo "The Response message is: " . $error->getResponseBody() . "\n";
					}
					else 
					{
						$itemId = $resultingCustomerObj->Id;
						insertInventoryQbId($itemId,$name);
					}
				}
			}	
			
		}
	}
	
	
	if(isset($inventory) && !empty($inventory))
	{
		
		foreach($inventory as $val)
		{
			
			if($val['cat'] != '' && $val['catName'] != '')
			{
					
				$inventoryname = $val['cat'];
					
				$data = getInventoryItems($val['cat']);
				
				
					
				if(empty($data))
				{
					
					$dateTime = new \DateTime('NOW');
								
					$Item = Item::create([
						  "Name" => $inventoryname,
						  "Description" => "TATA This is the sales description.",
						  "Active" => true,
						  "FullyQualifiedName" => "Office Supplies",
						  "Taxable" => true,
						  "UnitPrice" => 0,
						  "Type" => "Inventory",
						  "IncomeAccountRef"=> [
							"value"=> 52,
							"name" => "Landscaping Services:Job Materials:Fountains and Garden Lighting"
						  ],
						  "ExpenseAccountRef"=> [
							"value"=> 53,
							"name"=> "TATA Cost of Goods Sold"
						  ],
						  "AssetAccountRef"=> [
							"value"=> 54,
							"name"=> "TATA Inventory Asset"
						  ],
						  "TrackQtyOnHand" => true,
						  "QtyOnHand"=> 100,
						  "InvStartDate"=> $dateTime
					]);
					
					

					$resultingCustomerObj = $dataService->Add($Item);
					
					$error = $dataService->getLastError();
					if ($error) 
					{
						echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
						echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
						echo "The Response message is: " . $error->getResponseBody() . "\n";
					}
					else 
					{
						$itemId = $resultingCustomerObj->Id;
						insertInventoryQbId($itemId,$val['cat']);
					}
				}
			}
		}
	}
	

	
	
	
	
	
	
	

	


    
	


