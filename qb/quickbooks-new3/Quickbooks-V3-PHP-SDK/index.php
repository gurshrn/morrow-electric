<?php
$session_id = session_id();
if (empty($session_id))
{
    session_start();
}
include('src/config.php');
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Data\IPPReferenceType;
use QuickBooksOnline\API\Data\IPPAttachableRef;
use QuickBooksOnline\API\Data\IPPAttachable;
use QuickBooksOnline\API\Facades\Bill;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Data\IPPPurchase;

use QuickBooksOnline\API\Facades\PurchaseOrder;
use QuickBooksOnline\API\Facades\Vendor;

	$dataService = DataService::Configure(array(
         'auth_mode'        => "oauth2",
         'ClientID'         => "Q02O5nKwqCw2E8565Ei1IxfUKQjE3GZX5jyUVkjo7jipMSH5o8",
         'ClientSecret'     => "2vBLy0cOpeIsYIKG6yZBz8y0CTLEz2EBPDmdkwL8",
         'accessTokenKey'   => $_SESSION['access_token'],
         'refreshTokenKey'  => $_SESSION['refresh_token'],
         'QBORealmID'       => "193514836765544",
         'baseUrl'          => "Production"
	));


 
$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

$dataService->throwExceptionOnError(true);


	if($row['company_name'] != '')
	{
		$firstname = $row['company_name'];
		$lastname = '';
	}
	else
	{
		$firstname = $row['first_name'];
		$lastname = $row['last_name'];
	}
	$customerObj = Customer::create([
      "BillAddr" => [
         "Line1"=>  $row['home_address'],
         "City"=>  $row['home_city'],
         "Country"=>  "",
         "CountrySubDivisionCode"=>  "",
         "PostalCode"=>  $row['home_postal_code']
     ],
     "Notes" =>  $row['notes'],
     "Title"=>  "",
     "GivenName"=>  $firstname,
     "MiddleName"=>  $lastname,
     "FamilyName"=>  "",
     "Suffix"=>  "",
     "FullyQualifiedName"=> '',
     "CompanyName"=>  '',
     "DisplayName"=>  '',
     "PrimaryPhone"=>  [
         "FreeFormNumber"=>  $row['phone']
     ],
     "PrimaryEmailAddr"=>  [
         "Address" => $row['email']
     ]
]);


$resultingCustomerObj = $dataService->Add($customerObj);

    $error = $dataService->getLastError();
if ($error) 
{
    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
    echo "The Response message is: " . $error->getResponseBody() . "\n";
}
else 
{
	$id = $resultingCustomerObj->Id;
	updateCustomerQbId($id,$row['id']);
    echo $id;
	
}


    
 

    


//Add a new Invoice
/*$theResourceObj = Invoice::create([
    "Line" => [
    [
         "Amount" => 100.00,
         "DetailType" => "SalesItemLineDetail",
         "SalesItemLineDetail" => [
           "ItemRef" => [
             "value" => 1,
             "name" => "Services"
           ]
         ]
    ]
    ],
    "CustomerRef"=> [
          "value"=> 1
    ],
    "BillEmail" => [
          "Address" => "Familiystore@intuit.com"
    ],
    "BillEmailCc" => [
          "Address" => "a@intuit.com"
    ],
    "BillEmailBcc" => [
          "Address" => "v@intuit.com"
    ]
]);
$resultingObj = $dataService->Add($theResourceObj);

          
    //Add a new Invoice
   /* $entities = $dataService->Query("Select * from Deposit");
    $CompanyInfo = $dataService->getCompanyInfo();
    print_r($CompanyInfo); die;*/



