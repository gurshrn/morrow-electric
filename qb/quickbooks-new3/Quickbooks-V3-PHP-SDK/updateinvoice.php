<?php
	$session_id = session_id();
	if (empty($session_id))
	{
		session_start();
	}
	include('src/config.php');
	use QuickBooksOnline\API\DataService\DataService;
	use QuickBooksOnline\API\PlatformService\PlatformService;
	use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
	use QuickBooksOnline\API\Facades\Customer;
	use QuickBooksOnline\API\Facades\Invoice;
	use QuickBooksOnline\API\Facades\Item;
	use QuickBooksOnline\API\Data\IPPReferenceType;
	use QuickBooksOnline\API\Data\IPPAttachableRef;
	use QuickBooksOnline\API\Data\IPPAttachable;
	use QuickBooksOnline\API\Facades\Bill;
	use QuickBooksOnline\API\Facades\Payment;
	use QuickBooksOnline\API\Facades\Purchase;
	use QuickBooksOnline\API\Data\IPPPurchase;

	use QuickBooksOnline\API\Facades\PurchaseOrder;
	use QuickBooksOnline\API\Facades\Vendor;

	// $dataService = DataService::Configure(array(
		// 'auth_mode' => 'OAuth2',
		// 'ClientID' => 'Q0zH9nlzo11UMf3wIvwCE7f8OC3nxmQKMCHoR3CwrXepGHflRO', //Example 'Q0wDe6WVZMzyu1SnNPAdaAgeOAWNidnVRHWYEUyvXVbmZDRUfQ',
		// 'ClientSecret' => 'hfVIXsdRVxCri9A8J346NJjoT5KJVo3ZPu71kAwm',
		// 'accessTokenKey'   => $_SESSION['access_token'],
		// 'refreshTokenKey'  => $_SESSION['refresh_token'],
		// 'QBORealmID' => "123146076366249",
		// 'baseUrl' => "Development"
	// ));
	
	$dataService = DataService::Configure(array(
         'auth_mode'        => "oauth2",
         'ClientID'         => "Q02O5nKwqCw2E8565Ei1IxfUKQjE3GZX5jyUVkjo7jipMSH5o8",
         'ClientSecret'     => "2vBLy0cOpeIsYIKG6yZBz8y0CTLEz2EBPDmdkwL8",
         'accessTokenKey'   => $_SESSION['access_token'],
         'refreshTokenKey'  => $_SESSION['refresh_token'],
         'QBORealmID'       => "193514836765544",
         'baseUrl'          => "Production"
	));
 
	$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

	$dataService->throwExceptionOnError(true);
	
	$totalArrays=[];
	$dateTime = new \DateTime('NOW');
	
	
	
	if(isset($staffInfos) && !empty($staffInfos))
	{
		foreach($staffInfos as $key=>$val)
		{
			$firstnameuser = getuserName($val['qb_id']);
			
			$firstname = $firstnameuser[0]['first_name'];
			
			if($val['total_hours'] == '')
			{
				$qty = 0;
				
			}
			else
			{
				$qty = $val['total_hours'];
			}
			$totalArrays[] = array(
				"Description"=>'Staff - '.$firstname,
				"Amount" => $qty*$val['billrate'],
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => $val['qb_id'],
						"name" => 'Hours'
					),
					"UnitPrice" => $val['billrate'],
					"Qty" => $qty,
				),
			);
		}
	}
	
	
	
	if(!empty($inventoryId2))
	{	
		foreach($inventoryId2 as $vals)
		{
			if($vals['qty'] == '')
			{
				$qty9 = 0;
				
			}
			else
			{
				
				$qty9 = $vals['qty'];
				
			}
			
			
			$totalArrays[] = array(
				"Description"=>'Parts - '.$vals['catName'].' - '.$vals['cat'],
				"Amount" => 0*$qty9,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => $vals['qb_id'],
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty9,
					
				),
			);
		}
	}
	
	
		
		if(isset($vehicleinventoryId) && !empty($vehicleinventoryId))
		{
			foreach($vehicleinventoryId as $vehVal)
			{
				if($vehVal['hours'] == '')
				{
					$qty8 = 0;
					
				}
				else
				{
					$qty8 = $vehVal['hours'];
				}
				$totalArrays[] = array(
					"Description"=>'Vehicle - '.$vehVal['vehiclename'],
					"Amount" => $qty8*$vehVal['rate'],
					"DetailType" => 'SalesItemLineDetail',
					"SalesItemLineDetail" => array(
						"ItemRef" => array(
							"value" => $vehVal['qb_id'],
							"name" => 'Hours'
						),
						"UnitPrice" => $vehVal['rate'],
						"Qty" => $qty8,
						
					),
				);
			}
		}
		
		
		if(isset($equipinventoryId) && !empty($equipinventoryId))
		{
			foreach($equipinventoryId as $equipVal)
			{
				if($equipVal['hours'] == '')
				{
					$qty7 = 0;
					
				}
				else
				{
					$qty7 = $equipVal['hours'];
				}
				$totalArrays[] = array(
					"Description"=>'Equipment - '.$equipVal['equipmentname'],
					"Amount" => $qty7*$equipVal['rate'],
					"DetailType" => 'SalesItemLineDetail',
					"SalesItemLineDetail" => array(
						"ItemRef" => array(
							"value" => $equipVal['qb_id'],
							"name" => 'Hours'
						),
						"UnitPrice" => $equipVal['rate'],
						"Qty" => $qty7,
						
					),
				);
			}
		}
		if($generatorHours != '')
		{
			$generatorQty = 1;
			$totalArrays[] = array(
				"Description"=>'Generator Hours - '.$generatorHours,
				"Amount" => $generatorQty*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '108',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $generatorQty,
					
				),
			);
		}
		
		if($oilFilter != '')
		{
			if($oilFilter->qty == '')
			{
				$qty6 = 0;
				
			}
			else
			{
				$qty6 = $oilFilter->qty;
			}
			$totalArrays[] = array(
				"Description"=>'Oil Filter - '.$oilFilter->name,
				"Amount" => $qty6*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '96',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty6,
					
				),
			);
		}
		if($airFilter != '')
		{
			if($airFilter->qty == '')
			{
				$qty5 = 0;
				
			}
			else
			{
				$qty5 = $airFilter->qty;
			}
			$totalArrays[] = array(
				"Description"=>'Air Filter - '.$airFilter->name,
				"Amount" => $qty5*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '91',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty5,
					
				),
			);
		}
		if($battery != '')
		{
			if($battery->qty == '')
			{
				$qty4 = 1;
				
			}
			else
			{
				$qty4 = $battery->qty;
			}
			$totalArrays[] = array(
				"Description"=>'Battery - '.$battery->name,
				"Amount" => $qty4*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '4',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty4,
					
				),
			);
		}
		if($battery1 != '')
		{
			$qty3 = 1;
			
			$totalArrays[] = array(
				"Description"=>'Battery - '.$battery1,
				"Amount" => $qty3*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '4',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty3,
					
				),
			);
		}
		if($antifreeze != '')
		{
			$qty2 = 1;
			$totalArrays[] = array(
				"Description"=>'AntiFreeze - '.$antifreeze,
				"Amount" => $qty2*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '1',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty2,
					
				),
			);
		}
		if($oil != '')
		{
			if($oil->qty == '')
			{
				$qty1 = 0;
				
			}
			else
			{
				$qty1 = $oil->qty;
			}
			$totalArrays[] = array(
				"Description"=>'Oil - '.$oil->name,
				"Amount" => $qty1*0,
				"DetailType" => 'SalesItemLineDetail',
				"SalesItemLineDetail" => array(
					"ItemRef" => array(
						"value" => '95',
						"name" => 'Hours'
					),
					"UnitPrice" => 0,
					"Qty" => $qty1,
					
				),
			);
		}
		$theResourceObj = Invoice::create([
		"Line" => $totalArrays,
		"CustomerRef"=> [
			  "value"=> $customerId
		],
		"BillEmail" => [
			  "Address" => $email
		]
		
		]);
		$resultingObj = $dataService->Add($theResourceObj);
		
		$resultInvoicerror = $dataService->getLastError();
		if ($resultInvoicerror != null) 
		{
			echo "The Status code is: " . $resultInvoicerror->getHttpStatusCode() . "\n";
			echo "The Helper message is: " . $resultInvoicerror->getOAuthHelperError() . "\n";
			echo "The Response message is: " . $resultInvoicerror->getResponseBody() . "\n";
		}
		else 
		{
			$invoicesIds = $resultingObj->DocNumber;
			
			updateInvoiceId($invoicesIds,$invoiceId,$table);
			
		}
	
	
