<?php
include('./config.php');
require_once 'Quickbooks-V3-PHP-SDK/vendor/autoload.php'; 
require_once 'Quickbooks-V3-PHP-SDK/src/config.php';

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;

include('Quickbooks-V3-PHP-SDK/index.php');



// use QuickBooksOnline\API\Facades\Customer;
// use QuickBooksOnline\API\Facades\Invoice;
// use QuickBooksOnline\API\Facades\Item;
// use QuickBooksOnline\API\Data\IPPReferenceType;
// use QuickBooksOnline\API\Data\IPPAttachableRef;
// use QuickBooksOnline\API\Data\IPPAttachable;
// use QuickBooksOnline\API\Facades\Bill;
// use QuickBooksOnline\API\Facades\Payment;
// use QuickBooksOnline\API\Facades\Purchase;
// use QuickBooksOnline\API\Data\IPPPurchase;
//
// use QuickBooksOnline\API\Facades\PurchaseOrder;
// use QuickBooksOnline\API\Facades\Vendor;


  /*public function logout(){
     Session::forget('access_token');
		// unset($_SESSION['access_token']);
		return redirect('/qb');

	}

  public function main()
  {
    $session = Session::get('userRef');
    if($session == ''){
      return redirect('/user-login');
    }

    // include(app_path() . '/Services/OAuth_2/index.php');
    $output['parentUrl'] = "accounting integration";
    $output['childUrl']  = "qb";
    $output['title']  = "Connect to Quickbook";
    $value = Session::get('access_token');
    if(isset($value) && !empty($value))
    {
      Quickbook::updateToken();
      echo 'Quickbook connected.'; die;
      return redirect('/account-receivables');
    }
    else
    {
      echo view('include.header',$output);
      echo view('include.sidebar',$output);
      return view('backend.qb.connect',$output);
    }
  }*/
 /* public function OAuth2PHPExample()
  {
    include(app_path() . '/OAuth_2/OAuth2PHPExample.php');
       $value = Session::get('access_token');
      if(isset($value) && !empty($value))
      {
        Quickbook::updateToken();
        return redirect('/qb');
      }
  }
  public function OAuthOpenIDExample()
  {
    include(app_path() . '/Services/OAuth_2/OAuthOpenIDExample.php');
  }
  public function RefreshToken()
  {
    include(app_path() . '/Services/OAuth_2/RefreshToken.php');
    Quickbook::updateToken();
  }

  public function insertInv()
  {
    include(app_path() . '/Services/QuickBooks-V3-PHP-SDK-master/index.php');
  }
  public function getSaleInvoice()
  {

    $getToken =  Quickbook::getQbToken();
    // print_r($getToken);
    if($getToken->accessTokenKey != '')
    {
      $_SESSION['access_token'] = $getToken->accessTokenKey;
      $_SESSION['refresh_token'] = $getToken->refreshTokenKey;
    }else {
      unset($_SESSION['access_token']);
      unset($_SESSION['refresh_token']);
      return redirect('/qb');
    }

    $dataService = DataService::Configure(array(
      'auth_mode' => 'OAuth2',
      'ClientID' => $getToken->ClientID,
      'ClientSecret' => $getToken->ClientSecret,
      'accessTokenKey'   => $getToken->accessTokenKey,
      'refreshTokenKey'  => $getToken->refreshTokenKey,
      'QBORealmID' => $getToken->QBORealmID,
      'baseUrl' => "Development"
    ));

    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
    $CompanyInfo = $dataService->getCompanyInfo();
   // print_r($CompanyInfo); die;
    $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
    $accessToken = $OAuth2LoginHelper->refreshToken();
    $dataService->updateOAuth2Token($accessToken);
    $dataService->throwExceptionOnError(true);
    //Add a new Invoice
    if(isset($accessToken))
    { 
      $_SESSION['access_token'] = $accessToken->getAccessToken();
      $_SESSION['refresh_token'] = $accessToken->getRefreshToken();
      Session::set('access_token',$accessToken->getAccessToken());
      Session::set('refresh_token',$accessToken->getRefreshToken());
      Quickbook::updateToken();
    }else {
      unset($_SESSION['access_token']);
      unset($_SESSION['refresh_token']);
      return redirect('/qb');
    }

    

    $error = $dataService->getLastError();
    if ($error) {
      echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
      echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
      echo "The Response message is: " . $error->getResponseBody() . "\n";
      exit();
    }else {
      Quickbook::truncate();
      // echo '<pre>'; print_r($entities); die;

      foreach ($entities as $key => $invoice) {
        $invoiceData = array();
        $invoiceData['invoiceNumber'] =  isset($invoice->DocNumber) ? $invoice->DocNumber : '';
        $invoiceData['invoiceId'] =  isset($invoice->Id) ? $invoice->Id : '';
        $invoiceData['invoiceDate'] = isset($invoice->TxnDate) ? $invoice->TxnDate : '';
        $invoiceData['dueDate'] = isset($invoice->DueDate) ? $invoice->DueDate : '';
        $invoiceData['shippindDate'] = isset($invoice->ShipDate) ? $invoice->ShipDate : '';
        $invoiceData['trackingNumber'] = isset($invoice->TrackingNum) ? $invoice->TrackingNum : '';

        $invoiceData['shipVia'] = isset($invoice->ShipMethodRef) ? $invoice->ShipMethodRef : '';;
        $invoiceData['customerName'] =  isset($invoice->BillAddr->Line2) ? $invoice->BillAddr->Line2 : '';
        // $invoiceData['customerName'] = isset($invoice->customerData[0]->DisplayName) ? $invoice->customerData[0]->DisplayName : '';
        $invoiceData['phoneNumber'] = isset($invoice->customerData[0]->PrimaryPhone->FreeFormNumber) ? $invoice->customerData[0]->PrimaryPhone->FreeFormNumber : '';
        $invoiceData['email'] = isset($invoice->BillEmail->Address ) ? $invoice->BillEmail->Address : '';
        $invoiceData['address'] = isset($invoice->BillAddr->Line1) ? $invoice->BillAddr->Line1 : '';
        $invoiceData['city'] = isset($invoice->BillAddr->City) ? $invoice->BillAddr->City : '';
        $invoiceData['postCode'] = isset($invoice->BillAddr->PostalCode) ? $invoice->BillAddr->PostalCode : '';
        $invoiceData['country'] = isset($invoice->BillAddr->Country) ? $invoice->BillAddr->Country : '';
        $invoiceData['totalAmt'] = isset($invoice->TotalAmt) ? $invoice->TotalAmt : '';
        $invoiceData['balance'] = isset($invoice->Balance) ? $invoice->Balance : '';
        foreach ($invoice->Line as $lineKey => $line) {
          // echo '<pre>'; print_r($line->Id); die;
          $invoiceItemData= array();
          if(isset($line->Id)){
            $invoiceItemData['invoiceId']  = isset($invoice->Id) ? $invoice->Id : '';
            $invoiceItemData['lineNumber']  = isset($line->LineNum) ? $line->LineNum : '';
            $invoiceItemData['productCode']  = isset($line->SalesItemLineDetail->productCode) ? $line->SalesItemLineDetail->productCode : '';
            $invoiceItemData['description']  = isset($line->Description) ? $line->Description : '';
            $invoiceItemData['qty']  = isset($line->SalesItemLineDetail->Qty) ? $line->SalesItemLineDetail->Qty  : '';
            $invoiceItemData['rate']  = isset($line->SalesItemLineDetail->UnitPrice) ? $line->SalesItemLineDetail->UnitPrice : '';
            $invoiceItemData['netAmount']  = isset($line->Amount) ? $line->Amount : '';
            $invoiceItemData['vatAmount']  = isset($line->SalesItemLineDetail->TaxInclusiveAmt) ? $line->SalesItemLineDetail->TaxInclusiveAmt : '';
            $invoiceItemData['totalAmount']  = isset($line->Amount) ? $line->Amount : '';
          }
          Quickbook::saveInvoiceItems($invoiceItemData);
        }
        Quickbook::saveInvoices($invoiceData);
      }
      // echo '<pre>'; print_r($invoiceItemData);
      echo 'Invoice and Items sync succesfully.';
      die;


    }

    // $response['QueryResponse->Invoice']
    // echo '<pre>'; print_r($entities[0]->TotalAmt);
    // Echo some formatted output

  }
  public function getDeposit()
  {
     // echo 'as'; die;
    $getToken =  Quickbook::getQbToken();
    // print_r($getToken);
     $_SESSION['access_token'] = $getToken->accessTokenKey;
    $_SESSION['refresh_token'] = $getToken->refreshTokenKey;
    // Prep Data Services die;
    $dataService = DataService::Configure(array(
      'auth_mode' => 'OAuth2',
      'ClientID' => 'Q0wyzajGzOOZog3F8rtBcs81JJr46K0tBP4jTWxtabFI9hU0Xi', //Example 'Q0wDe6WVZMzyu1SnNPAdaAgeOAWNidnVRHWYEUyvXVbmZDRUfQ',
      'ClientSecret' => 'kRX8LH3FkcGJiM1ej2IL4rQRtUylmb5gHmsbHwDb',
      'accessTokenKey'   => $getToken->accessTokenKey,
      'refreshTokenKey'  => $getToken->refreshTokenKey,
      'QBORealmID' => "123146017550799",
      'baseUrl' => "Development"
    ));

    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

  //   $OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
  // echo '<pre>';  print_r($OAuth2LoginHelper); die;
  //   $accessToken = $OAuth2LoginHelper->refreshToken();
  //
  //   $dataService->updateOAuth2Token($accessToken);

    $dataService->throwExceptionOnError(true);
    //Add a new Invoice
    $entities = $dataService->Query("Select * from Deposit");
    $CompanyInfo = $dataService->getCompanyInfo();
    print_r($CompanyInfo); die;
    $error = $dataService->getLastError();
    if ($error) {
      echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
      echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
      echo "The Response message is: " . $error->getResponseBody() . "\n";
      exit();
    }
    // Echo some formatted output
    echo '<pre>'; print_r($entities);
    // include(app_path() . '/Services/QuickBooks-V3-PHP-SDK-master/getDeposit.php');
  }


  public function accountreceivables(Request $request){
    $output['parentUrl'] = "Account Receivables";
    $output['childUrl']  = "Account Receivables";
    $output['title']  = "Account Receivables";
    $output['accountreceivable']  = Quickbook::getAccountreceivable();

    if($request->input('ajax'))
    {
      return view('backend.qb.account-receivables-pagination',$output);
      echo json_encode($output);exit;
    }
    else
    {
      echo  view('include.header',$output);
      echo view('include.sidebar',$output);
      return view('backend.qb.account-receivables-list',$output);
    }
  }
  public function getInvoice(Request $request){
    $session = Session::get('userRef');
    if($session == ''){
      return redirect('/user-login');
    }
    $output['invDetail'] 	=   Quickbook::getInvoice($request->all());
    // print_r($output);
    if($request->input('admin')){
      $output['admin'] = $request->input('admin');
    }
    return view('backend.qb.invoice-detail', $output);
  }
*/


