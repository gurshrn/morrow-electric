<?php 
include('../setup.php');
$db = get_connection();	 
get_mysqlconnection();	
$today=date('Y-m-d');
$file_name='Service Region.pdf';  

$sql="SELECT DISTINCT name FROM `service_region` order by name asc ";       

$statement = $db->prepare($sql);	  
$statement->execute();	    
$result = $statement->fetchAll(); 
?>
<html>
<title>Morrow Electric</title>
<head>
<link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet -->
<link href="<?php echo SITE_URL; ?>assets/css/style.css" rel="stylesheet">



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 

<script src="<?php echo SITE_URL; ?>reports/jquery.columnizer.min.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(function(){
		
		$('.thin').columnize({width:300});
	});
</script>


<style>
  /*@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');*/
  
@font-face {
  font-family: 'CenturyGothic';
  src: url('fonts/CenturyGothic.eot?#iefix') format('embedded-opentype'),  url('fonts/CenturyGothic.woff') format('woff'), url('fonts/CenturyGothic.ttf')  format('truetype'), url('fonts/CenturyGothic.svg#CenturyGothic') format('svg');
  font-weight: normal;
  font-style: normal;
}
  
 @font-face {
  font-family: 'CenturyGothic-Bold';
  src: url('fonts/CenturyGothic-Bold.eot?#iefix') format('embedded-opentype'),  url('fonts/CenturyGothic-Bold.woff') format('woff'), url('fonts/CenturyGothic-Bold.ttf')  format('truetype'), url('fonts/CenturyGothic-Bold.svg#CenturyGothic-Bold') format('svg');
  font-weight: normal;
  font-style: normal;
}

 body{
	 
	 font-family: 'CenturyGothic'; 
	 margin:0;
	 padding:0; 

  }
  ul {
    margin: 0;
    padding: 0;
}
li{ 
           box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
	 list-style:none;
	 text-align:left;
	     margin: 0 0 15px;	
 } 
h4 {
    color: #000;
     font-family: 'CenturyGothic-Bold';
	 font-size:14px;
    margin: 0 0 10px;
    width: 100%;
    border-bottom: 1px solid #000;
} 
.mosaicflow__item {
    display:block;
    width: 100%;
    padding: 0 5px;
}
.cities-check, .client-name, .date-section,
.client-label {
	display:table-cell;
	vertical-align:top;
    padding: 5px 8px;
	
	   box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
}
h2{ 
     font-family: 'CenturyGothic-Bold';
    font-size:32px;
	margin:10px 0 20px;
	text-align:center;	
 }
 .cities-check {
	 float:left;   
    width: 16px;
    padding: 5px 3px;
}
.cities-check img{
  width:10px;	
 }
.client-name {
    padding: 1px 0px;
    font-size: 11px;
}
.client-label {
    font-size: 11px;
    width: 30%;
}
.date-section {
    width: 80px;
    font-size: 11px;
    float: right;
	padding:0; 
    text-align: right;
}
.label-control{
	display: table;
	padding:1px 0;
	width:100%;	
 }
.pull-right{
	float:right;	
}  
pull-left{
	float:left;	
 }
 .table-box {
    display: block;
    width: 100%;
}
.table-box .cities-check {
    width: 16px;

}
.table-box .client-name.full {
    width: 92%;

}

.case-1 {
    width: 100%;
    display: table;
}
.column {
    padding: 0 10px;
}  
  
 </style> 

</head>
<body>

	<div style="margin:0 auto; max-width:1200px;">

	<h2>Generator Service by Region</h2> 
	<div class="thin">
	<?php
	foreach($result as $row)
	{
		$service_reg=$row['name'];	 
		?>
		<h4><?php echo $service_reg; ?></h4>
		<?php
		 $sql1="SELECT * 
		FROM  `customer_generator` 
		WHERE  service_reg =  '".$service_reg."'";
		$statement1 = $db->prepare($sql1);	  
		$statement1->execute();	     
		$result1 = $statement1->fetchAll(); 
		foreach($result1 as $row1)  
		{
			$gen_id=$row1['id'];
			$label=get_gen_info($gen_id,'label');
			$cust_id=get_gen_info($gen_id,'cust_id');
			$type=get_gen_info($gen_id,'type');
			$cust_name=get_cust_info($cust_id,'company_name');
			$home_address=get_cust_info($cust_id,'home_address');
			$cottage_address=get_cust_info($cust_id,'cottage_address');
			$service_date=count_gen_progress_repair($gen_id);
			$home_address=get_cust_info($cust_id,'home_address');
			$cottage_address=get_cust_info($cust_id,'cottage_address');
			if($cottage_address!="")
			{
				$address=$cottage_address;
			}
			else
			{
				$address=$home_address;
			}	
			if($service_date!=0)
			{
				$service_date=date('M d, Y', strtotime($service_date));
			}	
			else
			{
				$service_date="";
			}	
			
			$tick=serviced_gen_worksheet_report($gen_id);
			$total_count=total_cust_gen($cust_id,$service_reg);
			
			if($cust_name=="")
			{
				$cust_name="(No Name)";
			}	 
			
			?>
			<?php
			if($total_count==1) 
			{
			?>
			<div class="case-1 <?php echo $gen_id; ?>">
				
				<div class="client-name">
				<?php if($tick > 0) { ?><img class="cities-check" src="<?php echo SITE_URL; ?>tick.png"><?php } ?><?php echo $cust_name; ?>
				</div> 
				<?php
				if($service_date!="")
				{	
				?>
					<div class="date-section">
					  <?php echo $service_date; ?>
					</div>     
				<?php
				}
				?>
			</div> 
			<?php
			}
			if($total_count > 1) 
			{	
			?>
			<div class="table-box case-2"> 
				
				<div class="client-name full cust_name"> 
				<?php if($tick > 0) { ?><img class="cities-check" src="<?php echo SITE_URL; ?>tick.png"><?php } ?><?php echo $cust_name; ?>
				</div>  
				<?php
				$sql2="SELECT * 
				FROM  `customer_generator` 
				WHERE cust_id =  '".$cust_id."' and service_reg='".$service_reg."'";
				$statement2 = $db->prepare($sql2);	   
				$statement2->execute();	     
				$result2 = $statement2->fetchAll(); 
				foreach($result2 as $row2)  
				{
					$gen_ids=$row2['id'];
					$labels=get_gen_info($gen_ids,'label');	
					$service_dates=count_gen_progress_repair($gen_ids);
					
					if($service_dates!=0)
					{
						$service_datess=date('M d, Y', strtotime($service_dates));
					}	
					else
					{
						$service_datess=""; 
					}	
					
					 
					
				?>	
				<div class="label-control">
					<div class="pull-left"> <?php echo $labels; ?></div>
					<div class="pull-right"> 
						<?php
						if($service_datess!="")
						{ 	
						?>
							<div class="date-section">
							  <?php echo $service_datess; ?>
							</div>      
						<?php 
						}    
						?>	 
					</div>
				</div> 
				<?php 
				}
				?>
			</div> 
			<?php
			} 
		}
		
	}
	?>
	</div>
</div>

</body>
</html>
