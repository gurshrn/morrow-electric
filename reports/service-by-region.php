<?php 
include('../setup.php');
if($_SESSION['uid']=="")
{
?>
<script>window.location.href="<?php echo SITE_URL; ?>";</script>
<?php	
die;	
}	

$db = get_connection();	 
get_mysqlconnection();	
$today=date('Y-m-d');
$year=date('Y');
$file_name='Service Region.pdf';  

$sql="SELECT DISTINCT name FROM `service_region` order by name asc ";       

$statement = $db->prepare($sql);	  
$statement->execute();	    
$result = $statement->fetchAll(); 
?>
<html>
<title>Morrow Electric</title>
<head>
<link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet -->
<link href="<?php echo SITE_URL; ?>assets/css/style.css" rel="stylesheet">



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 

<script src="<?php echo SITE_URL; ?>reports/jquery.columnizer.min.js" type="text/javascript" charset="utf-8"></script>
<script>
	$(function(){
		
		$('.thin').columnize({width:300, lastNeverTallest: true});
	});
</script>


<style>
  /*@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');*/
  
@font-face {
  font-family: 'CenturyGothic';
  src: url('fonts/CenturyGothic.eot?#iefix') format('embedded-opentype'),  url('fonts/CenturyGothic.woff') format('woff'), url('fonts/CenturyGothic.ttf')  format('truetype'), url('fonts/CenturyGothic.svg#CenturyGothic') format('svg');
  font-weight: normal;
  font-style: normal;
}
  
 @font-face {
  font-family: 'CenturyGothic-Bold';
  src: url('fonts/CenturyGothic-Bold.eot?#iefix') format('embedded-opentype'),  url('fonts/CenturyGothic-Bold.woff') format('woff'), url('fonts/CenturyGothic-Bold.ttf')  format('truetype'), url('fonts/CenturyGothic-Bold.svg#CenturyGothic-Bold') format('svg');
  font-weight: normal;
  font-style: normal;
}

 body{
	 
	 font-family: 'CenturyGothic'; 
	 margin:0;
	 padding:0; 

  }
  ul {
    margin: 0;
    padding: 0;
}
li{ 
           box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
	 list-style:none;
	 text-align:left;
	     margin: 0 0 15px;	
 } 
h4 {
    color: #000;
     font-family: 'CenturyGothic-Bold';
	 font-size:14px;
    margin: 0 0 10px;
    width: 100%;
    border-bottom: 1px solid #000;
} 
.mosaicflow__item {
    display:block;
    width: 100%;
    padding: 0 5px;
}
.cities-check, .client-name, .date-section,
.client-label {
	display:table-cell;
	vertical-align:top;
    padding: 5px 8px;
	
	   box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
}
h2{ 
     font-family: 'CenturyGothic-Bold';
    font-size:32px;
	margin:10px 0 20px;
	text-align:center;	
 }
 .cities-check {
	 float:left;   
    width: 16px;
    padding: 5px 3px;
}
.cities-check img{
  width:10px;	
 }
.client-name {
    padding: 1px 0px;
    font-size: 11px;
}
.client-label {
    font-size: 11px;
    width: 30%;
}
.date-section {
    width: 80px;
    font-size: 11px;
    float: right;
	padding:0; 
    text-align: right;
}
.label-control{
	display: table;
	padding:1px 0;
	width:100%;	
 }
.pull-right{
	float:right;	
}  
.pull-left{
	font-size:11px;
	float:left;	
 }
.pull-left i{
	font-size:10px;
} 
 .table-box {
    display: block;
    width: 100%;
}
.table-box .cities-check {
    width: 16px;

}
i{
	margin-right:3px;
}
.table-box .client-name.full {
    width: 92%;

}

.case-1 {
    width: 100%;
    display: table;
}
.column {
    padding: 0 10px;
} 
.table-box.case-2 .label-control .pull-left {
    font-size: 10px;
}
.table-box.case-2 .label-control .pull-right {
    font-size: 10px;
} 
  
 </style> 

</head>
<body>

	<div style="margin:0 auto; max-width:1200px;">

	<h2>Generator Service by Region</h2> 
	<div class="thin">
	<?php
	$cond="";
	if($_GET['type']=="current")
	{
		$cond=" and YEAR(cr_date)=".date('Y');
	}	
	
	$counntt=0;
	foreach($result as $row)
	{
		$service_reg=$row['name'];	 
		?>
		<h4><?php echo $service_reg; ?></h4>
		<?php
		$sql1="SELECT *
		FROM  `customer_generator`  
		WHERE  service_reg =  '".$service_reg."' ".$cond;
		$statement1 = $db->prepare($sql1);	  
		$statement1->execute();	      
		//$total_r=$statement1->rowCount();	      
		
		$result1 = $statement1->fetchAll();  
		foreach($result1 as $row1)  
		{
			$cust_id=$row1['cust_id'];
			$cust_name=get_cust_info($cust_id,'company_name');
			$home_address=get_cust_info($cust_id,'home_address');
			$cottage_address=get_cust_info($cust_id,'cottage_address');
			
			$total_count=total_cust_gen($cust_id,$service_reg);
			
			if($cust_name=="")
			{
				$cust_name="(No Name)";
			}
			
			$tick=serviced_gen_worksheet_report_n($cust_id,$service_reg);
			$insp=serviced_gen_worksheet_report_nn($cust_id,$service_reg); 	 
			?>
			<?php
			if($total_count==1) 
			{
				
				
				$service=cust_gen_service_date($cust_id,$service_reg);
				if($service!=0) 
				{
					$service_date=date('M d, Y', strtotime($service));
				}	
				else
				{
					$service_date="";
				}
				/*
				$cust_gen_id=get_cust_gen_1($cust_id);
				if($cust_gen_id!=0)
				{		
					$tick=serviced_gen_worksheet_report($cust_gen_id);
					$insp=serviced_gen_worksheet_report_1($cust_gen_id);
				} 
				else
				{
					$tick=0;
				}
				*/	
				
			?> 
				<div class="case-1 <?php echo $cust_id; ?>">
					
					<div class="client-name" <?php if($insp > 0) { echo 'style="background-color: yellow";'; } ?>>
					<?php if($tick > 0) { ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?></i><?php echo $cust_name; ?>
					</div> 
					
					<div class="date-section">
					  <?php echo $service_date; ?> 
					</div>     
					
				</div>  
			<?php
			}
			else
			{
			?>
				<div class="table-box case-2"> 
					<div class="client-name" <?php if($insp > 0) { echo 'style="background-color: yellow";'; } ?>>
					<?php if($tick > 0) { ?><i class="fa fa-check" aria-hidden="true"></i><?php } ?></i><?php echo $cust_name; ?> 
					</div> 
				
				<?php
				$sql11="SELECT * 
				FROM  `customer_generator` 
				WHERE cust_id =  '".$cust_id."' and service_reg='".$service_reg."'";  
				$statement11 = $db->prepare($sql11);	
				$statement11->execute();  
				$result11 = $statement11->fetchAll();
				foreach($result11 as $row11)
				{
					$gen_idd=$row11['id']; 
					$labels=$row11['label']; 
					$serviceee=count_gen_progress_repair($gen_idd); 
					if($serviceee!=0) 
					{
						$service_dateee=date('M d, Y', strtotime($serviceee));
					}	
					else
					{
						$service_dateee="";
					}
					
					$tick=serviced_gen_worksheet_report($gen_idd);
					$inspc=serviced_gen_worksheet_report_1($gen_idd);
				?>
				<div class="label-control"> 
					
					<div class="pull-left"><?php echo $labels; ?></div>
					<div class="pull-right">  
						<div class="date-section">
						 <?php echo $service_dateee; ?>
						</div>      
					</div>
				</div>
				<?php
				}
				?>
				</div>	
			<?php
			}	
			
		}
		
		$counntt=$counntt + $total_r;
	}
	
	
	
	
	?>
	</div>
</div>

</body>
</html>
