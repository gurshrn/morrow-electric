<?php

	//require_once   'setup.php';
	session_start();
	require_once  'config/constants.php';
	require_once   'config/functions.php';
	function get_connection()
{
	$hostname = 'localhost';
	
	try {
			$dbh = new PDO("mysql:host=$hostname;dbname=".DB, DBUSER, DBPWD);			
			
			}
		catch(PDOException $e)
			{
			echo $e->getMessage();
			die;
			}
			return $dbh; 
}

function get_mysqlconnection()
{
	$hostname = 'localhost';
	$con=mysql_connect($hostname,DBUSER,DBPWD);	
	mysql_select_db(DB,$con);	
}

	require('convert-html-to-pdf-with-fpdf/WriteHTML.php');
	
	
	if(!isset($_SESSION['uid']))
	{
		header("Location: http://morrowelectric.pro/");
		die();
		
	}
else{
	
	$id=$_REQUEST['id'];		
	$table=$_REQUEST['type'];
	
	get_mysqlconnection();
	
	$sql= "SELECT * FROM ".$table." WHERE id =".$id; 
	
	$result=mysql_query($sql);			
	$row =mysql_fetch_assoc($result); 
	
	$sql = "SELECT * FROM customers where id=".$row['customer_id'];
	$result1=mysql_query($sql);								
	$cus_result =mysql_fetch_assoc($result1);
	
	$created_by=$row['created_by'];	
	
	$sql = "SELECT * FROM users where id=".$created_by;
	$result_user=mysql_query($sql);								
	$user_result =mysql_fetch_assoc($result_user);
	
		
	$ufname=$user_result['first_name'];
	$ulname=$user_result['last_name'];	
	
	$originalDate = $row['created'];			
	$newDate = date("M d, y", strtotime($originalDate));
	$parts=unserialize($row['parts']);		
	 
	$pdf = new PDF();
	$pdf->AddPage();
	
	//$pdf->AddPage();
	$pdf->Image('logo.png',20,13);
	
	$pdf->Ln(20);
	
	//---------section1
	$phone = 'N/A';
	$fax = 'N/A';
	
	$first_name=$cus_result['first_name'];
	$last_name=$cus_result['last_name'];
	$display_name=$first_name." ".$last_name;		
	
	if(empty($first_name)){			
		$display_name=$cus_result['company_name'];
	}			
	$display_name=trim($display_name,'Mr.');
	$display_name=trim($display_name,'Mrs.');	
	
	$pdf->SetFont('Arial','',11);
	$pdf->SetMargins(18,0,0);	
	
	$pdf -> SetY(10);
	$pdf->SetMargins(0,0,18);
	$pdf->Cell(0,10,'126-1 Medora St',0,0,'R');	
	$pdf->Cell(0,20,' Port Carling ON',0,0,'R');	
	$pdf->Cell(0,30,' P0B 1J0',0,0,'R');	
	$pdf->SetFont('Arial','I',11);
	$pdf->Cell(0,40,'info@morrow-electric.com',0,0,'R');
	$pdf->SetFont('Arial','',11);
	
	
	//---------section2
	$date=date('M d, Y', strtotime($row['created']));
	$po_number=$row['po_number'];
	
	$pdf -> SetY(30);
	$pdf->SetMargins(0,0,18);
	$pdf->Cell(0,10,'Date: '.$date,0,0,'R');
	$pdf->Cell(0,20,'PO: '.$po_number,0,0,'R');
	
	//------------section2
	
	$address = '-';
	$city = '-';
	$province = '-';
	$postal = '-';
	
	
	if (!empty($cus_result['home_address'])) {
		$address = $cus_result['home_address'];
	}
	if (!empty($cus_result['home_city'])) {
		$city = $cus_result['home_city'];
	}
	if (!empty($cus_result['home_province'])) {
		$province = $cus_result['home_province'];
	}
	if (!empty($cus_result['home_postal_code'])) {
		$postal = $cus_result['home_postal_code'];
	}
	
	$pdf->Ln(0);
	$pdf->SetMargins(18,0,0);
	$pdf->WriteHTML('<br>'.trim($display_name).' <br>'.$address.' <br>'.$city.' '.$province.' '.$postal);
	
	
	$pdf->Ln(10);
	$pdf -> SetX(4);
	$pdf->SetFont('Arial','B',13);
	$pdf->Cell(0,0,$row['cat_name'],0,0,'C');
	
	$pdf->Ln(6);
	$pdf->SetFont('Arial','',11);
	$htmlTable1='
	<TABLE border="1">
	
	<TR>
	<TD bgcolor="#e7e4e4" width="65"><b>Item</b></TD>
	<TD bgcolor="#e7e4e4" width="65"><b>Qty</b></TD>
	<TD bgcolor="#e7e4e4" width="130"><b>Part No.</b></TD>
	<TD align="left" bgcolor="#e7e4e4" width="460"><b>Description</b></TD>
	</TR>
	';
	
	$HTML='';
	$count = 1; $qnt='-'; $part_no = '-'; $desc='-';
	$hours='-';
	if($table=='electrical_work')	
	{		
		$parts=unserialize($row['additional_info']);
		$hours=$row['hours'];	
	}
	else 
	{
		$parts=unserialize($row['parts']);
		$hours=$row['generator_hours'];
	}
	
	 foreach($parts['parts'] as $key =>$value)
	 {
		 
		if (!empty($value['qty'])) {
			$qnt = $value['qty'];
		}
		if (!empty($value['cat'])) {
			$part_no = $value['cat'];
		}
		if (!empty($value['inventory'])) {
			$desc = $value['inventory'];
		}
		 $HTML.='<TR>';
		 $HTML.='<TD width="65">'.$count.'</TD>';
		 $HTML.='<TD width="65">'.$qnt.'</TD>';
		 $HTML.='<TD width="130">'.$part_no.'</TD>';
		 $HTML.='<TD align="left" width="460">'.$desc.'</TD>';
		 $HTML.='</TR>';
		 $count++;
	 }	
	$pdf->SetMargins(18,0,0);
	$htmlTable3='</TABLE>';	
	//$pdf -> SetX(15);
	$pdf->WriteHTML("$htmlTable1");
	//$pdf -> SetX(15);
	$pdf->WriteHTML("$HTML");
	//$pdf -> SetX(15);
	$pdf->WriteHTML("$htmlTable3");
	
	$vehTable1='<br>
	<TABLE border="1">
	
	<TR>
		<TD bgcolor="#e7e4e4" width="100"><b>Item</b></TD>
		<TD bgcolor="#e7e4e4" width="150"><b>Travel</b></TD>
		<TD bgcolor="#e7e4e4" width="470"><b>Vehicle Name</b></TD>
	</TR>
	';
	
	$vehHTML='';
	$count = 1; 
	$vehicle = unserialize($row['vehicles']);
	
	
	foreach($vehicle['parts'] as $key =>$values)
	{
		
		if (!empty($values['vehicle'])) 
		{
			$veh = $values['vehicle'];
			$vehsql= "SELECT * FROM vehicles WHERE id =".$veh; 
			$vehresult=mysql_query($vehsql);			
			$vehrow =mysql_fetch_assoc($vehresult);
			
		}
		if (!empty($values['hours'])) {
			$vehHrs = $values['hours'];
		}
		
		$vehHTML.='<TR>';
		$vehHTML.='<TD width="100">'.$count.'</TD>';
		$vehHTML.='<TD width="150">'.$vehHrs.'</TD>';
		$vehHTML.='<TD width="470">'.ucfirst($vehrow['vehicle']).'</TD>';
		$vehHTML.='</TR>';
		$count++;
	}	
	$pdf->SetMargins(18,0,0);
	$vehhtmlTable3='</TABLE>';	
	$pdf->WriteHTML("$vehTable1");
	$pdf->WriteHTML("$vehHTML");
	$pdf->WriteHTML("$vehhtmlTable3");
	
	$EqpTable1='<br>
	<TABLE border="1">
	
	<TR>
		<TD bgcolor="#e7e4e4" width="100"><b>Item</b></TD>
		<TD bgcolor="#e7e4e4" width="150"><b>Hours</b></TD>
		<TD bgcolor="#e7e4e4" width="470"><b>Equipment Name</b></TD>
	</TR>
	';
	
	$EqpHTML='';
	$count = 1; 
	$equipments = unserialize($row['equipments']);
	
	foreach($equipments['parts'] as $key =>$value)
	{
		if (!empty($value['equipment'])) 
		{
			$eqp = $value['equipment'];
			
			$eqpsql= "SELECT * FROM equipment WHERE id =".$eqp; 
			$eqpresult=mysql_query($eqpsql);			
			$eqprow =mysql_fetch_assoc($eqpresult);
		}
		if (!empty($value['hours'])) {
			$eqpHrs = $value['hours'];
		}
		
		$EqpHTML.='<TR>';
		$EqpHTML.='<TD width="100">'.$count.'</TD>';
		$EqpHTML.='<TD width="150">'.$eqpHrs.'</TD>';
		$EqpHTML.='<TD width="470">'.ucfirst($eqprow['equipment']).'</TD>';
		$EqpHTML.='</TR>';
		$count++;
	}	
	$pdf->SetMargins(18,0,0);
	$eqphtmlTable3='</TABLE>';	
	$pdf->WriteHTML("$EqpTable1");
	$pdf->WriteHTML("$EqpHTML");
	$pdf->WriteHTML("$eqphtmlTable3");
	
	
	$notes = '-';
	if (!empty($row['notes'])) {
		$notes = $row['notes'];
	}
	
	$labour = '-';
	if (!empty($row['labour'])) {
		$labour = $row['labour'];
	}
	
	$travel = '-';
	if (!empty($row['travel'])) {
		$travel = $row['travel'];
	}
	
	$travel = '-';
	if (!empty($row['travel'])) {
		$travel = $row['travel'];
	}
	if (!empty($hours)) {
		$hours = $hours;
	}
	$equipment = '-';
	if (!empty($row['equipment'])) {
		$equipment = $row['equipment'];
	}
	
	if($_GET['sec']!="")
	{
		$query=" and work_sheet='".$_GET['sec']."'";
	}	
	else
	{
		$query="";
		
	}	
	$totalhours="SELECT  `staff_id` , SUM( hours )  'total' FROM  `staff_timesheet` WHERE work_id='".$_GET['id']."' ".$query." GROUP BY staff_id"; 
	$res_totalhours=mysql_query($totalhours); 
	while($row=mysql_fetch_array($res_totalhours))
	{
		$staff_id=$row['staff_id'];
		$hours_t=$row['total'];
		
		$fname=get_user_detail($staff_id,'first_name');
		$lname=get_user_detail($staff_id,'last_name');
		$name=$fname.' '.$lname;
		$labourr .=$name.' - '.$hours_t."<br>"; 
		
	}
	
	$cst=$labourr;
	$hours=''; 
	$pdf->SetMargins(0,0,18);
	$pdf->Ln(5);
	$pdf->WriteHTML('               <b>Service By:</b> '.$ufname.' '.$ulname);		
	$pdf->Ln(10);
	$pdf->WriteHTML('               <b>Labour:</b> '.$cst);	
	
	
	$pdf->Ln(10);
	//$pdf->WriteHTML('                 <b>Hours:</b> '.$hours);	
	
	
	/******************  Extra Field for generatorservices  *******************/	
	if($table=='generatorservices')	
	{	

		$sql="select * from generatorservices where id='".$_GET['id']."'";
		$ress=mysql_query($sql); 
		while($rowww=mysql_fetch_array($ress))
		{
			$new_oil_recycling=$rowww['oil_recycling'];
			$new_antifreeze=$rowww['antifreeze'];
			$new_antifreeze_disposal=$rowww['antifreeze_disposal'];
			$new_oil=$rowww['oil'];
			$new_oil_filter=$rowww['oil_filter'];
			$new_airfilter=$rowww['airfilter'];
			$new_battery=$rowww['battery'];
			$new_service_type=$rowww['service_type'];
			$new_full_service=$rowww['full_service'];
		}


		$oil_recycling = '-';
		if(!empty( $new_oil_recycling)) {			
			$oil_recycling = $new_oil_recycling;				
		}
		$antifreeze = '-';
		if(!empty( $new_antifreeze)) {			
			$antifreeze = $new_antifreeze;				
		}
		
		$antifreeze_disposal = '-';
		if(!empty( $new_antifreeze_disposal)) {			
			$antifreeze_disposal = $new_antifreeze_disposal;				
		}
		
		
		$oil_str = '-';
		if(!empty($new_oil)) {
			$oil=$new_oil;
			$oil_data=json_decode($oil);
			$oil_qty=$oil_data->qty;
			$oil_name=$oil_data->name;
			$oil_str=$oil_qty.', '.$oil_name;	
		}	
		
		$air_filter_type_str = '-';		
		if(!empty( $new_oil_filter)) {
			
			$air_filter_type = $new_oil_filter;		
			$air_filter_type_data=json_decode($air_filter_type);
			$air_filter_type_qty=$air_filter_type_data->qty;
			$air_filter_type_name=$air_filter_type_data->name;
			$air_filter_type_str=$air_filter_type_qty.', '.$air_filter_type_name;
		}
		
		$airfilter_str = '-';		
		if(!empty( $new_airfilter)) {
			
			$airfilter = $new_airfilter;		
			$airfilter_data=json_decode($airfilter);
			$airfilter_qty=$airfilter_data->qty;
			$airfilter_name=$airfilter_data->name;
			$airfilter_str=$airfilter_qty.', '.$airfilter_name;
		}
		
		$battery_str = '-';
		if(!empty( $new_battery)) {
			
			$battery = $new_battery;		
			$battery_data=json_decode($battery);
			$battery_qty=$battery_type_data->qty;
			$battery_name=$battery_data->name;
			$battery_str=$battery_qty.', '.$battery_name;
		}

		//added by BVR
		$service_type = '-';
		if(!empty( $new_service_type)) {			
			$service_type = $new_service_type;				
		}

		$full_service = ' ';
		if(!empty( $new_full_service)) {			
			$full_service = '+ '.$new_full_service;				
		} 
		//till here 
		
		
		
		
		$pdf->WriteHTML('                   <b>Oil Recycling :</b> '.$oil_recycling);	
		$pdf->WriteHTML('                   <b>Antifreeze:</b> '.$antifreeze);
		$pdf->Ln(10);
		$pdf->WriteHTML('              	     <b>Antifreeze Disposal:</b> '.$antifreeze_disposal);		
		$pdf->WriteHTML('          <b>Oil:</b> '.$oil_str);	
		$pdf->WriteHTML('                                           <b>Oil Filter:</b> '.$air_filter_type_str);
		$pdf->Ln(10);
		$pdf->WriteHTML('                 <b>Air Filter:</b> '.$airfilter_str);	
		$pdf->WriteHTML('                              <b>Battery:</b> '.$battery_str);
		$pdf->WriteHTML('                                       <b>Service Type:</b> '.$service_type.' '.$full_service);
	}
	
	
	
	
	/******************  Extra Field for generatortroubles  *******************/	
	if($table=='generatortroubles')	
		{				
					
			$sql="select * from generatortroubles where id='".$_GET['id']."'"; 
			$ress=mysql_query($sql);
			while($rowww=mysql_fetch_array($ress))
			{
				 $new_battery=$rowww['battery'];
				 $new_oil=$rowww['oil'];
				 $new_airfilter=$rowww['airfilter'];
				 $new_oil_filter=$rowww['oil_filter'];
				 $new_antifreeze=$rowww['antifreeze'];
			}	  
			
			
			
				
			  
			$antifreeze = "";
			if(!empty($new_antifreeze)) {			
				$antifreeze = $new_antifreeze;				
			}
			
			$oil_str = '-';
			if(!empty($new_oil)) {
				$oil=$new_oil;
				$oil_data=json_decode($oil);
				$oil_qty=$oil_data->qty;
				$oil_name=$oil_data->name;
				$oil_str=$oil_qty.', '.$oil_name;
			}
			 
			
			$airfilter_str = "";
			if(!empty($new_airfilter)) {
				$airfilter=$new_airfilter; 
				$airfilter_data=json_decode($airfilter); 
				$airfilter_qty=$airfilter_data->qty;
				$oil_name=$airfilter_data->name;
				$airfilter_str=$airfilter_qty.', '.$oil_name;	
			}
			
			$battery_str = "";
			
			
			if(!empty($new_battery)) {  
				
				$battery = $new_battery;		
				$battery_data=json_decode($battery);
				$battery_qty=$battery_data->qty; 
				$battery_name=$battery_data->name;
				$battery_str=$battery_qty.', '.$battery_name;
			}
			
			
			
			$oil_filter_str = '-';
			if(!empty($new_oil_filter)) {
				$oil_filter=$new_oil_filter;
				$oil_filter_data=json_decode($oil_filter);
				$oil_filter_qty=$oil_filter_data->qty; 
				$oil_filter_name=$oil_filter_data->name;
				$oil_filter_str=$oil_filter_qty.', '.$oil_filter_name;	
			}
			
			
			$pdf->WriteHTML('               <b>Antifreeze:</b> '.$antifreeze);
			$pdf->WriteHTML('               <b>Oil  :</b> '.$oil_str);	
			$pdf->Ln(10);
			
			$pdf->WriteHTML('               <b>Oil Filter:</b> '.$oil_filter_str);
			$pdf->Ln(10);
			$pdf->WriteHTML('               <b>Air Filter:</b> '.$airfilter_str);	
			$pdf->WriteHTML('               <b>Battery:</b> '.$battery_str);	  
				
		}
		
		/******************  Extra Field for generatorinspections  *******************/				
		if($table=='generatorinspections')	
		{
			
			$sql="select * from generatortroubles where id='".$_GET['id']."'";
			$ress=mysql_query($sql); 
			while($rowww=mysql_fetch_array($ress))
			{
				$new_antifreeze=$rowww['antifreeze'];
				$new_oil=$rowww['oil'];
				$new_air_filter_type=$rowww['air_filter_type'];
				$new_battery=$rowww['battery'];
				$new_oil_filter=$rowww['oil_filter'];
			}	
			
			
			$antifreeze = '-';
			if(!empty( $new_antifreeze)) {			
				$antifreeze = $new_antifreeze;				
			}
				
			$oil_str = '-';
			if(!empty($new_oil)) {
				$oil=$new_oil;
				$oil_data=json_decode($oil);
				$oil_qty=$oil_data->qty;
				$oil_name=$oil_data->name;
				$oil_str=$oil_qty.', '.$oil_name;	
			}	
			
			$airfilter_str = '-';
			if(!empty($new_air_filter_type)) {
				$airfilter=$new_air_filter_type;
				$airfilter_data=json_decode($airfilter);
				$airfilter_qty=$airfilter_data->qty;
				$oil_name=$airfilter_data->name;
				$airfilter_str=$airfilter_qty.', '.$oil_name;	
			}
			
			$battery_str = '-';
			if(!empty($new_battery)) {
				
				$battery = $new_battery;		
				$battery_data=json_decode($battery);
				
				$battery_qty=$battery_data->qty;
				$battery_name=$battery_data->name;
				$battery_str=$battery_qty.', '.$battery_name;
			}
			
			$oil_filter_str = '-';
			if(!empty($new_oil_filter)) {
				$oil_filter=$new_oil_filter; 
				$oil_filter_data=json_decode($oil_filter);
				$oil_filter_qty=$oil_filter_data->qty;
				$oil_filter_name=$oil_filter_data->name;
				$oil_filter_str=$oil_filter_qty.', '.$oil_filter_name;	
			}
		
		
			$pdf->WriteHTML('                 <b>Antifreeze:</b> '.$antifreeze);
			$pdf->WriteHTML('                 <b>Oil  :</b> '.$oil_str);	
			$pdf->Ln(10);
			
			$pdf->WriteHTML('                 <b>Oil Filter:</b> '.$oil_filter_str);
			$pdf->Ln(10);
			$pdf->WriteHTML('                 <b>Air Filter:</b> '.$airfilter_str);	
			$pdf->WriteHTML('                 <b>Battery:</b> '.$battery_str);	
				
		}
		
		
		if($table=='generatorstorages')	
			{	
			
			$sql="select * from generatorstorages where id='".$_GET['id']."'";
			$ress=mysql_query($sql); 
			while($rowww=mysql_fetch_array($ress))
			{
				$new_fogging_oil=$rowww['fogging_oil'];
				$new_battery_storage_charging=$rowww['battery_storage_charging'];
				
			}	
			
			$fogging_oil = '-';
			if(!empty($new_fogging_oil)) {			
				$fogging_oil = $new_fogging_oil;	 			
			}
			
			$battery_storage_charging = '-';
			if(!empty($new_battery_storage_charging)) {			
				$battery_storage_charging = $new_battery_storage_charging;	 			
			}
			$pdf->WriteHTML('                <b>Fogging Oil:</b> '.$fogging_oil);
			$pdf->WriteHTML('                <b>Battery Storage & Charging:</b> '.$battery_storage_charging);
			}
			
			
		if($table=='generatorestimates')	
		{	
			$antifreeze = '-';
			if(!empty( $row['antifreeze'])) {			
				$antifreeze = $row['antifreeze'];				
			}
			
			$battery = '-';
			if(!empty( $row['battery'])) {			
				$battery = $row['battery'];				
			}
			
			
			$airfilter_str = '-';
			if(!empty($row['airfilter'])) {
				$airfilter=$row['airfilter'];
				$airfilter_data=json_decode($airfilter);
				$airfilter_qty=$airfilter_data->qty;
				$oil_name=$airfilter_data->name;
				$airfilter_str=$airfilter_qty.', '.$oil_name;	
			}
				
			$pdf->WriteHTML('                   <b>Antifreeze:</b> '.$antifreeze);
			$pdf->WriteHTML('                   <b>Battery  :</b> '.$battery);	
			$pdf->Ln(10);
			$pdf->WriteHTML('                   <b>Airfilter :</b> '.$airfilter_str);	
			
		}
		
			
	$pdf->Ln(15);
	$pdf->SetMargins(18,0,0);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,0,'                   NOTES:',10,10,'L');
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',11);
	$pdf->MultiCell(185, 12,trim($notes), '', 'L', 0);
	//$pdf->SetMargins(15,0,0);
	$pdf->Output('morrow-'.$table.'.pdf','I'); 

}
?>