 var link='https://morrowelectric.pro/dev/';  
 $(document).ready(function(){ 	    
	 
	             //login submit 
				  $("#login").click(function(e){ 
					  var err = 0;
					 
					 
					   if(document.getElementById('uname').value == ''){
							   document.getElementById('uerror').innerHTML = 'Username is required.';
						   err =1;
					   }else{
						  document.getElementById('uerror').innerHTML = ''; 
					   }
						
                       if(document.getElementById('pwd').value == ''){
							   document.getElementById('perror').innerHTML = 'Password is required.';
							err = 1;
						}else{
						  document.getElementById('perror').innerHTML = ''; 
					   }							
					     
					   if(err == 0) 
					   {
					   data = $('#login-form').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){


									  if(result == 0)
									  { 
										 $("#message").html('<div class="error">Invalid username/password. Please try again.</div>');

									  }
									    else if(result == 2)
									  { 
										 $("#message").html('<div class="error">Your Account is not activated.</div>');

									  }
									  
									  else{
									      $("#message").html('<div class="success">User loged in Successfully.</div>');
									
									   window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
					   }
				  });
	jQuery('#uploadInv').validate({
		submitHandler: function(form) {
			data = new FormData(form),
			jQuery('#upload-csv-inventory').attr('disabled',false);
			jQuery('#upload-csv-inventory').val('Processing...');
			jQuery.ajax({ 
				type: "POST",
				data: data, 
				url: "handler.php",
				processData: false,
				contentType: false,
				dataType:"json",
				success: function(data) 
				{
					jQuery('#upload-csv-inventory').attr('disabled',true);
					jQuery('#upload-csv-inventory').val('Upload');
					if(data == 1)
					{
						document.getElementById("uploadInv").reset();
						$("#upload-message").html('<div id="result">Inventory uploaded successfully.</div>');
						setTimeout(function(){
						  location.reload();
						}, 3000);
					}
					else
					{
						$("#upload-message").html('<div id="result">Inventory not uploaded successfully.</div>');
					}
				} 
			});
		}	 
	});	

   //Generator Inspection
    $('#generator-inspection').validate({ // initialize the plugin
        rules: {
            generator_hours: {
                required: true,
				digits:true
            },
             oil: {
                required: true
            },
			
			oil_filter: {
                required: true
            },
            antifreeze: {
                required: true
            },
			airfilter: {
                required: true
            },
            battery: {
                required: true
            },			
			travel: {
                required: true
            },
            labour: {
                required: true
            },
            searchname: {
                required: true
            }
			,
            'staff_id[]': {
                required: true
            } 
			
        },
		
		
		 // Specify the validation error messages
        messages: {
            generator_hours: {
				 required:"Please enter Hours.",
				 digits: "Please enter a valid number."
				},
            oil: "Please enter Oil.",
            oil_filter: "Please enter Oil filter.",
            oil: "Please enter oil.",
			antifreeze: "Please enter Antifreeze.",
            airfilter: "Please enter Airfilter.",
			battery: "Please enter Battery.",
            travel: "Please enter Travel.",
            labour: "Please enter Labour.",
			searchname: "Please enter Customer Name.",
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#inspection').click(function() {
        if ($('#generator-inspection').valid()) {
                                
			            data = $('#generator-inspection').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }
									   else if(result == 2)
									  { 
										 $("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');

									  }
									  else{
										    document.getElementById("generator-inspection").reset();
									    $("#message").html('<div id="result">Data saved successfully.</div>');
									
										location.reload(); 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Generator Inspection


   
   
   //Generator Service
    $('#generator-service').validate({ // initialize the plugin
        rules: { 
            generator_hours: {
                required: true,
				digits:true
            },
             oil: {
                required: true
            },
			
			oil_filter: {
                required: true
            },
			
			oil_recycling: {
                required: true
            },
			
            antifreeze: {
                required: true
            },
			
			antifreeze_disposal: {
                required: true
            },
			
			airfilter: {
                required: true
            },
            battery: {
                required: true
            },			
			travel: {
                required: true
            },
			searchname:{ required: true	},
			'staff_id[]':{ required: true }
        },
		
		
		 // Specify the validation error messages
        messages: {
            generator_hours:  {
				 required:"Please enter Hours.",
				 digits: "Please enter a valid number."
				},
            oil: "Please enter Oil.",
			oil_recycling: "Please enter Oil recycling.",
            oil_filter: "Please enter Oil filter.",
            oil: "Please enter oil.",
			antifreeze: "Please enter Antifreeze.",
			antifreeze_disposal: "Please enter Antifreeze disposal.",
            airfilter: "Please enter Airfilter.",
			battery: "Please enter Battery.",
            travel: "Please enter Travel.",
			searchname: "Please enter Customer Name.",
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#service').click(function() {
		
		var gen=jQuery('.gen_sec').html();
		var len1=jQuery('input[name=genrator]:checked').length;
		var len2=jQuery('input[name=service_type]:checked').length;
		var service_typee=jQuery('input[name=service_typee]:checked').length;
		if(gen!="")
		{	
			if(len1=="0")
			{
				alert('Select Generator'); 
				return false;
			} 
		} 
		if(len2=="0")
		{
			alert('Select Service');  
			return false;
		} 

		if(service_typee=="0")
		{
			alert('Select Service or SPRING START-UP?');  
			return false;   
		}	
		 
        if ($('#generator-service').valid()) {
                                
			            data = $('#generator-service').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }
									  else if(result == 2)
									  { 
										 $("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');

									  }
									  else{
										   document.getElementById("generator-service").reset();
									   		  $("#message").html('<div id="result">Data saved successfully.</div>');
									
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
             $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Generator Service



    //Generator Estimate
    $('#generator-estimate').validate({ // initialize the plugin
        rules: {
            generator_hours: {
                required: true,
				digits:true
            },
            antifreeze: {
                required: true
            },
			airfilter: {
                required: true
            },
            battery: {
                required: true
            },			
			travel: {
                required: true
            },
            labour: {
                required: true
            },
			searchname:{ required: true	}, 
			'staff_id[]':{ required: true	} 
			
        },
		
		
		 // Specify the validation error messages
         messages: {
            generator_hours: {
				 required:"Please enter Hours.",
				 digits: "Please enter a valid number."
				},
          	antifreeze: "Please enter Antifreeze.",
            airfilter: "Please enter Airfilter.",
			battery: "Please enter Battery.",
            travel: "Please enter Travel.",
            labour: "Please enter Labour.",
			searchname: "Please enter Customer Name.",
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#estimate').click(function() {
		
		var gen=jQuery('.gen_sec').html();
		var len1=jQuery('input[name=genrator]:checked').length;
		var len2=jQuery('input[name=service_type]:checked').length;
		if(gen!="") 
		{	
			if(len1=="0")
			{
				alert('Select Generator'); 
				return false;
			} 
		}
		
			
        if ($('#generator-estimate').valid()) {
                                
			            data = $('#generator-estimate').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }
									  else if(result == 2)
									  { 
										 $("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');

									  }
									  else{
										  document.getElementById("generator-estimate").reset();
									     $("#message").html('<div id="result">Data saved successfully.</div>');
									
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
             $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Generator Estimate


   
   
     //Generator Storage
    $('#generator-storage').validate({ // initialize the plugin
        rules: {
            generator_hours: {
                required: true,
				 digits: true
            },
            fogging_oil: {
                required: true
            },
		
			travel: {
                required: true
            },
            labour: {
                required: true
            },
			searchname:{ required: true	},
			'staff_id[]':{ required: true	}
			
        },
		
		
		 // Specify the validation error messages
         messages: {
            generator_hours: {
				 required:"Please enter Hours.",
				 digits: "Please enter a valid number."
				},
          	fogging_oil: "Please enter Fogging oil.",
            travel: "Please enter Travel.",
            labour: "Please enter Labour.",
			searchname: "Please enter Customer Name.",
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#storage').click(function() {
		
		
		var gen=jQuery('.gen_sec').html();
		var len1=jQuery('input[name=genrator]:checked').length;
		var len2=jQuery('input[name=service_type]:checked').length;
		if(gen!="") 
		{	
			if(len1=="0")
			{
				alert('Select Generator'); 
				return false;
			} 
		}
		
        if ($('#generator-storage').valid()) {
                                
			            data = $('#generator-storage').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }
									  else if(result == 2)
									  { 
										 $("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');

									  }else{
										 document.getElementById("generator-storage").reset();
									    $("#message").html('<div id="result">Data saved successfully.</div>');
									
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Generator Storage
   
   
   
      //Generator Trouble
    $('#generator-trouble').validate({ // initialize the plugin
        rules: {
            generator_hours: {
                required: true
            },
             oil: {
                required: true
            },
			
			oil_filter: {
                required: true
            },
            antifreeze: {
                required: true
            },
			airfilter: {
                required: true
            },
            battery: {
                required: true
            },			
			travel: {
                required: true
            },
            labour: {
                required: true
            },
			searchname:{ required: true	},
			'staff_id[]':{ required: true }   
			
        },
		
		
		 // Specify the validation error messages
        messages: {
            generator_hours: "Please enter Generator hours.",
            oil: "Please enter Oil.",
            oil_filter: "Please enter Oil filter.",
            oil: "Please enter oil.",
			antifreeze: "Please enter Antifreeze.",
            airfilter: "Please enter Airfilter.",
			battery: "Please enter Battery.",
            travel: "Please enter Travel.",
            labour: "Please enter Labour.",
			searchname: "Please enter Customer Name.",
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

	
    $('#trouble').click(function() {
		
		var gen=jQuery('.gen_sec').html();
		var exist=jQuery('input[name=genrator]').length;
		var len1=jQuery('input[name=genrator]:checked').length;
		var trouble_status=jQuery('input[name=trouble_status]:checked').length;
			
			
				if(len1=="0")
				{
					alert('Select Generator'); 
					return false;  
				} 
			
		
			
			if(trouble_status=="0") 
			{
				alert('Is this worksheet in complete or repair section?'); 
				return false; 
			} 
		 
		
        if ($('#generator-trouble').valid()) {
                                
			            data = $('#generator-trouble').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');
									  }
									  else if(result == 2)
									  { 
										 $("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');

									  }
									  else{
										 document.getElementById("generator-trouble").reset();
									    $("#message").html('<div id="result">Data saved successfully.</div>');
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Generator Trouble

   
   
   
   //Add Customer
    $('#add-customer').validate({ // initialize the plugin
        rules: {
            first_name: {
                required: true
            },
             last_name: {
                required: true
            },			
			phone: {
                required: true,
				digits: true,
				maxlength: 10,
				minlength: 10,
            }/*,
            mobile: {
                required: true,
				digits: true,
				maxlength: 10,
				minlength: 10,
				
            },*/
			/*email: {
                required: true,
				email: true,
				remote: {
							url: "check-email.php",
							type: "post",
							
							
						
						 }
            }*/
            			
			
           
			
        },
		
		 // Specify the validation error messages
        messages: {
            first_name: "Please enter First name.",
            last_name: "Please enter Last name.",
            phone: {required:"Please enter  Phone Number.",
					 digits:"Please enter valid number.",
		}/*,
            mobile: {required:"Please enter  Mobile.",
					 digits:"Please enter valid number.",
		},*/
			/*email: {
                required: "Please enter email address.",
                email: "Please enter a valid email address.",
				remote: "Email already in use!"
            },*/
            
			
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#customer').click(function() {
        if ($('#add-customer').valid()) {
                              
			            data = $('#add-customer').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }else{
										   document.getElementById("add-customer").reset();
									    $("#message").html('<div id="result">Data saved successfully.</div>');
									
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
             $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Add Customer
  
	
	
	 
	 
	  
	 

  
   //Add Electrical Work
    $('#electrical-work-form').validate({ // initialize the plugin
        rules: {
            /*equipment: {
                required: true
            },
             hours: {
                required: true,
				digits: true 
            },	*/		
            
            'staff_id[]': {
                required: true   
            },
			
			searchname:{ required: true	},
			//po_number:{required: true}
						
        },
		
		 // Specify the validation error messages
        messages: {
           /* equipment: "Please enter Equipment.",
            hours: {
				required:"Please enter Hours.",
				 digits: "Please enter a valid number."
				},
            labour: "Please enter Labour.",
			travel: "Please enter Travel.",*/
			searchname: "Please enter Customer Name.",
			//po_number:"Please enter PO Number",
        },
		
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });

    $('#electrical').click(function() {
        if ($('#electrical-work-form').valid()) {
                        
						
						
			            data = $('#electrical-work-form').serialize();
						
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result)
								{
									  
									if(result == 0)
									  {  
										 $("#message").html('<div id="result">Unable to save! Please try again.</div>');

									  }else if(result == 2){
										 
										$("#message").html('<div id="result" style="color:red" >Please select Valid Customer name from list.</div>');
									
									 
									  }	
									  else 
									  { 
									    document.getElementById("electrical-work-form").reset();
									   $("#message").html('<div id="result">Data saved successfully.</div>');
									   setTimeout(function() { $("#result").remove(); }, 3000);
										location.reload();  

									  }  
								},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   //End Electrical Work
   
   
    //Add Staff Work
   
   jQuery.validator.addMethod("noSpace", function(value, element) { 
    return value.indexOf(" ") < 0 && value != ""; 
  }, "Space are not allowed");
   
    $('#add-staff').validate({ // initialize the plugin
        rules: {
            first_name: { required: true       },
             last_name: {   required: true },			
            password: { required: true },
			 username: {
						required:true,
						noSpace:true,
						remote: {
							url: link+"handler.php?action=userExists",
							type: "post"
						 }
					},  
            email: {
						required: true,
						email: true,
						remote: {
							url: link+"handler.php?action=emailExists",
							type: "post",							
						 }
					},
        },
		
		 // Specify the validation error messages
        messages: {
            first_name: "Please enter First Name.",
            last_name: "Please enter Last Name.",
            password: "Please enter passsword.",
			 username: {
						required:"Please enter value.",
						noSpace:"Space is not allowed.",
						remote: "Username already in use!"
					}, 
			 email: {
						required: "Please enter your email address.",
						email: "Please enter a valid email address.",
						remote: "Email already in use!"
						},
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });
	$('#add-vehicle').validate
	({ 
	
        rules: 
		{
            vehicle: { required: true  },
            rate: { required: true },			
        },
		
		messages: 
		{
            vehicle: "Please enter First Name.",
            rate: "Please enter Last Name.",
        },
		
        submitHandler: function (event) 
		{ 
        	event.preventDefault();
            return false; 
        }
    });
	$('#add-equipment').validate
	({ 
	
        rules: 
		{
            equipment: { required: true  },
            rate: { required: true },			
        },
		
		messages: 
		{
            equipment: "Please enter First Name.",
            rate: "Please enter Last Name.",
        },
		
        submitHandler: function (event) 
		{ 
        	event.preventDefault();
            return false; 
        }
    });
   
   
   
   // Add new Staff
    $('#staff').click(function() {
        if ($('#add-staff').valid()) {
                              
			            data = $('#add-staff').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){

									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }else{
									   $("#message").html('Data saved successfully.');
									
									  location.reload();
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
             $('html, body').animate({scrollTop:0}, 'slow');
        }
    });
	
	$(document).on('click','#vehicle',function() {
		if ($('#add-vehicle').valid()) 
		{
            data = $('#add-vehicle').serialize();
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: data,
				success:function(result){

					if(result == 0)
					{ 
						$("#message").html('Unable to save! Please try again.');

					}	
					else
					{
						$("#message").html('Data saved successfully.');
						location.reload();
					}									  
				},
				error:function(e){
					console.log(e);
				}	
			});
		} 
		else 
		{
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });
	$(document).on('click','#equipment',function() {
		if ($('#add-equipment').valid()) 
		{
            data = $('#add-equipment').serialize();
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: data,
				success:function(result){

					if(result == 0)
					{ 
						$("#message").html('Unable to save! Please try again.');

					}	
					else
					{
						$("#message").html('Data saved successfully.');
						location.reload();
					}									  
				},
				error:function(e){
					console.log(e);
				}	
			});
		} 
		else 
		{
            $('html, body').animate({scrollTop:0}, 'slow');
        }
    });

   
  


  
  

    

   //End Add Staff  
   
   //Add inventory 
    $('#inventory').click(function() {
								  
        if ($('#add-inventory').valid()) {
                              
			            data = $('#add-inventory').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									
									var obj = $.parseJSON(result);
									
									
									  if(result == 0)
									  { 
										 $("#message").html('Unable to save! Please try again.');

									  }else{
									   $("#message").html('Data saved successfully.');
									   
									  // var counter = 1;
									   
										$('#example').DataTable().row.add( [
											'<img src="assets/images/square-gif.gif">',
											obj.part_no,
											obj.manufacturer,
											obj.description,
											obj.cost,
											obj.price
											
										] ).draw( false );
									
										
											
										$('#myModal1').modal('hide');
										
									  // window.location=link; 
									  }									  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
             $('html, body').animate({scrollTop:0}, 'slow');
        }
    });
   // Validate Staff 
   $('#add-inventory').validate({ // initialize the plugin
        rules: {
            part_no: { required: true       },
            manufacturer: {   required: true },			
            description: { required: true }, 
			cost: { required: true },
			price: { required: true },
			 
            
        },
		
		 // Specify the validation error messages
        messages: {
            part_no: "Please enter Equipment.",
            manufacturer: "Please enter manufacturer.",
            description: "Please enter description.",
		    cost: {required:"Please enter cost.",digits:"Cost should be numeric."},
			price: {required:"Please enter description.",digits:"Price should be numeric."},
        },
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });
	
	$('#material').click(function() 
	{
		if ($('#add-material').valid()) 
		{
            data = $('#add-material').serialize();
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: data,
				success:function(result)
				{
					var obj = $.parseJSON(result);
					console.log(obj.category);
					if(result == 0)
					{ 
						$("#message").html('Unable to save! Please try again.');
					}
					else
					{
						$("#message").html('Data saved successfully.');
						$('#example2').DataTable().row.add( [
							'<img src="assets/images/square-gif.gif">',
							obj.part_no,
							obj.category,
							obj.title,
							obj.price,
							'<a href="javascript:void(0)" class="edit-btn-1" onclick="edit_material('+obj.id+');">Edit</a>',
						] ).draw( false );
						$("#fromToggle").slideUp('slow');
						$('#add-material').trigger('reset');
					}									  
				},
				error:function(e)
				{
					console.log(e);
				}	
			});
		} 
		else 
		{
			$('html, body').animate({scrollTop:0}, 'slow');
        }
    });
	
	$('#add-material').validate({ 
        rules: 
		{
            part_no: 
			{ 
				required: true      
			},
            title: 
			{   
				required: true 
			},			
			price: 
			{ 
				required: true 
			},
		},
		messages: 
		{
            part_no: "Please enter part no.",
            title: "Please enter title.",
            price: {required:"Please enter price.",digits:"Price should be numeric."},
        },
		
        submitHandler: function (event) 
		{ 
        	event.preventDefault();
            return false; 
        }
    });
   
   
   
   
   //////////////////////////////////////
   
   // Change Pass
    $('#chagne_pass').click(function() {
        if ($('#change-pass').valid()) {
                              
			            data = $('#change-pass').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									
								 if(result == 0)
									  { 
										 $("#message").html('Something went wrong.');

									  }
									  else if(result == 2){
										 
										   $("#message").html(' Old password is wrong.');
										   }
									  else{
									   $("#message").html('Password changed Successfully.');
									
									  setTimeout(function() { window.location.href = "http://morrowelectric.pro/?section=logout";  }, 3000);
									  }		
										 
						   		
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            //alert('form is not valid');
        }
    });

   //End  Change Pass   
   
    $('#change-pass').validate({ // initialize the plugin
        rules: {
            old_password: { required: true       },
             password: {   required: true },			
           password_confirm : {
						
						equalTo : "#password"
					},
        },
		
		 // Specify the validation error messages
        messages: {
            old_password: "Please enter Old Password.",
            password: "Please enter New Password.",
            password_confirm : {					
						equalTo : "Password Not match."
					}
		},
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });
   
      //End  Change Pass
   //////////////////////////////////////////
   
   
    // forget Pass
    $('#forget_pass').click(function() {
        if ($('#forget-pass').valid()) {
                              
			            data = $('#forget-pass').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									 
										 $("#passmessage").html(result);
						  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            //alert('form is not valid');
        }
    });

   //End  Forget Pass   
   
    $('#forget-pass').validate({ // initialize the plugin
        rules: {
            email: { 
					required: true,
					email: true      
				},            
        },
		
		 // Specify the validation error messages
        messages: {
			required:"Please enter Email.",
            email: "Email address is not valid.",
            
		},
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });
   
      //End  Change Pass
	  
	  
	 //-----
   
   
     //////////////////////////////////////
   
   // Reset Pass
    $('#reset_pass').click(function() {
        if ($('#reset-pass').valid()) {
                              
			            data = $('#reset-pass').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									 
										 $("#message").html(result);
										 window.setTimeout(function(){
											
											window.location.href = "http://morrowelectric.pro";
									
										}, 3000);
						  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
        } else {
            //alert('form is not valid');
        }
    });

   //End  Reset Pass   
   
    $('#reset-pass').validate({ // initialize the plugin
        rules: {
           
             password: {   required: true },			
          	 password_confirm : {
						
						equalTo : "#password"
					},
        },
		
		 // Specify the validation error messages
        messages: {            
            password: "Please enter New Password.",
            password_confirm : {					
						equalTo : "Password Not match."
					}
		},
		
        submitHandler: function (event) { 
        	event.preventDefault();
            return false; 
        }
    });
   
      //End  Reset Pass
   //////////////////////////////////////////
   
  
  
  
  /************************      USER INFO             *****************************/
  
  $('#save-user-info').click(function() {
      
                              
			            data = $('#user-info').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									 
										location.reload();
						  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
       
    });
  
    /************************     END USER INFO             *****************************/
  
  
  
  /**********************   NOTIFICAION ************************/
  
  
  $('#save_notificatoin').click(function() {
       // if ($('#reset-pass').valid()) {
		  
                             $('#message-cus').html('<i class="fa fa-spin fa-spinner" aria-hidden="true"></i>') 
			            data = $('#notification').serialize();
						$.ajax({type: "POST",
								url: "handler.php",
								data: data,
								success:function(result){
									 
										 //$("#message").html(result);
										 $('#message-cus').html('<i class="fa fa-check" aria-hidden="true"></i>') 
									
						  
									},
								error:function(e){
									console.log(e);
								}	
							  });
			
       /* } else {
            //alert('form is not valid');
        }*/
    });
  
  
  /*************************   NOTIFICATION END ************************/
  
  
  
   
   // Ajax name search
     
	   
			
				  				  
    });
 
	
	
	function validateForm() {
		var first_name = document.forms["myForm"]["first_name"].value;
		if (first_name == null || first_name == "") {
			alert("First Name must be filled out");
			return false;
		}
		 var last_name = document.forms["myForm"]["last_name"].value;
		 if (last_name == null ||last_name == "") {
			alert("Last Name must be filled out");
			return false;
		}
		var phone = document.forms["myForm"]["phone"].value;
		 if (phone == null ||phone == "") {
			alert("Phone must be filled out");
			return false;
		}
		//Uncomment to add validation on email
		
		/* var email = document.forms["myForm"]["email"].value;
		 if (email == null ||email == "") {
			alert("email must be filled out");
			return false;
		}
		
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (!filter.test(email)) {
			alert('Please provide a valid email address');   
			return false;
		}*/
	}
	
	function validateForm1() {
		var vehicle = document.forms["myForm1"]["vehicle"].value;
		if (vehicle == null || vehicle == "") {
			alert("First Name must be filled out");
			return false;
		}
		 var rate = document.forms["myForm1"]["rate"].value;
		 if (rate == null ||rate == "") {
			alert("Last Name must be filled out");
			return false;
		}
		
		//Uncomment to add validation on email
		
		/* var email = document.forms["myForm"]["email"].value;
		 if (email == null ||email == "") {
			alert("email must be filled out");
			return false;
		}
		
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (!filter.test(email)) {
			alert('Please provide a valid email address');   
			return false;
		}*/
	}

	function validateForm2() {
		var equipment = document.forms["myForm2"]["equipment"].value;
		if (equipment == null || equipment == "") {
			alert("First Name must be filled out");
			return false;
		}
		 var rate = document.forms["myForm2"]["rate"].value;
		 if (rate == null ||rate == "") {
			alert("Last Name must be filled out");
			return false;
		}
		
		//Uncomment to add validation on email
		
		/* var email = document.forms["myForm"]["email"].value;
		 if (email == null ||email == "") {
			alert("email must be filled out");
			return false;
		}
		
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

		if (!filter.test(email)) {
			alert('Please provide a valid email address');   
			return false;
		}*/
	}




/*$(document).keypress(function(e) {
    if (e.which == 13) {
       
        $('#login').trigger('click');
		$('#staff').trigger('click');
		$('#customer').trigger('click');
		$('#inventory').trigger('click');
		$('#estimate').trigger('click');
		$('#inspection').trigger('click');
		$('#service').trigger('click');
		$('#trouble').trigger('click');
		$('#storage').trigger('click');
		$('#electrical').trigger('click');
		$('#inventory').trigger('click');
		$('#chagne_pass').trigger('click');
		
    }
});*/

function get_service(e)
{
	if(e==1)
	{
		jQuery('.next_service').empty().append('INSPECTION');
	}
	if(e==2)
	{
		jQuery('.next_service').empty().append('FULL');
	}	
}