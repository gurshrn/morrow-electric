<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Morrow Electric</title>

<!-- Favicon -->

<link rel="shortcut icon" href="<?php echo SITE_URL?>/favicon.ico" type="image/x-icon">

<!-- Bootstrap -->
<link href="assets/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet -->
<link href="assets/css/style.css" rel="stylesheet">


<!-- FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<!----------------->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
<script src='https://www.google.com/recaptcha/api.js'></script> 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

</head>
<body>
<div id="preloader"></div>

<!-- header -->
<div class="mobile-header"><button id="btnSliderMenu" class="btnSliderMenu"><span class="bars"></span><span class="bars"></span><span class="bars"></span></button></div>
<header class="header clearfix">
  <div class="logo"><a href="<?php echo SITE_URL?>"><img src="assets/images/logo.png" alt="Morrow Electric"></a></div>
 
<?php if(isset($_SESSION['user'])){ ?>
 <div class="hdr-right clearfix">
    <div class="user-info-top clearfix">
      <h3>Welcome <strong><?php echo ucfirst($_SESSION['user']); ?></strong></h3>
      <div class="user-avatar"><img src="assets/images/user-dummy.gif"></div>
	  <a href="logout.php">Logout <i class="fa fa-sign-out"></i></a>
    </div>
    <div class="search-box">
      <form>
        <input name="" type="text" placeholder="Search">
        <input name="" type="submit">
      </form>
    </div>
  </div>
  </header>

<div class="main-wrap clearfix">
<?php } else { ?>
   <div class="hdr-right clearfix" style="display:none;">
   <div class="user-info-top clearfix">
   <h2>Morrow Electric</h2>
   </div>
   </div>
   </header>
<div class="main-wrap not-logged-in clearfix">
   
   <?php } ?>
