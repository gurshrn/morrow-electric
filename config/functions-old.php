<?php
function get_header()
{
require_once   'config/header.php';	
}

function get_footer()
{
require_once   'config/footer.php';	
}

function get_sidebar()
{
require_once   'config/leftbar.php';	
}


function get_navbar()
{
require_once   'config/navbar.php';	
}

function calculateWeekDate($month,$year)
{
$noOfDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
$returnData = array();
	for($day = 1; $day <= $noOfDaysInMonth; $day=$day+7) { 
		$weekStartDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"),$month, $day, $year));
		$returnData[] = weekRange($weekStartDate);
	}
return $returnData;
}

function weekRange($date) {
$ts = strtotime($date);
$start = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
$startWeekDate = date('Y-m-d', $start);
$endWeekDate = date('Y-m-d', strtotime('next sunday', $start));
return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next sunday', $start)));
}

function get_user_detail($user_id,$field)
{
	$db = get_connection();
	$sql="select ".$field." from users where id='".$user_id."'";
	$statement = $db->prepare($sql);	
	$statement->execute(); 
	$result = $statement->fetchAll();
	foreach($result as $row)
	{
		return $row[$field];
	} 	
	
}

function returnDates($fromdate, $todate) {
	$fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
	$todate = \DateTime::createFromFormat('Y-m-d', $todate);
	return new \DatePeriod(
		$fromdate,
		new \DateInterval('P1D'),
		$todate->modify('+1 day')
	);
}

function get_sum_hours($date,$user_id)
{
	
	$db = get_connection();
	$sql="SELECT SUM(hours) 'total' FROM staff_timesheet WHERE DATE(cr_date) =  '".$date."' AND staff_id ='".$user_id."' GROUP BY staff_id"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	if($count==0)
	{
		return "0"; 
	}
	else
	{		
		foreach($result as $row)
		{
			return $row['total'];  
		} 
	}
	

	
}


//load content
function load_content($section)
{
   if($section == 'login'){
		require_once   'pages/login.php';	
   }
   else if($section == 'logout'){
		
		require_once   'logout.php';
   }
   else  if($section == 'dashboard'){
		require_once   'pages/dashboard.php';	
   }
   else  if($section == 'addCustomer'){
		require_once   'pages/addCustomer.php';	
   }
   else if($section == 'customers'){
		require_once   'pages/customers.php';	
   }
   else if($section == 'customersList'){
		require_once   'pages/customersList.php';	
   }
   else if($section == 'electricalWork'){
		require_once   'pages/electricalWork.php';	
   }
   else if($section == 'generatorInspection'){
		require_once   'pages/generatorInspection.php';	
   }
   else if($section == 'generatorRepairEstimate'){
		require_once   'pages/generatorRepairEstimate.php';	
   }
   else if($section == 'generatorServices'){
		require_once   'pages/generatorServices.php';	
   }
   else if($section == 'generatorTrouble'){
		require_once   'pages/generatorTrouble.php';	
   }
   else if($section == 'generatorWinterStorage'){
		require_once   'pages/generatorWinterStorage.php';	
   }
   else if($section == 'staffList'){
		require_once   'pages/staff.php';	
   }
   else if($section == 'addStaff'){
		require_once   'pages/addStaff.php';	
   }
   else if($section == 'userInfo'){
	   require_once   'pages/userInfo.php';	
   }
   else if($section == 'resetPass'){
	   require_once   'pages/resetPass.php';	
   }
   else if($section == 'addInventory'){
	   require_once   'pages/addInventory.php';	
   }
    else if($section == 'listInventory'){
	   require_once   'pages/inventory.php';	
   }
   else if($section == 'notification'){
	   require_once   'pages/notification.php';	
   }
   else if($section == 'electricalWorkTime'){
	   require_once   'pages/electricalWorkTime.php';	 
   }
    else if($section == 'generatorInspectionTime'){
	   require_once   'pages/generatorinspectiontime.php';	 
   }
	 else if($section == 'generatorRepairEstimateTime'){
	   require_once   'pages/generatorRepairEstimateTime.php';	 
   }
    else if($section == 'generatorTroubleTime'){
	   require_once   'pages/generatorTroubleTime.php';	 
   }
   else if($section == 'generatorWinterStorageTime'){
	   require_once   'pages/generatorWinterStorageTime.php';	 
   }
    else if($section == 'generatorServicesTime'){
	   require_once   'pages/generatorServicesTime.php';	 
   }
     else if($section == 'timesheet'){
	   require_once   'pages/timesheet.php';	 
   }
   else{	     
	   require_once   'pages/dashboard.php';	
   }
  
}






function checkLogin()
{


	if($_SESSION['user']!="")
	{
		return 1;
	}else
	{
	 session_regenerate_id(true);    
		return 0;
	}
}






?>