<?php
function get_header()
{
require_once   'config/header.php';	
}

function get_footer()
{
require_once   'config/footer.php';	
}

function get_sidebar()
{
require_once   'config/leftbar.php';	
}


function get_navbar()
{
require_once   'config/navbar.php';	
}

function calculateWeekDate($month,$year)
{
$noOfDaysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
$returnData = array();
	for($day = 1; $day <= $noOfDaysInMonth; $day=$day+7) { 
		$weekStartDate = date("Y-m-d", mktime(date("H"), date("i"), date("s"),$month, $day, $year));
		$returnData[] = weekRange($weekStartDate);
	}
return $returnData;
}

function weekRange($date) {
$ts = strtotime($date);
$start = (date('w', $ts) == 0) ? $ts : strtotime('last monday', $ts);
$startWeekDate = date('Y-m-d', $start);
$endWeekDate = date('Y-m-d', strtotime('next sunday', $start));
return array(date('Y-m-d', $start), date('Y-m-d', strtotime('next sunday', $start)));
}

function get_user_detail($user_id,$field)
{
	$db = get_connection();
	$sql="select ".$field." from users where id='".$user_id."'";
	$statement = $db->prepare($sql);	
	$statement->execute(); 
	$result = $statement->fetchAll();
	foreach($result as $row)
	{
		return $row[$field];
	}
	
	
}

function returnDates($fromdate, $todate) {
	$fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
	$todate = \DateTime::createFromFormat('Y-m-d', $todate);
	return new \DatePeriod(
		$fromdate,
		new \DateInterval('P1D'),
		$todate->modify('+1 day')
	);
}

function get_sum_hours($date,$user_id)
{
	
	$db = get_connection();
	$sql="SELECT SUM(hours) 'total' FROM staff_timesheet WHERE DATE(cr_date) =  '".$date."' AND staff_id ='".$user_id."' GROUP BY staff_id"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	if($count==0)
	{
		return "0"; 
	}
	else
	{		
		foreach($result as $row)
		{
			return $row['total'];  
		} 
	}
	

	
}

function gen_service_report_sec($gen_id)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `worksheet_genrator` 
	WHERE  `work_sheet` =  'gen_trouble'
	ORDER BY DATE( cr_date ) desc";	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	$count = $statement->rowCount(); 
	
}


function count_gen_progress_repair_1()
{
	$i=0;
	$db = get_connection();
	$sql="SELECT * 
	FROM  `worksheet_genrator` 
	WHERE  `work_sheet` =  'gen_trouble'
	ORDER BY DATE( cr_date ) desc";	   
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	$count=$statement->rowCount();  
	if($count > 0)
	{
		foreach($result as $row) 						
		{						
			$work_id=$row['work_id']; 
			$work_status=get_work_sheet_info($work_id,'generatortroubles','trouble_status');
			if($work_status=="repair")
			{ 
				$i++;
			}
		}
		return $i;
	}
	else
	{
		return 0;
	}		
}

function count_gen_progress_repair($gen_id)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `worksheet_genrator` 
	WHERE  gen_id='".$gen_id."'";	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	$count = $statement->rowCount();  
	if($count > 0)
	{	
	foreach($result as $row) 						
	{	
		return $row['cr_date'];
	}
	
	}
	else
	{
		return $count; 
	}	
}

function serviced_gen_worksheet_report($gen_id)
{
	$db = get_connection();
	$sql="SELECT a . * , b . * 
	FROM worksheet_genrator AS a
	INNER JOIN generatorservices AS b ON a.work_id = b.id
	WHERE a.work_sheet =  'generatorServices'
	AND a.gen_id =  '".$gen_id."' 
	AND b.service_typee =  'service'";	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
}

function serviced_gen_worksheet_report_n($cust_id,$ser_reg)
{
	$db = get_connection();
	$sql="SELECT a . * , b . * 
	FROM customer_generator AS a
	INNER JOIN generatorservices AS b ON a.cust_id = b.customer_id
	WHERE a.service_reg =  '".$ser_reg."'
	AND a.cust_id =  '".$cust_id."' 
	AND b.service_typee =  'service'";	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
}

function serviced_gen_worksheet_report_nn($cust_id,$ser_reg)
{
	$db = get_connection();
	$sql="SELECT a . * , b . *  
	FROM customer_generator AS a
	INNER JOIN generatorservices AS b ON a.cust_id = b.customer_id
	WHERE a.service_reg =  '".$ser_reg."'
	AND a.cust_id =  '".$cust_id."' 
	AND b.service_type =  'inspection'";	     
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
}

function serviced_gen_worksheet_report_1($gen_id)
{
	$db = get_connection();
	$sql="SELECT a . * , b . * 
	FROM worksheet_genrator AS a
	INNER JOIN generatorservices AS b ON a.work_id = b.id
	WHERE a.work_sheet =  'generatorServices'
	AND a.gen_id =  '".$gen_id."' 
	AND b.service_typee =  'inspection'"; 	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	return $count = $statement->rowCount(); 
}

function serviced_gen_ins_worksheet_report($gen_id)
{
	$db = get_connection(); 
	$sql="SELECT a . * , b . * 
	FROM worksheet_genrator AS a
	INNER JOIN generatorservices AS b ON a.work_id = b.id
	WHERE a.work_sheet =  'generatorServices'
	AND a.gen_id =  '".$gen_id."' 
	AND b.service_typee =  'inspection'";	   
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
}



function get_staff_info($staff_id,$field)
{
	$db = get_connection();
	$sql="SELECT $field
	FROM  users where id='".$staff_id."'"; 	  
	$statement = $db->prepare($sql);	  
	$statement->execute();	    
	$result = $statement->fetchAll();
	
	foreach($result as $row) 						
	{	
		return $row[$field];
	
	}
	
	
}

function get_total_assigned_services()
{
	$db = get_connection();
	$sql="SELECT count(*) FROM  `genassignservices`";
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$result = $statement->fetchAll(); 
	return $count = $result[0][0];  
}

function count_expiring_warranty() 
{
	$db = get_connection();
	$today=date('Y-m-d');      
	$sql="SELECT id, cust_id, label, service_reg, model, 
	TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
	STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
	FROM  `customer_generator` 
	WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
	ORDER BY days_left asc ";	
	$statement = $db->prepare($sql);	   
	$statement->execute();	
	$data = $statement->fetchAll();    
	return $count = $statement->rowCount();  
	
	
	
}

function count_no_service_record() 
{
	$db = get_connection();
	$sql="SELECT DISTINCT gen_id FROM  `worksheet_genrator` ";
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$data1 = $statement->fetchAll();   
	
	foreach($data1 as $row1)
	{
		$work_gen[]=$row1['gen_id'];
	}
	
	$sql="SELECT DISTINCT id FROM  `customer_generator` ";
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$data = $statement->fetchAll();   
	$count = $statement->rowCount(); 
	foreach($data as $row) 
	{
		$cust_gen[]=$row['id']; 
	}

	
	$result=array_diff($cust_gen, $work_gen);
	$iZero = array_values($result);
	
	return count($iZero); 
	
	
}


function check_cust_gen($cust_id)
{
	$db = get_connection();
	$sql="select * from customer_generator where cust_id='".$cust_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $statement->rowCount(); 
	
}
 
function get_cust_gen_info($cust_id,$field)
{
	$db = get_connection();
	$sql="SELECT $field from customer_generator where id='".$cust_id."'"; 
	$statement = $db->prepare($sql);	 
	$statement->execute();  
	$result = $statement->fetchAll(); 
	$count = $statement->rowCount();
	foreach($result as $row)
		{
			return $row[$field];   
		}   
}

function get_gen_info($gen_id,$field)
{
	$db = get_connection();
	$sql="SELECT $field from customer_generator where id='".$gen_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	foreach($result as $row)
		{
			return $row[$field];   
		}   
}

function genassignservices($gen_id,$field)
{
	$db = get_connection();
	$sql="SELECT $field from genassignservices where gen_id='".$gen_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	foreach($result as $row)
		{
			return $row[$field];   
		} 
}

function get_work_sheet_info($work_id,$table,$field)
{
	$db = get_connection();
	$sql="SELECT $field from $table where id='".$work_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	foreach($result as $row)
		{
			return $row[$field];   
		}   
}

function get_cust_info($cust_id,$field)
{
	$db = get_connection();
	$sql="SELECT $field from customers where id='".$cust_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	foreach($result as $row)
		{
			return $row[$field];   
		}   
}

function get_part_no($part_no)
{
	$db = get_connection();
	$sql="SELECT * from inventory where id='".$part_no."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();
	foreach($result as $row)
	{
		return $row['part_no'];   
	}    
}

/***********  Generator Dashboard   *************/

 
function me_get_rows_count($table,$condition_query='') /***********    Get Row Count by conditions  query  **************/
{
	$db = get_connection();
	$condition_finale="";
	$count = 0;
	if(!empty($condition_query))
	{
		$condition_start="WHERE ";
		$condition_finale = $condition_start.$condition_query;
	}
	$sql="SELECT * from $table $condition_finale"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$count = $statement->rowCount();
	return $count;
}


function me_get_data($table,$condition_query='',$limit='') /***********    Get Row data by conditions query   **************/
{
	$db = get_connection();
	$condition_finale="";
	$result = array();
	if(!empty($condition_query))
	{
		$condition_start="WHERE ";
		$condition_finale = $condition_start.$condition_query;
	}
	$sql="SELECT * from $table $condition_finale $limit"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $result;
}


function me_get_value($table,$field,$condition_query='') /***********    Get Row Value by conditions  query  **************/
{
	$db = get_connection();
	$condition_finale="";
	$result = array();
	if(!empty($condition_query))
	{
		$condition_start="WHERE ";
		$condition_finale = $condition_start.$condition_query;
	}
	$sql="SELECT $field from $table $condition_finale"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	if(count($result) > 0)
	{
		foreach($result as $row)
		{
			return $row[$field];   
		}	
	}
	else
	{
		return "";
	}		
	
}


	
function createDateRangeArray($strDateFrom,$strDateTo,$count='')      /* get range between two days */
{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

    $aryRange=array();

    $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
    $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

    if ($iDateTo>=$iDateFrom)
    {
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
	if(!empty($count))
	{
		return count($aryRange);
	}
	else
	{
		return $aryRange;	
	}
    
}


function update_cust_gen_info($cust_id,$field,$value)
{
	$db = get_connection();
	$sql="Update customer_generator set $field='".$value."' where id='".$cust_id."'";
	mysql_query($sql);  
}

//load content
function load_content($section)
{
   if($section == 'login'){
		require_once   'pages/login.php';	
   }
   else if($section == 'logout'){
		
		require_once   'logout.php';
   }
   else  if($section == 'dashboard'){
		require_once   'pages/dashboard.php';	
   }
   else  if($section == 'addCustomer'){
		require_once   'pages/addCustomer.php';	
   }
   else if($section == 'customers'){
		require_once   'pages/customers.php';	
   }
   else if($section == 'customersList'){
		require_once   'pages/customersList.php';	
   }
   else if($section == 'electricalWork'){
		require_once   'pages/electricalWork.php';	
   }
   else if($section == 'generatorInspection'){
		require_once   'pages/generatorInspection.php';	
   }
   else if($section == 'generatorRepairEstimate'){
		require_once   'pages/generatorRepairEstimate.php';	
   }
   else if($section == 'generatorServices'){
		require_once   'pages/generatorServices.php';	
   }
   else if($section == 'generatorTrouble'){
		require_once   'pages/generatorTrouble.php';	
   }
   else if($section == 'generatorWinterStorage'){
		require_once   'pages/generatorWinterStorage.php';	
   }
   else if($section == 'staffList'){
		require_once   'pages/staff.php';	
   }
   else if($section == 'addStaff'){
		require_once   'pages/addStaff.php';	
   }
   else if($section == 'userInfo'){
	   require_once   'pages/userInfo.php';	
   }
   else if($section == 'resetPass'){
	   require_once   'pages/resetPass.php';	
   }
   else if($section == 'addInventory'){
	   require_once   'pages/addInventory.php';	
   }
   else if($section == 'listMaterial'){
	   require_once   'pages/material.php';	
   }
    else if($section == 'listInventory'){
	   require_once   'pages/inventory.php';	
   }
   else if($section == 'notification'){
	   require_once   'pages/notification.php';	
   }
   else if($section == 'electricalWorkTime'){
	   require_once   'pages/electricalWorkTime.php';	 
   }
    else if($section == 'generatorInspectionTime'){
	   require_once   'pages/generatorinspectiontime.php';	 
   }
	 else if($section == 'generatorRepairEstimateTime'){
	   require_once   'pages/generatorRepairEstimateTime.php';	 
   }
    else if($section == 'generatorTroubleTime'){
	   require_once   'pages/generatorTroubleTime.php';	 
   }
   else if($section == 'generatorWinterStorageTime'){
	   require_once   'pages/generatorWinterStorageTime.php';	 
   }
    else if($section == 'generatorServicesTime'){
	   require_once   'pages/generatorServicesTime.php';	 
   }
     else if($section == 'timesheet'){
	   require_once   'pages/timesheet.php';	 
   }
   else if($section == 'generators'){  
	   require_once   'pages/generator.php';	  
   } 
    else if($section == 'assign_service'){
	   require_once   'pages/assign_service.php';	   
   } 
    else if($section == 'genrator_report'){
	   require_once   'pages/genrator_report.php';	   
   } 
   
   else{	     
	   require_once   'pages/dashboard.php';	
   }
  
}

function checkLogin()
{


	if($_SESSION['user']!="")
	{
		return 1;
	}else
	{
	 session_regenerate_id(true);    
		return 0;
	}
}

function cust_has_genrator($cust_id)
{
	$db = get_connection();
	$sql="SELECT * from customer_generator where cust_id='".$cust_id."'"; 
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
	
}

function createDateRange_checkDate($start,$end)
{
   global $wpdb;	  
   
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(
         new DateTime($start),
         $interval,
         $realEnd
    );
	
    foreach($period as $date)  
	{ 
        $array[] = $date->format('Y-m-d'); 
    }
	
	
	return $array;
	
	
}

function count_services_this_week()
{
	$curr_monday = date( 'Y-m-d', strtotime('monday this week'));
	$curr_saturday = date( 'Y-m-d', strtotime('sunday this week') );	
	
	$arr=createDateRange_checkDate($curr_monday,$curr_saturday);  
	
	
	foreach($arr as $val)
	{
		$asd .="'".$val."'".",";
	} 
	
	$act_date=substr($asd,0,-1);
	$db = get_connection();
	$sql="SELECT count(*)
	FROM  `worksheet_genrator` 
	WHERE  `work_sheet` =  'generatorServices'
	AND DATE( cr_date ) 
	IN (".$act_date.")";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $result[0][0];   
	
}

function get_last_service($gen_id)
{
	$db = get_connection();
	$sql="SELECT *
	FROM  `genassignservices` 
	WHERE  `gen_id` =  '$gen_id'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	$count = $statement->rowCount();  
	if($count!=0)
	{
		foreach($result as $row) 						
		{						
		return $row['service_date']; 
		}
	}	
	else
	{
		return "0"; 
	}		
}


function check_work_sheet($gen_id)
{
	$db = get_connection();
	$sql="SELECT *
	FROM  `worksheet_genrator` 
	WHERE  `gen_id` =  '$gen_id' and work_sheet='gen_win_storage'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();   
	
}


function days_leftt($warr_exp)
{
	$endDate=date('Y-m-d');
	return round(abs(strtotime($endDate)-strtotime($warr_exp))/86400);
}


function count_gen_serviced_complete($gen_id)
{
	$db = get_connection();
	$sql="SELECT a. * , b. * , c.id, c.service_typee 
	FROM genassignservices AS a
	INNER JOIN worksheet_genrator AS b ON a.gen_id = b.gen_id
	INNER JOIN generatorservices AS c ON b.work_id = c.id
	WHERE work_sheet =  'generatorServices'
	AND c.service_type =  'service' and a.gen_id='".$gen_id."'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();    
	

}

function count_gen_serviced_complete_1($id)
{
	$db = get_connection();
	$sql="select * from genassignservices where type='service' and id='".$id."'";   
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll();
	return $count = $statement->rowCount();     
	
}

function total_cust_gen($cust_id,$service_reg)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `customer_generator` 
	WHERE cust_id =  '".$cust_id."' and service_reg='".$service_reg."'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll(); 
	return $count = $statement->rowCount(); 
}


function cust_gen_service_date($cust_id,$service_reg)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `customer_generator` 
	WHERE cust_id =  '".$cust_id."' and service_reg='".$service_reg."'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll(); 
	$count = $statement->rowCount(); 
	if($count > 0)
	{	
		foreach($result as $row)
		{
			$gen_id=$row['id']; 
		} 
		
		$countt=count_gen_progress_repair($gen_id);
		if($countt!=0) 
		{
			return $countt;
		}	
		else
		{
			return 0;
		}	
	}
	else
	{
		return 0;
	}	
}

function check_date_staff_report($date,$staff_id)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `genassignservices` 
	WHERE DATE(service_date) =  '".$date."' and staff_id='".$staff_id."'";  
	$statement = $db->prepare($sql); 	
	$statement->execute();   
	$result = $statement->fetchAll(); 
	return $count = $statement->rowCount();  
}

function get_cust_gen_1($cust_id)
{
	$db = get_connection();
	$sql="SELECT * 
	FROM  `customer_generator` 
	WHERE cust_id =  '".$cust_id."'";  
	$statement = $db->prepare($sql);	
	$statement->execute();  
	$result = $statement->fetchAll(); 
	$count = $statement->rowCount(); 
	if($count > 0)
	{	
		foreach($result as $row)
		{
			return $gen_id=$row['id']; 
		} 
	}
	else
	{
		return 0;
	}	

}
function updateCustomerQbId($qbId,$id)
{
	$db = get_connection();
	$sql="Update customers set qb_id='".$qbId."' where id='".$id."'";
	$statement = $db->prepare($sql);	
	$affec_row = $statement->execute();
	if($affec_row != 0)
	{
		echo 1;
	}
	else
	{
		echo 0;
	}
}
function updateUserQbId($qbId,$id)
{
	$db = get_connection();
	$sql="Update users set qb_id='".$qbId."' where id='".$id."'";
	$statement = $db->prepare($sql);	
	$affec_row = $statement->execute();
	if($affec_row != 0)
	{
		echo 1;
	}
	else
	{
		echo 0;
	}
}
function getInvoiceDetail($invoiceIds,$tables)
{
	$db = get_connection();
	if($tables == 'electrical_work')
	{
		
		$sql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$invoiceIds." and `work_sheet` ='elec_work' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$row = $statement->fetchAll();
		
		
		foreach($row as $val)
		{
			$staffArray[]=$val['staff_id'];
			
		}
		
		
		foreach($staffArray as $val1)
		{
			$field = 'first_name,last_name,billrate,qb_id';
			$id = $val1;
			$sql="select * from users where id='".$id."'";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			$staffInfos[] = $result[0];
		}
		
		
		
		$sqls = "select additional_info , equipments,hours,vehicles, id,customer_id,invoice_id from electrical_work where `id`=".$invoiceIds;
		$statements = $db->prepare($sqls); 	
		$statements->execute();   
		$rows = $statements->fetchAll();
		
		$inventory = unserialize($rows[0]['additional_info']);
		
		$equipments = unserialize($rows[0]['equipments']);
		$vehicles = unserialize($rows[0]['vehicles']);
		
		
		$equipHrs = $rows[0]['hours'];
		$inventoryParts = array();
		$vehicleParts = array();
		
		foreach($inventory as $inv)
		{
			foreach($inv as $val3)
			{
				
				if($val3['qty'] == '')
				{
					$val3['qty'] = 1;
				}
				$inventoryParts[] = $val3;
			}
		}
		
	
		foreach($inventoryParts as $parts)
		{
			$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
			$statements1 = $db->prepare($sqls1); 	
			$statements1->execute();   
			$rows13 = $statements1->fetchAll();
			foreach($rows13 as $val13)
			{
				
				
				$val13['cat'] = $parts['inventory'];
				$val13['qty'] = $parts['qty'];
				$val13['catName'] = $parts['cat'];
				$inventoryId2[] = $val13;
			}
			
			
		}
		
		foreach($vehicles as $veh)
		{
			foreach($veh as $vehdata)
			{
				
				if($vehdata['vehicle'] != '')
				{
					$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
					
				}
				$vehicleParts[] = $vehdata;
			}
		}
		foreach($vehicleParts as $vehParts)
		{
			if($vehParts['vehicle'] != '')
			{
				$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
				$vehstatements = $db->prepare($vehsqls); 	
				$vehstatements->execute();   
				$vehrows = $vehstatements->fetchAll();
				foreach($vehrows as $vehval)
				{
					$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
					$vehval['hours'] = $vehParts['hours'];
					$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
					$vehicleinventoryId[] = $vehval;
				}
			}
		}
		foreach($equipments as $equip)
		{
			foreach($equip as $equipdata)
			{
				
				if($equipdata['equipment'] != '')
				{
					$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
					
				}
				$equipParts[] = $equipdata;
				
			}
		}
		
		foreach($equipParts as $equParts)
		{
			if($equParts['equipment'] != '')
			{
				$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
				$eqstatements = $db->prepare($equsqls); 	
				$eqstatements->execute();   
				$eqrows = $eqstatements->fetchAll();
				
				foreach($eqrows as $eqval)
				{
					$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
					$eqval['hours'] = $equParts['hours'];
					$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
					$equipinventoryId[] = $eqval;
				}
			}
		}
		
	}
	if($tables == 'generatortroubles')
	{
		$sql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$invoiceIds." and `work_sheet` ='gen_trouble' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$row = $statement->fetchAll();
		
		
		foreach($row as $val)
		{
			$staffArray[]=$val['staff_id'];
			
		}
		
		
		foreach($staffArray as $val1)
		{
			$field = 'first_name,last_name,billrate,qb_id';
			$id = $val1;
			$sql="select * from users where id='".$id."'";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			$staffInfos[] = $result[0];
		}
		
		$sqls = "select parts as additional_info , oil,oil_filter,airfilter,battery,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatortroubles where `id`=".$invoiceIds;
		$statements = $db->prepare($sqls); 	
		$statements->execute();   
		$rows = $statements->fetchAll();
		$inventory = unserialize($rows[0]['additional_info']);
		$oils = json_decode($rows[0]['oil']);
		if($oils->name != '' && $oils->qty != '')
		{
			$oil = $oils;
		}
		$oilFilters = json_decode($rows[0]['oil_filter']);
		if($oilFilters->name != '' && $oilFilters->qty != '')
		{
			$oilFilter = $oilFilters;
		}
		$airFilters = json_decode($rows[0]['airfilter']);
		if($airFilters->name != '' && $airFilters->qty != '')
		{
			$airFilter = $airFilters;
		}
		$batterys = json_decode($rows[0]['battery']);
		if($batterys->name != '' && $batterys->qty != '')
		{
			$battery = $batterys;
		}
		
		$equipments = unserialize($rows[0]['equipments']);
		$vehicles = unserialize($rows[0]['vehicles']);
		
		$inventoryParts = array();
		$vehicleParts = array();
		foreach($inventory as $inv)
		{
			foreach($inv as $val3)
			{
				
				if($val3['qty'] == '')
				{
					$val3['qty'] = 1;
				}
				$inventoryParts[] = $val3;
			}
		}
		foreach($inventoryParts as $parts)
		{
			$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
			$statements1 = $db->prepare($sqls1); 	
			$statements1->execute();   
			$rows13 = $statements1->fetchAll();
			foreach($rows13 as $val13)
			{
				
				
				$val13['travel'] = $parts['travel'];
				$val13['cat'] = $parts['inventory'];
				$val13['catName'] = $parts['cat'];
				$val13['qty'] = $parts['qty'];
				$inventoryId2[] = $val13;
			}
			
			
		}
		foreach($vehicles as $veh)
		{
			foreach($veh as $vehdata)
			{
				
				if($vehdata['vehicle'] != '')
				{
					$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
					
				}
				$vehicleParts[] = $vehdata;
			}
		}
		foreach($vehicleParts as $vehParts)
		{
			if($vehParts['vehicle'] != '')
			{
				$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
				$vehstatements = $db->prepare($vehsqls); 	
				$vehstatements->execute();   
				$vehrows = $vehstatements->fetchAll();
				foreach($vehrows as $vehval)
				{
					$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
					$vehval['hours'] = $vehParts['hours'];
					$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
					$vehicleinventoryId[] = $vehval;
				}
			}
		}
		foreach($equipments as $equip)
		{
			foreach($equip as $equipdata)
			{
				
				if($equipdata['equipment'] != '')
				{
					$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
					
				}
				$equipParts[] = $equipdata;
				
			}
		}
		
		foreach($equipParts as $equParts)
		{
			if($equParts['equipment'] != '')
			{
				$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
				$eqstatements = $db->prepare($equsqls); 	
				$eqstatements->execute();   
				$eqrows = $eqstatements->fetchAll();
				
				foreach($eqrows as $eqval)
				{
					$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
					$eqval['hours'] = $equParts['hours'];
					$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
					$equipinventoryId[] = $eqval;
				}
			}
		}
	}
	if($tables == 'generatorestimates')
	{
		$sql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$invoiceIds." and `work_sheet` ='gen_repair_est' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$row = $statement->fetchAll();
		
		foreach($row as $val)
		{
			$staffArray[]=$val['staff_id'];
			
		}
		
		
		foreach($staffArray as $val1)
		{
			$field = 'first_name,last_name,billrate,qb_id';
			$id = $val1;
			$sql="select * from users where id='".$id."'";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			$staffInfos[] = $result[0];
		}
		
		
		$sqls = "select parts as additional_info , airfilter,battery,antifreeze,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorestimates where `id`=".$invoiceIds;
		$statements = $db->prepare($sqls); 	
		$statements->execute();   
		$rows = $statements->fetchAll();
		$inventory = unserialize($rows[0]['additional_info']);
		$airfilters = json_decode($rows[0]['airfilter']);
		if($airfilters->qty != '' && $airfilters->name != '')
		{
			$airFilter = $airfilters;
		}
		$battery1 = $rows[0]['battery'];
		$antifreeze = $rows[0]['antifreeze'];
		$equipHrs = $rows[0]['hours'];
		
		$equipments = unserialize($rows[0]['equipments']);
		$vehicles = unserialize($rows[0]['vehicles']);
		$inventoryParts = array();
		foreach($inventory as $inv)
		{
			
			foreach($inv as $val3)
			{
				if($val3['cat'] != '')
				{
					if($val3['qty'] == '')
					{
						$val3['qty'] = 1;
					}
					$inventoryParts[] = $val3;
				}
				
			}
		}
		foreach($inventoryParts as $parts)
		{
			$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
			$statements1 = $db->prepare($sqls1); 	
			$statements1->execute();   
			$rows13 = $statements1->fetchAll();
			foreach($rows13 as $val13)
			{
				
				
				$val13['travel'] = $parts['travel'];
				$val13['cat'] = $parts['inventory'];
				$val13['catName'] = $parts['cat'];
				$val13['qty'] = $parts['qty'];
				$inventoryId2[] = $val13;
			}
			
			
		}
		
		foreach($vehicles as $veh)
		{
			foreach($veh as $vehdata)
			{
				
				if($vehdata['vehicle'] != '')
				{
					$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
					
				}
				$vehicleParts[] = $vehdata;
			}
		}
		foreach($vehicleParts as $vehParts)
		{
			if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
			{
				$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
				$vehstatements = $db->prepare($vehsqls); 	
				$vehstatements->execute();   
				$vehrows = $vehstatements->fetchAll();
				foreach($vehrows as $vehval)
				{
					$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
					$vehval['hours'] = $vehParts['hours'];
					$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
					$vehicleinventoryId[] = $vehval;
				}
			}
		}
		foreach($equipments as $equip)
		{
			foreach($equip as $equipdata)
			{
				
				if($equipdata['equipment'] != '')
				{
					$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
					
				}
				$equipParts[] = $equipdata;
				
			}
		}
		
		foreach($equipParts as $equParts)
		{
			if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
			{
				$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
				$eqstatements = $db->prepare($equsqls); 	
				$eqstatements->execute();   
				$eqrows = $eqstatements->fetchAll();
				
				foreach($eqrows as $eqval)
				{
					$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
					$eqval['hours'] = $equParts['hours'];
					$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
					$equipinventoryId[] = $eqval;
				}
			}
		}
	}
	if($tables == 'generatorservices')
	{
		$sql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$invoiceIds." and `work_sheet` ='generatorServices' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$row = $statement->fetchAll();
		
		
		
		foreach($row as $val)
		{
			$staffArray[]=$val['staff_id'];
			
		}
		
		
		foreach($staffArray as $val1)
		{
			$field = 'first_name,last_name,billrate,qb_id';
			$id = $val1;
			$sql="select * from users where id='".$id."'";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			$staffInfos[] = $result[0];
		}
		
		
		$sqls = "select parts as additional_info , oil,oil_filter,airfilter,battery,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorservices where `id`=".$invoiceIds;
		$statements = $db->prepare($sqls); 	
		$statements->execute();   
		$rows = $statements->fetchAll();
		$inventory = unserialize($rows[0]['additional_info']);
		$equipments = unserialize($rows[0]['equipments']);
		$vehicles = unserialize($rows[0]['vehicles']);
		$oils = json_decode($rows[0]['oil']);
		if($oils->name != '' && $oils->qty != '')
		{
			$oil = $oils;
		}
		
		$oilFilters = json_decode($rows[0]['oil_filter']);
		
		if($oilFilters->name != '' && $oilFilters->qty != '')
		{
			$oilFilter = $oilFilters;
		}
		
		$airFilters = json_decode($rows[0]['airfilter']);
		if($airFilters->name != '' && $airFilters->qty != '')
		{
			$airFilter = $airFilters;
		}
		$batterys = json_decode($rows[0]['battery']);
		if($batterys->name != '' && $batterys->qty != '')
		{
			$battery = $batterys;
		}
		$equipHrs = $rows[0]['hours'];
		$inventoryParts = array();
		foreach($inventory as $inv)
		{
			
			foreach($inv as $val3)
			{
				if($val3['cat'] != '')
				{
					if($val3['qty'] == '')
					{
						$val3['qty'] = 1;
					}
					$inventoryParts[] = $val3;
				}
				
			}
		}
		
		foreach($inventoryParts as $parts)
		{
			$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
			$statements1 = $db->prepare($sqls1); 	
			$statements1->execute();   
			$rows13 = $statements1->fetchAll();
			foreach($rows13 as $val13)
			{
				
				
				$val13['travel'] = $parts['travel'];
				$val13['cat'] = $parts['inventory'];
				$val13['catName'] = $parts['cat'];
				$val13['qty'] = $parts['qty'];
				$inventoryId2[] = $val13;
			}
			
		}
		foreach($vehicles as $veh)
		{
			foreach($veh as $vehdata)
			{
				
				if($vehdata['vehicle'] != '')
				{
					$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
					
				}
				$vehicleParts[] = $vehdata;
			}
		}
		foreach($vehicleParts as $vehParts)
		{
			if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
			{
				$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
				$vehstatements = $db->prepare($vehsqls); 	
				$vehstatements->execute();   
				$vehrows = $vehstatements->fetchAll();
				foreach($vehrows as $vehval)
				{
					$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
					$vehval['hours'] = $vehParts['hours'];
					$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
					$vehicleinventoryId[] = $vehval;
				}
			}
		}
		foreach($equipments as $equip)
		{
			foreach($equip as $equipdata)
			{
				
				if($equipdata['equipment'] != '')
				{
					$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
					
				}
				$equipParts[] = $equipdata;
				
			}
		}
		
		foreach($equipParts as $equParts)
		{
			if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
			{
				$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
				$eqstatements = $db->prepare($equsqls); 	
				$eqstatements->execute();   
				$eqrows = $eqstatements->fetchAll();
				
				foreach($eqrows as $eqval)
				{
					$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
					$eqval['hours'] = $equParts['hours'];
					$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
					$equipinventoryId[] = $eqval;
				}
			}
		}
		
	}
	
	if($tables == 'generatorstorages')
	{
		$sql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$invoiceIds." and `work_sheet` ='gen_win_storage' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$row = $statement->fetchAll();
		
		
		
		foreach($row as $val)
		{
			$staffArray[]=$val['staff_id'];
			
		}
		
		
		foreach($staffArray as $val1)
		{
			$field = 'first_name,last_name,billrate,qb_id';
			$id = $val1;
			$sql="select * from users where id='".$id."'";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			$staffInfos[] = $result[0];
		}
		
		$sqls = "select parts as additional_info , equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorstorages where `id`=".$invoiceIds;
		$statements = $db->prepare($sqls); 	
		$statements->execute();   
		$rows = $statements->fetchAll();
		$inventory = unserialize($rows[0]['additional_info']);
		
		$equipments = unserialize($rows[0]['equipments']);
		$vehicles = unserialize($rows[0]['vehicles']);
		
		$inventoryParts = array();
		$vehicleParts = array();
		foreach($inventory as $inv)
		{
			
			foreach($inv as $val3)
			{
				if($val3['cat'] != '')
				{
					if($val3['qty'] == '')
					{
						$val3['qty'] = 1;
					}
					$inventoryParts[] = $val3;
				}
				
			}
		}
		foreach($inventoryParts as $parts)
		{
			$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
			$statements1 = $db->prepare($sqls1); 	
			$statements1->execute();   
			$rows13 = $statements1->fetchAll();
			foreach($rows13 as $val13)
			{
				
				
				$val13['cat'] = $parts['inventory'];
				$val13['qty'] = $parts['qty'];
				$val13['catName'] = $parts['cat'];
				$inventoryId2[] = $val13;
			}
			
			
		}
		foreach($vehicles as $veh)
		{
			foreach($veh as $vehdata)
			{
				
				if($vehdata['vehicle'] != '')
				{
					$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
					
				}
				$vehicleParts[] = $vehdata;
			}
		}
		foreach($vehicleParts as $vehParts)
		{
			if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
			{
				$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
				$vehstatements = $db->prepare($vehsqls); 	
				$vehstatements->execute();   
				$vehrows = $vehstatements->fetchAll();
				foreach($vehrows as $vehval)
				{
					$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
					$vehval['hours'] = $vehParts['hours'];
					$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
					$vehicleinventoryId[] = $vehval;
				}
			}
		}
		foreach($equipments as $equip)
		{
			foreach($equip as $equipdata)
			{
				
				if($equipdata['equipment'] != '')
				{
					$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
					
				}
				$equipParts[] = $equipdata;
				
			}
		}
		
		foreach($equipParts as $equParts)
		{
			if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
			{
				$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
				$eqstatements = $db->prepare($equsqls); 	
				$eqstatements->execute();   
				$eqrows = $eqstatements->fetchAll();
				
				foreach($eqrows as $eqval)
				{
					$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
					$eqval['hours'] = $equParts['hours'];
					$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
					$equipinventoryId[] = $eqval;
				}
			}
		}
	}
	
	
	$sql1 = "SELECT * FROM customers where id=".$rows[0]['customer_id'];
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$cus_result2 = $statement1->fetchAll();
	//print_r($cus_result);die;
	
	
	$hours = $row[0]['total_hours'];
	$itemname = $row[0]['cat_name'];
	$cost = $hours*$user_result[0]['billrate'];
	
	
	$customerId = $cus_result2[0]['qb_id'];
	
	$email = $cus_result2[0]['email'];
	
	$electricalId = $rows[0]['id'];
	$invoiceId = $rows[0]['invoice_id'];
	$tablesname = $tables;
	$staffItemId = $user_result[0]['qb_id'];
	$inventoryParts = $inventoryPart;
	include('./qb/quickbooks-new3/updateInvoice.php');
	
}
function updateInvoiceId($invoiceId,$electricalId,$tablesname)
{
	
	
	$db = get_connection();
	$sql="Update $tablesname set invoice_id='".$invoiceId."' where id='".$electricalId."'";
	$statement = $db->prepare($sql);	
	$affec_row = $statement->execute();
	if($affec_row != 0)
	{
		  echo $invoiceId;
	}
	else
	{
		echo 0;
	}
}
function insertInventoryQbId($itemId,$inventoryName)
{
	$db = get_connection();
	$sql="INSERT INTO store_inventory_qb(inventory,qb_id) VALUES('".$inventoryName."','".$itemId."')";
	$statement = $db->prepare($sql);	
	$affec_row = $statement->execute();
	if($affec_row)
	{
		echo 1;
	}
	else
	{
		echo 0;
	}
	
}
function getuserName($qbId)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM users where qb_id=".$qbId;
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$user = $statement1->fetchAll();
	return $user;
}
function getInventoryItems($itemname)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM store_inventory_qb where inventory='".$itemname."'";
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$item = $statement1->fetchAll();
	return $item;
}
function getVehicleName($vehicle)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM vehicles where id='".$vehicle."'";
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$item = $statement1->fetchAll();
	return $item;
}
function getEquipmentName($equip)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM equipment where id='".$equip."'";
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$item = $statement1->fetchAll();
	return $item;
}
function addInvoiceQb($cus_result2,$invoiceId,$table,$inventoryArray)
{
	$invoiceId = $invoiceId;
	$table = $table;
	$inventoryArray = $inventoryArray;
	$cus_result2 = $cus_result2;
	include('./qb/quickbooks-new3/invoice.php');
}
function addInvQb($inventoryArray,$invoiceId,$table)
{
	include('./qb/quickbooks-new3/inventory.php');
}
function addInvntoryQb1($data,$vehicles,$equipments)
{
	$db = get_connection();
	$inventoryParts = array();
	$vehicleParts = array();
	$inventoryArray = array();
	
	foreach($data as $inv)
	{
		foreach($inv as $val3)
		{
			
			if($val3['cat'] != '')
			{
				if($val3['qty'] == '')
				{
					$val3['qty'] = 1;
				}
				$inventoryParts[] = $val3;
			}
			
		}
	}
		
	foreach($inventoryParts as $parts)
	{
		$val13['cat'] = $parts['inventory'];
		$val13['qty'] = $parts['qty'];
		$val13['catName'] = $parts['cat'];
		$inventory[] = $val13;
	}
	
	
	
	foreach($vehicles as $veh)
	{
		foreach($veh as $vehdata)
		{
			
			if($vehdata['vehicle'] != '')
			{
				$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
				
			}
			$vehicleParts[] = $vehdata;
		}
	}
	
	
	foreach($vehicleParts as $vehParts)
	{
		
		$vehval['name'] = $vehParts['vehiclename'][0]['vehicle'];
		$vehval['hours'] = $vehParts['hours'];
		$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
		$inventoryArray[] = $vehval;
	}
	
	
	foreach($equipments as $equip)
	{
		foreach($equip as $equipdata)
		{
			
			if($equipdata['equipment'] != '')
			{
				$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
				
			}
			$equipParts[] = $equipdata;
			
		}
	}
	
	foreach($equipParts as $equParts)
	{
		$eqval['name'] = $equParts['equipmentname'][0]['equipment'];
		$eqval['hours'] = $equParts['hours'];
		$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
		$inventoryArray[] = $eqval;
	}
	
	
	
	define('PROJECT_ROOT', dirname(dirname(__FILE__)));
	require(PROJECT_ROOT .'/qb/quickbooks-new3/inventory.php');
}
function addCustomerQb1($customer_id)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM customers where id=".$customer_id;
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$cus_result2 = $statement1->fetchAll();
	define('PROJECT_ROOT', dirname(dirname(__FILE__)));
	require(PROJECT_ROOT .'/qb/quickbooks-new3/invoice.php');
}
function addStaffQb1($staff_id)
{
	$db = get_connection();
	foreach($staff_id as $val1)
	{
		
		$field = 'first_name,last_name,billrate,qb_id';
		$id = $val1;
		$sql="select * from users where id='".$val1."'";
		$statement = $db->prepare($sql);	
		$statement->execute(); 
		$result = $statement->fetchAll();
		$staffInfos[] = $result[0];
	}
	
	define('PROJECT_ROOT', dirname(dirname(__FILE__)));
	require(PROJECT_ROOT .'/qb/quickbooks-new3/product.php');
	
}




?>