 </div>
<div id="selectres" style="display:none"><?php getCategories()?></div>
<!-- end left bar -->
<!-- footer -->
<footer class="footer"> <p>Developed by <a href="https://creativeone.ca/" target="_blank">CREATIVE ONE®</a></p></footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 	 
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery-ui.js"></script>
	
	
	<script type="text/javascript">
		//pk jquery
	 jQuery(document).ready(function() {
		 jQuery(".btnSliderMenu").click(function() {
			 if (jQuery("#sliderMenu").hasClass("open")) {
				 jQuery("#sliderMenu").removeClass("open");
				 jQuery("#btnSliderMenu").removeClass("btnClose");
			 } else {
				 jQuery("#sliderMenu").addClass("open");
				 jQuery("#btnSliderMenu").addClass("btnClose");
			 }
		 });
	 });
	
	 if (jQuery(window).width() < 767) {
		 jQuery("table").wrap('<div class="responsive-tbl"></div>');
		 jQuery("li.has-dd>a").append('<i class="fa fa-angle-down"></i>');
		 jQuery("li.has-dd").children('ul').hide();
		 jQuery("li.has-dd").click(function(e) {
			 jQuery(this).toggleClass('opened');
			 jQuery(this).children('ul').show().toggleClass('pk');
		 });
	 };
    </script>

	<script src="assets/js/script.js"></script>	
	<script src="assets/js/jquery.validate.min.js"></script>
	<!--<script src="assets/js/jquery.dataTables.js"></script>-->
	<script type="text/javascript" language="javascript" src="assets/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://morrowelectric.pro/assets/js/dataTables.editor.min.js">
	</script>
	<script src="assets/js/form.js"></script> 
	<script>
	$(document).ready(function(){
	var i = 1;
	var catList = $("#selectres").html();
	//var abc="<option>AIR FILTER</option>";
	
		$(document).on('click','.addCF',function(){

		$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" name="data[parts][row' + i + '][qty]" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[parts][row' + i + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[parts][row' + i + '][inventory]"><div class="inventory"></div></div><div class="add-more valign addCF"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div><div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		i++;
	});
	
	$(document.body).on('click', '.editAddmore', function() {
		$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" name="data[parts][row' + i + '][qty]" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[parts][row' + i + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[parts][row' + i + '][inventory]"><div class="inventory"></div></div><div class="add-more addCF"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		i++;
	});
	
	$(document.body).on('click', '.editAddmore_serv', function() {
		$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" name="data[com][parts][row' + i + '][qty]" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[com][parts][row' + i + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[com][parts][row' + i + '][inventory]"><div class="inventory"></div></div><div class="add-more"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		i++;
	});
	
	$(document.body).on('keyup', '.inventory-list', function() {
		var nextDiv = $(this).next();
		searchText = $(this).val();
		var catname = $(this).parent().prev().children().val()
		$.ajax({
			type: "POST",
			url: "handler.php",
			data: "catname=" + catname + "&searchText=" + searchText + "&action=inventoryFilter",
			success: function(result) {
				if (searchText == "")
					nextDiv.html("")
				else
					nextDiv.html(result)
	
			},
			error: function(e) {
				console.log(e);
			}
		});
	
	
	});
	
	$(document.body).on('click', '.inv_result .inv_row', function() {
		$(this).parent().parent().prev().val($(this).html())
		$(".inv_result").hide()
	})
	
	$(document).on('click','.remCF',function(){
	
		$(this).parent().parent().remove();
	});
	
	$(document.body).on('click', '.close-edit', function() {
		$("#editFromToggle").slideUp("slow");
	});
	
	
	setTimeout(function() {
		$("#result").remove();
	}, 3000);
	
	$(window).load(function() {
		$('#preloader').fadeOut('slow', function() {
			$(this).remove();
		});
	});
	
	$(".notification-close").click(function() {
	$(".alert-normal").slideUp('slow')
	})
	});
	
	$(document.body).on('change', '#generator-service input[type=radio]', function() {
		var radioVal = $('input[name=service_type]:checked', '#generator-service').val();
	
		//alert(radioVal)
		if (radioVal == 'basic') {
			$(".hrs label").html("Basic +")
			$(".hrs input").addClass("basic")
			$(".hrs input").removeClass("intermediate")
			$(".hrs input").removeClass("full")
		} else if (radioVal == 'intermediate') {
			$(".hrs label").html("Intermediate +")
			$(".hrs input").addClass("intermediate")
			$(".hrs input").removeClass("full")
			$(".hrs input").removeClass("basic")
		} else {
			$(".hrs label").html("Full +")
			$(".hrs input").addClass("full")
			$(".hrs input").removeClass("intermediate")
			$(".hrs input").removeClass("basic")
		}
	});
	
	
	
	$(document.body).on('focus', 'input', function() {
		$(this).removeClass("is-blank");
		$(this).addClass("is-focus");
	
	})
	
	$(document.body).on('focus', '.notes', function() {
		$(this).removeClass("is-blank");
		$(this).addClass("is-focus");
	})
	
	
	$(document.body).on('blur', 'input', function() {
		// check if the input has any value (if we've typed into it)  
	
		if ($(this).val() != '') {
			$(this).removeClass('is-focus');
			$(this).addClass('is-filled');
		} else {
			$(this).removeClass('is-filled');
			$(this).removeClass('is-focus');
			$(this).addClass('is-blank');
			//$(this).addClass('is-outfocus');
		}
	});
	$(document.body).on('blur', '.notes', function() {
	
		// check if the input has any value (if we've typed into it)  
	
		if ($(this).val() != '') {
			$(this).removeClass('is-focus');
			$(this).addClass('is-filled');
		} else {
			$(this).removeClass('is-filled');
			$(this).removeClass('is-focus');
			$(this).addClass('is-blank');
			//$(this).addClass('is-outfocus');
		}
	});
	
	$(document.body).on('keyup', '#searchname', function() {
		var searchtext = $('#searchname').val();
	
		if (searchtext != "") {
			$("#user_list").html('<div class="sp-spinner"><i class="fa fa-spin blue fa-spinner"></i></div>');
	
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: "action=ajaxList&searchtext=" + searchtext,
				success: function(result) {
					$("#user_list").html(result);
	
				},
				error: function(e) {
					console.log(e);
				}
			});
		} else {
			$("#user_list").html("");
		}
	
	})
	
	
	$(document.body).on("click", ".view-btn", function() {
	
		$(".datatable tr").removeClass("open")
		$(this).parent().parent().addClass("open")
	})
	
	
	//jQuery(".invoiced-no").click(function(){
	$(document.body).on("click", ".invoiced-no", function() {
	
		var id = jQuery(this).attr('id')
		var value = jQuery(this).val();
		var table = jQuery(this).attr('u');
		jQuery(this).html('<i class="fa fa-spin blue fa-spinner"></i>')
		var currentDiv = jQuery(this);
		var nxtDiv = jQuery(this).next().next();
		var prevDiv = jQuery(this).prev().prev();
		if (jQuery(".invoiced-no i").hasClass("fa-check-circle-o")) {
			//alert(111)
		} else {
			//alert(3333)
		}
	
		jQuery.ajax({
			type: "POST",
			url: "../handler.php",
			data: "id=" + id + "&action=updateInvoice&table=" + table,
			success: function(result) {
	
				if (result == 0) {
	
	
					currentDiv.hide();
					prevDiv.html('<i class="fa blue fa-check-circle-o"></i>');
					prevDiv.show();
	
				} else {
					currentDiv.hide();
					nxtDiv.html('<i class="fa blue fa-check-circle"></i>')
					nxtDiv.show();
	
					// window.location=link; 
				}
			},
			error: function(e) {
				console.log(e);
			}
	
	
		})
	})
	jQuery(document).on('click','.printed-no',function(){
				var id = jQuery(this).attr('id')
				var update_printed = jQuery(this).attr('update_printed')
				var table = jQuery(this).attr('u');
					jQuery(this).html('<i class="fa fa-spin blue fa-spinner"></i>')
					var currentDiv = jQuery(this);
					var nxtDiv = jQuery(this).next().next();
					
					var prevDiv = jQuery(this).prev().prev();
					if (jQuery(".printed-no i").hasClass("fa-check-circle-o")) {
						//alert(111)
					} else {
						//alert(3333)
					}
					jQuery.ajax({
					type: "POST",
					url: "../handler.php",
					data: "id=" + id + "&action=updatePrinted&update_printed=" + update_printed + "&table=" + table,
					success: function(result) {
		
						if (result == 0) {
						
							currentDiv.hide();
							prevDiv.html('<i class="fa gray fa-check-circle-o"></i>');
							prevDiv.show();
		
						} else {
							currentDiv.hide();
							nxtDiv.html('<i class="fa gray fa-check-circle"></i>')
							nxtDiv.show();
						}
					},
					error: function(e) {
						console.log(e);
					}
		
		
				})
			});
	$('.display tbody').on('click', '.edit-btn', function() {
	
		$('html, body').animate({
			scrollTop: 0
		}, 'slow');
	
		var recId = $(this).attr("id");
		var table = $(this).attr("u");
		$.ajax({
			type: "POST",
			url: "handler.php",
			data: "id=" + recId + "&action=SheetsEdit&table=" + table,
			success: function(result) {
	
				$("#editFromToggle").show();
				$("#editFromToggle").html(result);
	
	
				//$('#editFromToggle input:not([value!=""])').addClass('is-filled')
				$('#editFromToggle input').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
				$('#editFromToggle .notes').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
	
			},
			error: function(e) {
				console.log(e);
			}
		});
	
	
	});
		
	</script>
	
	
</body>
</html>
<!-- End footer -->