 </div>
<?php if($_GET['section'] != 'assign_service') { ?>
<div id="selectres" style="display:none"><?php getCategories()?></div>
<div id="equipmentsCat" style="display:none"><?php getEquipments()?></div>
<div id="vehiclesCat" style="display:none"><?php getVehicles()?></div>
<?php } ?>
<!-- end left bar -->
<!-- footer -->
<footer class="footer"> <p>Developed by <a href="https://creativeone.ca/" target="_blank">CREATIVE ONE®</a></p></footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 	 
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	
	
	<script type="text/javascript">
		//pk jquery
	 jQuery(document).ready(function() {
		 jQuery(".btnSliderMenu").click(function() {
			 if (jQuery("#sliderMenu").hasClass("open")) {
				 jQuery("#sliderMenu").removeClass("open");
				 jQuery("#btnSliderMenu").removeClass("btnClose");
			 } else {
				 jQuery("#sliderMenu").addClass("open");
				 jQuery("#btnSliderMenu").addClass("btnClose");
			 }
		 });
	 });
	
	 if (jQuery(window).width() < 767) {
		 jQuery("table").wrap('<div class="responsive-tbl"></div>');
		 jQuery("li.has-dd>a").append('<i class="fa fa-angle-down"></i>');
		 jQuery("li.has-dd").children('ul').hide();
		 jQuery("li.has-dd").click(function(e) {
			 jQuery(this).toggleClass('opened');
			 jQuery(this).children('ul').show().toggleClass('pk');
		 });
	 };
    </script>

	<script src="assets/js/script.js"></script>	
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
	<!--<script src="assets/js/jquery.dataTables.js"></script>-->
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="assets/js/dataTables.editor.min.js">
	</script>
	<script src="assets/js/form.js"></script> 
	<script>
	<?php 
		if($_GET['section'] != 'assign_service') 
		{ 
	?>
			jQuery(document).on('change','#equiphours',function(){
				if($(this).val() != '')
				{
					if($(this).closest('.row').find('.equipmentRate').val() != '')
					{
						var hour = $(this).val();
						var rate = $(this).closest('.row').find('.equipmentRate').val();
						var totalRate = hour*rate;
						$(this).closest('.row').find('#rate').val(totalRate);
						$(this).closest('.row').find('#rate').addClass('is-focus');
						$(this).closest('.row').find('#rate').addClass('is-blank');
					}
					else
					{
						$(this).closest('.row').find('#rate').val('');
						$(this).closest('.row').find('#rate').removeClass('is-focus');
						$(this).closest('.row').find('#rate').addClass('is-blank');
					}
				}
				else
				{
					$(this).closest('.row').find('#rate').val('');
					$(this).closest('.row').find('#rate').removeClass('is-focus');
					$(this).closest('.row').find('#rate').addClass('is-blank');
				}
				
			});
	
	jQuery(document).on('change','#equipmentRate',function(){
		if($(this).val() != '')
		{
			
			$(this).closest('.row').find('#equiphours').prop("disabled",false);
		}
		else
		{
			$(this).closest('.row').find('#equiphours').val('');
			$(this).closest('.row').find('#equiphours').prop("disabled",true);
		}
		
	});
	
	

	jQuery(document).on('change','#vehiclehours',function(){
		if($(this).val() != '')
		{
			if($(this).closest('.row').find('.vehicleRate').val() != '')
			{
				var hour = $(this).val();
				var rate = $(this).closest('.row').find('.vehicleRate').val();
				var totalRate = hour*rate;
				$(this).closest('.row').find('#vehiclerate').val(totalRate);
				$(this).closest('.row').find('#vehiclerate').addClass('is-focus');
				$(this).closest('.row').find('#vehiclerate').removeClass('is-blank');
			}
			else
			{
				$(this).closest('.row').find('#vehiclerate').val('');
				$(this).closest('.row').find('#vehiclerate').removeClass('is-focus');
				$(this).closest('.row').find('#vehiclerate').addClass('is-blank');
			}
		}
		else
		{
			$(this).closest('.row').find('#vehiclerate').val('');
			$(this).closest('.row').find('#vehiclerate').removeClass('is-focus');
			$(this).closest('.row').find('#vehiclerate').addClass('is-blank');
		}
		
	});
	
	
	jQuery(document).on('change','#vehicleRate',function(){
		if($(this).val() != '')
		{
			
			$(this).closest('.row').find('#vehiclehours').prop("disabled",false);
			
		}
		else
		{
			$(this).closest('.row').find('#vehiclehours').val('');
			$(this).closest('.row').find('#vehiclehours').prop("disabled",true);
		}
		
	});
	
	<?php } ?>
	
	
	
	$(document).ready(function(){
	<?php if($_GET['section'] != 'assign_service') { ?>
	var i = 1;
	var catList = $("#selectres").html();
	var equipList = $("#equipmentsCat").html();
	var vehList = $("#vehiclesCat").html();
	//var abc="<option>AIR FILTER</option>";
	
		$(document).on('click','.addEquipment',function(){
			var eupCount = $('.eupCount').val();
		
			if(eupCount != '')
			{
				var eqi = parseInt(eupCount);
				$('.eupCount').val(eqi+1);
			}
			++eqi;
			$(".equipmentFields").append('<div class="row"><div class="col-sm-8"><input type="hidden" class="equipmentRate"><select id="equipmentRate" name="equip[parts][row_' + eqi + '][equipment]" class=""><option value="">Select Equipment</option>' + equipList + '</select></div><div class="col-sm-3"><div class="group"><input name="equip[parts][row_' + eqi + '][hours]" id="equiphours" type="text"  class="is-blank validNumber" disabled><label class="im-label">Hours </label><label id="uerror"></label></div></div><div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		});
		$(document).on('click','.addVehicles',function(){
			var vehCount = $('.vehCount').val();
		
			if(vehCount != '')
			{
				var vehi = parseInt(vehCount);
				$('.vehCount').val(vehi+1);
				
			}
			
			++vehi;
			$(".vehicleField").append('<div class="row"><div class="col-sm-8"><input type="hidden" class="vehicleRate"><select id="vehicleRate" name="vehicle[parts][row_' + vehi + '][vehicle]" class=""><option value="">Select Vehicles</option>' + vehList + '</select></div><div class="col-sm-3"><div class="group"><input name="vehicle[parts][row_'+ vehi + '][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" disabled><label class="im-label">Travel </label><label id="uerror"></label></div></div><div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
			
		});
		var i3 = 1;
		$(document).on('click','.addCF',function(){

			$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" class="validNumber" name="data[parts][row_' + i3 + '][qty]" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[parts][row_' + i3 + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[parts][row_' + i3 + '][inventory]"><div class="inventory"></div></div><div class="add-more valign addCF"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div><div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
			i3++;
		});
	
	$(document.body).on('click', '.editAddmore', function() {
		
		$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" class="validNumber" name="data[parts][row' + i + '][qty]" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[parts][row_' + i + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[parts][row' + i + '][inventory]"><div class="inventory"></div></div><div class="add-more addCF"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		
	});
	
	$(document.body).on('click', '.editAddmore_serv', function() {
		var invCount = $('.invCount').val();
		
		if(invCount != '')
		{
			var invi = parseInt(invCount);
			$('.invCount').val(invi+1);
		}
		++invi;
		$("#customFields").append('<div class="row"><div class="col-sm-2 col-xs-3"><input type="number" name="data[parts][row_' + invi + '][qty]" class="validNumber" placeholder="Qty"></div><div class="col-sm-4 col-xs-4"><select name="data[parts][row_' + invi + '][cat]"><option value="">Select Category</option>' + catList + '</select></div><div class="col-sm-5 col-xs-4"><input type="text" placeholder="Inventory" class="inventory-list"   name="data[parts][row_' + invi + '][inventory]"><div class="inventory"></div></div><div class="add-more"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div></div>');
		
	});
	<?php } ?>
	$(document.body).on('keyup', '.inventory-list', function() {
		var nextDiv = $(this).next();
		searchText = $(this).val();
		var catname = $(this).parent().prev().children().val()
		$.ajax({
			type: "POST",
			url: "handler.php",
			data: "catname=" + catname + "&searchText=" + searchText + "&action=inventoryFilter",
			success: function(result) {
				if (searchText == "")
					nextDiv.html("")
				else
					nextDiv.html(result)
	
			},
			error: function(e) {
				console.log(e);
			}
		});
	
	
	});
	
	$(document.body).on('click', '.inv_result .inv_row', function() {
		$(this).parent().parent().prev().val($(this).html())
		$(".inv_result").hide()
	})
	
	$(document).on('click','.remCF',function(){
	
		$(this).parent().parent().remove();
	});
	
	$(document.body).on('click', '.close-edit', function() {
		$("#editFromToggle").slideUp("slow");
	});
	
	
	setTimeout(function() {
		$("#result").remove();
	}, 3000);
	
	$(window).load(function() {
		$('#preloader').fadeOut('slow', function() {
			$(this).remove();
		});
	});
	
	$(".notification-close").click(function() {
	$(".alert-normal").slideUp('slow')
	})
	});
	
	
	<?php 
		if($_GET['section'] != 'assign_service') 
		{ 
	?>
	
			$(document.body).on('change', '#generator-service input[type=radio]', function() {
				var radioVal = $('input[name=service_type]:checked', '#generator-service').val();
			
				//alert(radioVal)
				if (radioVal == 'basic') {
					$(".hrs label").html("Basic +")
					$(".hrs input").addClass("basic")
					$(".hrs input").removeClass("intermediate")
					$(".hrs input").removeClass("full")
				} else if (radioVal == 'intermediate') {
					$(".hrs label").html("Intermediate +")
					$(".hrs input").addClass("intermediate")
					$(".hrs input").removeClass("full")
					$(".hrs input").removeClass("basic")
				} else {
					$(".hrs label").html("Full +")
					$(".hrs input").addClass("full")
					$(".hrs input").removeClass("intermediate")
					$(".hrs input").removeClass("basic")
				}
			});
	<?php } ?>
	
	
	
	$(document.body).on('focus', 'input', function() {
		$(this).removeClass("is-blank");
		$(this).addClass("is-focus");
	
	})
	
	$(document.body).on('focus', '.notes', function() {
		$(this).removeClass("is-blank");
		$(this).addClass("is-focus");
	})
	
	
	$(document.body).on('blur', 'input', function() {
		// check if the input has any value (if we've typed into it)  
	
		if ($(this).val() != '') {
			$(this).removeClass('is-focus');
			$(this).addClass('is-filled');
		} else {
			$(this).removeClass('is-filled');
			$(this).removeClass('is-focus');
			$(this).addClass('is-blank');
			//$(this).addClass('is-outfocus');
		}
	});
	$(document.body).on('blur', '.notes', function() {
	
		// check if the input has any value (if we've typed into it)  
	
		if ($(this).val() != '') {
			$(this).removeClass('is-focus');
			$(this).addClass('is-filled');
		} else {
			$(this).removeClass('is-filled');
			$(this).removeClass('is-focus');
			$(this).addClass('is-blank');
			//$(this).addClass('is-outfocus');
		}
	});
	
	<?php 
		if($_GET['section'] != 'assign_service') 
		{ 
	?>
	
	$(document.body).on('keyup', '#searchname', function() {
		var searchtext = $('#searchname').val();
	
		if (searchtext != "") {
			$("#user_list").html('<div class="sp-spinner"><i class="fa fa-spin blue fa-spinner"></i></div>');
	
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: "action=ajaxList&searchtext=" + searchtext,
				success: function(result) {
					$("#user_list").html(result);
	
				},
				error: function(e) {
					console.log(e);
				}
			});
		} else {
			$("#user_list").html("");
		}
	
	})
	
	<?php } ?>
	
	
	$(document.body).on("click", ".view-btn", function() {
	
		$(".datatable tr").removeClass("open")
		$(this).parent().parent().addClass("open")
	})
	
	<?php 
		if($_GET['section'] != 'assign_service') 
		{ 
	?>
	
		$(document.body).on("click", ".invoiced-no", function() {
			var updateClass = $(this).attr('data-attr');
			
			var id = jQuery(this).attr('id')
			var value = jQuery(this).val();
			var table = jQuery(this).attr('u');
			jQuery(this).html('<i class="fa fa-spin blue fa-spinner"></i>')
			var currentDiv = jQuery(this);
			var nxtDiv = jQuery(this).next().next();
			var prevDiv = jQuery(this).prev().prev();
			if (jQuery(".invoiced-no i").hasClass("fa-check-circle-o")) {
				//alert(111)
			} else {
				//alert(3333)
			}
		
			jQuery.ajax({
				type: "POST",
				url: "handler.php",
				data: "id=" + id + "&action=updateInvoice&table=" + table,
				success: function(result) {
					console.log(result);
		
					if (result == 0) {
		
		
						currentDiv.hide();
						prevDiv.html('<i class="fa gray fa-check-circle"></i>');
						prevDiv.show();
		
					} else {
						$('#'+updateClass).html(result);
						$('.'+updateClass).attr('invoice-id',result);
						currentDiv.hide();
						nxtDiv.html('<i class="fa blue fa-check-circle-o"></i>')
						nxtDiv.show();
		
						// window.location=link; 
					}
				},
				error: function(e) {
					console.log(e);
				}
		
		
			})
		})
	$(document.body).on("click", ".syn-invoice-data", function() {
		var updateClass = $(this).attr('data-attr');
		var id = jQuery(this).attr('id')
		jQuery(this).html('<i class="fa fa-spin blue fa-spinner"></i>')
		var currentDiv = jQuery(this);
		var table = jQuery(this).attr('u');
		var nxtDiv = jQuery(this).next().next();
		var prevDiv = jQuery(this).prev().prev();
		if (jQuery(".invoiced-no i").hasClass("fa-check-circle-o")) {
			//alert(111)
		} else {
			//alert(3333)
		}
	
		jQuery.ajax({
			type: "POST",
			url: "handler.php",
			data: "id=" + id + "&action=syncInventory&table=" + table,
			success: function(result) {
				console.log(result);
	
				if (result == 0) {
	
	
					// currentDiv.hide();
					// prevDiv.html('<i class="fa gray fa-check-circle"></i>');
					// prevDiv.show();
	
				} else {
					currentDiv.html('<i class="fa blue fa-check-circle"></i>');
					currentDiv.removeClass('syn-invoice-data');
					
	
					//window.location=link; 
				}
			},
			error: function(e) {
				console.log(e);
			}
	
	
		})
	})
	jQuery(document).on('click','.printed-no',function(){
				var id = jQuery(this).attr('id')
				var update_printed = jQuery(this).attr('update_printed')
				var table = jQuery(this).attr('u');
					jQuery(this).html('<i class="fa fa-spin blue fa-spinner"></i>')
					var currentDiv = jQuery(this);
					var nxtDiv = jQuery(this).next().next();
					
					var prevDiv = jQuery(this).prev().prev();
					if (jQuery(".printed-no i").hasClass("fa-check-circle-o")) {
						//alert(111)
					} else {
						//alert(3333)
					}
					jQuery.ajax({
					type: "POST",
					url: "../handler.php",
					data: "id=" + id + "&action=updatePrinted&update_printed=" + update_printed + "&table=" + table,
					success: function(result) {
		
						if (result == 0) {
						
							currentDiv.hide();
							prevDiv.html('<i class="fa gray fa-check-circle-o"></i>');
							prevDiv.show();
		
						} else {
							currentDiv.hide();
							nxtDiv.html('<i class="fa gray fa-check-circle"></i>')
							nxtDiv.show();
						}
					},
					error: function(e) {
						console.log(e);
					}
		
		
				})
			});
	
	$('.display tbody').on('click', '.edit-btn', function() {
	
		$('html, body').animate({ 
			scrollTop: 0
		}, 'slow');
	
		var recId = $(this).attr("id");
		var table = $(this).attr("u");
		var invoiceIds = $(this).attr('invoice-id');
		$.ajax({
			type: "POST",
			url: "handler.php",
			data: "id=" + recId + "&action=SheetsEdit&table=" + table,
			success: function(result) {
				
	
				$("#editFromToggle").show();
				$("#editFromToggle").html(result);
				if(invoiceIds != 0)
				{
					jQuery('.alert-warning-msg').css('display','block');
					jQuery('.alert-warning-msg').html('Caution: This worksheet has been invoiced in QuicksBooks. Changes will need to be updated manually in QuickBooks and will not automatically change the exiting invoice.');
				}
	
	
				//$('#editFromToggle input:not([value!=""])').addClass('is-filled')
				$('#editFromToggle input').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
				$('#editFromToggle .notes').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
	
			},
			error: function(e) {
				console.log(e);
			}
		});
	
	
	});
	
	<?php } ?>
	
	/*================Numeric validation  and dot with tab working====================*/

	jQuery(document).on('keypress', '.validNumber', function (eve) 
	{
		if (eve.which == 0) {
			return true;
		}
		else
		{
			if (eve.which == '.') 
			{
				eve.preventDefault();
			}
			if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});
		
	</script>
	
	
</body>
</html>
<!-- End footer -->