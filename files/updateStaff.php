<?php 
ob_start();
	require_once   '../setup.php';
	date_default_timezone_set("UTC"); 	
	$db = get_connection();
	
	extract($_REQUEST);
		
	
	//die;
	$time = gmdate( 'Y-m-d H:i:s' ); 
	
	$sql = "UPDATE users SET 
			first_name = :first_name, 
            last_name = :last_name,			
			email = :email,
			role = :role ,  
			status = :status,     
			billrate = :billrate,     
			payroll = :payroll     
            WHERE id = :id";
	    
	$statement = $db->prepare($sql);	
	
	$affec_row = $statement->execute(array(
			":first_name" => $first_name,
			":last_name" => $last_name,
			":email" => $email,	
			":role" => $role,
			":status" => $status,		
			":id" => $id,
			":billrate"=>$billrate,
			":payroll"=>$payroll
		));
	//echo $statement->debugDumpParams(); //debug query
		
	$url=SITE_URL;	
		
	if($affec_row != 0)  
	{
		 header("location:".$url."?section=staffList");
		//header('location: http://webmaster.iu.edu/'); 
		exit;

	}else
	{
		echo 0;
	}		


ob_flush();
?>