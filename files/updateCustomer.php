<?php 
ob_start();
	require_once   '../setup.php';
	date_default_timezone_set("UTC"); 	
	$db = get_connection();
	
	extract($_REQUEST);	
	
	$time = gmdate( 'Y-m-d H:i:s' ); 
	$sql = "UPDATE customers SET 
			company_name = :company_name,
			first_name = :first_name, 
            last_name = :last_name, 
            phone = :phone,  
            mobile = :mobile,  
            home_address = :home_address,
			cottage_address = :cottage_address,
			home_city = :home_city,
			home_province = :home_province,
			home_postal_code = :home_postal_code,
			email = :email,
			cottage_city = :cottage_city,
			cottage_province = :cottage_province,
			cottage_country= :cottage_country,
			cottage_postal_code = :cottage_postal_code,
			notes = :notes		
			  
            WHERE id = :id";
	    
	$statement = $db->prepare($sql);		
	
	$affec_row = $statement->execute(array(
			":company_name" => $company_name,
			":first_name" => $first_name,
			":last_name" => $last_name,
			":phone" => $phone,
			":mobile" => $mobile,
			":home_address" => $home_address,
			":cottage_address" => $cottage_address,
			":home_city" => $home_city,
			":home_province" => $home_province,
			":home_postal_code" => $home_postal_code,
			":email" => $email,
			":cottage_city" => $cottage_city,
			":cottage_province" => $cottage_province,
			":cottage_country" => $cottage_country,
			":cottage_postal_code" => $cottage_postal_code,
			":notes" => $notes	,
			":id" => $id	
					
		));
	//$statement->debugDumpParams(); //debug query
		
	$url=SITE_URL;	
		
	if($affec_row != 0)
	{
		 header("location:".$url."?section=customersList");
		//header('location: http://webmaster.iu.edu/'); 
		exit;

	}else
	{
		echo 0;
	}		


ob_flush();
?>