<?php
$date=$_GET['date'];

$today=date('Y-m-d');
$file_name='assign-for-service.pdf'; 

$sql="SELECT DISTINCT staff_id
FROM  `genassignservices`";       

$statement = $db->prepare($sql);	  
$statement->execute();	    
$result = $statement->fetchAll(); 
$count_1 = $statement->rowCount(); 
$html .= '

<head>
<style>

@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600,700");



body {
    font-family: "Open Sans", sans-serf;
	margin: 0;
	padding: 0;
}
p {
	margin:0;
}
h3{
  color:#000;
  font-family: "Open Sans", sans-serf;
  font-size:25px;	
 }
table{
  border-right:1px solid #666;	
    border-left:1px solid #666;	
  border-bottom:1px solid #666;
     border-collapse: collapse;
  } 
th, td{
	padding:10px;
font-family: "Open Sans", sans-serf;	
 }
th{
	background-color:#d9e2f3;
	border-left:1px solid #666;
	border-top:1px solid #666;	
 }
td{
		border-left:1px solid #666;
	border-top:1px solid #666;	
    font-size:14px;	
 }
caption img{
	marging-bottom:20px;	
 }
.logo{
	display:block;
	margin:auto;
	width:150px;	
 } 
td span {
	float:left;
	font-family: "Open Sans", sans-serf;
    margin: 5px 15px 0 0;
}  

</style>
</head>
<body>

<div style="margin:0 auto; width:700px;"> 
      <div class="logo">
	     <img src="http://portagepromo.ca/morrow-logo.png">
	  </div>';
	foreach($result as $row)	{
	$staff_id=$row['staff_id'];	
	$first_name=get_staff_info($staff_id,'first_name');
	$last_name=get_staff_info($staff_id,'last_name');
	$count=check_date_staff_report($date,$staff_id);
		if($count > 0)		
		{			
		$html .='<table border="0" style="margin:0 auto; text-align:left; width:100%;"> 
	 
			<caption><h3>Assign for Service  | '.$first_name.' '.$last_name.' </h3></caption>
			<tr> 
			 <th width="100" align="left">Date</th>
			 <th align="left">Customer</th>
			 <th align="left">Type</th> 
			 <th align="left">Generator Info</th> 
			</tr>'; 
				$sql1="SELECT * 
				FROM  `genassignservices` 
				WHERE  service_date =  '".$date."' and staff_id =  '".$staff_id."'";
				$statement1 = $db->prepare($sql1);	  
				$statement1->execute();	     
				$result1 = $statement1->fetchAll(); 
				foreach($result1 as $row1)  
				{
					$gen_id=$row1['gen_id'];
					$cust_id=get_gen_info($gen_id,'cust_id');
					$label=get_gen_info($gen_id,'label');
					
					$type=$row1['type']; 
					
					$cust_name=get_cust_info($cust_id,'company_name');
					$home_address=get_cust_info($cust_id,'home_address');
					$cottage_address=get_cust_info($cust_id,'cottage_address');
					$service_date=$row1['service_date']; 
					$service_date=date('M j, Y', strtotime($service_date));
					if($cottage_address!="")
					{
						$address=$cottage_address;
					}
					else
					{
						$address=$home_address;
					}	
					
					
					$oil_qty=get_cust_gen_info($gen_id,'oil_qty');
					$air_filter_id=get_cust_gen_info($gen_id,'air_filter'); 
					$oil_filter_id=get_cust_gen_info($gen_id,'oil_filter'); 
					
					
										
					
					$air=get_part_no($air_filter_id); 
					$oil=get_part_no($oil_filter_id);
						
					$html .='<tr>
					  <td width="90">'.$service_date.'</td>
					 <td>'.$label.'<br>'.$address.'</td>  
					 <td style="text-transform: capitalize;">'.$type.'</td> 
					 <td width="250" style="text-transform: capitalize;">
					<p><span>Oil Qty: '.$oil_qty.'</span> <span>OF: '.$oil.'</span> <span>  AF: '.$air.'</span> </p>';
					if($row1['notes']!="")
					{
						$html .='<p style="clear:both;">Notes : '.$row1['notes'].'</p>';
					}	
					   	
					
					$html .='</tr>';  
				}
		$html .='</table>'; 
			if($count_1 > 1)
			{
				$html .='<pagebreak>';
			}		
		}	
		
	} 
$html .='</div> 
</body>';  


 
//echo $html; 
//die;   

//==============================================================
//==============================================================
//==============================================================
include("pdf/mpdf.php");

$mpdf=new mPDF('c','A4');  

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
//$stylesheet = file_get_contents('mpdfstyletables.css');
//$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);

$mpdf->Output($file_name,'D');
exit;
?>