<?php 
require_once   'setup.php';
date_default_timezone_set('America/Los_Angeles');

function get_connection()
{
	$hostname = 'localhost';
	
	try {
			$dbh = new PDO("mysql:host=$hostname;dbname=".DB, DBUSER, DBPWD);			
			
			}
		catch(PDOException $e)
			{
			echo $e->getMessage();
			die;
			}
			return $dbh; 
}

function get_mysqlconnection()
{
	$hostname = 'localhost';
	$con=mysql_connect($hostname,DBUSER,DBPWD);	
	mysql_select_db(DB,$con);	
}



//get requested action
if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $action = $_REQUEST['action'];
   call_user_func($action);
}


//validate login credentials
function validateLogin()
{
	unset($_REQUEST['action']);

	$username = $_REQUEST['username'];
	$pwd = $_REQUEST['password'];
	$hash = md5(SALT .$pwd);
	
	$db = get_connection();
	
	//authenticate user 
	$sql= "SELECT * FROM users WHERE username = :username AND password = :password"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':username' => $username, ':password' => $hash));
	$row = $statement->fetch();

	if(!empty($row['id']))
	{
		if($row['status']==1)
		{
		$_SESSION['CREATED'] = time();
		$_SESSION['user'] = $username;
		$_SESSION['uid'] = $row['id'];
	  
		//set last logged in time
		$time = date( 'Y-m-d H:i:s' ); 
		$dt = new DateTime($time);
		$dt->setTimeZone(new DateTimeZone('EST'));
		$time=$dt->format('Y-m-d H:i:s'); 
		$statement2 = $db->prepare("UPDATE users SET 	last_logged_in = :time Where id = :id ");
		$statement2->execute(array(':time' => $time, ':id' =>  $row['id']));

	  //$statement2->debugDumpParams(); //debug query
	
	  echo 1;
	  }
	  else 
	   echo 2;
	 
	}
	else
	{
		echo 0;
	}
 
}

function uploadInventory()
{
	
	$db = get_mysqlconnection();
	
	$currentDir = getcwd();
	
	$fileExtensions = ['xlsx']; // Get all the file extensions

    $fileName = $_FILES['upload-inventory']['name'];
    $fileSize = $_FILES['upload-inventory']['size'];
    $fileTmpName  = $_FILES['upload-inventory']['tmp_name'];
    $fileType = $_FILES['upload-inventory']['type'];
    $fileExtension = strtolower(end(explode('.',$fileName)));

    $uploadPath = $currentDir .'/inventory.xlsx';
	$didUpload = move_uploaded_file($fileTmpName, $uploadPath);

	if ($didUpload) 
	{
		require 'readtextfile/Classes/PHPExcel.php';

		$inputfilename = $currentDir .'/inventory.xlsx';
		$exceldata = array();

		
		$inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
		$objReader = PHPExcel_IOFactory::createReader($inputfiletype);
		$objPHPExcel = $objReader->load($inputfilename);
		

		$sheet = $objPHPExcel->getSheet(0); 
		$highestRow = $sheet->getHighestRow(); 
		$highestColumn = $sheet->getHighestColumn();
		

		for ($row = 2; $row <= $highestRow; $row++)
		{ 
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			
			if(count(array_filter($rowData[0])) != 0)
			{
				$check= "select * from inventory where part_no='".$rowData[0][0]."' and category='".$rowData[0][2]."'"; 
				$result = mysql_query($check);
				$rowcount=mysql_num_rows($result);
				
				
				if($rowcount == 0)
				{
					$sql = "INSERT INTO inventory (part_no,description,category,cost,price)VALUES ('".$rowData[0][0]."', '".$rowData[0][1]."', '".$rowData[0][2]."', '".$rowData[0][3]."', '".$rowData[0][4]."')";
					$res=mysql_query($sql);
				}
				else
				{
					$updatesql = "Update inventory set part_no = '".$rowData[0][0]."',description = '".$rowData[0][1]."',category = '".$rowData[0][2]."',cost = '".$rowData[0][3]."',price = '".$rowData[0][4]."' where part_no = '".$rowData[0][0]."' and category = '".$rowData[0][2]."'";
					$res=mysql_query($updatesql);
				}
			}
		}
		echo 1;
	}
	else
	{
		echo 2;
	}
}

//Electrical Work
function addElectricalWork()
{
	
	unset($_REQUEST['action']);
	get_mysqlconnection();
	extract($_REQUEST);	
	$data=$_REQUEST['data'];
	$equip=$_REQUEST['equip'];
	$vehicle=$_REQUEST['vehicle'];
	$additional_info=serialize($data);	
	$equipments = serialize($equip);
	$vehicles = serialize($vehicle);
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$created_by=$_SESSION['uid'];	 
	
	$labour=mysql_real_escape_string($_REQUEST['labour']);
	$travel=mysql_real_escape_string($_REQUEST['travel']);
	$notes=mysql_real_escape_string($_REQUEST['notes']); 
	
	$po_number=mysql_real_escape_string($_REQUEST['po_number']); 
	   
	
	$hourss=$_REQUEST['hours'];
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet'];
	
	
	
	if(!empty($customer_id)) 
	{
		$sql="INSERT INTO electrical_work(customer_id,service_by,worksheet_type,cat_name,po_number,hours,details, labour,additional_info,equipments,vehicles,
		notes,created_by, created)
		VALUES($customer_id,'$service_by','$worksheet_type','$cat_name','$po_number','$hourss', '$details', '$labour', '$additional_info','$equipments','$vehicles','$notes','$created_by', '$time')";  
		
		$res=mysql_query($sql);   
		 
		

		if($res) 
		{	
			$last="select id from electrical_work where created_by='".$created_by."' order by id desc limit 0,1";
			$res_last=mysql_query($last);
			while($row=mysql_fetch_array($res_last)) 
			{
				$row_id=$row['id']; 
			}	
			
			$cr_date=date("Y-m-d H:i:s");
			
			$staff_idd=array_filter($staff_id);
					
			$i=0; 
			foreach($staff_idd as $val) 
			{
				$hr=$hours[$i];  
				if($hr=='')
				{
					$hourss=0;
				}
				else
				{
					$hourss=$hr;  
				}
			 
			
				$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
				$res_ins=mysql_query($ins); 
				$i++;  
			}
			
			addStaffQb1($staff_id);
			addCustomerQb1($customer_id);
			addInvntoryQb1($data,$vehicle,$equip);
			  
		}
		else
		{
			echo 0;
		}
	}		
	else 
	{
	echo 2;
	}
	
	
}
function addInvntoryQb($data,$vehicles,$equipments)
{
	
	
	$db = get_connection();
	$inventoryParts = array();
	$vehicleParts = array();
	$inventoryArray = array();
	foreach($data as $inv)
	{
		foreach($inv as $val3)
		{
			
			if($val3['cat'] != '')
			{
				if($val3['qty'] == '')
				{
					$val3['qty'] = 1;
				}
				$inventoryParts[] = $val3;
			}
			
		}
	}
		
	foreach($inventoryParts as $parts)
	{
		$val13['cat'] = $parts['inventory'];
		$val13['qty'] = $parts['qty'];
		$val13['catName'] = $parts['cat'];
		$inventory[] = $val13;
	}
	
	foreach($vehicles as $veh)
	{
		foreach($veh as $vehdata)
		{
			
			if($vehdata['vehicle'] != '')
			{
				$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
				
			}
			$vehicleParts[] = $vehdata;
		}
	}
	
	foreach($vehicleParts as $vehParts)
	{
		
		$vehval['name'] = $vehParts['vehiclename'][0]['vehicle'];
		$vehval['hours'] = $vehParts['hours'];
		$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
		$inventoryArray[] = $vehval;
	}
	
	
	foreach($equipments as $equip)
	{
		foreach($equip as $equipdata)
		{
			
			if($equipdata['equipment'] != '')
			{
				$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
				
			}
			$equipParts[] = $equipdata;
			
		}
	}
	
	foreach($equipParts as $equParts)
	{
		$eqval['name'] = $equParts['equipmentname'][0]['equipment'];
		$eqval['hours'] = $equParts['hours'];
		$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
		$inventoryArray[] = $eqval;
	}
	
	
	include('./qb/quickbooks-new3/inventory.php');
}
function addCustomerQb($customer_id)
{
	$db = get_connection();
	$sql1 = "SELECT * FROM customers where id=".$customer_id;
	$statement1 = $db->prepare($sql1); 	
	$statement1->execute();   
	$cus_result2 = $statement1->fetchAll();
	include('./qb/quickbooks-new3/invoice.php');
}
function addStaffQb($staff_id)
{
	$db = get_connection();
	foreach($staff_id as $val1)
	{
		$field = 'first_name,last_name,billrate,qb_id';
		$id = $val1;
		$sql="select * from users where id='".$val1."'";
		$statement = $db->prepare($sql);	
		$statement->execute(); 
		$result = $statement->fetchAll();
		$staffInfos[] = $result[0];
	}
	include('./qb/quickbooks-new3/product.php');
}

//End Electrical Work




//Generator Service
function addGeneratorService()
{
	unset($_REQUEST['action']);
	
	//echo '<pre>'; print_r($_REQUEST); echo '</pre>';
	
	extract($_REQUEST);	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s');  
	$created_by=$_SESSION['uid'];
	get_mysqlconnection();		
		 
	
	$service_typee=$_REQUEST['service_typee'];  
	$genrator=$_REQUEST['genrator'];  
	$customer_id=$_REQUEST['data1']['customer_id'];	
	$service_by=$_REQUEST['data1']['service_by'];
	
	$equip=$_REQUEST['equip'];
	$vehicle=$_REQUEST['vehicle'];
	$equipments = serialize($equip);
	$vehicles = serialize($vehicle);
	
	$service_type=$_REQUEST['service_type']; 
	
	$full_service = $_REQUEST['data1']['full_service'];
	$cat_name=$_REQUEST['data1']['cat_name'];	
	$po_number=mysql_real_escape_string($_REQUEST['data']['po_number']); 
	$equipment=$_REQUEST['equipment'];
	$generator_hours=$_REQUEST['data1']['generator_hours'];	
	$oil=json_encode($_REQUEST['data1']['oil']);
	$oil_recycling=$_REQUEST['data1']['oil_recycling'];		
	$oil_filter=json_encode($_REQUEST['data1']['oil_filter']);
	$antifreeze=$_REQUEST['data1']['antifreeze'];
	$antifreeze_disposal=$_REQUEST['data1']['antifreeze_disposal'];	
	$airfilter=json_encode($_REQUEST['data1']['airfilter']);	
	$battery=json_encode($_REQUEST['data1']['battery']);	
	$parts=serialize($_REQUEST['data']);	
	
	
	
	$travel=mysql_real_escape_string($_REQUEST['data1']['travel']);
	$notes=mysql_real_escape_string($_REQUEST['data1']['notes']);
	
	$hourss=$_REQUEST['hours'];
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet'];
	if(!empty($customer_id))
	{	
		if($service_type=="full")
		{
			$next_service="Inspection";
		}	
		if($service_type=="inspection") 
		{
			$next_service="Full";  
		}	
		
		$sql="INSERT INTO generatorservices(customer_id,service_by,cat_name,po_number, equipments,generator_hours, oil, oil_filter, oil_recycling, antifreeze, antifreeze_disposal, airfilter, battery, vehicles, parts, notes, service_type, full_service, created_by, created,next_service,service_typee) 
		VALUES($customer_id,'$service_by','$cat_name','$po_number', '$equipments','$generator_hours', '$oil', '$oil_filter', '$oil_recycling',  '$antifreeze', '$antifreeze_disposal',  '$airfilter', '$battery', '$vehicles' , '$parts', '$notes', '$service_type', '$full_service', '$created_by', '$time','$next_service','$service_typee')"; 
		
		$res=mysql_query($sql);  
			
		if($res)
		{
			$last="select id from generatorservices where created_by='".$created_by."' order by id desc limit 0,1";
			$res_last=mysql_query($last); 
			while($row=mysql_fetch_array($res_last)) 
			{
				$row_id=$row['id']; 
			}	
			
			$cr_date=date("Y-m-d H:i:s");
			
			$staff_idd=array_filter($staff_id);
					
			$i=0; 
			foreach($staff_idd as $val) 
			{
				$hr=$hours[$i];  
				if($hr=='')
				{
					$hourss=0;
				}
				else
				{
					$hourss=$hr;  
				}
			 
			
				$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
				$res_ins=mysql_query($ins);  
			$i++;  
			}
			
			$work_gen="insert into worksheet_genrator (work_id,work_sheet,gen_id,cr_date,status) values ('".$row_id."','".$work_sheet."','".$genrator."','".$cr_date."','1')"; 
			mysql_query($work_gen);    
			
			echo 1;
			addStaffQb1($staff_id);
			addCustomerQb1($customer_id);
			addInvntoryQb1($_REQUEST['data'],$vehicle,$equip);
			  
		}else
		{
			echo 0;
		}
	}
	else 
	{
		echo 2; 
	}
			

}
//End Generator Service


//Generator Inspection
function addGeneratorInspection()
{
	
	unset($_REQUEST['action']);	
	extract($_REQUEST);	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s');  
	 
	get_mysqlconnection();	
	$created_by=$_SESSION['uid'];		 
	$customer_id=$_REQUEST['data']['customer_id'];	
	$service_by=$_REQUEST['data']['service_by'];	
	$cat_name=$_REQUEST['data']['cat_name'];	
	$po_number=mysql_real_escape_string($_REQUEST['data']['po_number']);
	$generator_hours=json_encode($_REQUEST['data']['generator_hours']);	
	$oil=json_encode($_REQUEST['data']['oil']);	
	$oil_filter=json_encode($_REQUEST['data']['oil_filter']);	
	$antifreeze=$_REQUEST['data']['antifreeze'];	
	$air_filter_type=json_encode($_REQUEST['data']['air_filter_type']);	
	$battery=json_encode($_REQUEST['data']['bettery_type']);	
	$parts=serialize($_REQUEST['data']['com']); 
		
	//echo $travel1=$_REQUEST['data']['travel'];
	$labour=mysql_real_escape_string($_REQUEST['data']['labour']);	 
	$travel=mysql_real_escape_string($_REQUEST['data']['travel']);
	$notes=mysql_real_escape_string($_REQUEST['data']['notes']);	
	
	

	
	$data=$_REQUEST['data'];
	$additional_info=serialize($data);	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 	 
	$created_by=$_SESSION['uid'];
	
	
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet'];
	
	
	if(!empty($customer_id))
	{
	$sql="INSERT INTO generatorinspections(customer_id,service_by,cat_name,po_number,equipment,generator_hours, oil, oil_filter, antifreeze, air_filter_type, battery, travel, labour, parts, notes, created_by, created)
    VALUES($customer_id,'$service_by','$cat_name','$po_number','$equipment','$generator_hours', '$oil', '$oil_filter', '$antifreeze', '$air_filter_type', '$battery', '$travel', '$labour', '$parts', '$notes', '$created_by', '$time')";
	$res=mysql_query($sql);
		
	if($res)
	{
		
			$last="select id from generatorinspections where created_by='".$created_by."' order by id desc limit 0,1";
			$res_last=mysql_query($last);
			while($row=mysql_fetch_array($res_last)) 
			{
				$row_id=$row['id']; 
			}	
			
			$cr_date=date("Y-m-d H:i:s");
			
			$staff_idd=array_filter($staff_id);
					
			$i=0; 
			foreach($staff_idd as $val) 
			{
				$hr=$hours[$i];  
				if($hr=='')
				{
					$hourss=0;
				}
				else
				{
					$hourss=$hr;  
				}
			 
			
				$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
				$res_ins=mysql_query($ins);  
			$i++;  
			}
		
		
		echo 1;
		  
	}else
	{
		echo 0;
	}
	}
	else 
	echo 2;
}

//End Generator Inspection


//Generator Repair Estimate
function addGeneratorEstimate()
{
	unset($_REQUEST['action']);	
	extract($_REQUEST);	
	get_mysqlconnection();
	
	$airfilter=$_REQUEST['airfilter'];
	$airfilter=json_encode($airfilter);
	
	$equip=$_REQUEST['equip'];
	$vehicle=$_REQUEST['vehicle'];
	$equipments = serialize($equip);
	$vehicles = serialize($vehicle);
	
	$additional=$_REQUEST['data'];
	$parts=serialize($data);	
	$travel=mysql_real_escape_string($_REQUEST['travel']);
	$labour=mysql_real_escape_string($_REQUEST['labour']);	
	$notes=mysql_real_escape_string($_REQUEST['notes']);	
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 	 
	$created_by=$_SESSION['uid'];
	
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet'];
	$genrator=$_REQUEST['genrator']; 
	
	$po_number=mysql_real_escape_string($_REQUEST['po_number']); 
	
		
	if(!empty($customer_id))
	{
	$sql="INSERT INTO generatorestimates(customer_id,service_by,cat_name,po_number,equipments,generator_hours, antifreeze, airfilter, battery, vehicles, labour, parts, notes, created_by, created)
    VALUES($customer_id,'$service_by','$cat_name','$po_number','$equipments','$generator_hours', '$antifreeze', '$airfilter', '$battery', '$vehicles', '$labour', '$parts', '$notes', '$created_by', '$time')"; 
	$res=mysql_query($sql);
		
	if($res)
	{
		$last="select id from generatorestimates where created_by='".$created_by."' order by id desc limit 0,1";
		$res_last=mysql_query($last); 
		while($row=mysql_fetch_array($res_last)) 
		{ 
			$row_id=$row['id']; 
		}	
		
		$cr_date=date("Y-m-d H:i:s");
		
		
		$staff_idd=array_filter($staff_id);
				
		$i=0; 
		foreach($staff_idd as $val) 
		{
			$hr=$hours[$i];  
			if($hr=='')  
			{
				$hourss=0;
			}
			else
			{
				$hourss=$hr;  
			}
		 
		
			$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
			$res_ins=mysql_query($ins); 
		$i++;  
		}
		
		$work_gen="insert into worksheet_genrator (work_id,work_sheet,gen_id,cr_date,status) values ('".$row_id."','".$work_sheet."','".$genrator."','".$cr_date."','1')"; 
		mysql_query($work_gen);   
		
		
		echo 1;
		addStaffQb1($staff_id);
		addCustomerQb1($customer_id);
		addInvntoryQb1($_REQUEST['data'],$vehicle,$equip);
		  
	}else
	{
		echo 0;
	}
	}
	else 
	echo 2;		
}

//End Generator Repair Estimate


//Generator Storage
function addGeneratorStorage()
{
	get_mysqlconnection(); 
	unset($_REQUEST['action']);		
	extract($_REQUEST);	
	
	$travel=mysql_real_escape_string($_REQUEST['travel']);
	$labour=mysql_real_escape_string($_REQUEST['labour']);
	$notes=mysql_real_escape_string($_REQUEST['notes']);	
	
	$data=$_REQUEST['data'];	
	$parts=serialize($data);	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s');  
	$created_by=$_SESSION['uid'];
	
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet']; 
	$genrator=$_REQUEST['genrator']; 
	
	$equip=$_REQUEST['equip'];
	$vehicle=$_REQUEST['vehicle'];
	$equipments = serialize($equip);
	$vehicles = serialize($vehicle);
	
	$po_number=mysql_real_escape_string($_REQUEST['po_number']); 
	
	if(!empty($customer_id))
	{
	$sql="INSERT INTO generatorstorages(customer_id,service_by,cat_name,po_number,equipments,generator_hours,fogging_oil, battery_storage_charging, vehicles, labour, parts, notes, created_by, created)
    VALUES($customer_id,'$service_by','$cat_name','$po_number','$equipments','$generator_hours', '$fogging_oil', '$battery_storage_charging', '$vehicles', '$labour', '$parts', '$notes', '$created_by', '$time')";
	 
	$res=mysql_query($sql); 
		
	if($res)
	{
		$last="select id from generatorstorages where created_by='".$created_by."' order by id desc limit 0,1";
		$res_last=mysql_query($last);
		while($row=mysql_fetch_array($res_last))  
		{
			$row_id=$row['id'];   
		}	
		
		$cr_date=date("Y-m-d H:i:s");
		
		$staff_idd=array_filter($staff_id);
				
		$i=0; 
		foreach($staff_idd as $val) 
		{
			$hr=$hours[$i];  
			if($hr=='')
			{
				$hourss=0;
			}
			else
			{
				$hourss=$hr;  
			}
		 
		
			$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
			$res_ins=mysql_query($ins); 
		$i++;  
		}
		
		$work_gen="insert into worksheet_genrator (work_id,work_sheet,gen_id,cr_date,status) values ('".$row_id."','".$work_sheet."','".$genrator."','".$cr_date."','1')"; 
		mysql_query($work_gen);   
		
		echo 1;
		addStaffQb1($staff_id);
		addCustomerQb1($customer_id);
		addInvntoryQb1($_REQUEST['data'],$vehicle,$equip);
		  
	}else
	{
		echo 0;
	}
	}
	else 
	echo 2;		
}

//End Generator Storage


//Generator Trouble
function addGeneratorTrouble()
{
	get_mysqlconnection();
	unset($_REQUEST['action']);	
	//echo '<pre>'; print_r($_REQUEST); echo '</pre>';
	extract($_REQUEST);
	
	
	 
	//echo '<pre>'; print_r($_REQUEST); echo '</pre>';	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$created_by=$_SESSION['uid'];	
	$created_by=$_SESSION['uid'];		 
	$customer_id=$_REQUEST['data1']['customer_id'];	
	$service_by=$_REQUEST['data1']['service_by'];
	$service_type=$_REQUEST['service_type'];
	$cat_name=$_REQUEST['data1']['cat_name'];	
	$po_number=mysql_real_escape_string($_REQUEST['po_number']);
	$generator_hours=$_REQUEST['hours'];	
	$oil=json_encode($_REQUEST['data1']['oil']);
	$oil_recycling=$_REQUEST['data1']['oil_recycling'];		
	$oil_filter=json_encode($_REQUEST['data1']['oil_filter']);
	$antifreeze=$_REQUEST['data1']['antifreeze'];
	$antifreeze_disposal=$_REQUEST['data1']['antifreeze_disposal'];	
	$airfilter=json_encode($_REQUEST['data1']['airfilter']);	
	$battery=json_encode($_REQUEST['data1']['battery']);	
	$parts=serialize($_REQUEST['data']); 

	$notes=$_REQUEST['data1']['notes'];
	$labour=mysql_real_escape_string($_REQUEST['labour']);
	$diagnosis=mysql_real_escape_string($_REQUEST['data1']['diagnosis']);
	
	$staff_id=$_REQUEST['staff_id'];
	$hours=$_REQUEST['hour'];
	$work_sheet=$_REQUEST['work_sheet'];
	$genrator=$_REQUEST['genrator'];
	$trouble_status=$_REQUEST['trouble_status']; 

	$equip=$_REQUEST['equip'];
	$vehicle=$_REQUEST['vehicle'];
	$equipments = serialize($equip);
	$vehicles = serialize($vehicle);	
	
	if(!empty($customer_id)) 
	{	
	$sql="INSERT INTO generatortroubles(customer_id,service_by,cat_name,po_number,generator_hours, oil, oil_filter, antifreeze, airfilter, battery, vehicles, equipments,labour, parts, notes, created_by,created,trouble_status)
    VALUES($customer_id,'$service_by','$cat_name','$po_number','$generator_hours', '$oil', '$oil_filter','$antifreeze','$airfilter', '$battery', '$vehicles','$equipments' ,'$labour', '$parts',  '$diagnosis',  '$created_by', '$time','$trouble_status')";
	
	 
	
	$res=mysql_query($sql);
		
	if($res)
	{
		$last="select id from generatortroubles where created_by='".$created_by."' order by id desc limit 0,1";
		$res_last=mysql_query($last);
		while($row=mysql_fetch_array($res_last))  
		{
			$row_id=$row['id']; 
		}	
		
		$cr_date=date("Y-m-d H:i:s");
		
		$staff_idd=array_filter($staff_id);
				
		$i=0; 
		foreach($staff_idd as $val) 
		{
			$hr=$hours[$i];  
			if($hr=='')
			{
				$hourss=0;
			}
			else
			{
				$hourss=$hr;  
			}
		 
		
			$ins="insert into staff_timesheet (work_sheet,work_id,staff_id,hours,cr_date) Values ('".$work_sheet."','".$row_id."','".$val."','".$hourss."','".$cr_date."')";   
			$res_ins=mysql_query($ins);  
		$i++;  
		}
		
		$work_gen="insert into worksheet_genrator (work_id,work_sheet,gen_id,cr_date,status) values ('".$row_id."','".$work_sheet."','".$genrator."','".$cr_date."','1')"; 
		mysql_query($work_gen);   
		
		echo 1;
		addStaffQb1($staff_id);
		addCustomerQb1($customer_id);
		addInvntoryQb1($_REQUEST['data'],$vehicle,$equip);
		  
	}else
	{
		echo 0;
	}	
	}
	else 
	echo 2;
}

//End Generator Trouble



//Add Customer
function addCustomer()
{
	unset($_REQUEST['action']);
	$db = get_connection();
	//echo '<pre>'; print_r($_REQUEST); echo '</pre>';
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s');  
	 
	$statement = $db->prepare("INSERT INTO customers(company_name,first_name,last_name,phone, mobile, home_address,
	cottage_address, home_city, home_province, home_postal_code, email, cottage_city, cottage_province, cottage_postal_code, notes, created_by, created)
    VALUES(:company_name,:first_name, :last_name, :phone, :mobile, :home_address, :cottage_address, :home_city, :home_province,
	:home_postal_code, :email, :cottage_city, :cottage_province, :cottage_postal_code, :notes, :created_by, :created)");
		
	
	$affec_row = $statement->execute(array(
			":company_name" => $company_name,
			":first_name" => $first_name,
			":last_name" => $last_name,
			":phone" => $phone,
			":mobile" => $mobile,
			":home_address" => $home_address,
			":cottage_address" => $cottage_address,
			":home_city" => $home_city,
			":home_province" => $home_province,
			":home_postal_code" => $home_postal_code,
			":email" => $email,
			":cottage_city" => $cottage_city,
			":cottage_province" => $cottage_province,
			":cottage_postal_code" => $cottage_postal_code,
			":notes" => $notes,
			":created_by" => $_SESSION['uid'],
			":created" => $time
		));
	//$statement->debugDumpParams(); //debug query
		
		//die;
		
	if($affec_row != 0)
	{
		  //echo 1;
		$stmt = $db->prepare("...");
		$stmt->execute();
		$id = $db->lastInsertId();
			
		$sql= "SELECT * FROM customers WHERE id = $id"; 
		$statement = $db->prepare($sql);	
		$statement->execute(array('id' => $id));
		$row = $statement->fetch();
		include('./qb/quickbooks-new3/qb.php');
	}
	else
	{
		echo 0;
	}		
}

//End Add Customer





//Ajax search for Customer

	function ajaxList()
	{
	
	//$db = get_connection();	
	get_mysqlconnection();
	mysql_set_charset("UTF8");
	$cust_name= mysql_real_escape_string($_POST['searchtext']);  
	//$sql= "SELECT  * FROM  customers where  company_name LIKE '%".$cust_name."%' || first_name LIKE '%".$cust_name."%' ||  last_name LIKE '%".$cust_name."%' ";	
	$sql= "SELECT * FROM customers WHERE CONCAT(  `company_name` ,  ' ',  `first_name`,  ' ',  `last_name` ) LIKE  '%".$cust_name."%' ";	
	
	$result=mysql_query($sql);	
	echo "<div id='result'>";	
	while($row=mysql_fetch_assoc($result))
	{
		$first_name=$row['first_name'];
		$last_name=$row['last_name'];		
		$display_name=$first_name." ".$last_name;
		if(empty($first_name)){			
		$display_name=$row['company_name'];
		}			
	
		$fulladdress=$display_name." ".$row['home_address'].", ".$row['home_city']." ".$row['home_postal_code'];
		echo "<div class='curr_row' >".htmlentities($fulladdress)."</div><span class='asd' style='display:none' >".$row['id']."</span>";
	}
	echo "</div>";
	?>
	<script>
		$('#result .curr_row').click(function() { 
			var txt=$(this).next('.asd').html();
			$("#searchname").val($(this).text())
			$("#customer_id").val($(this).next().text())				
			$("#result").hide();
			jQuery.ajax({type: "POST",
			url: "handler_new.php", 
			data: "cust_id="+txt+"&action=CheckCustomerGenrator",  
			success:function(result)    
			{
				jQuery('.gen_sec').empty().append(result); 
							
			},
			error:function(e){
			console.log(e);
			}	
			});

			
		})
	</script>
	<?php
	
}
//End Customer List for generators

// Customer listing Ajax
function updateCustomer()
{
	unset($_REQUEST['action']);
	$db = get_connection();
	echo '<pre>'; print_r($_REQUEST); echo '</pre>';
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$sql = "UPDATE users SET 
			first_name = :first_name, 
            last_name = :last_name, 
            phone = :phone,  
            mobile = :mobile,  
            home_address = :home_address,
			cottage_address = :cottage_address,
			home_city = :home_city,
			home_province = :home_province,
			home_postal_code = :home_postal_code,
			email = :email,
			cottage_city = :cottage_city,
			cottage_province = :cottage_province,
			cottage_postal_code = :cottage_postal_code,
			notes = :notes		
			  
            WHERE id = :id";
	 
	$statement = $db->prepare($sql);		
	
	$affec_row = $statement->execute(array(
			":first_name" => $first_name,
			":last_name" => $last_name,
			":phone" => $phone,
			":mobile" => $mobile,
			":home_address" => $home_address,
			":cottage_address" => $cottage_address,
			":home_city" => $home_city,
			":home_province" => $home_province,
			":home_postal_code" => $home_postal_code,
			":email" => $email,
			":cottage_city" => $cottage_city,
			":cottage_province" => $cottage_province,
			":cottage_postal_code" => $cottage_postal_code,
			":notes" => $notes			
		));
	//$statement->debugDumpParams(); //debug query
		
		
		
	if($affec_row != 0)
	{
		  echo 1;
	}
	else
	{
		echo 0;
	}		

}
// end Customer list 

function addVehicle(){
	unset($_REQUEST['action']);
	$db = get_connection();	
	
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$vehicle = $_REQUEST['vehicle'];
	$rate = $_REQUEST['rate'];
	
	 
	$statement = $db->prepare("INSERT INTO vehicles(vehicle,rate,createddatetime)
    VALUES(:vehicle, :rate,:created)");	

	$affec_row = $statement->execute(array(
		":vehicle" => $vehicle,
		":rate" => $rate,
		":created"=>$time,
	));
	if($affec_row != 0)
	{
		echo 1;
		
	}
	else
	{
		echo 0;
	}		
}
function addEquipment(){
	unset($_REQUEST['action']);
	$db = get_connection();	
	
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$equipment = $_REQUEST['equipment'];
	$rate = $_REQUEST['rate'];
	
	 
	$statement = $db->prepare("INSERT INTO equipment(equipment,rate,createddatetime)
    VALUES(:equipment, :rate,:created)");	

	$affec_row = $statement->execute(array(
		":equipment" => $equipment,
		":rate" => $rate,
		":created"=>$time,
	));
	if($affec_row != 0)
	{
		echo 1;
		
	}
	else
	{
		echo 0;
	}		
}
function addStaff(){
	unset($_REQUEST['action']);
	$db = get_connection();	
	
	
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	$billrate = $_REQUEST['billrate'];
	$payroll = $_REQUEST['payroll'];
	
	 
	$statement = $db->prepare("INSERT INTO users(first_name,last_name,email,username,password,role,created,billrate,payroll)
    VALUES(:first_name, :last_name,:email,:username, :password,:role,:created,:billrate,:payroll)");	

	$affec_row = $statement->execute(array(
			":first_name" => $first_name,
			":last_name" => $last_name,
			":email" => $email,
			":username" => $username,			
			":password" => md5(SALT.$password),
			":role" => $role,
			":created"=>$time,
			":billrate"=>$billrate,
			":payroll"=>$payroll
		));
	//$statement->debugDumpParams(); //debug query
		
		
		
	if($affec_row != 0)
	{
		echo 1;
		
	}else
	{
		echo 0;
	}		
}
 

function addInventory(){ 
	unset($_REQUEST['action']);
	$db = get_connection();	
	
	
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	 
	 
	$statement = $db->prepare("INSERT INTO inventory(part_no,category,description,cost,price)
    VALUES(:part_no, :category,:description,:cost, :price)");	
	
	$affec_row = $statement->execute(array(
			":part_no" => $part_no,
			":category" => $category,
			":description" => $description,
			":cost" => $cost,			
			":price" => $price
			
		));
	//$statement->debugDumpParams(); //debug query
	
	if($affec_row != 0)
	{
	
	$stmt = $db->prepare("...");
	$stmt->execute();
	 $id = $db->lastInsertId();
	
	$sql= "SELECT * FROM inventory WHERE id = :id"; 
	$statement = $db->prepare($sql);	
	$statement->execute(array(':id' => $id));
	$row = $statement->fetch();		
	//print_r($row);
	echo json_encode($row);
	
		 
	}else
	{
		echo 0;
	}		
}
function addMaterial()
{ 
	unset($_REQUEST['action']);
	$db = get_connection();	
	
	extract($_REQUEST);
	
	$time = date( 'Y-m-d H:i:s' ); 
	$dt = new DateTime($time);
	$dt->setTimeZone(new DateTimeZone('EST'));
	$time=$dt->format('Y-m-d H:i:s'); 
	 
	$statement = $db->prepare("INSERT INTO material(part_no,category,title,price)
	VALUES(:part_no, :category,:title,:price)");	
	
	$affec_row = $statement->execute(array(
		":part_no" => $part_no,
		":category" => $category,
		":title" => $title,
		":price" => $price
	));
	if($affec_row != 0)
	{
		$stmt = $db->prepare("...");
		$stmt->execute();
		$id = $db->lastInsertId();	

		$sql= "SELECT * FROM material WHERE id = :id"; 
		$statement = $db->prepare($sql);	
		$statement->execute(array(':id' => $id));
		$row = $statement->fetch();		
		echo json_encode($row);
	}
	else
	{
		echo 0;
	}		
}




function emailExists(){
	//Email Exist or not
	$db = get_connection();		
	$email= $_POST['email']; 	
	
	$sql= "SELECT email FROM users WHERE email = :email"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':email' => $email));
	$row = $statement->fetch();		
	 if($statement->rowCount()==0)	
		echo 'true';
		else
		echo 'false'; 
		
}

function userExists(){
	//username Exist or not
	$db = get_connection();		
	$username= $_POST['username']; 	
	
	$sql= "SELECT email FROM users WHERE username = :username"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':username' => $username));
	$row = $statement->fetch();		
	 if($statement->rowCount()==0)	
		echo 'true';
		else
		echo 'false'; 
		
}

function changePass(){
	//Change Password not
	$db = get_connection();	
	
	$id=$_REQUEST['user_id'];
	$password=md5(SALT.$_REQUEST['old_password']);
	$new_password=md5(SALT.$_REQUEST['password']);
	
	
	$sql= "SELECT id,password FROM users WHERE id = :id and password=:password"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':id' => $id,':password' => $password));
	$row = $statement->fetch();	
	//echo "<pre>";	print_r($row); echo "</pre>";
	 if($statement->rowCount()==0)	
		echo '2';
		else
		{
		  $statement2 = $db->prepare("UPDATE users SET 	password = :password Where id = :id ");
     	 $affec_row= $statement2->execute(array(':password' => $new_password, ':id' =>  $id));
			 if($affec_row != 0)
			{
				  echo 1;
			}else
			{
				echo 0;
			}	
		}
	
}


function forgetPass(){
	//Forget Password not
	$db = get_connection();	
	extract($_REQUEST);
	$sql= "SELECT * FROM users WHERE email = :email"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':email' => $email));
	$row = $statement->fetch();	
	
	 if($statement->rowCount()==0)	
		echo 'You have entered wrong email ID';
		else
		{
		
		$email=$row['email'];	
		$id=$row['id'];
		$url=SITE_URL."?section=resetPass&email=".$email."&id=".$id;
		echo "<html><head>
			  <title>Sending HTML email using PHP</title>
		   </head>   
		   <body>";      
      
         $to = $email;
         $subject = "Reset Password";
         
         $message = "<b>You have requested to Reset you password. Please click below link to reset your password</b>";
         $message .= "<a href='$url'>click here</a>";
         
         $header = "From:abc@somedomain.com \r\n";
         //$header = "Cc:afgh@somedomain.com \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
         
         if( $retval == true ) {
            echo "Message sent successfully...";
         }else {
            echo "Message could not be sent...";
         }
      
      
 echo "</body></html>";
		  
		}
	
}


function resetPass(){
	//Forget Password not
		$db = get_connection();	
		extract($_REQUEST);
		$password=md5(SALT.$_REQUEST['password']);
		$statement2 = $db->prepare("UPDATE users SET 	password = :password Where id = :id and email = :email");
		$statement2->execute(array(':password' => $password, ':id' =>  $id, ':email' =>  $email));		
		echo 'Password changed Successfully.'; 		
	
}
function syncInventory()
{
	$db = get_connection();
	extract($_REQUEST);	
	$table=$_REQUEST['table'];
	$id=$_REQUEST['id'];
	if($table == 'electrical_work')
	{
		$sql = "SELECT staff_id FROM staff_timesheet where work_id='".$id."' AND work_sheet='elec_work' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$staff_id = $statement->fetchAll();
		
		foreach($staff_id as $staff)
		{
			$staffId[] = $staff['staff_id'];
		}
		
		$sql2 = "SELECT * FROM electrical_work where id='".$id."'";
		$statement2 = $db->prepare($sql2); 	
		$statement2->execute();   
		$data = $statement2->fetchAll();
		
		$inventory = unserialize($data[0]['additional_info']);
		$vehicle = unserialize($data[0]['vehicles']);
		$equipment = unserialize($data[0]['equipments']);
		
		echo 1;
		addStaffQb1($staffId);
		addCustomerQb1($data[0]['customer_id']);
		addInvntoryQb1($inventory,$vehicle,$equipment);
	}
	if($table == 'generatorestimates')
	{
		$sql = "SELECT staff_id FROM staff_timesheet where work_id='".$id."' AND work_sheet='gen_repair_est' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$staff_id = $statement->fetchAll();
		
		foreach($staff_id as $staff)
		{
			$staffId[] = $staff['staff_id'];
		}
		
		$sql2 = "SELECT * FROM generatorestimates where id='".$id."'";
		$statement2 = $db->prepare($sql2); 	
		$statement2->execute();   
		$data = $statement2->fetchAll();
		
		$inventory = unserialize($data[0]['parts']);
		$vehicle = unserialize($data[0]['vehicles']);
		$equipment = unserialize($data[0]['equipments']);
		
		
		echo 1;
		addStaffQb1($staffId);
		addCustomerQb1($data[0]['customer_id']);
		addInvntoryQb1($inventory,$vehicle,$equipment);
	}
	if($table == 'generatorservices')
	{
		$sql = "SELECT staff_id FROM staff_timesheet where work_id='".$id."' AND work_sheet='generatorServices' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$staff_id = $statement->fetchAll();
		
		foreach($staff_id as $staff)
		{
			$staffId[] = $staff['staff_id'];
		}
		
		$sql2 = "SELECT * FROM generatorservices where id='".$id."'";
		$statement2 = $db->prepare($sql2); 	
		$statement2->execute();   
		$data = $statement2->fetchAll();
		
		$inventory = unserialize($data[0]['parts']);
		$vehicle = unserialize($data[0]['vehicles']);
		$equipment = unserialize($data[0]['equipments']);
		
		
		echo 1;
		addStaffQb1($staffId);
		addCustomerQb1($data[0]['customer_id']);
		addInvntoryQb1($inventory,$vehicle,$equipment);
	}
	if($table == 'generatortroubles')
	{
		$sql = "SELECT staff_id FROM staff_timesheet where work_id='".$id."' AND work_sheet='gen_trouble' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$staff_id = $statement->fetchAll();
		
		foreach($staff_id as $staff)
		{
			$staffId[] = $staff['staff_id'];
		}
		
		$sql2 = "SELECT * FROM generatortroubles where id='".$id."'";
		$statement2 = $db->prepare($sql2); 	
		$statement2->execute();   
		$data = $statement2->fetchAll();
		
		$inventory = unserialize($data[0]['parts']);
		$vehicle = unserialize($data[0]['vehicles']);
		$equipment = unserialize($data[0]['equipments']);
		
		
		echo 1;
		addStaffQb1($staffId);
		addCustomerQb1($data[0]['customer_id']);
		addInvntoryQb1($inventory,$vehicle,$equipment);
	}
	if($table == 'generatorstorages')
	{
		$sql = "SELECT staff_id FROM staff_timesheet where work_id='".$id."' AND work_sheet='gen_win_storage' group by staff_id";
		$statement = $db->prepare($sql); 	
		$statement->execute();   
		$staff_id = $statement->fetchAll();
		
		foreach($staff_id as $staff)
		{
			$staffId[] = $staff['staff_id'];
		}
		
		$sql2 = "SELECT * FROM generatorstorages where id='".$id."'";
		$statement2 = $db->prepare($sql2); 	
		$statement2->execute();   
		$data = $statement2->fetchAll();
		
		$inventory = unserialize($data[0]['parts']);
		$vehicle = unserialize($data[0]['vehicles']);
		$equipment = unserialize($data[0]['equipments']);
		
		
		echo 1;
		addStaffQb1($staffId);
		addCustomerQb1($data[0]['customer_id']);
		addInvntoryQb1($inventory,$vehicle,$equipment);
	}
}

function updateInvoice(){

	//Forget Password not
		$db = get_connection();
		extract($_REQUEST);	
		$table=$_REQUEST['table'];
		$id=$_REQUEST['id'];
		
		
		$sql= "SELECT * FROM $table WHERE id = :id"; 
		$statement3 = $db->prepare($sql);
        $statement3->execute(array(':id' => $id));
		$row = $statement3->fetch();
	
		if($row['invoice']==0)
		{	
	
			$statement2 = $db->prepare("UPDATE $table SET 	invoice = :invoice Where id = :id");
			$affec_row=$statement2->execute(array(':invoice' => 1, ':id' =>  $id));		
		
		}
		if($row['invoice']==1)
		{	
			$statement2 = $db->prepare("UPDATE $table SET 	invoice = :invoice Where id = :id");
			$affec_row=$statement2->execute(array(':invoice' => 0, ':id' =>  $id));		
		}
		
		
		
		
			
		if($row['invoice']==0)
		{
			
			$data = $row;
			
			if($table == 'electrical_work')
			{
				
				
				$staffsql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$id." and `work_sheet` ='elec_work' group by staff_id";
				$staffstatement = $db->prepare($staffsql); 	
				$staffstatement->execute();   
				$staffrow = $staffstatement->fetchAll();
				
				
				
				foreach($staffrow as $staffval)
				{
					$staffArray[]=array('staffval'=>$staffval['staff_id'],'total_hours'=>$staffval['total_hours']);
					
				}
				
				
				foreach($staffArray as $staffval1)
				{
					
					$field = 'first_name,last_name,billrate,qb_id';
					$staffsid = $staffval1['staffval'];
					$staffsql1="select * from users where id='".$staffsid."'";
					$staffstatement1 = $db->prepare($staffsql1);	
					$staffstatement1->execute(); 
					$staffresult = $staffstatement1->fetchAll();
					$staffresult[0]['total_hours'] = $staffval1['total_hours'];
					$staffInfos[] = $staffresult[0];
				}
				
				
				$inventorysql = "select additional_info , equipments,hours,vehicles, id,customer_id,invoice_id from electrical_work where `id`=".$id;
				$inventorystatements = $db->prepare($inventorysql); 	
				$inventorystatements->execute();   
				$inventoryrows = $inventorystatements->fetchAll();
				
				$inventory = unserialize($inventoryrows[0]['additional_info']);
				
				$equipments = unserialize($inventoryrows[0]['equipments']);
				$vehicles = unserialize($inventoryrows[0]['vehicles']);
				
				
				$equipHrs = $inventoryrows[0]['hours'];
				$inventoryParts = array();
				$vehicleParts = array();
				
				foreach($inventory as $inv)
				{
					foreach($inv as $val3)
					{
						
						if($val3['qty'] == '')
						{
							$val3['qty'] = 1;
						}
						$inventoryParts[] = $val3;
					}
				}
				
			
				foreach($inventoryParts as $parts)
				{
					$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
					$statements1 = $db->prepare($sqls1); 	
					$statements1->execute();   
					$rows13 = $statements1->fetchAll();
					foreach($rows13 as $val13)
					{
						
						
						$val13['cat'] = $parts['inventory'];
						$val13['qty'] = $parts['qty'];
						$val13['catName'] = $parts['cat'];
						$inventoryId2[] = $val13;
					}
					
					
				}
				
				foreach($vehicles as $veh)
				{
					foreach($veh as $vehdata)
					{
						
						if($vehdata['vehicle'] != '')
						{
							$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
							
						}
						$vehicleParts[] = $vehdata;
					}
				}
				foreach($vehicleParts as $vehParts)
				{
					if($vehParts['vehicle'] != '')
					{
						$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
						$vehstatements = $db->prepare($vehsqls); 	
						$vehstatements->execute();   
						$vehrows = $vehstatements->fetchAll();
						foreach($vehrows as $vehval)
						{
							$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
							$vehval['hours'] = $vehParts['hours'];
							$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
							$vehicleinventoryId[] = $vehval;
						}
					}
				}
				foreach($equipments as $equip)
				{
					foreach($equip as $equipdata)
					{
						
						if($equipdata['equipment'] != '')
						{
							$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
							
						}
						$equipParts[] = $equipdata;
						
					}
				}
				
				foreach($equipParts as $equParts)
				{
					if($equParts['equipment'] != '')
					{
						$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
						$eqstatements = $db->prepare($equsqls); 	
						$eqstatements->execute();   
						$eqrows = $eqstatements->fetchAll();
						
						foreach($eqrows as $eqval)
						{
							$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
							$eqval['hours'] = $equParts['hours'];
							$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
							$equipinventoryId[] = $eqval;
						}
					}
				}
				
			}
			
			if($table == 'generatortroubles')
			{
				$staffsql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$id." and `work_sheet` ='gen_trouble' group by staff_id";
				$staffstatement = $db->prepare($staffsql); 	
				$staffstatement->execute();   
				$staffrow = $staffstatement->fetchAll();
				
				foreach($staffrow as $staffval)
				{
					$staffArray[]=array('staffval'=>$staffval['staff_id'],'total_hours'=>$staffval['total_hours']);
					
				}
				
				
				foreach($staffArray as $staffval1)
				{
					
					$field = 'first_name,last_name,billrate,qb_id';
					$staffsid = $staffval1['staffval'];
					$staffsql1="select * from users where id='".$staffsid."'";
					$staffstatement1 = $db->prepare($staffsql1);	
					$staffstatement1->execute(); 
					$staffresult = $staffstatement1->fetchAll();
					$staffresult[0]['total_hours'] = $staffval1['total_hours'];
					$staffInfos[] = $staffresult[0];
				}
				
				
				
				$inventorysql = "select parts as additional_info , oil,oil_filter,airfilter,battery,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatortroubles where `id`=".$id;
				$inventorystatements = $db->prepare($inventorysql); 	
				$inventorystatements->execute();   
				$inventoryrows = $inventorystatements->fetchAll();
				
				$inventory = unserialize($inventoryrows[0]['additional_info']);
				
				$equipments = unserialize($inventoryrows[0]['equipments']);
				$vehicles = unserialize($inventoryrows[0]['vehicles']);
				
				$oils = json_decode($inventoryrows[0]['oil']);
				if($oils->name != '' && $oils->qty != '')
				{
					$oil = $oils;
				}
				$generatorHours = $inventoryrows[0]['hours'];
				$oilFilters = json_decode($inventoryrows[0]['oil_filter']);
				if($oilFilters->name != '' && $oilFilters->qty != '')
				{
					$oilFilter = $oilFilters;
				}
				
				$airFilters = json_decode($inventoryrows[0]['airfilter']);
				if($airFilters->name != '' && $airFilters->qty != '')
				{
					$airFilter = $airFilters;
				}
				
				$batterys = json_decode($inventoryrows[0]['battery']);
				if($batterys->name != '' && $batterys->qty != '')
				{
					$battery = $batterys;
				}
						
				$inventoryParts = array();
				$vehicleParts = array();
				foreach($inventory as $inv)
				{
					foreach($inv as $val3)
					{
						
						if($val3['qty'] == '')
						{
							$val3['qty'] = 1;
						}
						$inventoryParts[] = $val3;
					}
				}
				foreach($inventoryParts as $parts)
				{
					$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
					$statements1 = $db->prepare($sqls1); 	
					$statements1->execute();   
					$rows13 = $statements1->fetchAll();
					foreach($rows13 as $val13)
					{
						
						
						$val13['travel'] = $parts['travel'];
						$val13['cat'] = $parts['inventory'];
						$val13['catName'] = $parts['cat'];
						$val13['qty'] = $parts['qty'];
						$inventoryId2[] = $val13;
					}
					
					
				}
				foreach($vehicles as $veh)
				{
					foreach($veh as $vehdata)
					{
						
						if($vehdata['vehicle'] != '')
						{
							$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
							
						}
						$vehicleParts[] = $vehdata;
					}
				}
				foreach($vehicleParts as $vehParts)
				{
					if($vehParts['vehicle'] != '')
					{
						$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
						$vehstatements = $db->prepare($vehsqls); 	
						$vehstatements->execute();   
						$vehrows = $vehstatements->fetchAll();
						foreach($vehrows as $vehval)
						{
							$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
							$vehval['hours'] = $vehParts['hours'];
							$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
							$vehicleinventoryId[] = $vehval;
						}
					}
				}
				foreach($equipments as $equip)
				{
					foreach($equip as $equipdata)
					{
						
						if($equipdata['equipment'] != '')
						{
							$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
							
						}
						$equipParts[] = $equipdata;
						
					}
				}
				
				foreach($equipParts as $equParts)
				{
					if($equParts['equipment'] != '')
					{
						$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
						$eqstatements = $db->prepare($equsqls); 	
						$eqstatements->execute();   
						$eqrows = $eqstatements->fetchAll();
						
						foreach($eqrows as $eqval)
						{
							$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
							$eqval['hours'] = $equParts['hours'];
							$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
							$equipinventoryId[] = $eqval;
						}
					}
				}
			}
			if($table == 'generatorestimates')
			{
				$staffsql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$id." and `work_sheet` ='gen_repair_est' group by staff_id";
				$staffstatement = $db->prepare($staffsql); 	
				$staffstatement->execute();   
				$staffrow = $staffstatement->fetchAll();
				
				
				foreach($staffrow as $staffval)
				{
					$staffArray[]=array('staffval'=>$staffval['staff_id'],'total_hours'=>$staffval['total_hours']);
					
				}
				
				
				foreach($staffArray as $staffval1)
				{
					
					$field = 'first_name,last_name,billrate,qb_id';
					$staffsid = $staffval1['staffval'];
					$staffsql1="select * from users where id='".$staffsid."'";
					$staffstatement1 = $db->prepare($staffsql1);	
					$staffstatement1->execute(); 
					$staffresult = $staffstatement1->fetchAll();
					$staffresult[0]['total_hours'] = $staffval1['total_hours'];
					$staffInfos[] = $staffresult[0];
				}
				
				$inventorysql = "select parts as additional_info , airfilter,battery,antifreeze,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorestimates where `id`=".$id;
				$inventorystatements = $db->prepare($inventorysql); 	
				$inventorystatements->execute();   
				$inventoryrows = $inventorystatements->fetchAll();
				
				$inventory = unserialize($inventoryrows[0]['additional_info']);
				
				$equipments = unserialize($inventoryrows[0]['equipments']);
				$vehicles = unserialize($inventoryrows[0]['vehicles']);
				
				$airfilters = json_decode($inventoryrows[0]['airfilter']);
				$generatorHours = $inventoryrows[0]['hours'];
				if($airfilters->qty != '' && $airfilters->name != '')
				{
					$airFilter = $airfilters;
				}
				$battery1 = $inventoryrows[0]['battery'];
				$antifreeze = $inventoryrows[0]['antifreeze'];
				$equipHrs = $inventoryrows[0]['hours'];
				
				$inventoryParts = array();
				foreach($inventory as $inv)
				{
					
					foreach($inv as $val3)
					{
						if($val3['cat'] != '')
						{
							if($val3['qty'] == '')
							{
								$val3['qty'] = 1;
							}
							$inventoryParts[] = $val3;
						}
						
					}
				}
				foreach($inventoryParts as $parts)
				{
					$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
					$statements1 = $db->prepare($sqls1); 	
					$statements1->execute();   
					$rows13 = $statements1->fetchAll();
					foreach($rows13 as $val13)
					{
						
						
						$val13['travel'] = $parts['travel'];
						$val13['cat'] = $parts['inventory'];
						$val13['catName'] = $parts['cat'];
						$val13['qty'] = $parts['qty'];
						$inventoryId2[] = $val13;
					}
					
					
				}
				
				foreach($vehicles as $veh)
				{
					foreach($veh as $vehdata)
					{
						
						if($vehdata['vehicle'] != '')
						{
							$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
							
						}
						$vehicleParts[] = $vehdata;
					}
				}
				foreach($vehicleParts as $vehParts)
				{
					if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
					{
						$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
						$vehstatements = $db->prepare($vehsqls); 	
						$vehstatements->execute();   
						$vehrows = $vehstatements->fetchAll();
						foreach($vehrows as $vehval)
						{
							$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
							$vehval['hours'] = $vehParts['hours'];
							$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
							$vehicleinventoryId[] = $vehval;
						}
					}
				}
				foreach($equipments as $equip)
				{
					foreach($equip as $equipdata)
					{
						
						if($equipdata['equipment'] != '')
						{
							$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
							
						}
						$equipParts[] = $equipdata;
						
					}
				}
				
				foreach($equipParts as $equParts)
				{
					if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
					{
						$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
						$eqstatements = $db->prepare($equsqls); 	
						$eqstatements->execute();   
						$eqrows = $eqstatements->fetchAll();
						
						foreach($eqrows as $eqval)
						{
							$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
							$eqval['hours'] = $equParts['hours'];
							$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
							$equipinventoryId[] = $eqval;
						}
					}
				}
			}
			if($table == 'generatorservices')
			{
				$staffsql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$id." and `work_sheet` ='generatorServices' group by staff_id";
				$staffstatement = $db->prepare($staffsql); 	
				$staffstatement->execute();   
				$staffrow = $staffstatement->fetchAll();
				
				foreach($staffrow as $staffval)
				{
					$staffArray[]=array('staffval'=>$staffval['staff_id'],'total_hours'=>$staffval['total_hours']);
					
				}
				
				
				foreach($staffArray as $staffval1)
				{
					
					$field = 'first_name,last_name,billrate,qb_id';
					$staffsid = $staffval1['staffval'];
					$staffsql1="select * from users where id='".$staffsid."'";
					$staffstatement1 = $db->prepare($staffsql1);	
					$staffstatement1->execute(); 
					$staffresult = $staffstatement1->fetchAll();
					$staffresult[0]['total_hours'] = $staffval1['total_hours'];
					$staffInfos[] = $staffresult[0];
				}
				
				$inventorysql = "select parts as additional_info , oil,oil_filter,airfilter,battery,equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorservices where `id`=".$id;
				$inventorystatements = $db->prepare($inventorysql); 	
				$inventorystatements->execute();   
				$inventoryrows = $inventorystatements->fetchAll();
				
				$inventory = unserialize($inventoryrows[0]['additional_info']);
				$equipments = unserialize($inventoryrows[0]['equipments']);
				$vehicles = unserialize($inventoryrows[0]['vehicles']);
				$generatorHours = $inventoryrows[0]['hours'];
				$oils = json_decode($inventoryrows[0]['oil']);
				if($oils->name != '' && $oils->qty != '')
				{
					$oil = $oils;
				}
				
				$oilFilters = json_decode($inventoryrows[0]['oil_filter']);
				
				if($oilFilters->name != '' && $oilFilters->qty != '')
				{
					$oilFilter = $oilFilters;
				}
				
				$airFilters = json_decode($inventoryrows[0]['airfilter']);
				if($airFilters->name != '' && $airFilters->qty != '')
				{
					$airFilter = $airFilters;
				}
				$batterys = json_decode($inventoryrows[0]['battery']);
				if($batterys->name != '' && $batterys->qty != '')
				{
					$battery = $batterys;
				}
				$equipHrs = $inventoryrows[0]['hours'];
				
				$inventoryParts = array();
				foreach($inventory as $inv)
				{
					
					foreach($inv as $val3)
					{
						if($val3['cat'] != '')
						{
							if($val3['qty'] == '')
							{
								$val3['qty'] = 1;
							}
							$inventoryParts[] = $val3;
						}
						
					}
				}
				
				foreach($inventoryParts as $parts)
				{
					$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
					$statements1 = $db->prepare($sqls1); 	
					$statements1->execute();   
					$rows13 = $statements1->fetchAll();
					foreach($rows13 as $val13)
					{
						
						
						$val13['travel'] = $parts['travel'];
						$val13['cat'] = $parts['inventory'];
						$val13['catName'] = $parts['cat'];
						$val13['qty'] = $parts['qty'];
						$inventoryId2[] = $val13;
					}
					
				}
				foreach($vehicles as $veh)
				{
					foreach($veh as $vehdata)
					{
						
						if($vehdata['vehicle'] != '')
						{
							$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
							
						}
						$vehicleParts[] = $vehdata;
					}
				}
				foreach($vehicleParts as $vehParts)
				{
					if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
					{
						$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
						$vehstatements = $db->prepare($vehsqls); 	
						$vehstatements->execute();   
						$vehrows = $vehstatements->fetchAll();
						foreach($vehrows as $vehval)
						{
							$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
							$vehval['hours'] = $vehParts['hours'];
							$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
							$vehicleinventoryId[] = $vehval;
						}
					}
				}
				foreach($equipments as $equip)
				{
					foreach($equip as $equipdata)
					{
						
						if($equipdata['equipment'] != '')
						{
							$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
							
						}
						$equipParts[] = $equipdata;
						
					}
				}
				
				foreach($equipParts as $equParts)
				{
					if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
					{
						$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
						$eqstatements = $db->prepare($equsqls); 	
						$eqstatements->execute();   
						$eqrows = $eqstatements->fetchAll();
						
						foreach($eqrows as $eqval)
						{
							$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
							$eqval['hours'] = $equParts['hours'];
							$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
							$equipinventoryId[] = $eqval;
						}
					}
				}
				
			}
			if($table == 'generatorstorages')
			{
				$staffsql = "select SUM(hours) 'total_hours',id, staff_id,work_id,work_sheet from staff_timesheet where `work_id`=".$id." and `work_sheet` ='gen_win_storage' group by staff_id";
				$staffstatement = $db->prepare($staffsql); 	
				$staffstatement->execute();   
				$staffrow = $staffstatement->fetchAll();
				
				foreach($staffrow as $staffval)
				{
					$staffArray[]=array('staffval'=>$staffval['staff_id'],'total_hours'=>$staffval['total_hours']);
					
				}
				
				
				foreach($staffArray as $staffval1)
				{
					
					$field = 'first_name,last_name,billrate,qb_id';
					$staffsid = $staffval1['staffval'];
					$staffsql1="select * from users where id='".$staffsid."'";
					$staffstatement1 = $db->prepare($staffsql1);	
					$staffstatement1->execute(); 
					$staffresult = $staffstatement1->fetchAll();
					$staffresult[0]['total_hours'] = $staffval1['total_hours'];
					$staffInfos[] = $staffresult[0];
				}
				
				$inventorysql = "select parts as additional_info , equipments,generator_hours as hours,vehicles, id,customer_id,invoice_id from generatorstorages where `id`=".$id;
				$inventorystatements = $db->prepare($inventorysql); 	
				$inventorystatements->execute();   
				$inventoryrows = $inventorystatements->fetchAll();
				$inventory = unserialize($inventoryrows[0]['additional_info']);
						
				$equipments = unserialize($inventoryrows[0]['equipments']);
				$vehicles = unserialize($inventoryrows[0]['vehicles']);
				$generatorHours = $inventoryrows[0]['hours'];
				$inventoryParts = array();
				$vehicleParts = array();
				foreach($inventory as $inv)
				{
					
					foreach($inv as $val3)
					{
						if($val3['cat'] != '')
						{
							if($val3['qty'] == '')
							{
								$val3['qty'] = 1;
							}
							$inventoryParts[] = $val3;
						}
						
					}
				}
				foreach($inventoryParts as $parts)
				{
					$sqls1 = "select qb_id from store_inventory_qb where `inventory`='".$parts['inventory']."'";
					$statements1 = $db->prepare($sqls1); 	
					$statements1->execute();   
					$rows13 = $statements1->fetchAll();
					foreach($rows13 as $val13)
					{
						
						
						$val13['cat'] = $parts['inventory'];
						$val13['qty'] = $parts['qty'];
						$val13['catName'] = $parts['cat'];
						$inventoryId2[] = $val13;
					}
					
					
				}
				foreach($vehicles as $veh)
				{
					foreach($veh as $vehdata)
					{
						
						if($vehdata['vehicle'] != '')
						{
							$vehdata['vehiclename'] = getVehicleName($vehdata['vehicle']);
							
						}
						$vehicleParts[] = $vehdata;
					}
				}
				foreach($vehicleParts as $vehParts)
				{
					if($vehParts['vehicle'] != '' &&  $vehParts['hours'] != '')
					{
						$vehsqls = "select qb_id from store_inventory_qb where `inventory`='".$vehParts['vehiclename'][0]['vehicle']."'";
						$vehstatements = $db->prepare($vehsqls); 	
						$vehstatements->execute();   
						$vehrows = $vehstatements->fetchAll();
						foreach($vehrows as $vehval)
						{
							$vehval['vehiclename'] = $vehParts['vehiclename'][0]['vehicle'];
							$vehval['hours'] = $vehParts['hours'];
							$vehval['rate'] = $vehParts['vehiclename'][0]['rate'];
							$vehicleinventoryId[] = $vehval;
						}
					}
				}
				
				foreach($equipments as $equip)
				{
					foreach($equip as $equipdata)
					{
						
						if($equipdata['equipment'] != '')
						{
							$equipdata['equipmentname'] = getEquipmentName($equipdata['equipment']);
							
						}
						$equipParts[] = $equipdata;
						
					}
				}
				
				foreach($equipParts as $equParts)
				{
					if($equParts['equipment'] != '' &&  $equParts['hours'] != '')
					{
						$equsqls = "select qb_id from store_inventory_qb where `inventory`='".$equParts['equipmentname'][0]['equipment']."'";
						$eqstatements = $db->prepare($equsqls); 	
						$eqstatements->execute();   
						$eqrows = $eqstatements->fetchAll();
						
						foreach($eqrows as $eqval)
						{
							$eqval['equipmentname'] = $equParts['equipmentname'][0]['equipment'];
							$eqval['hours'] = $equParts['hours'];
							$eqval['rate'] = $equParts['equipmentname'][0]['rate'];
							$equipinventoryId[] = $eqval;
						}
					}
				}
			}
			
			$inventoryArray = array_merge($vehicleinventoryId,$equipinventoryId);
			
			
			$customersql = "SELECT * FROM customers where id=".$data['customer_id'];
			$customerstatement = $db->prepare($customersql); 	
			$customerstatement->execute();   
			$cus_result2 = $customerstatement->fetchAll();
			
			$customerId = $cus_result2[0]['qb_id'];
			$invoiceId = $_REQUEST['id'];
			$table=$_REQUEST['table'];
			
			$electricalId=$rows[0]['id'];
			include('./qb/quickbooks-new3/updateInvoice.php');
		}
		else
		{
			echo 0;
		}
	
}


function updateApproved(){
	//Forget Password not
		$db = get_connection();
		extract($_REQUEST);	
		$table=$_REQUEST['table'];
		$approved=$_REQUEST['value'];
		$statement2 = $db->prepare("UPDATE $table SET 	approved = :approved Where id = :id");
		$affec_row=$statement2->execute(array(':approved' => $approved, ':id' =>  $id));		
			
		if($affec_row != 0)
		{
			  echo 1;
		}else
		{
			echo 0;
		}	
	
}
function updatePrinted(){
	//Forget Password not
		$db = get_connection();
			
		extract($_REQUEST);	
		$table=$_REQUEST['table'];
		$id=$_REQUEST['id'];
		
		$sql= "SELECT * FROM $table WHERE id = :id"; 
		$statement3 = $db->prepare($sql);
		$statement3->execute(array(':id' => $id));
		$row = $statement3->fetch();	
		if($row['printed']==0)
		{	
		$statement2 = $db->prepare("UPDATE $table SET 	printed = :printed Where id = :id");
		$affec_row=$statement2->execute(array(':printed' => 1, ':id' =>  $id));		
		}
		if($row['printed']==1)
		{	
		$statement2 = $db->prepare("UPDATE $table SET 	printed = :printed Where id = :id");
		$affec_row=$statement2->execute(array(':printed' => 0, ':id' =>  $id));		
		}
			
		if($row['printed']==0)
		{
			  echo 1;
		}else
		{
			echo 0;
		}
	
}


function userRole($uid){
	//username Exist or not
	$db = get_connection();		
	$id= $uid; 	
	
	$sql= "SELECT role FROM users WHERE id = :id"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':id' => $id));
	$row = $statement->fetch();	
	return $row;
	
	
		
}

/************ Delete Staff ***************/
function staffDel() 
{
$db = get_connection();
echo $id=$_REQUEST['id'];
$sql = "DELETE FROM users WHERE id =  :id";
$stmt = $db->prepare($sql);
$stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);   
$stmt->execute();
}
/************ Edit Staff ***************/
function staffEdit() 
{
	$db = get_connection();
	$id=$_REQUEST['id'];
	$sql= "SELECT * FROM users WHERE id = :id"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':id' => $id));
	$row = $statement->fetch();	
	
	
echo '<div class="" id="'.$id.'">

					 <div class="section-hdr">
            <h3>Edit Staff Information</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
        </div>
					 <div class="table-responsive">
						 
						 <form name="myForm" role="form" class="cs-form"  method="post" action="files/updateStaff.php" onsubmit="return validateForm1()">
								<div class="row">
								
								  <div class="col-sm-6">  
									<input name="id" id="user_id" type="hidden"  value="'.$row['id'].'" >
									<div class="group">
									<input name="first_name" id="first_name" type="text"  value="'.$row['first_name'].'" >
									<label class="im-label">First Name </label>
									</div>
								  </div>
								  <div class="col-sm-6">
								  <div class="group">
									<input name="last_name" type="text" value="'.$row['last_name'].'">
									<label class="im-label">Last Name </label>
									</div>
								  </div>
								   <div class="col-sm-6">
								   <div class="group">
										<input name="email" type="text"  value="'.$row['email'].'">
										<label class="im-label">Email </label>
										</div>
								  </div>
								   <div class="col-sm-6">
										<select name="role">';?>										
										  
										  <option value="staff" <?php if($row['role']=='staff'){ ?> selected="selected" <?php } ?>>Staff</option>
										  <option value="admin" <?php if($row['role']=='admin'){ ?> selected="selected" <?php } ?>>Admin</option>
									<?php echo '</select>
								  </div>
									<div class="col-sm-6">
									  <div class="group">
										<input name="billrate" class="validNumber" type="text" value="'.$row['billrate'].'">
										<label class="im-label">Billable Rate </label>
									  </div>
									</div>
									<div class="col-sm-6">
									  <div class="group">
										<input name="payroll" class="validNumber payroll" type="text" value="'.$row['payroll'].'">
										<label class="im-label">Payroll Rate </label>
									  </div>
									</div>
								   <div class="col-sm-6">';?>
								   <span>Freeze Account:<?php // echo $row['status'] ?> </span>
								   <label class="cstm-radio"><input  type="radio" name="status" class="radiobox" value="1" <?php if($row['status']==0){?> checked  <?php } ?>><span class='custm-radio'></span> Yes</label>
						<label class="cstm-radio"><input  type="radio" name="status" class="radiobox"  value="0" <?php if($row['status']==1){?> checked <?php } ?>>  <span class='custm-radio'></span> No</label>
						
						<?php 
										
								  echo '</div>						
								 <input type="hidden" value="" name="action"/>
							  </div>
							  <div class="actions">
								<p><input class="cs-btn btn-green" type="submit" value="Save" style="display:block; margin-left:auto; margin-right:auto;">
									<div id="message"> </div>
								</p>
							  </div>
							</div>
							 </form>
						 </div>
						</div>
						<script>
						$(".close-edit").click(function(){
				
				$("#editFromToggle").slideUp("slow");
			});
					  </script>
					
				';


}

/************ Edit Vehicle ***************/
function vehicleEdit() 
{
	$db = get_connection();
	$id=$_REQUEST['id'];
	$sql= "SELECT * FROM vehicles WHERE id = :id"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':id' => $id));
	$row = $statement->fetch();	
	
	
	echo '<div class="" id="'.$id.'"><div class="section-hdr"><h3>Edit Vehicle Information</h3><a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a></div><div class="table-responsive"><form name="myForm1" role="form" class="cs-form" id="update-vehicle" method="post" action="files/updateVehicle.php" onsubmit="return validateForm1()"><div class="row"> <div class="col-sm-6"><input type="hidden" name="id" value="'.$row['id'].'"><div class="group"><input name="vehicle" type="text"  value="'.$row['vehicle'].'" ><label class="im-label">Vehicle </label></div></div><div class="col-sm-6"><div class="group"><input name="rate" type="text" value="'.$row['rate'].'"><label class="im-label">Rate </label></div></div> <input type="hidden" value="" name="action"> </div><div class="text-center"><p> <input class="cs-btn btn-green is-filled" type="submit" value="Save"><div id="message"> </div></p></div></div></form></div></div><script>$(".close-edit").click(function(){$("#editFromToggle1").slideUp("slow");}); </script>';


}

function equipmentEdit() 
{
	$db = get_connection();
	$id=$_REQUEST['id'];
	$sql= "SELECT * FROM equipment WHERE id = :id"; 
	$statement = $db->prepare($sql);
	$statement->execute(array(':id' => $id));
	$row = $statement->fetch();	
	
	
	echo '<div class="" id="'.$id.'"><div class="section-hdr"><h3>Edit Equipment Information</h3><a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a></div><div class="table-responsive"><form name="myForm2" role="form" class="cs-form" id="update-equipment" method="post" action="files/updateEquipment.php" onsubmit="return validateForm2()"><div class="row"> <div class="col-sm-6"><input type="hidden" name="id" value="'.$row['id'].'"><div class="group"><input name="equipment" type="text"  value="'.$row['equipment'].'" ><label class="im-label">Vehicle </label></div></div><div class="col-sm-6"><div class="group"><input name="rate" type="text" value="'.$row['rate'].'"><label class="im-label">Rate </label></div></div> <input type="hidden" value="" name="action"> </div><div class="text-center"><p> <input class="cs-btn btn-green is-filled" type="submit" value="Save"><div id="message"> </div></p></div></div></form></div></div><script>$(".close-edit").click(function(){$("#editFromToggle2").slideUp("slow");}); </script>';


}


/* GET CATEGORY LIST */
function getCategories() 
{
	$db = get_connection();			   	  	  
	$statement = $db->prepare("SELECT DISTINCT category FROM  `inventory`  order by category asc");
	$statement->execute();
	$result = $statement->fetchAll();
	$html='';   
	
	foreach($result as $cat)
	{
	$item=rtrim($cat["category"]," ");
	  $html.="<option>".$item."</option>";	
	} 	
	 echo  $html;
	 //die;
}

function getEquipments()
{
	$db = get_connection();			   	  	  
	$statement = $db->prepare("SELECT * FROM  `equipment`  order by equipment asc");
	$statement->execute();
	$result = $statement->fetchAll();
	$html='';   
	
	foreach($result as $equip)
	{
		$item=rtrim($equip["equipment"]," ");
		$html.="<option value='".$equip["id"]."' data-attr='".$equip["rate"]."'>".$item."</option>";	
	} 	
	 echo  $html;
}

function getVehicles()
{
	$db = get_connection();			   	  	  
	$statement = $db->prepare("SELECT * FROM  `vehicles`  order by vehicle asc");
	$statement->execute();
	$result = $statement->fetchAll();
	$html='';   
	
	foreach($result as $vehicle)
	{
		$item=rtrim($vehicle["vehicle"]," ");
		$html.="<option value='".$vehicle["id"]."' data-attr='".$vehicle["rate"]."'>".$item."</option>";	
	} 	
	 echo  $html;
}


function viewCustomer()
{

	$uid=$_SESSION['uid'];
	$role=	userRole($uid);

	$db = get_connection();	
	get_mysqlconnection();	
	$id=$_REQUEST['id'];
	
	$new_row=$id;
	$sql= "SELECT * FROM customers WHERE id = $id"; 
	$result=mysql_query($sql);
	
	$row = mysql_fetch_assoc($result);
	
		$first_name=$row['first_name'];
		$last_name=$row['last_name'];
		
		$display_name=$first_name." ".$last_name;
		if(empty($first_name)){			
		$display_name=$row['company_name'];
		}			
		$display_name=trim($display_name,'Mr.');
		$display_name=trim($display_name,'Mrs.'); 
		$check=check_cust_gen($new_row);
		?>
		<script>
		function closeMee()
		{
			jQuery('.top_menuu>a:first').remove();
		}
		</script>
		<div class="pull-left"> 
			<!--<a href="javascript:void(0);"  class="cs-btn btn-blue new_gen1" <?php if($check > 0) { echo 'style="display:none;"'; } else { echo 'style="display:inline-block;"'; } ?> onclick="show_cust_gen('0','<?php echo $new_row; ?>');">New Generator</a> -->  
			<a href="javascript:void(0);"  class="cs-btn btn-blue new_gen1"  onclick="show_cust_gen('0','<?php echo $new_row; ?>');">New Generator</a> 
			
        </div> 
		<input type="hidden" id="cust_idd" value="<?php echo $new_row; ?>"> 
		<?php 
	
	echo '<div class="customer-header">
	<h3 class="section-hdr">Customers Profile <a href="javascript:void(0);" onclick="closeMee();" class="close"><i class="fa fa-plus"></i></a></h3> 
	 
     <div class="cust-wrap"> 
	
       <div class="cust-pro-img"> <img src="'.SITE_URL.'/assets/images/customer-profile-img.jpg"/> </div>
       <div class="cust-info">
         <h2>'.$display_name.' <a class="edit-btn-pro" href="javascript:void(0)"  id='.$row['id'].'  onclick="editFunction('.$row['id'].')">Edit</a></h2>
         <div class="row">
           <div class="col-sm-4">';
		   if(!empty($row['phone']))
            echo '<p class="phone">'.$row['phone'].'</p>';
			else 
			echo '<p class="phone">N/A</p>';
			
			if(!empty($row['email']))
             echo '<p class="email">'.$row['email'].'</p>';
			 else 
			echo '<p class="email">N/A</p>';
			 
			 if(!empty($row['mobile']))
             echo '<p class="mobile">'.$row['mobile'].'</p>';
			 else 
			echo '<p class="mobile">N/A</p>';
			
		echo '</div>
           <div class="col-sm-4">
		 
		<p class="home-addr">'.$row['home_address'].'<br>
               '.$row['home_city'].'<br>
               '.$row['home_province'].'<br>
			   '.$row['home_country'].'<br>
			   </p>
           </div>
           <div class="col-sm-4">';
		   if(!empty($row['cottage_address']))
		   {		   
           	echo '<p class="cott-addr">'.$row['cottage_address'].'<br>
               '.$row['cottage_city'].'<br>			  
               '.$row['cottage_province'].'<br>
               '.$row['cottage_country'].'</p>';
			   }
		   else 
		   {
		   echo '<p class="cott-addr">N/A</p>';
		   }
	   echo '
           </div>
         </div>
       </div>
     </div> 
	 </div>';
	  ?>
	  <div class="cust_gen_area" id="cust_gen_area_1" style="display:none;"></div> 
	  <?php
	  echo '<div id="editFromWorksheet"></div>';
	  
	  ?>
	  
	  <div class="table-responsive gen_section">
	  <?php
	  if($check > 0)
	  {	   
	  ?>
	   <h2 class="cust_work">Generators</h2>
	  <table id="" class="display table data-tbl" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th></th>
        <th>Label</th>
        <th>Type </th>
        <th>Model </th>       
        <th>Installation Date </th>
        <th>SN</th>
        <th></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th></th>
        <th>Label</th>
        <th>Type </th>
        <th>Model </th>       
        <th>Installation Date </th>
        <th>SN</th>
        <th></th>
      </tr>
    </tfoot>
    <tbody>
	<?php
	$sql="select * from customer_generator where cust_id='".$new_row."' order by install_date desc";
	$res=mysql_query($sql);
	while($row=mysql_fetch_array($res))
	{	
	?>
	<tr>	
        <td></td>
        <td><?php echo $row['label']; ?></td>
        <td><?php echo $row['type']; ?> </td>
        <td><?php echo $row['model']; ?> </td>       
        <td><?php $install_date=$row['install_date']; echo date('F d, Y', strtotime($install_date)); ?></td>
        <td><?php echo $row['sn']; ?></th>  
        <td><a href="javascript:void(0)" class="btn-edit" onclick="show_cust_gen('1','<?php echo $row['id']; ?>');">Edit</a></td>   
    </tr> 
	<?php
	}
	?>
	</tbody>
	</table>
	<?php
	  }
	?>
	</div>
	<?php	
	 
	  echo '<div class="table-responsive" id="cust_work">
	   <h2 class="cust_work">Worksheets</h2> 
	  <table id="" class="display table data-tbl" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th></th>
        <th>Date</th>
        <th>Name </th>
        <th>Serviced By </th>       
        <th>Invoiced </th>
        <th></th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <th></th>
        <th>Date</th>
        <th>Name </th>
        <th>Serviced By</th>
        
        <th>Invoiced</th>
        <th></th>
      </tr>
    </tfoot>
    <tbody>
     
   
	'; 
	
	
		
		$sql= "SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`,invoice,'electrical_work' as tablename FROM electrical_work WHERE customer_id=$id
		UNION
		SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`,invoice, 'generatorinspections' as tablename FROM generatorinspections WHERE customer_id=$id
		UNION
		SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`,invoice, 'generatorservices' as tablename FROM generatorservices WHERE customer_id=$id
		UNION
		SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`, invoice,'generatorstorages' as tablename FROM generatorstorages WHERE customer_id=$id
		UNION
		SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`, invoice,'generatortroubles' as tablename FROM generatortroubles WHERE customer_id=$id
		UNION
		SELECT id,created_by,customer_id,service_by,customer_id,cat_name,`created`,invoice, 'generatorestimates' as tablename FROM generatorestimates WHERE customer_id=$id
		ORDER BY `created` DESC
		"; 
		$result=mysql_query($sql);
		//echo mysql_num_rows($result);
		if(mysql_num_rows($result)>0)
		{
		while($row = mysql_fetch_assoc($result))
		{
					
				$statement1 = $db->prepare("SELECT * FROM customers where id=".$row['customer_id']);				
				$statement1->execute();
				$cus_result = $statement1->fetchAll();							
				$first_name=$cus_result[0]['first_name'];
				$last_name=$cus_result[0]['last_name'];
				
				$display_name=$first_name." ".$last_name;
				if(empty($first_name)){			
				$display_name=$cus_result[0]['company_name'];
				}			
				$display_name=trim($display_name,'Mr.');
				$display_name=trim($display_name,'Mrs.');
				
				$created_by=$row['created_by'];				
				$statement2 = $db->prepare("SELECT * FROM users where id=".$created_by);				
				$statement2->execute();
				$users_result = $statement2->fetchAll();	
				$ufname=$users_result[0]['first_name'];
				$ulname=$users_result[0]['last_name'];				
				
			?>
      <tr>
        <td><img src="assets/images/square-gif.gif"></td>
		
        <?php 
		
			$originalDate = $row['created'];			
			$newDate = date("M d, y", strtotime($originalDate));				
			echo "<td>".$newDate ."</td>";
			$row_id=$row['id'];
			$tablename=$row["tablename"];
			$pdflink=SITE_URL."/pdf.php?id=".$row_id."&type=".$tablename;
			
			//$onclick="javascript:closeOnLoad('".$pdflink."');";
			echo "<td>".$display_name."</td>";
			echo "<td>".ucfirst($ufname)." ".ucfirst($ulname)."</td>";
			//echo "<td>".$row['cat_name']."</td>";   
			if($row['invoice']==0)
			{
			echo '<td><a href="javascript:void(0)" class="invoiced-no" id="'.$row_id.'" u="'.$row["tablename"].'"><i class="fa blue fa-check-circle-o"></i></a><span class="sr-only">No</span>';
			echo '<a style="display:none;" href="javascript:void(0)" class="invoiced-no" id="'.$row_id.'" u="'.$row["tablename"].'"><i class="fa blue fa-check-circle"></i></a><span class="sr-only">Yes</span></td>';
			//echo '<td><span >No</span></td>';
			}
			else
			{
			echo '<td><a style="display:none;" href="javascript:void(0)" class="invoiced-no" id="'.$row_id.'" u="'.$row["tablename"].'"><i class="fa blue fa-check-circle-o"></i></a><span class="sr-only">No</span>';
			echo '<a href="javascript:void(0)" class="invoiced-no" id="'.$row_id.'" u="'.$row["tablename"].'"><i class="fa blue fa-check-circle"></i></a><span class="sr-only">Yes</span></td>';
			
			//echo '<td><span class="sr-only">Yes</span></td>';
			
			
			
			}
			
			$id= "myModal".$i;
			echo '<td>';
			$d1=$row["created"]; // Created			
			$d2= date("Y/m/d h:i:s", strtotime("-30 minutes"));			
			$date1=date_create($d1);
			$date2=date_create($d2);
			$diff=date_diff($date1,$date2);
			echo '<a href="'.$pdflink.'"  target="_blank" ><img src="https://icons.iconarchive.com/icons/hopstarter/soft-scraps/256/Adobe-PDF-Document-icon.png" width="20"></a>';
			// Uncomment below code to show edit botton , FUNCTIONALITY PENDING // By SS
			if($role['0']=='admin' ) // Time  is more then 30 min
			{
			echo "<a href='javascript:void(0)'   u='".$row["tablename"]."' class='edit-btn' id=".$row['id']." >Edit</a>";
			}
			else if($diff->invert!==0)
			{
			echo "<a href='javascript:void(0)'   u='".$row["tablename"]."' class='edit-btn' id=".$row['id']." >Edit</a>";
			}		
				
				
				echo '</td> </tr>';
			$i++;
			
		
		}
		}
		else
		{
		echo "<tr><td colspan='5'>No Record.</td></tr>";
		}
		echo ' </tbody>
  </table></div>';
		?>
			
  <script> 
  $(document).ready(function(){
	jQuery('.top_menuu').find('.new_gen1').remove();
	jQuery(".new_gen1").detach().insertBefore('.here'); 

  $(".edit-btn").click(function(){ 
  $('html, body').animate({
			scrollTop: 0
		}, 'slow');
	
		var recId = $(this).attr("id");
		var table = $(this).attr("u");
		$.ajax({
			type: "POST",
			url: "handler.php",
			data: "id=" + recId + "&action=SheetsEdit&table=" + table,
			success: function(result) {
			
				$(".customer-profile > .customer-header").slideUp('slow');
				$("#editFromWorksheet").show();
				$("#editFromWorksheet").html(result);
	
	
				//$('#editFromToggle input:not([value!=""])').addClass('is-filled')
				$('#editFromWorksheet input').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
				$('#editFromWorksheet .notes').filter(function() {
					return this.value;
				}).addClass('is-filled');
	
	
			},
			error: function(e) {
				console.log(e);
			}
		});
   }); 				
	
					 
   });
	
	
  </script>
  <?php  
		
	
	
}

function cusEditForm() 
{
	$db = get_connection();	
	get_mysqlconnection();	
	$id=$_REQUEST['id'];
	$sql= "SELECT * FROM customers WHERE id = $id"; 
	$result=mysql_query($sql);
	
	$row = mysql_fetch_assoc($result);
	
	
	echo '
	
	<div class="edit_customer_info" id="'.$id.'">    
     
	  <div class="section-hdr">
            <h3>Edit Customer Information</h3>
            <a href="javascript:void(0);" onclick="closeMe()" class="close"><i class="fa fa-plus"></i></a>
        </div>
     <div class="table-responsive-1"><div class="clearfix"> 
      <form name="myForm" role="form" class="cs-form" id="update-customer" method="post" action="files/updateCustomer.php" onsubmit="return validateForm()">
	  <!--<a class="view-btn-pro" href="javascript:void(0)"  id='.$row['id'].'  onclick="myFunction('.$row['id'].')">View</a>-->
	  <div class="row">
        <div class="col-sm-6">
		 <div class="group">
          <input name="company_name" id="company_name" type="text"   value="'.$row['company_name'].'">
		  <label class="im-label">Company Name</label>
		  </div>
        </div>
      </div>
        <div class="row">
          <div class="col-sm-6">  
		   
		  	<input name="id" id="user_id" type="hidden"  value="'.$row['id'].'" >
			<div class="group">
            <input name="first_name" id="first_name" type="text" value="'.$row['first_name'].'" >
			<label class="im-label">First Name</label>
			</div>
          </div>
          <div class="col-sm-6">
		  <div class="group">
            <input name="last_name" type="text" value="'.$row['last_name'].'">
			<label class="im-label">Last Name</label>
			</div>
          </div>
           
           <div class="col-sm-6">
            <div class="row">
              <div class="col-sm-6">
			  <div class="group">
                <input name="phone" type="text"  value="'.$row['phone'].'">
				<label class="im-label">Home Phone</label>
				</div>
              </div>
              <div class="col-sm-6">
			  <div class="group">
                <input name="mobile" type="text"  value="'.$row['mobile'].'">
				<label class="im-label">Mobile Phone</label>
				</div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
		  <div class="group">
            <input name="email" type="text"  value="'.$row['email'].'">
			<label class="im-label">Email address</label>
			</div>
          </div>
          <div class="col-sm-6">
          <p class="label">Home Address</p>
          <div class="group">
            <input type="text" name="home_address"  value="'.$row['home_address'].'">
			<label class="im-label">Address Line 1</label>
			</div>
			<div class="group">
			 <input type="text" name="home_address2"  value="'.$row['home_address2'].'">
			 <label class="im-label">Address Line 2</label>
			 </div>
            <div class="row">
              <div class="col-sm-3">
			  <div class="group">
                <input name="home_city" type="text"  value="'.$row['home_city'].'">
				<label class="im-label">City</label>
				</div>
              </div>
              <div class="col-sm-3">
			  <div class="group">
                <input name="home_province" type="text" value="'.$row['home_province'].'">
				<label class="im-label">Province</label>
				</div>
              </div>
			    <div class="col-sm-3">
				<div class="group">
                <input name="home_country" type="text"  value="'.$row['home_country'].'">
				<label class="im-label">Country</label>
				</div>
              </div>
              <div class="col-sm-3">
			  <div class="group">
                <input name="home_postal_code" type="text"  value="'.$row['home_postal_code'].'">
				<label class="im-label">Postal Code</label>
				</div>
              </div>
            </div>
          </div>
          <div class="col-sm-6">
          <p class="label">Cottage Address</p>
		  <div class="group">
            <input type="text" name="cottage_address"   value="'.$row['cottage_address'].'">
			<label class="im-label">Address Line 1</label>
			</div>
			<div class="group">
			 <input type="text" name="cottage_address2"  value="'.$row['cottage_address2'].'">
			 <label class="im-label">Address Line 2</label>
			 </div>
            <div class="row">
              <div class="col-sm-3">
			  <div class="group">
                <input name="cottage_city" type="text"  value="'.$row['cottage_city'].'">
				<label class="im-label">City</label>
				</div>
              </div>
              <div class="col-sm-3">
			  <div class="group">
                <input name="cottage_province" type="text"  value="'.$row['cottage_province'].'">
				<label class="im-label">Province</label>
				</div>
              </div>
			     <div class="col-sm-3">
				 <div class="group">
                <input name="cottage_country" type="text"  value="'.$row['cottage_country'].'">
				<label class="im-label">Country</label>
				</div>
              </div>
              <div class="col-sm-3">
			  <div class="group">
                <input name="cottage_postal_code" type="text"  value="'.$row['cottage_postal_code'].'">
				<label class="im-label">Postal Code</label>
				</div>
              </div>
            </div>
          </div>          
        </div>
		<div class="group">
        <textarea name="notes" cols="" rows=""  class="notes">'.$row['notes'].'</textarea>
		<label class="im-label">Notes</label>
		</div>
		 <input type="hidden" value="" name="action"/>
		 
     
	  
      <div class="clr"></div>
      <div class="actions">
       <input class="cs-btn btn-green" type="submit" value="Save">
			<div id="message"> </div>
		
      </div>
    </div>
	 </form>
    
</div>
	';
	
	}


/* GET CATEGORY LIST END*/


function inventoryFilter()
{	

	$searchText= $_POST['searchText'];
	$catname= $_POST['catname'];
	get_mysqlconnection();	
	$sql="SELECT  * FROM  inventory where  category LIKE '%".$catname."%' &&  part_no LIKE '%".$searchText."%'";
	$result=mysql_query($sql);
	echo $html= "<div class='inv_result'>";
	while($row=mysql_fetch_assoc($result))	
	{
	$rowdata=$row['part_no']." - ".$row['description'];
		 echo "<div class='inv_row' >".$rowdata."</div>";
	}
	?>	
	</div>
	
	<script>
	$('.inv_result .inv_row').click(function() {			
			$(this).parent().parent().prev().val($(this).html())
			$(".inv_result").hide()							
		})
		
		</script>
	
	
	<?php
}


function notificationMessage()
{	
	get_mysqlconnection();
	$message=$_REQUEST['message'];
	$is_enable=$_REQUEST['is_enable'];
	$sql="UPDATE  `morrowel_morrow`.`notification` SET  `message` = '".$message."',`is_enable` = '".$is_enable."' WHERE  `notification`.`id` =1";
	$result=mysql_query($sql);
	if($result)
	{
		echo 1;
	}else
	{
		echo 0;
	}	
}

/************ User Info ***************/

function editUserInfo()
{	
	get_mysqlconnection();
	/*echo "<pre>";
	print_r($_REQUEST);
	echo "</pre>";*/ 
	$user_id=$_REQUEST['user_id'];
	$email=$_REQUEST['email'];
	$first_name=$_REQUEST['first_name'];
	$last_name=$_REQUEST['last_name'];
	$phone=$_REQUEST['phone'];
	
	echo $sql="UPDATE  `users` SET  `first_name` ='$first_name',`phone` =  '$phone',`last_name`='$last_name',`email` =  '$email' WHERE  `users`.`id` =$user_id;";
	$result=mysql_query($sql);
	if($result)
	{
		echo 1;
	}else
	{
		echo 0;
	}	
}
/************ End User Info ***************/


/************ Edit Staff ***************/

function DeleteTimecardStaff()
{
	get_mysqlconnection();
	$id=$_REQUEST['id'];
	$sql= "delete from staff_timesheet where id='".$id."'"; 
	$result=mysql_query($sql);
	echo "1";
}


function sheetsEdit() 
{
	get_mysqlconnection();
	$id=$_REQUEST['id'];
	$table=$_REQUEST['table'];
		
	$sql= "SELECT w.*,c.company_name,c.first_name,c.last_name FROM ".$table." as w  
	INNER JOIN customers c ON w.customer_id = c.id
	WHERE w.id =".$id; 
	
	$result=mysql_query($sql);
	$row =mysql_fetch_assoc($result);
	
	
	
	$first_name=$row['first_name'];
	$last_name=$row['last_name'];
	
	$display_name=$first_name." ".$last_name;
	if(empty($first_name)){			
	$display_name=$row['company_name'];
	}
	$po_number=$row['po_number'];
	
	/********************************************* electrical_work  ********************************/	
	$customer_id=$row['customer_id'];
	?>
	<script>
	function remove_ul(e,row_id)
	{
		var id=jQuery('.n-box').length;
		jQuery.ajax({type: "POST", 
		url: "handler.php",
		data: "id="+row_id+"&action=DeleteTimecardStaff",   
		success:function(result) 
		{
			if(result==1)
			{
				jQuery('.box_'+row_id).fadeOut(3000);
				jQuery('.box_'+row_id).remove(); 
			}	
			
		},
		error:function(e){
			console.log(e);
		}	
		}); 
	}
	
	function add_timecard_staff()
	{
		
		var staff_id='';
		jQuery.each(jQuery("select[name='staff_id[]']"), function() {
		staff_id += (staff_id?',':'') + jQuery(this).val();
		});
		
		/* alert(staff_id);
		return false; */
		
		var id=jQuery('.n-box').length;
		jQuery.ajax({type: "POST", 
		url: "handler.php",
		data: "id="+id+"&staff_id="+staff_id+"&action=addTimecardStaff",  
		success:function(result) 
		{
			
			jQuery(result).insertAfter('.n-box:last'); 
		},
		error:function(e){
			console.log(e);
		}	
		}); 
	}
	
	</script>
	<?php
	if($table=='electrical_work')
	{
		$db = get_connection();
		echo '<div class="New_rana_css" id="'.$id.'">
		<div class="section-hdr">
		  <h3>Edit Electrical Worksheet</h3>
		  <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a> </div>
		  
		<form role="form" method="post" action="files/update_sheet.php" class="cs-form" id="electrical-work-form">  
		
		 <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'" />
			<div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
			</div>
		 
		  <div class="table-responsive">
		 
		 
      <div class="pull-left">
	   
        <h3>Electrical Work</h3>
		
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div>
		
		
		<div class="gen-ins-area clearfix" >
		<div class="rows">
		
		  <input name="table" id="table" type="hidden" value="electrical_work">
		  <input name="sid" type="hidden" value="'.$id.'">	
           <div class="row">
		  <div class="col-sm-6">
		  <div class="group">';
				$statement = $db->prepare("SELECT * FROM  `staff_timesheet` WHERE  `work_id` ='".$id."' and work_sheet='elec_work'");	
				$statement->execute();
				$result = $statement->fetchAll();
				$count=count($result);
				if($count > 0)
				{
					$counti = 1;
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
                        <div class="row">
                        <div class="col-md-8">
						<select name="staff_id[]" style="width:100%">
						<option value=""></option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row11) 
						{
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
						} 
						echo '</select></div>
                         <div class="col-md-3">
					   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
						if($counti != 1)
						{   
							echo '<a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')"><i class="fa fa-minus"></i></a>';
						}
						echo '</div></div>';  
						$counti++;
					
					}
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
				else
				{
					echo '<div class="n-box">
                    <div class="row">
                        <div class="col-md-8">
					<select name="staff_id[]" style="width:100%">
					<option value="">Select Staff</option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row11) 
					{
							
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select></div>
                       <div class="col-md-3">
				   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
				   echo '</div></div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
			echo '</div>
			<div class="clr"></div>
		  </div>';
		  
			$vehicles=unserialize($row['vehicles']);
			$statement = $db->prepare("SELECT * FROM  `vehicles`");	
			$statement->execute();
			$result = $statement->fetchAll();
		
			$html='';
			$i1=1;
			
			echo '<div class="col-sm-6"><div class="group"><div class="vehicleField"><input type="hidden" class="vehCount" value="'.count($vehicles['parts']).'">';
			foreach($vehicles['parts'] as $key=>$vals)
			{ 
				if($i1!=1)
				{   
					$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
				}
				else
				{
					$symbol = '';
				}
				echo '<div class="row">
						<div class="col-sm-8">
							<input type="hidden" class="vehicleRate">
							<select id="vehicleRate" name="vehicle[parts][row_'.$i1.'][vehicle]" class="">
								<option value="">Select Vehicles</option>';
								foreach($result as $val)
								{
									if(!empty($vals['vehicle']))
									{
										if($val['id'] == $vals['vehicle'])
										{
											$sel = 'selected="selected"';
										}
										else
										{
											$sel = '';
										}
									}
									else
									{
										$sel = '';
									}
									
									
									echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['vehicle'].'</option>';
								}
							echo '</select>
						</div>
						<div class="col-sm-3">
							<div class="group">
							  <input name="vehicle[parts][row_'.$i1.'][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" value="'.$vals['hours'].'">
							  <label class="im-label">Travel</label>
							  <label id="uerror"></label>
							</div>
                            
						</div>';
					echo $symbol;
					echo '</div>';
					echo '</div>';
				$i1++;
			}
			
			echo '<div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a></div></div></div>';
		  
		echo '<div class="row newboxes">
		<div class="col-sm-7">
		<div class="box" id="customFields">
		<label class="box-title parts">Parts</label>
		';   
   		$data=unserialize($row['additional_info']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		echo '<input type="hidden" value="'.count($data['parts']).'" class="invCount">';
		foreach($data['parts'] as $key=>$val)
	   {    
	  
		if($i==1)
		{   
			
			$symbol = '<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>';
		}
		else 
		{
			$class='remCF';
			$symbol = '<div class="add-more"><a href="javascript:void(0);" class="'.$class.'"><i class="fa fa-minus"></i></a></div>';
		}
	   
	   
		 echo ' <div class="row">			
				  <div class="col-sm-2 col-xs-3">
				  <div class="group">
					<input type="text" name="data[parts][row_'.$i.'][qty]"  value="'.$val['qty'].'">
					<label class="im-label">Qty </label>    
					</div> 
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				  <input class="inventory-list" type="text" name="data[parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">				  	
					<div class="inventory"></div>
				  </div>';
				  
				  echo $symbol;
				  
			   
			
		echo '</div>';
		$i++;
		}
		$equipments=unserialize($row['equipments']);
		$statement1 = $db->prepare("SELECT * FROM  `equipment`");	
		$statement1->execute();
		$result1 = $statement1->fetchAll();
	
		$html='';
		$i2=1;
		
		echo '</div></div><div class="col-sm-5"><label class="box-title"></label><input type="hidden" class="eupCount" value="'.count($equipments['parts']).'"><div class="equipmentFields">';
		foreach($equipments['parts'] as $key=>$vals1)
		{
			
			if($i2!=1)
			{   
				$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
			}
			else
			{
				$symbol = '';
			}
			echo '<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="equipmentRate">
						<select id="equipmentRate" name="equip[parts][row_'.$i2.'][equipment]" class="">
							<option value="">Select Equipment</option>';
							foreach($result1 as $val1)
							{
								if(!empty($vals1['equipment']))
								{
									if($val1['id'] == $vals1['equipment'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								
								
								echo '<option value="'.$val1['id'].'" '.$sel.'>'.$val1['equipment'].'</option>';
							}
						echo '</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="equip[parts][row_'.$i2.'][hours]" id="equiphours" value="'.$vals1['hours'].'" type="text" class="is-blank validNumber">
						  <label class="im-label">Hours </label>
						  <label id="uerror"></label>
						</div>
					</div>';
				echo $symbol;
				echo '</div>';
				
			$i2++;
		}
        echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a></div></div></div>';
	
		echo '<div class="group">
		  <textarea class="notes" name="notes" cols="" rows="">'.$row['notes'].'</textarea>
		  <label class="im-label">Notes</label>    
		  </div>
		  

		  <input type="hidden" value="updateElectricalWork" name="action"/>
		  <div class="clr"></div>
		  <div class="" align="centre">
		  <input type="submit" value="Update" name="submit" class="cs-btn btn-green"/>
		 
			<div id="message" style="color:#8EC94A"> </div>
			</p>
		  </div>
		</div>
  </form>
						 </div>
						</div>'; ?>
		<script>
			$(document).ready(function() {
				$(".close-edit").click(function() {
					$(".customer-profile > .customer-header").show('slow');
					$("#editFromToggle").slideUp("slow");
					$("#editFromWorksheet").slideUp("slow");
				});
			});
		</script>
					
				
				<?php }
				
/********************************************* generatorservices  ********************************/		

	if($table=='generatorservices')
	{
	$db = get_connection();
	$generator_hours=$row['generator_hours']; 
	$oil=$row['oil']; 
	$oil=json_decode($row['oil']);	
	$oil_filter=json_decode($row['oil_filter']);		
	$oil_recycling=$row['oil_recycling'];
	$full_service = $row['full_service'];
	$antifreeze=$row['antifreeze'];
	$antifreeze_disposal=$row['antifreeze_disposal'];
	$airfilter=json_decode($row['airfilter']);
	$battery=json_decode($row['battery']);
	
	$travel=$row['travel'];
	
	$notes=$row['notes'];
	$service_typee=$row['service_typee'];
	
	
	
		echo '
			 <div class="section-hdr">
            <h3>Edit Generator Service Worksheet</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
        </div>
		 <form role="form" action="files/update_sheet.php" class="cs-form" method="post" id="generator-service" autocomplete="off">
		  <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'" />
		  <div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
			</div>
		  <div class="table-responsive">
		 <div class="pull-left">
        <h3>Generator Service</h3>
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr"> 
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div>
   
    <div class="gen-ins-area clearfix">
      <div class="row">
        <div class="col-sm-6">
		 <input name="table" id="table" type="hidden" value="generatorservices">
		  <input name="sid" type="hidden" value="'.$id.'">	
		  <div class="group">
          <input name="data1[generator_hours]" class="validNumber" id="generator_hours" type="text"  value="'.$generator_hours.'">
		   <label class="im-label">Generator Hours</label> 
		  </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
              <div class="row">
                <div class="col-sm-4 col-xs-4">
				 <div class="group">
                  <input type="text" class="validNumber" name="data1[oil][qty]" value="'.$oil->qty.'">
				  <label class="im-label">Qty</label> 
				  </div>
                </div>
                <div class="col-sm-8 col-xs-8">
				 <div class="group">
                  <input  name="data1[oil][name]" id="oil" type="text" class="" value="'.$oil->name.'">
				  <label class="im-label">Oil</label> 
				  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
			 <div class="group">
              <input name="data1[oil_recycling]" id="oil_recycling" class="validNumber" type="text"  value="'.$oil_recycling.'">
			   <label class="im-label">Oil Recycling</label> 
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text" class="validNumber" name="data1[oil_filter][qty]" value="'.$oil_filter->qty.'">
			   <label class="im-label">Qty</label> 
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input name="data1[oil_filter][name]" id="oil_filter" class="" type="text"  value="'.$oil_filter->name.'">
			   <label class="im-label">Oil Filter</label> 
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
			 <div class="group">
              <input name="data1[antifreeze]" id="antifreeze" class="validNumber" type="text"  value="'.$antifreeze.'">
			   <label class="im-label">Antifreeze</label> 
			  </div>
            </div>
            <div class="col-sm-6">
			 <div class="group">
              <input name="data1[antifreeze_disposal]" class="validNumber" id="antifreeze_disposal" type="text" value="'.$antifreeze_disposal.'">
			  <label class="im-label">Antifreeze Disposal</label> 
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text" name="data1[airfilter][qty]" class="validNumber" value="'.$airfilter->qty.'">
			   <label class="im-label">Qty</label> 
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input name="data1[airfilter][name]" id="airfilter" class="" type="text" value="'.$airfilter->name.'">
			   <label class="im-label">Air Filter</label> 
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text"  class="validNumber" name="data1[battery][qty]" value="'.$battery->qty.'">
			  <label class="im-label">Qty</label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input name="data1[battery][name]" class="" id="battery" type="text"  value="'.$battery->name.'">
			  <label class="im-label">Battery</label>
			  </div>
            </div>
          </div>
        </div>';
        ?>
		<div class="col-sm-6"> 
          <p class="cs-radio" style="float:left">Service
            <label>
            <input name="service_type" value="full" onclick="get_service('1');" type="radio" <?php if($row['service_type']=='full'){ echo "checked='checked'";}?>>
            <span class="custm-radio"></span> Full</label>  
            <label>
            <input name="service_type" value="inspection" onclick="get_service('2');" type="radio" <?php if($row['service_type']=='inspection'){ echo "checked='checked'";}?>>  
            <span class="custm-radio"></span> Inspection</label>   
			
			
			</p>
		</div>  
        <?php
			$vehicles=unserialize($row['vehicles']);
			$statement = $db->prepare("SELECT * FROM  `vehicles`");	
			$statement->execute();
			$result = $statement->fetchAll();
		
			$html='';
			$i1=1;
			echo '<input type="hidden" class="vehCount" value="'.count($vehicles['parts']).'"><div class="col-sm-6"><div class="group"><div class="vehicleField">';
			foreach($vehicles['parts'] as $key=>$vals)
			{ 
				if($i1!=1)
				{   
					$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
				}
				else
				{
					$symbol = '';
				}
				echo '<div class="row">
						<div class="col-sm-8">
							<input type="hidden" class="vehicleRate">
							<select id="vehicleRate" name="vehicle[parts][row_'.$i1.'][vehicle]" class="">
								<option value="">Select Vehicles</option>';
								foreach($result as $val)
								{
									if(!empty($vals['vehicle']))
									{
										if($val['id'] == $vals['vehicle'])
										{
											$sel = 'selected="selected"';
										}
										else
										{
											$sel = '';
										}
									}
									else
									{
										$sel = '';
									}
									
									
									echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['vehicle'].'</option>';
								}
							echo '</select>
						</div>
						<div class="col-sm-3">
							<div class="group">
							  <input name="vehicle[parts][row_'.$i1.'][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" value="'.$vals['hours'].'">
							  <label class="im-label">Travel</label>
							  <label id="uerror"></label>
							</div>
						</div>';
					echo $symbol;
					echo '</div>';
				$i1++;
			}
			
			echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a></div></div></div>';
       
	  
	    echo '<div class="col-sm-7">
      <div class="box" id="customFields">
        <label class="box-title">Parts</label>';
        
		
		$data=unserialize($row['parts']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		echo '<input type="hidden" value="'.count($data['parts']).'" class="invCount">';
		
	   foreach($data['parts'] as $key=>$val)
	   {    
	   
		if($i==1)
		{   
			$symbol='<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class="'.$class.'"><i class="fa fa-plus"></i></a></div>';
		}
		else 
		{
			$class='remCF';
			$symbol='<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class="'.$class.'"><i class="fa fa-minus"></i></a></div>';
		}
	   
	   
		 echo ' <div class="row">			
				  <div class="col-sm-2 col-xs-3">
				   <div class="group">
					<input type="text" name="data[parts][row_'.$i.'][qty]"  value="'.$val['qty'].'">
					 <label class="im-label">Qty</label> 
					</div>
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				   <input class="inventory-list" type="text" name="data[parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">
					<div class="inventory"></div>
				  </div>';
				  echo $symbol;
				  
			   
			
		echo '</div>';
		$i++;
		}
		$equipments=unserialize($row['equipments']);
		$statement1 = $db->prepare("SELECT * FROM  `equipment`");	
		$statement1->execute();
		$result1 = $statement1->fetchAll();
	
		$html='';
		$i2=1;
		
		echo '</div></div><div class="col-sm-5"><label class="box-title"></label><input type="hidden" class="eupCount" value="'.count($equipments['parts']).'"><div class="equipmentFields">';
		foreach($equipments['parts'] as $key=>$vals1)
		{
			
			if($i2!=1)
			{   
				$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
			}
			else
			{
				$symbol = '';
			}
			echo '<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="equipmentRate">
						<select id="equipmentRate" name="equip[parts][row_'.$i2.'][equipment]" class="">
							<option value="">Select Equipment</option>';
							foreach($result1 as $val1)
							{
								if(!empty($vals1['equipment']))
								{
									if($val1['id'] == $vals1['equipment'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								
								
								echo '<option value="'.$val1['id'].'" '.$sel.'>'.$val1['equipment'].'</option>';
							}
						echo '</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="equip[parts][row_'.$i2.'][hours]" id="equiphours" value="'.$vals1['hours'].'" type="text" class="is-blank validNumber">
						  <label class="im-label">Hours </label>
						  <label id="uerror"></label>
						</div>
					</div>';
				echo $symbol;
				echo '</div>';
				
			$i2++;
		}
        echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a></div></div></div>';
		echo '<div class="row"><div class="col-md-12">'; 
			$statement = $db->prepare("SELECT * 
				FROM  `staff_timesheet` 
				WHERE  `work_id` ='".$id."' and work_sheet='generatorServices'");	
				$statement->execute();
				$result = $statement->fetchAll();
				$count=count($result);
				if($count > 0)
				{
					$counti = 1;
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
                        <div class="row">
                        <div class="col-md-8">
						<select name="staff_id[]" style="width:100%">
						<option value=""></option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row11) 
						{
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
						} 
						echo '</select></div>
                        <div class="col-md-3">
					   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
						if($counti != 1)
						{   
							echo '<div class="add-more valign"><a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')"><i class="fa fa-minus"></i></a></div>';
						}
						echo '</div></div>';  
						$counti++;
					
					}
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
				else
				{
					echo '<div class="n-box">
                    <div class="row">
                        <div class="col-md-8">
					<select name="staff_id[]" style="width:100%">
					<option value="">Select Staff</option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row11) 
					{
							
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select></div>
                  
                        <div class="col-md-3">
				   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
				   echo '</div></div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}	
	 echo '</div></div>';
	 echo '<div class="group">
      <textarea name="data1[notes]" id="notes" cols="" rows="" class="notes">'.$notes.'</textarea>
	   <label class="im-label">Notes</label> 
	  </div>
      
	   <input type="hidden" value="updateGeneratorservices" name="action"/>
	   
      '; 
	  ?>
		<div class="group">
			 <span style="float:right">
			 <label>
			   <input name="service_typee" value="service" type="radio" <?php if($service_typee=="service") { echo 'checked'; } ?>> 
			   SERVICE</label>
			 <label> 
			   <input name="service_typee" value="spring" type="radio" <?php if($service_typee=="spring") { echo 'checked'; } ?>>  
			   SPRING START-UP</label>   
			 <br>
			</span> 
		</div>
		<div class="clr"></div>
		<?php
      echo '<div class="">	
          <div align="centre"><label style="margin-right:15px;">NEXT SERVICE <span class="next_service"><b>';
		  if($row['service_type']=='full'){ echo "INSPECTION";} else { echo 'FULL';} 
		  echo '</b></span></label><input type="submit" value="Update" name="submit" class="cs-btn btn-green"/></div>
        <div id="message"> </div>
      </div> 
    </div>
  </form>
		
		';
	
	}
	/********************************************* generatorinspections  ********************************/
	if($table=='generatorinspections')
	{
	$db = get_connection();	
	$generator_hours=$row['generator_hours']; 
	$oil=$row['oil']; 
	$oil=json_decode($row['oil']);	
	$oil_filter=json_decode($row['oil_filter']);		
	$air_filter_type=json_decode($row['air_filter_type']);
	
	$antifreeze=$row['antifreeze'];
	$antifreeze_disposal=$row['antifreeze_disposal'];
	$airfilter=json_decode($row['airfilter']);
	$bettery=json_decode($row['battery']);
	
	$equipment=$row['equipment'];
	$labour=$row['labour'];
	$travel=$row['travel'];
	
	$notes=$row['notes'];
		
	echo '
	<div class="section-hdr">
            <h3>Edit Information</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
	 </div>
	<form action="files/update_sheet.php" method="post" role="form" class="cs-form" id="generator-inspection" autocomplete="off">
	 <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'" />
	 <div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
			</div>
		  <div class="table-responsive">
	
	 <div class="pull-left">
        <h3>NEW WORKSHEET <span class="sepr">|</span> Generator Inspection</h3>
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div>
	  
	
    
    <div class="gen-ins-area clearfix">
      <div class="row">
        <div class="col-sm-6">  
		<input name="table" id="table" type="hidden" value="generatorinspections">
		  <input name="sid" type="hidden" value="'.$id.'">	
		  <div class="group">       
          <input name="data[generator_hours]"  id="generator_hours" type="text" placeholder="Generator Hours"  value="'.$generator_hours.'">
		  <label class="im-label">Generator Hours</label> 
		  </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group"> 
              <input type="text" name="data[oil][qty]" class=""  value="'.$oil->qty.'">
			   <label class="im-label">Qty </label>
			   </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			<div class="group">   
              <input name="data[oil][name]" id="oil" type="text" value="'.$oil->name.'">
			   <label class="im-label">Oil </label>
			 </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			<div class="group">   
               <input type="text" name="data[oil_filter][qty]"  value="'.$oil_filter->qty.'">
			    <label class="im-label">Qty </label>
			   </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			<div class="group">   
              <input name="data[oil_filter][name]" id="oil_filter" type="text"  value="'.$oil_filter->name.'">
			   <label class="im-label">Oil Filter </label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
		<div class="group">   
          <input name="data[antifreeze]" id="antifreeze" type="text" value="'.$antifreeze.'">
		   <label class="im-label">Antifreeze </label>
		  </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			<div class="group">   
               <input type="text" name="data[air_filter_type][qty]"  value="'.$air_filter_type->qty.'">
			    <label class="im-label">Qty </label>
			   </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			<div class="group">   
              <input type="text"  name="data[air_filter_type][name]"  value="'.$air_filter_type->name.'">
			   <label class="im-label">Air Filter Type </label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			<div class="group">   
              <input type="text" name="data[bettery][qty]"  value="'.$bettery->qty.'">
			   <label class="im-label">Bettery </label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			<div class="group">   
              <input type="text" name="data[bettery][name]"  value="'.$bettery->name.'">
			   <label class="im-label">Battery Type </label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
		<div class="group">   
          <input name="data[travel]" id="travel" type="text"  value="'.$travel.'">
		   <label class="im-label">Travel </label>
		  </div>
        </div>
        <div class="col-sm-6">
		<div class="group">   
         <div class="group">';
			$statement = $db->prepare("SELECT * 
				FROM  `staff_timesheet` 
				WHERE  `work_id` ='".$id."' and work_sheet='gen_insp'");	
				$statement->execute();
				$result = $statement->fetchAll();
				$count=count($result); 
				if($count > 0)
				{	
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
						<select name="staff_id[]" style="width:40%">
						<option value=""></option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row11) 
						{
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
						} 
						echo '</select>
					   <input name="hour[]" type="text" class="is-blank" placeholder="Hours" style="width:40%" value="'.$row1['hours'].'">
					   <a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')">Remove</a>
					</div>';  
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
					}
				}
				else
				{
					echo '<div class="n-box">
					<select name="staff_id[]" style="width:40%">
					<option value=""></option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row11) 
					{
							
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select>
				   <input name="hour[]" type="text" class="is-blank" placeholder="Hours" style="width:40%" value="'.$row1['hours'].'">';
				   echo '</div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				} 	
			
			
        echo '</div>
		  </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="box new_boxs" id="customFields">
        <label class="box-title">Parts</label>
        ';
		
		$data=unserialize($row['parts']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		
	   foreach($data['parts'] as $key=>$val)
	   {    
	  
		if($i==1)
		{   
			$class='editAddmore_serv';
			$symbol='fa-plus';
		}
		else 
		{
			$class='remCF';
			$symbol='fa-minus';
		}
	   
	   
		 echo ' <div class="row">			
				  <div class="col-sm-2 col-xs-3">
				   <div class="group">  
					<input type="text" name="data[com][parts][row_'.$i.'][qty]" value="'.$val['qty'].'">
					 <label class="im-label">Qty </label>
					 </div>
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[com][parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				  <input class="inventory-list" type="text"  name="data[com][parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">
					<div class="inventory"></div>
				  </div>
				  <div class="add-more addCF"><a href="javascript:void(0);" class="'.$class.'"><i class="fa '.$symbol.'"></i></a></div>
			   
			
		</div>';
		$i++;
		}
	
	
    echo '  </div>
        </div>
        <div class="col-sm-5">
          <label class="box-title"></label>
          <div class="row">
            <div class="col-sm-6">
			<div class="group">   
              <input name="data[equipment]" id="equipment" type="text" value="'.$equipment.'">
			   <label class="im-label">Equipment </label>
			  </div>
            </div>
            <div class="col-sm-6">
			<div class="group">   
              <input name="hours" id="hours" type="text"  value="'.$generator_hours.'">
			   <label class="im-label">Hours </label>
			  </div>
            </div>
          </div>
        </div>
      </div>
	  <div class="group">   
      <textarea name="data[notes]" id="notes" cols="" rows="" class="notes" > '.$notes.'</textarea>
	   <label class="im-label">Notes </label>
	  </div>
      <input type="hidden" value="updateGeneratorInspection" name="action"/>
      <div class="clr"></div>
      <div class="actions">       
        <input type="submit" value="Update" name="submit" class="cs-btn btn-green"/>
       
        </p>
      </div>
    </div>
  </form>';
	
	}
	/********************************************* generatorestimates  ********************************/
	if($table=='generatorestimates')
	{
	$db = get_connection();		
	$generator_hours=$row['generator_hours'];	
	$antifreeze=$row['antifreeze'];	
	$battery=$row['battery'];
	$travel=$row['travel'];	
	
	$airfilter=json_decode($row['airfilter']);
	$labour=$row['labour'];
	$equipment=$row['equipment'];
	$notes=$row['notes'];
		
	echo '
	<div class="section-hdr">
            <h3>Edit Generator Estimate Worksheet</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
	 </div>
	<form role="form" action="files/update_sheet.php" method="post" class="cs-form" id="generator-estimate" autocomplete="off">
     <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'"/>
	 <div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
			</div>
		
		 
		  <div class="table-responsive">
	 <div class="pull-left">
        <h3>Generator Estimate</h3>
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div>
	
	
    <div class="gen-ins-area clearfix">
      <div class="row">
         <div class="col-sm-6">  
			<input name="table" id="table" type="hidden" value="generatorestimates">
		 	<input name="sid" type="hidden" value="'.$id.'">	
			<div class="group">        
         	<input name="generator_hours" class="validNumber" id="generator_hours" type="text"   value="'.$generator_hours.'">
			<label class="im-label">Generator Hours </label>
			</div>
        </div>
        <div class="col-sm-6">
		<div class="group"> 
          <input name="antifreeze" id="antifreeze" class="validNumber" type="text" value="'.$antifreeze.'">
		  <label class="im-label">Antifreeze </label>
		  </div>
        </div>
        <div class="col-sm-6">
		<div class="group"> 
          <input name="battery"  id="battery" class="validNumber" type="text" value="'.$battery.'">
		  <label class="im-label">Battery </label>
		  </div>
        </div>';
		
		$vehicles=unserialize($row['vehicles']);
			$statement = $db->prepare("SELECT * FROM  `vehicles`");	
			$statement->execute();
			$result = $statement->fetchAll();
		
			$html='';
			$i1=1;
			echo '<div class="col-sm-6"><input type="hidden" class="vehCount" value="'.count($vehicles['parts']).'"><div class="group"><div class="vehicleField">';
			foreach($vehicles['parts'] as $key=>$vals)
			{ 
				if($i1!=1)
				{   
					$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
				}
				else
				{
					$symbol = '';
				}
				echo '<div class="row">
						<div class="col-sm-8">
							<input type="hidden" class="vehicleRate">
							<select id="vehicleRate" name="vehicle[parts][row_'.$i1.'][vehicle]" class="">
								<option value="">Select Vehicles</option>';
								foreach($result as $val)
								{
									if(!empty($vals['vehicle']))
									{
										if($val['id'] == $vals['vehicle'])
										{
											$sel = 'selected="selected"';
										}
										else
										{
											$sel = '';
										}
									}
									else
									{
										$sel = '';
									}
									
									
									echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['vehicle'].'</option>';
								}
							echo '</select>
						</div>
						<div class="col-sm-3">
							<div class="group">
							  <input name="vehicle[parts][row_'.$i1.'][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" value="'.$vals['hours'].'">
							  <label class="im-label">Travel</label>
							  <label id="uerror"></label>
							</div>
						</div>';
					echo $symbol;
					echo '</div>';
				$i1++;
			}
			
		echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a></div></div></div>';
       
        echo '<div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			<div class="group"> 
              <input type="text" name="airfilter[qty]" class="validNumber" value="'.$airfilter->qty.'">
			  <label class="im-label">Qty </label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			<div class="group"> 
              <input name="airfilter[name]" id="airfilter" class="validNumber" type="text"  value="'.$airfilter->name.'">
			  <label class="im-label">Air Filter </label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
		<div class="group1"> 
          <div class="group">';
			$statement = $db->prepare("SELECT * 
				FROM  `staff_timesheet` 
				WHERE  `work_id` ='".$id."' and work_sheet='gen_repair_est'");	
				$statement->execute();
				$result = $statement->fetchAll();
				$count=count($result);
				if($count > 0)
				{
					$counti = 1;
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
                        <div class="row">
                        <div class="col-md-8">
						<select name="staff_id[]" style="width:100%">
						<option value=""></option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row11) 
						{
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
						} 
						echo '</select>
                        </div>
                        <div class="col-md-3">
					   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
						if($counti != 1)
						{   
							echo '<div class="add-more valign"><a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')"><i class="fa fa-minus spaced"></i></a></div>';
						}
						echo '</div></div>';  
						$counti++;
					
					}
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
				else
				{
					echo '<div class="n-box">
					<select name="staff_id[]" style="width:100%">
					<option value="">Select Staff</option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row11) 
					{
							
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select>
				   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'">';
				   echo '</div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}	
			
			
        echo '</div>
		  </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="box" id="customFields">
        <label class="box-title">Parts</label>
        ';
		
		$data=unserialize($row['parts']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		echo '<input type="hidden" value="'.count($data['parts']).'" class="invCount">';
		
		
	   foreach($data['parts'] as $key=>$val)
	   {    
	  
		if($i==1)
		{   
			$symbol='<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>';
		}
		else 
		{
			$symbol='<div class="add-more"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
		}
	   
	   
		 echo ' <div class="row">			
				  <div class="col-sm-2 col-xs-3">
				  <div class="group"> 
					<input type="text" name="data[parts][row_'.$i.'][qty]" value="'.$val['qty'].'">
					<label class="im-label">Qty </label>
					</div>
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				  <input class="inventory-list" type="text"  name="data[parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">
					<div class="inventory"></div>
				  </div>';

					echo $symbol;
			   
			
		echo '</div>';
		$i++;
		}
		
		$equipments=unserialize($row['equipments']);
		$statement1 = $db->prepare("SELECT * FROM  `equipment`");	
		$statement1->execute();
		$result1 = $statement1->fetchAll();
	
		$html='';
		$i2=1;
		
		echo '</div></div><div class="col-sm-5"><label class="box-title"></label><div class="equipmentFields">';
		foreach($equipments['parts'] as $key=>$vals1)
		{
			
			if($i2!=1)
			{   
				$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
			}
			else
			{
				$symbol = '';
			}
			echo '<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="equipmentRate">
						<select id="equipmentRate" name="equip[parts][row_'.$i2.'][equipment]" class="">
							<option value="">Select Equipment</option>';
							foreach($result1 as $val1)
							{
								if(!empty($vals1['equipment']))
								{
									if($val1['id'] == $vals1['equipment'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								
								
								echo '<option value="'.$val1['id'].'" '.$sel.'>'.$val1['equipment'].'</option>';
							}
						echo '</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="equip[parts][row_'.$i2.'][hours]" id="equiphours" value="'.$vals1['hours'].'" type="text" class="is-blank validNumber">
						  <label class="im-label">Hours </label>
						  <label id="uerror"></label>
						</div>
					</div>';
				echo $symbol;
				echo '</div>';
				
			$i2++;
		}
        echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a></div></div></div>';
	
	
    
	  echo '<div class="group"> 
      <textarea name="notes" id="notes" cols="" rows="" class="notes">'.$notes.'</textarea>
	  <label class="im-label">Notes </label>
	  </div>
      <input type="hidden" value="updateGeneratorEstimate" name="action"/>
      <div class="clr"></div>
      <div class="">
        
		<div align="center"><input type="submit" value="Update" name="submit" class="cs-btn btn-green"/></div>
      </div>
    </div>
  </form>';
	
	}
	
	
	/********************************************* generatortroubles  ********************************/
	if($table=='generatortroubles')
	{
	$db = get_connection();			
	$generator_hours=$row['generator_hours'];	
	$antifreeze=$row['antifreeze'];		
	$travel=$row['travel'];		
	$oil=json_decode($row['oil']);
	$airfilter=json_decode($row['airfilter']);
	$battery=json_decode($row['battery']);
	$oil_filter=json_decode($row['oil_filter']);
	$labour=$row['labour'];
	$equipment=$row['equipment'];
	$notes=$row['notes'];
	$trouble_status=$row['trouble_status'];
		
	echo ' 
	<div class="section-hdr">
            <h3>Edit Generator Trouble Worksheet</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
	 </div>
	<form role="form" action="files/update_sheet.php" method="post" class="cs-form" id="generator-troubles" autocomplete="off">  
	 <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'" />
	 <div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
			</div>
	 
		  <div class="table-responsive">
	 <div class="pull-left">
        <h3>Generator Trouble</h3>
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
		  <input name="sid" type="hidden" value="'.$id.'">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div>  
    
    <div class="gen-ins-area clearfix">
      <div class="row">
        <input name="data1[customer_id]" id="customer_id" type="hidden"> 
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-4 col-xs-4">
              <div class="group">
                <input type="text" name="data1[oil][qty]" class="validNumber" value="'.$oil->qty.'">
                <label class="im-label">Qty</label>
              </div>
            </div>
            <div class="col-sm-8 col-xs-8">
              <div class="group">
                <input  name="data1[oil][name]" id="oil" class="" type="text" value="'.$oil->name.'">
                <label class="im-label">Oil</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
              <div class="group">
                <input type="text" name="data1[oil_filter][qty]" class="validNumber" value="'.$oil_filter->qty.'">
                <label class="im-label">Qty</label>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8">
              <div class="group">
                <input name="data1[oil_filter][name]" id="oil_filter" class="" type="text" value="'.$oil_filter->name.'">
                <label class="im-label">Oil Filter</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="group">
            <input name="data1[antifreeze]" id="antifreeze" class="validNumber" type="text" value="'.$antifreeze.'">
            <label class="im-label">Antifreeze</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
              <div class="group">
                <input type="text"  name="data1[airfilter][qty]" class="validNumber" value="'.$airfilter->qty.'">
                <label class="im-label">Qty</label>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8">
              <div class="group">
                <input name="data1[airfilter][name]" id="airfilter" class="" type="text" value="'.$airfilter->name.'">
                <label class="im-label">Air Filter</label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
              <div class="group">
                <input type="text"   name="data1[battery][qty]" class="validNumber" value="'.$battery->qty.'">
                <label class="im-label">Qty</label>
              </div>
            </div>
            <div class="col-sm-9 col-xs-8">
              <div class="group">
                <input name="data1[battery][name]"  id="battery" class="" type="text" value="'.$battery->name.'">
                <label class="im-label">Battery</label>
              </div>
            </div>
          </div>
        </div>';
       
			$vehicles=unserialize($row['vehicles']);
			$statement = $db->prepare("SELECT * FROM  `vehicles`");	
			$statement->execute();
			$result = $statement->fetchAll();
		
			$html='';
			$i1=1;
			echo '<div class="col-sm-6"><div class="group"><input type="hidden" class="vehCount" value="'.count($vehicles['parts']).'"><div class="vehicleField">';
			foreach($vehicles['parts'] as $key=>$vals)
			{ 
				if($i1!=1)
				{   
					$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
				}
				else
				{
					$symbol = '';
				}
				echo '<div class="row">
						<div class="col-sm-8">
							<input type="hidden" class="vehicleRate">
							<select id="vehicleRate" name="vehicle[parts][row_'.$i1.'][vehicle]" class="">
								<option value="">Select Vehicles</option>';
								foreach($result as $val)
								{
									if(!empty($vals['vehicle']))
									{
										if($val['id'] == $vals['vehicle'])
										{
											$sel = 'selected="selected"';
										}
										else
										{
											$sel = '';
										}
									}
									else
									{
										$sel = '';
									}
									
									
									echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['vehicle'].'</option>';
								}
							echo '</select>
						</div>
						<div class="col-sm-3">
							<div class="group">
							  <input name="vehicle[parts][row_'.$i1.'][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" value="'.$vals['hours'].'">
							  <label class="im-label">Travel</label>
							  <label id="uerror"></label>
							</div>
						</div>';
					echo $symbol;
					echo '</div>';
				$i1++;
			}
			
			echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a></div></div></div>';
        echo '<div class="col-sm-6">
          <div class="group" style="display:none;">
            <input name="labour" id="labour" type="text" value="'.$labour.'" style="display:none;">
            <label class="im-label" style="display:none;">Labour</label>
          </div>
		   <div class="group">';
			$statement = $db->prepare("SELECT * 
				FROM  `staff_timesheet` 
				WHERE  `work_id` ='".$id."' and work_sheet='gen_trouble'");	
				$statement->execute();
				$result = $statement->fetchAll(); 
				$count=count($result);
				if($count > 0)
				{
					$counti	= 1;				
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
                        <div class="row">
                        <div class="col-md-8">
						<select name="staff_id[]" style="width:100%">
						<option value="">Select Staff</option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row11) 
						{
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
						} 
						echo '</select></div>
                         <div class="col-md-3">
					   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
						if($counti != 1)
						{   
							echo '<div class="add-more valign"><a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')"><i class="fa spaced fa-minus"></i></a></div>';
						}
						echo '</div></div>';
						$counti++;
					
					}
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
				else
				{
					echo '<div class="n-box">
                     <div class="row">
                        <div class="col-md-8">
					<select name="staff_id[]" style="width:100%">
					<option value=""></option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row11) 
					{
							
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select></div>
                   
                        <div class="col-md-3">
				   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
				   echo '</div></div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}	
			
			
        echo '</div>
		  
		  
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="box" id="customFields">
            <label class="box-title">Parts</label>';
			
           $data=unserialize($row['parts']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		echo '<input type="hidden" value="'.count($data['parts']).'" class="invCount">';
	   foreach($data['parts'] as $key=>$val)
	   {    
	  
		if($i==1)
		{  
			$symbol= '<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>';	
		}
		else 
		{
			$symbol= '<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
		}
	   
	   
		 echo ' <div class="row">			
				  <div class="col-sm-2 col-xs-3">
				  <div class="group"> 
					<input type="text" name="data[parts][row_'.$i.'][qty]" value="'.$val['qty'].'" >
					<label class="im-label">Qty </label> 
					</div>
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				  <input class="inventory-list" type="text"  name="data[parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">
					<div class="inventory"></div>
				  </div>';
				  
			   echo $symbol;
			
		echo '</div>';
		$i++;
		}
        $equipments=unserialize($row['equipments']);
		$statement1 = $db->prepare("SELECT * FROM  `equipment`");	
		$statement1->execute();
		$result1 = $statement1->fetchAll();
	
		$html='';
		$i2=1;
		
		echo '</div></div><div class="col-sm-5"><input type="hidden" class="eupCount" value="'.count($equipments['parts']).'"><label class="box-title"></label><div class="equipmentFields">';
		foreach($equipments['parts'] as $key=>$vals1)
		{
			
			if($i2!=1)
			{   
				$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
			}
			else
			{
				$symbol = '';
			}
			echo '<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="equipmentRate">
						<select id="equipmentRate" name="equip[parts][row_'.$i2.'][equipment]" class="">
							<option value="">Select Equipment</option>';
							foreach($result1 as $val1)
							{
								if(!empty($vals1['equipment']))
								{
									if($val1['id'] == $vals1['equipment'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								
								
								echo '<option value="'.$val1['id'].'" '.$sel.'>'.$val1['equipment'].'</option>';
							}
						echo '</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="equip[parts][row_'.$i2.'][hours]" id="equiphours" value="'.$vals1['hours'].'" type="text" class="is-blank validNumber">
						  <label class="im-label">Hours </label>
						  <label id="uerror"></label>
						</div>
					</div>';
				echo $symbol;
				echo '</div>';
				
			$i2++;
		}
        echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a></div></div></div>';
      echo '<div class="group">
        <textarea name="notes" id="notes" class="notes" cols="" rows="">'.$notes.'</textarea>
        <label class="im-label">Diagnosis</label>
      </div>
	  <div class="group">
		 <span style="float:right">';
		 ?>
		 <label>
		   <input name="trouble_status" value="complete" type="radio" <?php if($trouble_status=="complete"){ echo "checked";} ?>>
		   COMPLETE</label>
		 <label>
		   <input name="trouble_status" value="repair" type="radio" <?php if($trouble_status=="repair"){ echo "checked";} ?>>
		   IN PROGRESS REPAIR</label>  
		 <?php
		 echo '<br>
		</span>
		</div>
	  
      <input type="hidden" value="updateGeneratorTrouble" name="action"/>
      <div class="clr"></div>
      <div class="" align="centre">              
         <input type="submit" value="Update" name="submit" class="cs-btn btn-green"/>        
      </div>
    </div>';
	
	}
	
/********************************************* generatorstorages  ********************************/
	if($table=='generatorstorages')
	{
	
	$db = get_connection();	
	$generator_hours=$row['generator_hours'];	
	$fogging_oil=$row['fogging_oil'];	
	$battery_storage_charging=$row['battery_storage_charging'];
	$travel=$row['travel'];		
	$labour=$row['labour'];
	$equipment=$row['equipment'];
	$notes=$row['notes'];
		
	echo '
	<div class="section-hdr">
            <h3>Edit Generator Storages Worksheet</h3>
            <a href="javascript:void(0);" class="close close-edit"><i class="fa fa-plus"></i></a>
	 </div>
	<form role="form" action="files/update_sheet.php" method="post" class="cs-form" id="generator-storages" autocomplete="off">   
	 <input id="customer_id" type="hidden" name="customer_id"  value="'.$customer_id.'" />
		<div class="alert alert-warning alert-warning-msg" role="alert" style="display:none;">
				This is a warning alert—check it out!
		</div>
		  <div class="table-responsive"> 
     <div class="pull-left">
        <h3>Generator Storages</h3>
        <p class="search-customer"><span id="username"></span>
		
          <input type="text" name="searchname" id="searchname" Placeholder="Search Customer..." value="'.$display_name.'" autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">         
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number"   value="'.$po_number.'"/>
			<label class="im-label">PO Number </label>            
          </div>
        </div>
      </div> 
	
	
	
    <div class="gen-ins-area clearfix">
      <div class="row">
        <div class="col-sm-6">
          <input name="table" id="table" type="hidden" value="generatorstorages">
		  <input name="sid" type="hidden" value="'.$id.'">
		  <div class="group"> 
          <input name="generator_hours"  id="generator_hours" class="validNumber" type="text"  value="'.$generator_hours.'">
		   <label class="im-label">Generator Hours </label>
		  
		  </div>
        </div>
        <div class="col-sm-6">
		<div class="group"> 
          <input name="fogging_oil"  id="fogging_oil" type="text" class="validNumber" value="'.$fogging_oil.'">
		  <label class="im-label">Generator Hours </label>
		  </div>
        </div>
        <div class="col-sm-12 gws">
          <p class="cs-radio">Battery Storage & Charging
            <label>
              <input type="radio" name="battery_storage_charging" value="1" id="RadioGroup1_0" checked>
              <span class="custm-radio"></span> 1</label>
            <label>
              <input type="radio" name="battery_storage_charging" value="2" id="RadioGroup1_1">
              <span class="custm-radio"></span> 2</label>
            <label>
              <input type="radio" name="battery_storage_charging" value="3" id="RadioGroup1_2">
              <span class="custm-radio"></span> 3</label>
          </p>
        </div>';
			$vehicles=unserialize($row['vehicles']);
			$statement = $db->prepare("SELECT * FROM  `vehicles`");	
			$statement->execute();
			$result = $statement->fetchAll();
		
			$html='';
			$i1=1;
			echo '<div class="col-sm-6"><div class="group"><input type="hidden" class="vehCount" value="'.count($vehicles['parts']).'"><div class="vehicleField">';
			foreach($vehicles['parts'] as $key=>$vals)
			{ 
				if($i1!=1)
				{   
					$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
				}
				else
				{
					$symbol = '';
				}
				echo '<div class="row">
						<div class="col-sm-8">
							<input type="hidden" class="vehicleRate"> 
							<select id="vehicleRate" name="vehicle[parts][row_'.$i1.'][vehicle]" class="">
								<option value="">Select Vehicles</option>';
								foreach($result as $val)
								{
									if(!empty($vals['vehicle']))
									{
										if($val['id'] == $vals['vehicle'])
										{
											$sel = 'selected="selected"';
										}
										else
										{
											$sel = '';
										}
									}
									else
									{
										$sel = '';
									}
									
									
									echo '<option value="'.$val['id'].'" '.$sel.'>'.$val['vehicle'].'</option>';
								}
							echo '</select>
						</div>
						<div class="col-sm-3">
							<div class="group">
							  <input name="vehicle[parts][row_'.$i1.'][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" value="'.$vals['hours'].'">
							  <label class="im-label">Travel</label>
							  <label id="uerror"></label>
							</div>
						</div>';
					echo $symbol;
					echo '</div>';
				$i1++;
			}
			
		echo '</div> <div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a></div></div></div>';
        echo '<div class="col-sm-6">
		<div class="group" style="display:none;">  
          <input name="labour" id="labour"  type="text" value="'.$labour.'">
		  <label class="im-label">Labour </label>
		  </div>
		  <div class="group">';
			$statement = $db->prepare("SELECT * 
				FROM  `staff_timesheet` 
				WHERE  `work_id` ='".$id."' and work_sheet='gen_win_storage'");	
				$statement->execute();
				$result = $statement->fetchAll();
				$count=count($result);
				if($count > 0)
				{
					$counti = 1;
					foreach($result as $row1) 
					{
						$row_id=$row1['id'];
						$staff_id=$row1['staff_id'];
						echo '<div class="n-box box_'.$row_id.'">
                        <div class="row">
                        <div class="col-md-8">
						<select name="staff_id[]" style="width:100%">
						<option value="">Select Staff</option>'; 
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						
						foreach($result as $row11) 
						{
							
							if($staff_id==$row11['id'])
							{
								$asd='selected';
							}
							else
							{
								$asd=''; 
							}		
							echo '<option value="'.$row11['id'].'" '.$asd.' >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
							
						} 
						echo '</select></div>
                         <div class="col-md-3">
					   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%" value="'.$row1['hours'].'"></div>';
						if($counti != 1)
						{   
							echo '<a href="javascript:void(0);" onclick="remove_ul(1,'.$row_id.')"><i class="fa fa-minus spaced"></i></a>';
						}
						echo '</div></div>';

						$counti++;						
					
					}
					echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}
				else
				{
					echo '<div class="n-box">
					<select name="staff_id[]" style="width:40%">
					<option value=""></option>'; 
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					
					
					foreach($result as $row11) 
					{
						
						echo '<option value="'.$row11['id'].'" >'.$row11['first_name'].' '.$row11['last_name'].'</option>'; 
					} 
					echo '</select>
				   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:40%" value="'.$row1['hours'].'">';
				   echo '</div>';
				   echo '<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>';	
				}	
			
			
        echo '</div>
		  
		  
        </div>
      </div>
      <div class="row"><div class="col-sm-7"><div class="box" id="customFields">
        <label class="box-title">Parts</label> ';
		
	  	$data=unserialize($row['parts']);
		$sql1 ="SELECT DISTINCT category FROM  `inventory`  ";
		$catresult=mysql_query($sql1);		 
		
		$html='';
		while($cat_row=mysql_fetch_assoc($catresult))
		{			
			$item=rtrim($cat_row["category"]," ");			
			$html.="<option value='".$item."' >".$item."</option>";
		} 
		$i=1;
		echo '<input type="hidden" value="'.count($data['parts']).'" class="invCount">';
	   foreach($data['parts'] as $key=>$val)
	   {    
	  
		if($i==1)
		{   
			$symbol='<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>';
		}
		else 
		{
			$symbol='<div class="add-more editAddmore_serv"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
		} 
          
		 echo '<div class="row">			
				  <div class="col-sm-2 col-xs-3">
				  <div class="group"> 
					<input type="text" name="data[parts][row_'.$i.'][qty]" value="'.$val['qty'].'">
					<label class="im-label">Qty </label>
					</div>
				  </div>
				  <div class="col-sm-4 col-xs-4">
					<select id="" class="select" name="data[parts][row_'.$i.'][cat]"> 
					';
					
					if(empty($val['cat']))
					{
						echo '<option value="">Select Category</option>';
						echo $html;
					}
					else 
					{
						echo '<option value="'.$val['cat'].'">'.$val['cat'].'</option>';
						echo $html;
					}
					
				echo '</select>
				  </div>
				  <div class="col-sm-5 col-xs-4">
				  <input class="inventory-list" type="text" name="data[parts][row_'.$i.'][inventory]" value="'.$val['inventory'].'" placeholder="Inventory">
					<div class="inventory"></div>
				  </div>';
				  
				  echo $symbol;
			   
			
		echo '</div>';
		$i++;
		}
	
	
		$equipments=unserialize($row['equipments']);
		$statement1 = $db->prepare("SELECT * FROM  `equipment`");	
		$statement1->execute();
		$result1 = $statement1->fetchAll();
	
		$html='';
		$i2=1;
		
		echo '</div></div><div class="col-sm-5"><input type="hidden" class="eupCount" value="'.count($equipments['parts']).'"><label class="box-title"></label><div class="equipmentFields">';
		foreach($equipments['parts'] as $key=>$vals1)
		{
			
			if($i2!=1)
			{   
				$symbol='<div class="add-more valign"><a href="javascript:void(0);" class="remCF"><i class="fa fa-minus"></i></a></div>';
			}
			else
			{
				$symbol = '';
			}
			echo '<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="equipmentRate">
						<select id="equipmentRate" name="equip[parts][row_'.$i2.'][equipment]" class="">
							<option value="">Select Equipment</option>';
							foreach($result1 as $val1)
							{
								if(!empty($vals1['equipment']))
								{
									if($val1['id'] == $vals1['equipment'])
									{
										$sel = 'selected="selected"';
									}
									else
									{
										$sel = '';
									}
								}
								else
								{
									$sel = '';
								}
								
								
								echo '<option value="'.$val1['id'].'" '.$sel.'>'.$val1['equipment'].'</option>';
							}
						echo '</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="equip[parts][row_'.$i2.'][hours]" id="equiphours" value="'.$vals1['hours'].'" type="text" class="is-blank validNumber">
						  <label class="im-label">Hours </label>
						  <label id="uerror"></label>
						</div>
					</div>';
				echo $symbol;
				echo '</div>';
				
			$i2++;
		}
        echo '</div><div class="left_aligned"><a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a></div></div></div>';
		echo '<div class="group"> 
      <textarea name="notes" id="notes" cols="" rows="" class="notes" >'.$notes.'</textarea>
	  <label class="im-label">Notes </label>
	  </div>
      <input type="hidden" value="updateGeneratorStorage" name="action"/>
      <div class="clr"></div> 
     
		<div align="center"><input type="submit" value="Update" name="submit" class="cs-btn btn-green"/></div>
    </div>
	</form>
	';
	
	}

}


function addTimecardStaff()
{
	$db = get_connection();
	unset($_REQUEST['action']); 
	$id=$_REQUEST['id'];  
	$count=$id+1;
?>

<div class="n-box box_<?php echo $count; ?>">
    <div class="row">
    <div class="col-md-8">
	  <select name="staff_id[]" style="width:100%">
		<option value="">Select Staff</option> 
		<?php
		$statement = $db->prepare("select * from users where id!='1'");	
		$statement->execute();
		$result = $statement->fetchAll();
		foreach($result as $row)
		{
		?>
			<option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
		<?php 
		}
		?>
	  </select></div>
<div class="col-md-3">
	   <input name="hour[]" type="text" class="is-blank validNumber" placeholder="Hours" style="width:100%">
        </div>
        <div class="add-more valign">
	   <a href="javascript:void(0);" onclick="remove_ul('1','<?php echo $count; ?>')"><i class="fa fa-minus spaced"></i></a></div>
    </div>
</div> 
<?php	
}

function ShowReportTime()
{
	$db = get_connection();
	unset($_REQUEST['action']); 
	 $curr_monday=$_REQUEST['mon'];  
	 $curr_saturday=$_REQUEST['sat'];   
?>
<script>
jQuery('#add_customer').validate({
		
		rules: {
			'staff_id[]': {
				required: true 
			}
		},
		submitHandler: function(form) {
			var staff_id='';
			var curr_mon=jQuery('#add_customer').find('input[name=curr_monday]').val();
			var curr_sun=jQuery('#add_customer').find('input[name=curr_sunday]').val();
			jQuery.each(jQuery(".asd>select"), function()  
			{
				staff_id += (staff_id?',':'') + jQuery(this).val();
			});  
			
			var link='<a  class="custom edit_cstm cs-btn btn-green" target="_blank" href=<?php echo SITE_URL; ?>pdft.php?mon='+curr_mon+'&sun='+curr_sun+'&staff_id='+staff_id+' onclick="hide_all();"> Create Report</a>';
			jQuery('.gen_report').show();
			jQuery('.gen_report').empty().append(link);  
			
			
		}   
		
	});
	

function hide_all()
{
	jQuery('.add_cust_box').hide(); 
}	

function get_value()
{
	
}

</script>
<div class="new_custmer"> 
<a href="javascript:void(0);" onclick="jQuery('.add_cust_box').slideUp();" class="close_new"><i class="fa fa-times" aria-hidden="true"></i></a>
<h4 class="close_title">NEW REPORT</h4>
</div>
<div class="close_container custmr_edt_cnt mb-20 pb-20">
<form method="post" action="" id="add_customer" name="add_customer" target="_blank">  
	<input type="hidden" value="AddCustomer" name="action">	
	
	<input type="hidden" value="<?php echo session_id(); ?>" id="session_id" name="session_id">	
	<input type="hidden" value="<?php echo $curr_monday ?>" name="curr_monday">	 
	<input type="hidden" value="<?php echo $curr_saturday; ?>" name="curr_sunday">	
	
	<div class="cstm-wid btm_inpt new_report">
		<div class="col-1">								 
			<ul>
				<li>
					<h4><?php echo date('F d, Y', strtotime($curr_monday)); ?> – <?php echo date('F d, Y', strtotime($curr_saturday)); ?></h4>
				</li>
			</ul>
		</div>
		<div class="col-1 second-col-1">
			<h4> Staff Name</h4>
			<ul>
				<li>
					<div class="selectn_box pb-20"> 
						<div class="asd">
							<select class="show-menu-arrow cs-select valid" name="staff_id[]" id="staff_id" onchange="get_value();">
								<option value=""></option>
								<?php
								$sql="select * from users where id!='1'";
								$statement = $db->prepare($sql);	
								$statement->execute();
								$result = $statement->fetchAll();
								foreach($result as $row) 
								{	
									$staff_id=$row['id'];
									$first_name=$row['first_name'];
									$last_name=$row['last_name'];
									$name=$first_name.' '.$last_name;
								?>
									<option value="<?php echo $staff_id; ?>" ><?php echo $name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					
					</div>
					<div class="text-center add-staff"><a onclick="add_timecard_staff_11();" class="custom edit_cstm cs-btn btn-blue" href="javascript:void(0);">Add Staff +</a></div>
				</li>
				
				
			</ul>
			
		</div>
		<div class="col-1 last-col-1 ">
			
			<ul>
				
				<li>
					<button name="submit" type="submit" class="custom edit_cstm cs-btn btn-green">Save</button>
					<div class="gen_report" style="display:none;"></div>
				</li>
			</ul>
		</div>
		
	</div>
	
</form>

</div>
<?php	 
}

function addTimecardStaff_11()
{
	$db = get_connection();
	unset($_REQUEST['action']); 
	$id=$_REQUEST['id']; 
	$staff_id=$_REQUEST['staff_id']; 
	$count=$id+1;
?>

<div class="asd new-added box_<?php echo $count; ?>">
	<select id="staff_id" name="staff_id[]" class="show-menu-arrow cs-select" onchange="get_value();">
		<option value=""></option>
		<?php
		if($staff_id!="")
		{	
			$sql="select * from users where id!=1 and id not in ($staff_id)";   
		}
		else
		{
			$sql="select * from users where id!=1";   
		}	
		
		$statement = $db->prepare($sql);	
		$statement->execute();
		$result = $statement->fetchAll();
		foreach($result as $row) 
		{
			$staff_id=$row['id'];
			$first_name=$row['first_name'];
			$last_name=$row['last_name'];
			$name=$first_name.' '.$last_name;
		?>
			<option value="<?php echo $staff_id; ?>" <?php if($staff_id==$user_id) { echo 'selected'; } ?>><?php echo $name; ?></option>
		<?php
		}
		?>
	</select>
<a href="javascript:void(0);" class="remove-new-added" onclick="remove_ul('1','<?php echo $count; ?>')" ><i class="fa fa-times" aria-hidden="true"></i></a>   
</div>
	
<?php	
}


?>			