<?php
require '../setup.php';
$uid=$_SESSION['uid'];
$role=	userRole($uid);
if($uid!="")
{	
$id=$_POST['id'];
$db = get_connection();		
$statement = $db->prepare("select * from material where id='".$id."'");	
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row)
{			
	$id=$row['id'];
	$part_no=$row['part_no'];
	$title=trim($row['title']);
	$price=$row['price'];
	$category=$row['category'];
}	
?>

<script>
jQuery(document).ready(function()
{
	jQuery('#edit-material-1').validate({
		
			submitHandler: function(form) {
						
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: 'ajax/edit_material.php', 
				success: function(data) 
				{
					dataa = JSON.parse(data);
					id=dataa.id;
					part_no=dataa.part_no;
					title=dataa.title;
					price=dataa.price;
					category=dataa.category;
					jQuery('.part_'+id).html(part_no);
					jQuery('.title_'+id).html(title);
					jQuery('.price_'+id).html(price);
					jQuery('.category'+id).html(category);
					jQuery('#edit-inventory').empty();
				}
			});
		}
		
		
	});
});
</script>
<br><br><br>
<div id="edit-inventory"> 
  <div class="section-hdr">
	<h3>Edit Material Form</h3> 
	<a href="javascript:void(0);" class="close close-edit-material"><i class="fa fa-plus"></i></a> </div>
  <div class="gen-ins-area clearfix">
	<form class="cs-form" id="edit-material-1" action="" method="post"> 
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	  <div class="row">
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_part_no" id="part_no" type="text" value="<?php echo $part_no; ?>" <?php if($part_no!="") { ?> class="is-filled" <?php } ?> >
			<label class="im-label">Part No</label>
		  </div>
		</div>
		<div class="col-sm-6">
		  <div class="group">
		   <!-- <input name="category" type="text" >-->
		   <select id="" name="category">
			  <option value="">Select Category</option>
			 <?php
			 $db = get_connection();			   	  	  
				$statement = $db->prepare("SELECT DISTINCT category FROM  `inventory`  order by category asc");
				$statement->execute();
				$result = $statement->fetchAll();
				$html='';   
				
				foreach($result as $cat)
				{
				$item=rtrim($cat["category"]," ");
					if($item==$category)
					{
						$asd="selected";
					}
					else
					{
						$asd="";
					}		
				$html.="<option ".$asd.">".$item."</option>";	 
				} 	
				 echo  $html;
			 ?>
			</select>
		  <!--  <label class="im-label">Category</label>-->
		  </div>
		</div>
		
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_title" id="title" type="text" value="<?php echo $title; ?>" <?php if($title!="") { ?> class="is-filled" <?php } ?> >
			<label class="im-label">Title</label>
		  </div>
		</div>
	
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_price" type="text" value="<?php echo $price; ?>" <?php if($price!="") { ?> class="is-filled" <?php } ?> > 
			<label class="im-label">Price</label>
		  </div>
		</div>
	  </div>
	  
	
	<div class="clr"></div>
	<div class="text-center">
	  <input type="submit" name="submit" value="Save" class="cs-btn btn-green" />  
	  <div id="message"> </div>
	  </p>
	</div>
	</form>
  </div>
</div>
<?php
}
else
{	
?>
<script>window.location.href="http://morrowelectric.pro";</script>
<?php
}
?>
<script>
	$(".close-edit-material").click(function()
	{
		$("#edit-inventory").css('display','none');
	});
</script>