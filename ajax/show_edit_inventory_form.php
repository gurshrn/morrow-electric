<?php
require '../setup.php';
$uid=$_SESSION['uid'];
$role=	userRole($uid);
if($uid!="")
{	
$id=$_POST['id'];
$db = get_connection();		
$statement = $db->prepare("select * from inventory where id='".$id."'");	
$statement->execute();
$result = $statement->fetchAll();
foreach($result as $row)
{			
	$id=$row['id'];
	$part_no=$row['part_no'];
	$description=trim($row['description']);
	$cost=$row['cost']; 
	$price=$row['price'];
	$category=$row['category'];
}	
?>

<script>
jQuery(document).ready(function()
{
	jQuery('#edit-inventory-1').validate({
		
			submitHandler: function(form) {
						
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: 'ajax/edit_inventory.php', 
				success: function(data) 
				{
					dataa = JSON.parse(data);
					id=dataa.id;
					part_no=dataa.part_no;
					descp=dataa.descp;
					cost=dataa.price; 
					price=dataa.cost;
					category=dataa.category;
					jQuery('.row_'+id).find('.bal').html('1');
					jQuery('.row_'+id).find('.part').html('2');
					jQuery('.row_'+id).find('.category').html('3');
					jQuery('.row_'+id).find('.descp').html('4');
					jQuery('.row_'+id).find('.price').html('5');
					jQuery('.row_'+id).find('.cost').html('6'); 
					jQuery('.row_'+id).find('.daga').html('7');
					jQuery('#edit-inventory').empty();
				}
			});
		}
		
		
	});
});
</script>
<br><br><br>
<div id="edit-inventory"> 
  <div class="section-hdr">
	<h3>Edit Inventory Form</h3> 
	<a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a> </div>
  <div class="gen-ins-area clearfix">
	<form class="cs-form" id="edit-inventory-1" action="" method="post"> 
	<input type="hidden" name="id" value="<?php echo $id; ?>" />
	  <div class="row">
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_part_no" id="part_no" type="text" value="<?php echo $part_no; ?>" <?php if($part_no!="") { ?> class="is-filled" <?php } ?> >
			<label class="im-label">Part No</label>
		  </div>
		</div>
		<div class="col-sm-6">
		  <div class="group">
		   <!-- <input name="category" type="text" >-->
		   <select id="" name="category">
			  <option value="">Select Category</option>
			 <?php
			 $db = get_connection();			   	  	  
				$statement = $db->prepare("SELECT DISTINCT category FROM  `inventory`  order by category asc");
				$statement->execute();
				$result = $statement->fetchAll();
				$html='';   
				
				foreach($result as $cat)
				{
				$item=rtrim($cat["category"]," ");
					if($item==$category)
					{
						$asd="selected";
					}
					else
					{
						$asd="";
					}		
				$html.="<option ".$asd.">".$item."</option>";	 
				} 	
				 echo  $html;
			 ?>
			</select>
		  <!--  <label class="im-label">Category</label>-->
		  </div>
		</div>
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_description" id="description" type="text" value="<?php echo $description; ?>" <?php if($description!="") { ?> class="is-filled" <?php } ?> >
			<label class="im-label">Description</label>
		  </div>
		</div>
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_cost" type="text" value="<?php echo $cost; ?>" <?php if($cost!="") { ?> class="is-filled" <?php } ?>>
			<label class="im-label">Cost</label>
		  </div>
		</div>
		<div class="col-sm-6">
		  <div class="group">
			<input name="edit_price" type="text" value="<?php echo $price; ?>" <?php if($price!="") { ?> class="is-filled" <?php } ?> > 
			<label class="im-label">Price</label>
		  </div>
		</div>
	  </div>
	  
	
	<div class="clr"></div>
	<div class="text-center">
	  <input type="submit" name="submit" value="Save" class="cs-btn btn-green" />  
	  <div id="message"> </div>
	  </p>
	</div>
	</form>
  </div>
</div>
<?php
}
else
{	
?>
<script>window.location.href="http://morrowelectric.pro";</script>
<?php
}
?>