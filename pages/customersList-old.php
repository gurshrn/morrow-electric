<?php 
$uid=$_SESSION['uid'];
$role=	userRole($uid);
/*if( $role['0']!='admin')
{
?>
 <div class="area-main">
	<?php echo "You are not authorized to this location";?>
</div>
<?php }
else 
{*/
?>

<div class="area-main">
  <div class="top-hdr clearfix">
    <div class="pull-left">
      <!--<a href="<?php echo SITE_URL; ?>?section=addStaff" class="cs-btn btn-black">Add Staff</a>-->
    </div>
    <div class="pull-right"> <a href="<?php echo SITE_URL; ?>?section=addCustomer" class="cs-btn btn-blue">New Customer</a>
      <?php 
		if( $role['0']=='admin')
		{?>
      <a href="?section=listInventory" class="cs-btn btn-blue">Inventory</a>
      <?php } ?>
    </div>
    <div id="editFromToggle" class="collapsible-area" style="display:none"></div>
  </div>
  <div class="customer-profile" id="customer-profile"  style="display:none"> </div>
  <div class="table-responsive">
    <div class="tb-scroll-nav clearfix"> <span class='scroll-btn-left'><i class='fa fa-chevron-left'></i></span><span class='scroll-btn-right'><i class='fa fa-chevron-right'></i></span></div>
    <h3 class="section-hdr">Customers</h3>
	<div class="table-responsive">
    <table id="example" class="display table responsive staff-tbl" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Name <i class="fa fa-sort"></i></th>
          <th>Email Address <i class="fa fa-sort"></i></th>
          <th>Telephone <i class="fa fa-sort"></i></th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>Name</th>
          <th>Email Address</th>
          <th>Telephone</th>
          <th></th>
          <th></th>
        </tr>
      </tfoot>
    </table>
	</div>
  </div>
</div>
<script type="text/javascript">	
	   jQuery(document).ready(function() {
	   //$("table.dataTable td.dataTables_empty").html('<img src="http://morrowelectric.pro/assets/images/loader-pk.gif">')
		if (jQuery(window).width() < 767) {			
			jQuery('.scroll-btn-right').click(function(event) {
			var pos = jQuery('table#example').parent().parent().scrollLeft() + 70;
			jQuery('table#example').parent().parent().scrollLeft(pos);
			});			
			jQuery('.scroll-btn-left').click(function(event) {
			var pos = jQuery('table#example').parent().scrollLeft() - 70;
			jQuery('table#example').parent().scrollLeft(pos);
			});   
		}; 
		});  
  </script>
<!--<link rel="stylesheet" type="text/css" href="<?php //echo SITE_URL ?>/assets/css/jquery.dataTables.css">-->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" class="init">		
		/* Formatting function for row details - modify as you need */
function format ( d ) {
    // `d` is the original data object for the row
    return '<table cellpadding="0" cellspacing="0" border="0">'+	
	   '<tr class="row-collapse">'+ '<td>Telephone</td><td>Home</td><td>Cottage</td><td>Note</td>'+'<tr>'+
        '<tr>'+           
            '<td>'+d.mobile+' - Home</br>'+d.mobile+' - Cottage</br>'+d.mobile+' - Mobile</td><td>'+d.home_address+'</td><td align="left">'+d.cottage_address+'</td><td>'+d.notes+'</td>'+
        '</tr>'+        
    '</table>';
}
 
$(document).ready(function() {

    var table = $('#example').DataTable( {
		responsive: true,
		"oLanguage": { "sSearch": "" } ,		
        "ajax": "https://www.morrowelectric.pro/files/customerAjaxList.php",
        "columns": [
             { "data": "display_name" },
			 { "data": "email" }, 
             { "data": "phone" },            
			 { "className":      'details-control',
			   "targets": 0,
               "orderable":      false,
               "data":           null,
               "defaultContent": '<span class="row-toggler"><i class="fa fa-angle-down"></i></span>'			
            },
		
			{	<?php if( $role['0']=='admin'){?> "data": "view_link",<?php } else { ?>"data": null,"defaultContent":'<span></span>',<?php } ?>
			"targets": 0,
			"orderable":      false,
			 },   
        ],
        /*"order": [[1, 'asc']]*/
    } );
     
    // Add event listener for opening and closing details
    $('#example tbody').on('click', 'td.details-control', function () {
	
        var tr = $(this).closest('tr');
        var row = table.row( tr );
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	$('div.dataTables_filter input').attr('placeholder', 'Search...');
	jQuery("#example").wrap("<div class='responsive-table'></div>");
	
	$(document.body).on('click', '.edit-btn-pro' ,function(){		
	
	
	})
		
} );

function editFunction(id)
{
$('html, body').animate({scrollTop:0}, 'slow');

$.ajax({type: "POST",
	url: "handler.php",
	data: "id="+id+"&action=cusEditForm",
	success:function(result){
	$("#customer-profile").slideDown();									
	$("#customer-profile").html(result);
	
	$('#customer-profile input').filter(function() {
	return this.value;
	}).addClass('is-filled');
	$('#customer-profile .notes').filter(function() {
	return this.value;
	}).addClass('is-filled');
	$(".close").click(function(){
	$("#customer-profile").slideUp('slow');
	});
	
	},
	error:function(e){
	console.log(e);
	}	
	});
	}

function myFunction(id)
{
$('html, body').animate({scrollTop:0}, 'slow');

$.ajax({type: "POST",
	url: "handler.php",
	data: "id="+id+"&action=viewCustomer",
	success:function(result){
	$("#customer-profile").slideDown();									
	$("#customer-profile").html(result);
	
	$('#customer-profile input').filter(function() {
	return this.value;
	}).addClass('is-filled');
	$('#customer-profile .notes').filter(function() {
	return this.value;
	}).addClass('is-filled');
	$(".close").click(function(){
	$("#customer-profile").slideUp('slow');
	});
	
	},
	error:function(e){
	console.log(e);
	}	
	});
}
</script>
<?php //} ?>
