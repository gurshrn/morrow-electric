<div class="area-main">
  <?php get_navbar(); 
 // print_r($_SESSION);
  ?>
  <?php $db = get_connection();?> 
  <div class="timeshts_cont display_block">
		
		<div class="recen_cont display_block">
			<div class="inner_pdng">
				<?php
				$dateArr = calculateWeekDate();
				$count=count($dateArr);
				for($i=0;$i<$count;$i++)
				{   
				$start= $dateArr[$i][0];
				$monArr[]= $dateArr[$i][0];
				
				} 
				
				if(isset($_GET['d']))
				{
					$arr=explode('/',$_GET['d']);
					
					$mon=$arr[0];
					$sun=$arr[1];
					
					$curr_monday = $mon;
					$curr_saturday = $sun; 
					
					$givenDate=$curr_monday;  
					
					$prev_monday=date('Y-m-d', strtotime('previous monday', strtotime($givenDate)));
					$next_monday= date('Y-m-d', strtotime('next monday', strtotime($givenDate)));
					
					
				}	
				else
				{	
				$curr_monday = date( 'Y-m-d', strtotime('monday this week'));
				$curr_saturday = date( 'Y-m-d', strtotime('sunday this week') );	
				
				$givenDate=$curr_monday;  
				
				$prev_monday=date('Y-m-d', strtotime('previous monday', strtotime($givenDate)));
				$next_monday= date('Y-m-d', strtotime('next monday', strtotime($givenDate)));
				}
				?>
				<?php
				if(in_array($prev_monday, $monArr))
				{
					$sunday=date("Y-m-d",strtotime($prev_monday."+6 day"));
				?>
				<a class="navbtn left" href="?section=electricalWorkTime&d=<?php echo $prev_monday ?>/<?php echo $sunday; ?>">Prev</a>
				<?php	
				}	
				
				if(in_array($next_monday, $monArr))
				{
					$sunday=date("Y-m-d",strtotime($next_monday."+6 day"));
				?>
				<a class="navbtn right" href="?section=electricalWorkTime&d=<?php echo $next_monday ?>/<?php echo $sunday; ?>">Next</a>
				<?php	 
				}	
				?>
				<span class="date pull-right"><?php echo date('F d, Y', strtotime($curr_monday)); ?> – <?php echo date('F d, Y', strtotime($curr_saturday)); ?></span>
			</div>
        </div>
		
		<div class="calndr_cont display_block">
			<?php
			
					
			$sql="select distinct id from users where id!=1 order by last_name ASC";
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			foreach($result as $row)
			{	
				$staff_id=$row['id'];  
				$count=0; 
			?>
			<div class="display_block mn_clndr">
				<div class="clndr_lft">
					<span></span>
					<strong><?php echo get_user_detail($staff_id,'first_name'); ?> <?php echo get_user_detail($staff_id,'last_name'); ?></strong>
				</div>
				<div class="clndr_rt">
					<div class="recen_cont display_block">
						<div class="inner_pdng">
							<span class="date pull-left"><?php echo date('M', strtotime($curr_monday)); ?> <?php echo date('d', strtotime($curr_monday)); ?>-<?php echo date('M', strtotime($curr_saturday)); ?> <?php echo date('d', strtotime($curr_saturday)); ?>, <?php echo date('Y', strtotime($curr_saturday)); ?></span>
						</div> 
					</div>
					<div class="table_calndr">
						<table>
							<thead>
								<?php
								$datePeriod = returnDates($curr_monday, $curr_saturday);
								foreach($datePeriod as $date) {
								$date_d=$date->format('Y-m-d');	
								?>
								<th><?php echo date('D', strtotime($date_d));  ?></th>
								<?php } ?>

							</thead>
							<tbody>
								<tr>
									<?php
									$datePeriod = returnDates($curr_monday, $curr_saturday);
									foreach($datePeriod as $date)  
									{
									$datee=$date->format('Y-m-d');	
									$total=get_sum_hours($datee,$staff_id,'gen_insp');
									$count=$count+$total;
									?>
									<td><?php echo get_sum_hours($datee,$staff_id,'gen_insp'); ?> Hours</td>
									<?php  
									} 
									?>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td><strong>TOTAL</strong></td>
									<td><strong><?php echo $count; ?> Hours</strong></td>
								</tr>
							</tbody>
						</table>
					</div>	
				
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>
</div> 
