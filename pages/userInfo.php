<style>
body {font-family: Arial;}

/* Style the tab */
.tab {
    overflow: hidden;
    border:0;border-bottom: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
  
    border-top: none;
}
	.user-info-area {
    border: 1px solid #e9e9e9;padding-bottom: 30px;
}
	.tabcontent .btn-blue {
    margin-bottom: 20px;
    margin-top: 20px;
}
</style>



	<div class="area-main user">
		<div class="user-info-area clearfix">
			<div class="tab">
				<button class="tablinks" onclick="openCity(event, 'London')">Vehicles</button>
				<button class="tablinks" onclick="openCity(event, 'Paris')">Equipments</button>
				<button class="tablinks" onclick="openCity(event, 'Tokyo')">Profile</button>
			</div>

			<div id="London" class="tabcontent" style="display:block">
			
				<div class="pull-left"><a href="javascript:void(0)" id='addVehicle' class="cs-btn btn-blue">Add Vehicles</a></div>
					
					<div id="fromToggle1" class="collapsible-area" style="display:none">
						<div class="section-hdr">
							<h3>Vehicle Details</h3>
							<a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a> </div>
							<div class="gen-ins-area clearfix">
								<form role="form" class="cs-form" id="add-vehicle">
									<div class="row">
										<div class="col-sm-6">
											<div class="group">
												<input name="vehicle" type="text" >
												<label class="im-label">Vehicle</label>
											</div>
										</div>
										<div class="col-sm-6">
											<div class="group">
												<input name="rate" type="text" class="validNumber">
												<label class="im-label">Rate</label>
											</div>
										</div>
									</div>
									<input type="hidden" value="addVehicle" name="action"/>
								</form>
								<div class="clr"></div>
								<div class="text-center">
									<p><a href="javascript:void(0)" id="vehicle" class="cs-btn btn-green">Create</a>
										<div id="message"> </div>
									</p>
								</div>
							</div>
						</div>
						<div id="editFromToggle1" class="collapsible-area" style="display:none"></div>
							<div class="table-responsive">
								<h3 class="section-hdr">Vehicle List</h3>
								<?php 
									$db = get_connection();			
									$sql="SELECT  * FROM  vehicles ORDER BY id DESC";	
									$statement = $db->prepare($sql);		
									$statement->execute();
									$result = $statement->fetchAll();
								?>
									<table id="example" class="display table data-tbl" cellspacing="0" width="100%">
										<thead>
											<tr>
											<th> </th>
											<th>Vehicle Name</th>
											<th>Rate</th>
											<th align="center">Action </th>
											</tr>
										</thead>
										<tfoot>
											<tr>
											  <th> </th>
											  <th>Vehicle Name</th>
											  <th>Rate</th>
											  <th>Edit / Delete</th>
											</tr>
										</tfoot>
										<tbody>
											<?php 
												$i=2;
												foreach($result as $row)
												{			
											?>
													<tr id="row_<?php echo $i?>">
														<td><img src="assets/images/square-gif.gif"></td>
														<?php 
							
															echo "<td class='fname'><span>".$row['vehicle']."</span><input name='first_name_".$i."' type='text' value='".$row['first_name']."' style='display:none'></td>";
															
															
															echo "<td><span>".'$ '.$row['rate']."</span><input name='first_name_".$i."' type='text' value='".$row['last_name']."' style='display:none'></td>";
															
												
															$id= "myModal".$i;
															echo "<td><a id=".$row['id']." href='javascript:void(0)' class='editVehicleInfo'>Edit</a>";
														$i++;
												}
											?>
										</tbody>
									</table>
    <script type="text/javascript" language="javascript" class="init">		
		$(document).ready(function() 
		{
			jQuery(document).on('keypress', '.validNumber', function (eve) 
			{
				if (eve.which == 0) {
					return true;
				}
				else
				{
					if (eve.which == '.') 
					{
						eve.preventDefault();
					}
					if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 
					{
						if (eve.which != 8)
						{
							eve.preventDefault();
						}
					}
				}
			});
		//table = $('#example').DataTable();		
		
			table=$('#example').DataTable({
			responsive: true,
			"oLanguage": { "sSearch": "" } ,
			})		
			
			$('div.dataTables_filter input').attr('placeholder', 'Search...');	
			jQuery("#example").wrap("<div class='responsive-table'></div>");
			
			})
	
	
			$('#example tbody').on( 'click', '.staff-del', function () {
				$(this).parent().parent().addClass("deletion ");
				var username= $(this).attr("u");
				 
				if(confirm ("Are you sure you want to Delete: " +username+"?"))
				{	
					$(this).fadeOut("slow", function() {
						table.row( $(this).parents('tr') ).remove().draw();
					});	
    
					id=$(this).attr("id");		
					$.ajax({
						type: "POST",
						url: "handler.php",
						data: "id="+id+"&action=staffDel",
						success:function(result){
						
						},
						error:function(e){
							console.log(e);
						}	
					});
				}
				else 
				{
					$(this).parent().parent().removeClass("deletion ");
					return;  
				}
			});

			$('#example tbody').on( 'click', '.editVehicleInfo', function () {

				$('html, body').animate({scrollTop:0}, 'slow');

				var recId=$(this).attr("id");	
				$.ajax({
					type: "POST",
					url: "handler.php",
					data: "id="+recId+"&action=vehicleEdit",
					success:function(result){
						$("#fromToggle1").hide();	
						$("#editFromToggle1").show();	
						$("#editFromToggle1").html(result);
						$('#editFromToggle1 input').filter(function() 
						{
							return this.value;
						}).addClass('is-filled');
						$('#editFromToggle1 .notes').filter(function() {
							return this.value;
						}).addClass('is-filled');
					},
					error:function(e){
						console.log(e);
					}	
				});

	/*var rowId=$(this).parent().parent().attr("id")
	$("#"+rowId+" td input").show();
  $("#"+rowId+" td select").show();
	$("#"+rowId+" td span").hide();
  $(this).html('save')
  $(this).next().show()
  $(this).hide();*/
} );

	$(document).ready(function() {	
		
			
			
			$("#addVehicle").click(function(){
				$("#editFromToggle1").hide();			
				$("#fromToggle1").slideToggle('slow');  
			});
			$(".not-allowed").click(function(){
				alert("You can't delete yourself.")
			});
			$(".close").click(function(){
				$("#fromToggle1").slideUp('slow');
				$("#editFromToggle1").slideUp('slow');
			});
		
		} );
	
	</script>
  </div>
	
</div>

		<div id="Paris" class="tabcontent">
		   <div class="pull-left"><a href="javascript:void(0)" id='addEquipment' class="cs-btn btn-blue">Add Equipments</a></div>
					
					<div id="fromToggle2" class="collapsible-area" style="display:none">
					  <div class="section-hdr">
						<h3>Equipments Details</h3>
						<a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a> </div>
					  <div class="gen-ins-area clearfix">
							<form role="form" class="cs-form" id="add-equipment">
							  <div class="row">
								<div class="col-sm-6">
								  <div class="group">
									<input name="equipment"  type="text" >
									<label class="im-label">Equipment</label>
								  </div>
								</div>
								<div class="col-sm-6">
								  <div class="group">
									<input name="rate" type="text" class="validNumber">
									<label class="im-label">Rate</label>
								  </div>
								</div>
							</div>
						<input type="hidden" value="addEquipment" name="action"/>
					</form>
				<div class="clr"></div>
				<div class="text-center">
					<p><a href="javascript:void(0)" id="equipment" class="cs-btn btn-green">Create</a>
						<div id="message"> </div>
					</p>
				</div>
			</div>
		</div>
		<div id="editFromToggle2" class="collapsible-area" style="display:none"></div>
		<div class="table-responsive">
			<h3 class="section-hdr">Equipment List</h3>
			<?php 
			$db = get_connection();			
			$sql1="SELECT  * FROM  equipment ORDER BY id DESC";	
			$statement1 = $db->prepare($sql1);		
			$statement1->execute();
			$result1 = $statement1->fetchAll();
			  ?>
    <table id="example2" class="display table data-tbl" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th> </th>
          <th>Equipment Name</th>
          <th>Rate</th>
          <th align="center">Action </th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th> </th>
          <th>Equipment Name</th>
          <th>Rate</th>
          <th>Edit / Delete</th>
        </tr>
      </tfoot>
      <tbody>
        <?php 
					$i=2;
			foreach($result1 as $row1)
			{			
			
			?>
        <tr id="row_<?php echo $i?>">
          <td><img src="assets/images/square-gif.gif"></td>
          <?php 
							
				echo "<td class='fname'><span>".$row1['equipment']."</span><input name='first_name_".$i."' type='text' value='".$row['first_name']."' style='display:none'></td>";
				
				
				echo "<td><span>".'$ '.$row1['rate']."</span><input name='first_name_".$i."' type='text' value='".$row['last_name']."' style='display:none'></td>";
				
	
				$id= "myModal".$i;
				echo "<td><a id=".$row1['id']." href='javascript:void(0)' class='editEquipment'>Edit</a>";
			$i++;
			}
		  
		  ?>
      </tbody>
    </table>
<script type="text/javascript" language="javascript" class="init">		
	$(document).ready(function() {
		jQuery(document).on('keypress', '.validNumber', function (eve) 
		{
			if (eve.which == 0) {
				return true;
			}
			else
			{
				if (eve.which == '.') 
				{
					eve.preventDefault();
				}
				if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 
				{
					if (eve.which != 8)
					{
						eve.preventDefault();
					}
				}
			}
		});
	
	table=$('#example2').DataTable({
	responsive: true,
	"oLanguage": { "sSearch": "" } ,
	})		
	
	$('div.dataTables_filter input').attr('placeholder', 'Search...');	
	jQuery("#example2").wrap("<div class='responsive-table'></div>");
	
	})
	
	
	$('#example tbody').on( 'click', '.staff-del', function () {
		 $(this).parent().parent().addClass("deletion ");
		 var username= $(this).attr("u");
		 
		if(confirm ("Are you sure you want to Delete: " +username+"?"))
		{	
			$(this).fadeOut("slow", function() {
			table.row( $(this).parents('tr') ).remove().draw();
			});	
    
			id=$(this).attr("id");		
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: "id="+id+"&action=staffDel",
				success:function(result){
				
				},
				error:function(e)
				{
					console.log(e);
				}	
			});
							  
		}
		else 
		{
			$(this).parent().parent().removeClass("deletion ");
			return;  
		}
	});

	$('#example2 tbody').on( 'click', '.editEquipment', function () {

		$('html, body').animate({scrollTop:0}, 'slow');

		var recId=$(this).attr("id");	
			$.ajax({
				type: "POST",
				url: "handler.php",
				data: "id="+recId+"&action=equipmentEdit",
				success:function(result){
					$("#fromToggle2").hide();	
					$("#editFromToggle2").show();	
					$("#editFromToggle2").html(result);
					$('#editFromToggle2 input').filter(function() 
					{
						return this.value;
					}).addClass('is-filled');
					$('#editFromToggle2 .notes').filter(function() {
						return this.value;
					}).addClass('is-filled');
				},
				error:function(e){
					console.log(e);
				}	
			});
	});

	$(document).ready(function() 
	{	
		$("#addEquipment").click(function(){
			$("#editFromToggle2").hide();			
			$("#fromToggle2").slideToggle('slow');  
		});
		$(".not-allowed").click(function(){
			alert("You can't delete yourself.")
		});
		$(".close").click(function(){
			$("#fromToggle2").slideUp('slow');
			$("#editFromToggle2").slideUp('slow');
		});
	});
	
</script>
  </div>
		</div>

		<div id="Tokyo" class="tabcontent">
			<div class="user-info-tbl">
				  <?php 
				   $db = get_connection();			   
				   $uid=$_SESSION['uid'];	  
					$statement = $db->prepare("SELECT * FROM users where id=".$uid);
					$statement->execute();
					$result = $statement->fetchAll();		
				  ?>
				  <form action="" method="post" id="user-info">
				   <input type="hidden" value="<?php echo $result[0]['id']?>" name="user_id"/>
				  
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
					  <td>First name</td> 
					  <td><span class="showinfo"><?php echo $result[0]['first_name']?></span><input type="text" name="first_name" value="<?php echo $result[0]['first_name']?>" style="display:none;"/></td>
					</tr>
					<tr>
					  <td>Last name</td>
					  <td><span class="showinfo"><?php echo $result[0]['last_name']?></span><input type="text" name="last_name" value="<?php echo $result[0]['last_name']?>"  style="display:none;"/></td>
					</tr>
					<tr>
					  <td>E mail address</td>
					  <td><span class="showinfo"><?php echo $result[0]['email']?></span><input type="text" name="email" value="<?php echo $result[0]['email']?>"  style="display:none"/></td>
					</tr>
					<tr>
					  <td>Phone No</td>
					  <td><span class="showinfo"><?php echo $result[0]['phone']?></span><input type="text" name="phone" value="<?php echo $result[0]['phone']?>"  style="display:none"/></td>
					</tr>
					<tr>
					  <td>Last logged in</td>
					  <td><?php echo $result[0]['last_logged_in']?></td>
					</tr>
				</table>
				<input type="hidden" value="editUserInfo" name="action"/>
				</form>
			</div>
	
		<div class="clr"></div>
		<div class="actions text-left">
		  <p> <a id='addForm' href="javascript:void(0)" class="cs-btn btn-gray">Change Password</a>
		  <a   href="javascript:void(0)" class="cs-btn btn-gray edit-info" > Edit </a>
					<a   href="javascript:void(0)" class="cs-btn btn-gray save-info" id="save-user-info" style="display:none">  Save </a>
		  </p>
		  <div id="fromToggle" style="display:none;"><div class="change-pw">
		   <div class="section-hdr">
				<h3>Change Password</h3>
				<a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a>
			</div>
			   
				<div class="table-responsive">
				  <form role="form" class="cs-form" id="change-pass">
				  <input name="user_id" type="hidden" value="<?php echo $_SESSION['uid']?>">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td>Old Password</td>
						<td><input name="old_password" type="password" placeholder="Old Password"></td>
					  </tr>
					  <tr>   
						<td>New password</td>
						<td><input name="password" id="password" type="password" placeholder="New Password"></td>
					  </tr>
					  <tr>
						<td>Confirm Password</td>
						<td><input name="password_confirm" id="password_confirm" type="password" placeholder="Confirm Password">
						  <input type="hidden" value="changePass" name="action"/>
						</td>
					  </tr>
					</table>
				  </form>
				  <div class="text-center"><a id="chagne_pass" href="javascript:void(0)" class="cs-btn btn-red">Change</a><div id="message"> </div>
				  
				  
				  </div>
				</div>
				
			  </div></div>
		   
			</div>
		</div>
     <!--h3 class="heading">Options</h3-->
	
    
  </div>
</div>
 <script type="text/javascript" language="javascript" class="init">		
		$(document).ready(function() {	
				
			$("#addForm").click(function(){
				$("#fromToggle").slideToggle('slow');
			});
			$(".close").click(function(){
				$("#fromToggle").slideUp('slow');
			});  
			
			$(".edit-info").click(function(){ 
				$(this).hide().next().show();
				$(".showinfo").hide().next().show();
			 })
			 
			/* $(".save-info").click(function(){ 
				$(this).hide().prev().show();
				//$(".showinfo").show().next().hide();
			 })*/
		
		} );
	</script>
	<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>