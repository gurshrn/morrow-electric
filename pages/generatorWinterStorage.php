
<div class="area-main">
  <?php get_navbar(); ?>
   <?php $db = get_connection();?>
    <div id="cust_gen_area_1">
		<input type="hidden" id="ram" value="1">
		<div class="cust_gen_area"></div>
	</div>  
  <form role="form" class="cs-form" id="generator-storage" autocomplete="off">
    <div class="section-hdr">
      <div class="pull-left">
        <h3>NEW WORKSHEET <span class="sepr">|</span> Generator Winter Storage</h3>
        <p class="search-customer"><span id="username"></span>
          <input type="text" name="searchname" id="searchname" value="" Placeholder="Search Customer..." autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">
          <?php //echo date("F d, Y")?>
        </div>
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number" />
            <label class="im-label">PO Number</label>
          </div>
        </div>
      </div>
    </div>
    <div class="gen-ins-area clearfix">
      <div class="row">
		 <input name="customer_id" id="customer_id" type="hidden">
          <input name="service_by" id="service_by" type="hidden" value="<?php echo $_SESSION['user']; ?>">
          <input name="cat_name" id="cat_name" type="hidden" value="Winter Storate">
		<div class="col-sm-12">  
			 <div class="gen_sec"></div> 
		</div>
        <div class="col-sm-6">
         
          <div class="group">
            <input name="generator_hours"  class="validNumber" id="generator_hours" type="text" >
			<label class="im-label">Generator Hours</label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="group">
            <input name="fogging_oil" class="validNumber" id="fogging_oil" type="text">
			<label class="im-label">Fogging Oil</label>
          </div>
        </div>
        <div class="col-sm-12 gws">
          <p class="cs-radio">Battery Storage & Charging
            <label>
            <input type="radio" name="battery_storage_charging" value="1" id="RadioGroup1_0" checked>
            <span class="custm-radio"></span> 1</label>
            <label>
            <input type="radio" name="battery_storage_charging" value="2" id="RadioGroup1_1">
            <span class="custm-radio"></span> 2</label>
            <label>
            <input type="radio" name="battery_storage_charging" value="3" id="RadioGroup1_2">
            <span class="custm-radio"></span> 3</label>
          </p>
        </div>
        <div class="col-sm-6">
        <div class="group">
			<div class="vehicleField">
				<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="vehicleRate">
						<input type="hidden" class="vehCount" value="0">
						<select id="vehicleRate" name="vehicle[parts][row_0][vehicle]" class="">
							<option value="">Select Vehicles</option>
							<?php   echo getVehicles();  ?>
						</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="vehicle[parts][row_0][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" disabled>
						  <label class="im-label">Travel</label>
						  <label id="uerror"></label>
						</div>
					</div>
					
				</div>
			</div>
			<a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a>
        </div>
      </div>
        <div class="col-sm-6">
          <div class="group">
				<div class="n-box">
					<div class="row">
						<div class="col-md-8">
						  <select name="staff_id[]" style="width:100%">
							<option value="">Select Staff</option> 
							<?php
							$statement = $db->prepare("select * from users where id!='1'");	
							$statement->execute();
							$result = $statement->fetchAll();
							foreach($result as $row)
							{
							?> 
								<option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
							<?php 
							}
							?>
						  </select> 
						</div>
						<div class="col-md-3">
							<div class="group">
								<input name="hour[]" type="text" class="is-blank validNumber"  style="width:100%">
								<label class="im-label">Hours</label>
							</div>
							
						</div>
					</div>
				</div> 
				<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>
			</div> 
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="box" id="customFields">
            <label class="box-title">Parts</label>
			<input type="hidden" value="0" class="invCount">
            <div class="row">
              <div class="col-sm-2 col-xs-3">
                <div class="group">
                  <input type="number" class="validNumber" name="data[parts][row][qty]">
				  <label class="im-label">Qty</label>
                </div>
              </div>
              <div class="col-sm-4 col-xs-4">
                <select name="data[parts][row][cat]">
                  <option value="">Select Category</option>
                  <?php   echo getCategories();  ?>
                </select>
              </div>
              <div class="col-sm-5 col-xs-4">
				 <input class="inventory-list" type="text" placeholder="Inventory" name="data[parts][row][inventory]">
				   
				
                <!--<div class="group">
                  <input class="inventory-list" type="text"  name="data[parts][row][inventory]">
				  <label class="im-label">Inventory</label>
                </div>-->
                <div class="inventory"></div>
              </div>
              <div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
        <label class="box-title"></label>
        <div class="equipmentFields">
			<div class="row">
				<div class="col-sm-8">
					<input type="hidden" class="equipmentRate">
					<input type="hidden" class="eupCount" value="0">
					<select id="equipmentRate" name="equip[parts][row_0][equipment]" class="">
						<option value="">Select Equipment</option>
						<?php   echo getEquipments();  ?>
					</select>
				</div>
				<div class="col-sm-3">
					<div class="group">
					  <input name="equip[parts][row_0][hours]" id="equiphours" type="text"  class="is-blank validNumber" disabled>
					  <label class="im-label">Hours </label>
					  <label id="uerror"></label>
					</div>
				</div>
				
			</div>
        </div>
		<a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a>
      </div>
      </div>
      <div class="group">
        <textarea class="notes" name="notes" id="notes" cols="" rows="" ></textarea>
		<label class="im-label">Notes</label>
      </div>
      <input type="hidden" value="addGeneratorStorage" name="action"/>
	  <input type="hidden" value="gen_win_storage" name="work_sheet"/> 
      <div class="clr"></div>
      <div class="actions"> <a href="javascript:void(0)" class="cs-btn btn-green" id="storage">Save Changes</a>
        </p>
        <div id="message"> </div>
      </div>
    </div>
  </form>
</div>
<script>
function add_timecard_staff()
{
	
	var staff_id='';
	jQuery.each(jQuery("select[name='staff_id[]']"), function() {
	staff_id += (staff_id?',':'') + jQuery(this).val();
	});
	
	/* alert(staff_id);
	return false; */
	
	var id=jQuery('.n-box').length;
	jQuery.ajax({type: "POST", 
	url: "handler.php",
	data: "id="+id+"&staff_id="+staff_id+"&action=addTimecardStaff",  
	success:function(result) 
	{
		
		jQuery(result).insertAfter('.n-box:last'); 
	},
	error:function(e){
		console.log(e);
	}	
	}); 
}

function remove_ul(e,id)
{
	if(e==1)
	{
		jQuery('.box_'+id).fadeOut(2000);
		jQuery('.box_'+id).remove();
	}
	else
	{
		
		jQuery('.box_'+id).fadeOut(3000);
		jQuery('.box_'+id).remove();
		
	}		
}

function show_cust_gen(e,cust_id)
{
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.cust_gen_area').empty().append(loader);
	jQuery('.cust_gen_area').show(); 
	
	jQuery("html, body").animate({
        scrollTop:jQuery('#cust_gen_area_1').offset().top 
		}, 2000);  
	
	
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_new.php",
	data: "e="+e+"&cust_id="+cust_id+"&action=ShowCustomerGen",     
	success:function(result)   
	{
		
		jQuery('.cust_gen_area').empty().append(result); 
		
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 


}

function sel_item(inv,id,item)
{
	if(inv==1)
	{
		jQuery('input[name=inventory_1]').val(item);
		jQuery('input[name=inventory_1]').addClass('is-filled');
		jQuery('#oil_filter_id').val(id); 
		jQuery('.oil_filter').empty();
	}	
	else if(inv==2)     
	{
		jQuery('input[name=inventory_2]').val(item);
		jQuery('input[name=inventory_2]').addClass('is-filled'); 
		jQuery('#air_filter_id').val(id);
		jQuery('.air_filter').empty(); 
	}	
	else if(inv==3)
	{
		jQuery('input[name=inventory_3]').val(item); 
		jQuery('input[name=inventory_3]').addClass('is-filled');  
		jQuery('#spark_plug_id').val(id);
		jQuery('.spark_plug').empty();
	}	
}
</script>