
<div class="area-main login-page">
  <div class="gen-ins-area clearfix">
    <div class="login-box">
      <h3>User Login</h3>
      <div id="message"> </div>
      <form role="form" class="cs-form" method="post" action="#" id="login-form">
        <div class="row">
          <div class="col-sm-12 ">
            <div class="group">
              <input name="username" id="uname" type="text" placeholder="E-mail / Username">
              <label id="uerror"></label>
            </div>
          </div>
          <div class="col-sm-12">
		  <div class="group">
            <input name="password" id="pwd" type="Password" placeholder="Password" >
            <label id="perror"></label>
			</div>
          </div>
        </div>
        <input type="hidden" value="validateLogin" name="action"/>
        <input type="button" value="Login" id="login" class="cs-btn btn-green"/>
        <div class="text-center">
          <p><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal1">Forgot password?</a></p>
        </div>
      </form>
      
    </div>
  </div>
  
  <div class="modal fade cs-popup" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content"> <a href="javascript:void(0);" data-dismiss="modal" class="close-pop"><span class="sr-only">close</span><i class="fa fa-times-circle"></i></a>
            <div class="table-responsive clearfix">
              <h2>Enter your email to search for your account.</h2>
              <form role="form" class="cs-form" id="forget-pass">
                <label>Email</label>
                <input name="email" type="text" placeholder="Email*">
                <input type="hidden" value="forgetPass" name="action"/>
              </form>
            </div>
            <div class="text-center"><a id="forget_pass" href="javascript:void(0)" class="cs-btn btn-red">Send</a>
              <div id="passmessage"> </div>
            </div>
          </div>
        </div>
      </div>
</div>
<script type="text/javascript">
jQuery('body').addClass('no-header');

</script>
