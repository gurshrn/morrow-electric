
<?php 
$uid=$_SESSION['uid'];
$role=	userRole($uid);
if($uid!="")
{	
?>
<div class="area-main">
	<div class="top-hdr clearfix">
		<div class="pull-left">
			<a id='addForm' href="javascript:void(0)" class="cs-btn btn-blue" >New Item</a>
		</div>
		<div class="pull-right"> 
			<a  href="<?php echo SITE_URL?>" class="cs-btn btn-blue" >Dashboard</a>
		</div>
		<div id="fromToggle" class="collapsible-area" style="display:none">
			<div class="section-hdr">
				<h3>Material Form</h3>
				<a href="javascript:void(0);" class="close">
					<i class="fa fa-plus"></i>
				</a> 
			</div>
			<div class="gen-ins-area clearfix">
				<form role="form" class="cs-form" id="add-material">
					<div class="row">
						<div class="col-sm-6">
							<div class="group">
								<input name="part_no" id="part_no" type="text">
								<label class="im-label">Part No</label>
							</div>
						</div>
						 <div class="col-sm-6">
							  <div class="group">
							  
							   <select id="" name="category">
								  <option value="">Select Category</option>
								  <?php   echo getCategories();  ?>
								</select>
							
							  </div>
							</div>
						<div class="col-sm-6">
						  <div class="group">
							<input name="title" id="title" type="text">
							<label class="im-label">Title</label>
						  </div>
						</div>
						<div class="col-sm-6">
						  <div class="group">
							<input name="price" type="text">
							<label class="im-label">Price</label>
						  </div>
						</div>
					</div>
					<input type="hidden" value="addMaterial" name="action"/> 
				</form>
				<div class="clr"></div>
				<div class="text-center"> 
				  <p><a href="javascript:void(0)" id="material" class="cs-btn btn-green" >Add Item</a>
				  <div id="message"> </div>
				  </p>
				</div>
			</div>
		</div>
		<div class="edit_form_sec"></div> 
	</div>
	<div class="table-responsive">
		<h3 class="section-hdr">Material</h3>
		<?php 
			$db = get_connection();		
			$statement = $db->prepare(" SELECT  * FROM  material order by id desc");	
			
			$statement->execute();
			$result = $statement->fetchAll();
		?>
		<table id="example2" class="display table data-tbl" cellspacing="0" width="100%">
			<thead>
				<tr>
				  <th></th>
				  <th>Part No</th>
				  <th>Category</th>
				  <th>Title</th>
				  <th>Price</th>
				  <th>&nbsp;</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th></th>
					<th>Part No</th>
					<th>Category</th>
					<th>Title</th>
					<th>Price</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
			<tbody>
				<?php 
					$i=1;
					if(!empty($result))
					{
						foreach($result as $row)
						{			
				?>
							<tr class="row_<?php echo $row['id']; ?>">
								<td>
									<img src="assets/images/square-gif.gif">
								</td>
								<?php 
									$originalDate = $row['created'];
									$newDate = date("d M, Y", strtotime($originalDate));
								?>
								<td class="part_<?php echo $row['id'];?>"><?php echo $row['part_no'];?></td>
								<td class="category_<?php echo $row['id'];?>"><?php echo $row['category'];?></td>
								<td class="title_<?php echo $row['id'];?>"><?php echo $row['title'];?></td>
								<td class="price_<?php echo $row['id'];?>"><?php echo $row['price'];?></td>
								<td>
									<a href="javascript:void(0)" class="edit-btn-1" onclick="edit_material('<?php echo $row['id']; ?>');">Edit</a>
								</td>
				<?php 
							echo " </tr>";
						}
					}
					
				?>
		  </tbody>
		</table>
    <script type="text/javascript" language="javascript" class="init">		
		$(document).ready(function() 
		{	
			$("#addForm").click(function()
			{
				$("#fromToggle").slideToggle('slow');
			});
			$(".close").click(function()
			{
				$("#fromToggle").slideUp('slow');
			});
			
			$('#example2').DataTable({
				"oLanguage": { "sSearch": "" } ,
				"pageLength": 50
			})	
			$('div.dataTables_filter input').attr('placeholder', 'Search...');	
			$("#example2").wrap("<div class='responsive-table'></div>");
		});
		
		function edit_material(id)
		{
			jQuery.ajax({
				type: "POST",
				url:"ajax/show_edit_material.php",
				data:{id:id,format:'raw'},
				success:function(resp){
					if( resp !="")
					{
						jQuery('.edit_form_sec').empty().append(resp);
					}
				}
			});
		}
		 
		
	</script>
  </div> 
</div>
<?php } else { ?>
<script>window.location.href="http://morrowelectric.pro";</script>
<?php } ?>
 
<?php //} ?>
