<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link rel="shortcut icon" type="image/ico" href="http://www.datatables.net/favicon.ico">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=2.0">
	<title>Editor example - In table form controls</title>
	
</head>
<body class="dt-example">
	<div class="container">
		<section>
			
			<a class="editor_create">Create new record</a>
			<table id="example" class="display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>First name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Role</th>						
						<th>Edit / Delete</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>First name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Role</th>	
						<th>Edit / Delete</th>
					</tr>
				</tfoot>
			</table>
			<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="http://morrowelectric.pro/assets/css/editor.dataTables.min.css">
	<style type="text/css" class="init">
	
	</style>
	<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.0.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js">
	</script>
	<script type="text/javascript" language="javascript" src="http://morrowelectric.pro/assets/js/dataTables.editor.min.js">
	</script>
	<script type="text/javascript" language="javascript" class="init">
	


var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
	editor = new $.fn.dataTable.Editor( {
		"ajax": "http://morrowelectric.pro/php/staff.php",
		"table": "#example",
		"fields": [ {
				"label": "First name2222:",
				"name": "first_name"
			}, {
				"label": "Last name:",
				"name": "last_name"
			}, {
				"label": "Role:",
				"name": "role"
			}, {
				"label": "Office:",
				"name": "email"
			}
		]
	} );

	// New record
	$('a.editor_create').on('click', function (e) {
		e.preventDefault();

		editor.create( {
			title: 'Create new record',
			buttons: 'Add'
		} );
	} );

	// Edit record
	$('#example').on('click', 'a.editor_edit', function (e) {
		e.preventDefault();

		editor.edit( $(this).closest('tr'), {
			title: 'Edit record',
			buttons: 'Update'
		} );
	} );
  
	// Delete a record
	$('#example').on('click', 'a.editor_remove', function (e) {
		e.preventDefault();

		editor.remove( $(this).closest('tr'), {
			title: 'Delete record',
			message: 'Are you sure you wish to remove this record?',
			buttons: 'Delete'
		} );
	} );

	$('#example').DataTable( {
		ajax: "http://morrowelectric.pro/php/staff.php",
		columns: [			
			{ data: "first_name" },
			{ data: "last_name" },
			{ data: "email" },
			{ data: "role" },			
			{
				data: null, 
				className: "center",
				defaultContent: '<a href="" class="editor_edit">Edit</a> / <a href="" class="editor_remove">Delete</a>'
			}
		]
	} );
} );



	</script>
			
		
</body>
</html>