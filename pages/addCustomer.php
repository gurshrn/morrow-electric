<?php 
$uid=$_SESSION['uid'];
$role=	userRole($uid);

?>

<div class="area-main">
  <div class="top-hdr clearfix">
    <div class="pull-left"></div>
    <div class="pull-right"> <a href="<?php echo SITE_URL; ?>?section=addCustomer" class="cs-btn btn-blue">New Customer</a>
      <?php 
		if( $role['0']=='admin')
		{?>
      <a href="?section=listInventory" class="cs-btn btn-blue">Inventory</a>
      <?php } ?>
    </div>
  </div>
  <div class="section-hdr">
    <div class="pull-left">
      <h3 class="text-upper">Customer Details</h3>
    </div>
  </div>
  <div class="gen-ins-area clearfix">
    <form role="form" class="cs-form" id="add-customer">
      <div class="row">
        <div class="col-sm-6">
          <div class="group">
            <input name="company_name" id="company_name" type="text" >
            <label class="im-label">Company Name </label>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="group">
            <input name="first_name" id="first_name" type="text">
            <label class="im-label">First Name </label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="group">
            <input name="last_name" type="text" >
            <label class="im-label">Last Name </label>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-6">
              <div class="group">
                <input name="phone" type="text"  maxlength="10">
                <label class="im-label">Home Phone </label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="mobile" type="text" maxlength="10">
                <label class="im-label">Mobile Phone </label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="group">
            <input name="email" type="text" >
            <label class="im-label">Email address </label>
          </div>
        </div>
        <div class="col-sm-6">
          <p class="label">Home</p>
          <div class="group">
            <input type="text" name="home_address" >
            <label class="im-label">Address Line 1 </label>
          </div>
          <div class="group">
            <input type="text" name="home_address2" >
            <label class="im-label">Address Line 2 </label>
          </div>
          <div class="row">
            <div class="col-sm-3">
              <div class="group">
                <input name="home_city" type="text" >
                <label class="im-label">City </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="home_province" type="text" >
                <label class="im-label">Province </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="home_country" type="text" >
                <label class="im-label">Country </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="home_postal_code" type="text" >
                <label class="im-label">Postal Code </label>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <p class="label">Cottage </p>
          <div class="group">
            <input type="text" name="cottage_address">
            <label class="im-label">Address Line 1 </label>
          </div>
          <div class="group">
            <input type="text" name="cottage_address2" >
            <label class="im-label">Address Line 2 </label>
          </div>
          <div class="row">
            <div class="col-sm-3">
              <div class="group">
                <input name="cottage_city" type="text" >
                <label class="im-label">City </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="cottage_province" type="text" >
                <label class="im-label">Province </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="cottage_country" type="text" >
                <label class="im-label">Country </label>
              </div>
            </div>
            <div class="col-sm-3">
              <div class="group">
                <input name="cottage_postal_code" type="text" >
                <label class="im-label">Postal Code </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="group">
        <textarea name="notes" cols="" rows="" class="notes"></textarea>
        <label class="im-label">Note </label>
      </div>
      <input type="hidden" value="addCustomer" name="action"/>
    </form>
    <div class="clr"></div>
    <div class="actions">
      <p><a href="javascript:void(0)" id="customer" class="cs-btn btn-green">Save</a>
      <div id="message"> </div>
      </p>
    </div>
  </div>
</div>
