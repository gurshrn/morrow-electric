<?php $db = get_connection();?> 
<div class="area-main">
	<?php
	if(isset($_GET['d']))
	{
		$arr=explode('/',$_GET['d']);
		
		$mon=$arr[0];
		$sun=$arr[1];
		
		$curr_monday = $mon;
		$curr_saturday = $sun; 
	}	
	else
	{	
	$curr_monday = date( 'Y-m-d', strtotime('thursday last week'));
	$curr_saturday = date( 'Y-m-d', strtotime('wednesday this week') ); 	
	}
	?>
  
	<div class="top-hdr clearfix">
	<div class="pull-left"></div>
	<div class="pull-right"> 
		<a href="javascript:void(0);" class="cs-btn btn-blue" onclick="show_report('<?php echo $curr_monday; ?>','<?php echo $curr_saturday; ?>');">New Report</a>
	</div> 
  
	<div style="display:none;" class="box add_cust_box"></div>
	
	
   <div class="timeshts_cont display_block">
		
		<div class="recen_cont display_block">
			<div class="inner_pdng">
				<?php
				
				if(isset($_GET['d']))
				{
					$arr=explode('/',$_GET['d']);
					
					$mon=$arr[0];
					$sun=$arr[1];
					
					$curr_monday = $mon;
					$curr_saturday = $sun; 
					
					$givenDate=$curr_monday;  
					
					$prev_monday=date('Y-m-d', strtotime('previous friday', strtotime($givenDate)));
					$next_monday= date('Y-m-d', strtotime('next friday', strtotime($givenDate)));
					
					  
				}	
				else
				{	
				$curr_monday = date( 'Y-m-d', strtotime('friday last week'));
				$curr_saturday = date( 'Y-m-d', strtotime('thursday this week') );	
				 
				$givenDate=$curr_monday;   
				
				$prev_monday=date('Y-m-d', strtotime('previous friday', strtotime($givenDate)));
				$next_monday= date('Y-m-d', strtotime('next friday', strtotime($givenDate)));
				}
				
				$month=date('m', strtotime($givenDate));
				$year=date('Y', strtotime($givenDate)); 
				
				$dateArr = calculateWeekDate($month,$year); 
				$count=count($dateArr);
				for($i=0;$i<$count;$i++)
				{   
				$start= $dateArr[$i][0];
				$monArr[]= $dateArr[$i][0];
				
				} 
				
				 
				?>
				<?php
				
					$sunday=date("Y-m-d",strtotime($prev_monday."+6 day"));  
				?>
				<a class="navbtn left" href="?section=timesheet&d=<?php echo $prev_monday ?>/<?php echo $sunday; ?>">Prev</a>
				<?php	
				$sunday=date("Y-m-d",strtotime($next_monday."+6 day")); 
				?>
				<a class="navbtn right" href="?section=timesheet&d=<?php echo $next_monday ?>/<?php echo $sunday; ?>">Next</a>
				
				<span class="date pull-right"><?php echo date('F d, Y', strtotime($curr_monday)); ?> – <?php echo date('F d, Y', strtotime($curr_saturday)); ?></span>
			</div>
        </div>
		
		<div class="calndr_cont display_block">
			<?php
			
			$user_id=$_SESSION['uid']; 
			$user_role=get_user_detail($user_id,'role');
			
			if($user_role=="admin")
			{		
				$sql="select distinct id from users where id!=1 order by last_name ASC";
			}
			else
			{
				$sql="select * from users where id='".$user_id."'";
			} 	
			$statement = $db->prepare($sql);	
			$statement->execute(); 
			$result = $statement->fetchAll();
			foreach($result as $row)
			{	
				$staff_id=$row['id'];    
				$count=0; 
			?>
			<div class="display_block mn_clndr">
				<div class="clndr_lft">
					<span></span>
					<strong><?php echo get_user_detail($staff_id,'first_name'); ?> <?php echo get_user_detail($staff_id,'last_name'); ?></strong>
				</div>
				<div class="clndr_rt">
					<div class="recen_cont display_block">
						<div class="inner_pdng">
							<span class="date pull-left"><?php echo date('M', strtotime($curr_monday)); ?> <?php echo date('d', strtotime($curr_monday)); ?>-<?php echo date('M', strtotime($curr_saturday)); ?> <?php echo date('d', strtotime($curr_saturday)); ?>, <?php echo date('Y', strtotime($curr_saturday)); ?></span>
						</div> 
					</div>
					<div class="table_calndr">
						<table>
							<thead>
								<?php
								$datePeriod = returnDates($curr_monday, $curr_saturday);
								foreach($datePeriod as $date) {
								$date_d=$date->format('Y-m-d');	
								?>
								<th><?php echo date('D', strtotime($date_d));  ?></th>
								<?php } ?>

							</thead>
							<tbody>
								<tr>
									<?php
									$datePeriod = returnDates($curr_monday, $curr_saturday);
									foreach($datePeriod as $date)  
									{
									$datee=$date->format('Y-m-d');	
									$total=get_sum_hours($datee,$staff_id);
									$count=$count+$total;
									?>
									<td><?php echo get_sum_hours($datee,$staff_id); ?> Hours</td>
									<?php  
									} 
									?>
								</tr>
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td><strong>TOTAL</strong></td>
									<td><strong><?php echo $count; ?> Hours</strong></td>
								</tr>
							</tbody>
						</table>
					</div>	
				
				</div>
			</div>
			<?php
			}
			?>
		</div>
	</div>
</div> 
<script>
function add_timecard_staff_11()
{
	jQuery('.gen_report').empty().hide();
	var staff_id='';
	jQuery.each(jQuery("select[name='staff_id[]']"), function() {
	staff_id += (staff_id?',':'') + jQuery(this).val();
	}); 
	 
	
	
	var id=jQuery('.selectn_box>.asd').length; 
	jQuery.ajax({type: "POST",
	url: "handler.php",
	data: "id="+id+"&staff_id="+staff_id+"&action=addTimecardStaff_11",  
	success:function(result)
	{
		
		jQuery(result).insertAfter('.selectn_box>.asd:last');
	},
	error:function(e){
		console.log(e);
	}	
	}); 
}

function remove_ul(e,id)
{
	if(e==1)
	{
		jQuery('.box_'+id).fadeOut(2000);
		jQuery('.box_'+id).remove();
	}
	else
	{
		
		jQuery('.box_'+id).fadeOut(3000);
		jQuery('.box_'+id).remove();
		 
	}		
}

function show_report(monday,saturday)
{
	var loader='<center><img src="assets/images/loader.gif" /></center>';	
	jQuery('.add_cust_box').show();
	jQuery('.add_cust_box').empty().append(loader); 
	jQuery.ajax({type: "POST",
	url: "handler.php", 
	data: "mon="+monday+"&sat="+saturday+"&action=ShowReportTime",     
	success:function(result) 
	{
		jQuery('.add_cust_box').empty().append(result);  
		
		
	},
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}
</script> 