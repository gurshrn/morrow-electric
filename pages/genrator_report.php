<?php  $db = get_connection();?>
<div class="area-main generator-area">
	<div class="top-hdr clearfix">
		
		<div class="pull-right"> <a href="<?php echo SITE_URL; ?>?section=assign_service" class="cs-btn btn-blue">Assign for service</a> <a href="<?php echo SITE_URL; ?>?section=genrator_report" class="cs-btn btn-blue">New Report</a> </div>
	</div>
	<div class="user-info-area clearfix">
		
		<div class="report_sec">
		<h1>New Report</h1>
		<ul>
			<li>	
				<label>
					<input name="report_type" value="1" type="radio" onclick="show_hide_reg_drop(1);">
					<span class="custm-radio"></span>SERVICE BY REGION</label> 
				<label>  	
			</li>	
			<li>	
				<label>
					<input name="report_type" value="2" type="radio" onclick="show_hide_reg_drop(2);">
					<span class="custm-radio"></span>SPRING START-UP & WINTERIZING</label> 
				<label>      	
			</li>	
			<li class="reg_sec" style="display:none;">
				<?php
				$sql="SELECT distinct service_reg
				FROM  `customer_generator`";	
				$statement = $db->prepare($sql);	
				$statement->execute();	
				$data = $statement->fetchAll(); 
				?>
				<select name="region">
					<option value="">Region</option>
					<?php
					foreach($data as $row) 						
					{						
					$service_reg=$row['service_reg']; 
					?>
					<option value="<?php echo $service_reg; ?>"><?php echo $service_reg; ?></option>
					<?php
					}
					?>
				</select>
			</li>
			<li>
				<a class="cs-btn btn-green" href="javascript:void(0);" onclick="create_report();" style="display:none;">Create Report</a>
			</li>	 
		</ul>
		</div>
		
		 
		<div class="section-wrap clearfix assign_listing">
			<p>Your report listing will be show here</p>
		</div>
		 
			
	</div>
		
</div>
<script>
function show_hide_reg_drop(e)
{
	if(e==1)
	{
		alert('aaaaaaaa');
		var region="";
		jQuery('select[name=region]>option').removeAttr('selected');
		jQuery('.reg_sec').show();
		jQuery('.report_submit').empty().append('<a class="cs-btn btn-green"  href="<?php echo SITE_URL; ?>region_report.php?region='+region+'">Create Report</a>');     
		
	}	
	else
	{
		jQuery('.reg_sec').hide();
	}	
	jQuery('.cs-btn').show();
}

function create_report()
{
	var report_type=jQuery('input[name=report_type]:checked').val();
	var region=jQuery('select[name=region]').val();
	if(report_type==1)
	{
		var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
		jQuery('.assign_listing').empty().append(loader);
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "region="+region+"&action=ReportbyRegion",       
		success:function(result)       
		{
			jQuery('.assign_listing').empty().append(result);
		}, 
		error:function(e){  
			console.log(e); 
		}	
		}); 
	}	
	else
	{
		var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
		jQuery('.assign_listing').empty().append(loader);
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "&action=ReportbySpringWinter",       
		success:function(result)       
		{
			jQuery('.assign_listing').empty().append(result);
		}, 
		error:function(e){  
			console.log(e); 
		}	
		}); 
	}	
}

</script>