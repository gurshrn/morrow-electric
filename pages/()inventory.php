<?php 
$uid=$_SESSION['uid'];
$role=	userRole($uid);
/*if( $role['0']!='admin')
{
?>

<div class="area-main"> <?php echo "You are not authorized to this location";?> </div>
<?php }
else 
{*/
?>

<div class="area-main">
  <div class="top-hdr clearfix">
    <div class="pull-left"><a id='addForm' href="javascript:void(0)" class="cs-btn btn-blue" >New Item</a></div>
    <div class="pull-right"> <a  href="<?php echo SITE_URL?>" class="cs-btn btn-blue" >Dashboard</a>
      <!--<a  href="<?php echo SITE_URL?>?section=electricalWork" class="cs-btn btn-blue" >New Worksheet</a> 
		<a  href="<?php echo SITE_URL?>?section=addCustomer" class="cs-btn btn-blue" >New Customer</a> -->
    </div>
    <div id="fromToggle" class="collapsible-area" style="display:none">
      <div class="section-hdr">
        <h3>Inventory Form</h3>
        <a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a> </div>
      <div class="gen-ins-area clearfix">
        <form role="form" class="cs-form" id="add-inventory">
          <div class="row">
            <div class="col-sm-6">
              <div class="group">
                <input name="part_no" id="part_no" type="text" >
                <label class="im-label">Part No</label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
               <!-- <input name="category" type="text" >-->
			   <select id="" name="category">
                  <option value="">Select Category</option>
                  <?php   echo getCategories();  ?>
                </select>
              <!--  <label class="im-label">Category</label>-->
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="description" id="description" type="text" >
                <label class="im-label">Description</label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="cost" type="text">
                <label class="im-label">Cost</label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="price" type="text" >
                <label class="im-label">Price</label>
              </div>
            </div>
          </div>
		  <!--
		  <div class="captcha">
			<div class="g-recaptcha" data-callback='doSomething' data-sitekey="6LfL0CcTAAAAAJC_f6IAi0Fl42uuMpbq7JTS1qWh" ></div>
			<script>function doSomething() { jQuery('#inventory').show(); }</script>
		  </div>
		  -->
          <input type="hidden" value="addInventory" name="action"/>
        </form>
        <div class="clr"></div>
        <div class="text-center"> 
          <p><a href="javascript:void(0)" id="inventory" class="cs-btn btn-green" >Add Item</a>
          <div id="message"> </div>
          </p>
        </div>
      </div>
    </div>
	<div class="edit_form_sec"></div> 
	
	
  </div>
  <div class="table-responsive">
    <h3 class="section-hdr">Inventory </h3>
    <?php 
	  $db = get_connection();		
	  	$statement = $db->prepare(" SELECT  * FROM  inventory order by category asc");	
		
		$statement->execute();
		$result = $statement->fetchAll();
	  ?>
    <table id="example" class="display table data-tbl" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th></th>
          <th>Part No</th>
          <!-- <th>Manufacturer</th>-->
          <th>Category</th>
          <th>Description</th>
          <th>Cost</th>
          <th>Price</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th></th>
          <th>Part No</th>
          <!-- <th>Manufacturer</th>-->
          <th>Category</th>
          <th>Description</th>
          <th>Cost</th>
          <th>Price</th>
          <th>&nbsp;</th>
        </tr>
      </tfoot>
      <tbody>
        <?php 
			$i=1;
			foreach($result as $row)
			{			
			/*$first_name=$cus_result[0]['first_name'];
			$last_name=$cus_result[0]['last_name'];*/
			?>
        <tr class="row_<?php echo $row['id']; ?>">
          <td><img src="assets/images/square-gif.gif"></td>
          <?php 
			$originalDate = $row['created'];
			$newDate = date("d M, Y", strtotime($originalDate));				
				echo "<td class='part'>".$row['part_no']."</td>";
			
				//echo "<td>".$row['manufacturer']."</td>";
				echo "<td class='category'>".$row['category']."</td>";
				echo "<td class='descp'>".trim($row['description'])."</td>";
				echo "<td class='price'>".$row['price']."</td>";
				echo "<td class='cost'>".$row['cost']."</td>";
				?> 
				<td><a href="javascript:void(0)" class="edit-btn-1" onclick="edit_inventory('<?php echo $row['id']; ?>');">Edit</a></td>
				<?php
				echo " </tr>";
			}

		  ?>
      </tbody>
    </table>
    <script type="text/javascript" language="javascript" class="init">		
		$(document).ready(function() {	
		
			$('#example').DataTable({
			"oLanguage": { "sSearch": "" } ,
			})	
			$('div.dataTables_filter input').attr('placeholder', 'Search...');	
			$("#example").wrap("<div class='responsive-table'></div>");
			
			$("#addForm").click(function(){
				$("#fromToggle").slideToggle('slow');
			});
			$(".close").click(function(){
				$("#fromToggle").slideUp('slow');
			});
			
			
		} );
		
		 
		 
		
	</script>
  </div>
</div>
<?php //} ?>
