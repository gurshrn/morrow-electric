<?php $db = get_connection();?>
<div class="area-main generator-area">
	
<div class="user-info-area clearfix">
	
	<div class="section-wrap clearfix">
		
		<div class="assign_box box"></div> 
		
		
		<div class="filter_gen box">
			
				<h3 class="section-hdr">Filter Generators <a href="<?php echo SITE_URL; ?>?section=generator" class="close"><i class="fa fa-plus"></i></a></h3>
				<div class="row">
					<div class="col-sm-12">
						<form name="filter" id="filter" action="" method="post" class="cs-form"> 
							<input type="hidden" name="action" value="FilterGenService">
							<?php
							$sql="SELECT distinct service_reg
							FROM  `customer_generator`";	
							$statement = $db->prepare($sql);	
							$statement->execute();	
							$data = $statement->fetchAll(); 
							?>
						  <div class="form-group">
							<select name="ser_reg" class="is-filled inline-to" onchange="filter_by_region(1);">
								<option value="">Choose A Region</option>  
								<?php
								foreach($data as $row) 						
								{						
								$service_reg=$row['service_reg']; 
								?>
								<option value="<?php echo $service_reg; ?>"><?php echo $service_reg; ?></option>
								<?php
								}
								?>
							</select>
						 
							<a class="cs-btn btn-green" href="javascript:void(0);" onclick="filter_by_region(0);">Reset Filter</a>
						  </div>   
						 
						  
						</form>	
					</div>
				</div>
			
			
		</div>
		
		<div class="heading small clearfix">
			<div class="pull-left">Assign for Service</div>
		</div>
		<div class="details-table assign_listing">
			<?php
			$display_row=10; 
			// $sql="SELECT * 
			// FROM  `customer_generator` 
			// ORDER BY DATE( install_date ) desc ";
			// $statement = $db->prepare($sql);	
			// $statement->execute();
			// $count = $statement->rowCount();
			// if($count > 0)
			// { 
			$sql="SELECT * 
			FROM  `customer_generator` 
			ORDER BY DATE( install_date ) desc";	
			$statement = $db->prepare($sql);	
			$statement->execute();	
			$data = $statement->fetchAll(); 
			?>
			<input type="hidden" class="recent_service_total_rec" value="<?php //echo $count; ?>">
			<input type="hidden" class="recent_service_display_row" value="<?php echo $display_row; ?>">  
			<input type="hidden" class="recent_service_index" value="2">    
			<div class="responsive-table recent_service">
				<table id="example" class="display table data-tbl" cellspacing="0" width="100%">
					<thead>
						<tr>
						<th></th>
						<th style="width: 18%;">Last Serviced</th> 
						<th style="width: 25%;">Customer Name</th> 
						<th>Label</th> 
						<th style="width: 10%;">Type</th>
						<th>Region</th>
						<th></th> 
						</tr>
					</thead>
					<tbody>
						<?php 
						foreach($data as $row) 						
						{						
						$gen_id=$row['id'];   
						$label=$row['label'];   
						$cr_date=$row['install_date'];   
						$cust_id=$row['cust_id']; 
						$cust_name=get_cust_info($cust_id,'company_name'); 
						$type=$row['type']; 
						$service_reg=$row['service_reg']; 
						$last_serviced=get_last_service($gen_id); 
						$last_serviceds=get_last_service($gen_id); 
						if($last_serviced==0)
						{
							$last_serviced="(None)";
						}
						else
						{
							$last_serviced=date('F j, Y', strtotime($last_serviced));
						}		
						?>	 
							<tr id="gen_id_<?php echo $gen_id; ?>"> 
								<td>
									&nbsp;
								</td>
								<td><?php echo $last_serviced; ?></td> 
								<td><?php echo $cust_name; ?></td> 
								<td><?php echo $label; ?></td> 
								<td><?php echo $type; ?></td>  
								<td><?php echo $service_reg; ?></td> 
								
								<td>
								<?php
								if($last_serviceds==0)
								{
								?>
								<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('0',<?php echo $gen_id; ?>);">Assign</a>
								<?php
								}
								else
								{	
								?>
								<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('1',<?php echo $gen_id; ?>);">Edit</a> 
								<?php
								}
								?>
								</td>
							</tr>
							<?php
							
						} 
						?>
						
					</tbody>
				</table>
			</div>
			<?php
			//}
			?>
		</div> 
	</div>
	
	<div class="clr"></div> 
</div>
</div>
<script>
jQuery(function($) {
	jQuery('#filter').validate({
		
		rules: {
			ser_reg: {
				required: true
			},
			
			install_date: {
				required: true 
			}
		},
		 
		submitHandler: function(form) {
						
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: '<?php echo SITE_URL; ?>handler_gen.php', 
				success: function(data) 
				{
					jQuery('.assign_listing').empty().append(data);
				} 
			});
		}
		
	});
});
$(document).ready(function() {	
		
			$('#example').DataTable({
			"oLanguage": { "sSearch": "" } ,
			})	
			$('div.dataTables_filter input').attr('placeholder', 'Search...');	
			$("#example").wrap("<div class='responsive-table'></div>");
			
			
			
			
		} );
function get_install_date()
{
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	var reg=jQuery('select[name=ser_reg]').val();
	jQuery('.install_date_sec').empty().append(loader); 
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "reg="+reg+"&action=GetInstallDate",     
	success:function(result)    
	{
		jQuery('.install_date_sec').empty().append(result); 
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}

function assign_gen(e,gen_id)
{
	
	var ser_on=jQuery('#gen_id_'+gen_id).find('td:eq(1)').html();	
	 
	jQuery('.box').slideUp(); 
	jQuery('.assign_box').slideDown(); 
	
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.assign_box').empty().append(loader); 
	jQuery("html, body").animate({
        scrollTop: jQuery('.section-wrap').offset().top 
    }, 500);
	
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "e="+e+"&gen_id="+gen_id+"&ser_on="+ser_on+"&action=ShowAssignGen",     
	success:function(result)      
	{
		
		jQuery('.assign_box').empty().append(result);
		
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}


function filter_by_region(e)
{
	if(e==1)
	{
		var region=jQuery('select[name=ser_reg]').val();
	}
	else
	{
		jQuery('#filter').trigger('reset'); 
		var region="";
	}		
	
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.assign_listing').empty().append(loader);
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "&region="+region+"&action=FilterByRegion_1",        
	success:function(result)      
	{
		jQuery('.assign_listing').empty().append(result);
		jQuery('#example').dataTable( {
			 columnDefs: [
			   { type: 'date-dd-mmm-yyyy', targets: 0 }
			 ]
		});
	},  
	error:function(e){ 
		console.log(e); 
	}	
	});
}


function reset_filter()
{
	jQuery('#filter').trigger('reset'); 
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.assign_listing').empty().append(loader);
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "&action=ResetGenService",       
	success:function(result)      
	{
		jQuery('.assign_listing').empty().append(result);
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}

function fil_close()
{
	jQuery('.filter_gen').slideUp();
}

</script> 