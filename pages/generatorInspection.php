
<div class="area-main">
  <?php get_navbar(); ?>
   <?php $db = get_connection();?>
  <form role="form" class="cs-form" id="generator-inspection">
    <div class="section-hdr">
      <div class="pull-left">
        <h3>NEW WORKSHEET <span class="sepr">|</span> Generator Inspection</h3>
        <p class="search-customer"><span id="username"></span>
          <input type="text" name="searchname" id="searchname" value="" Placeholder="Search Customer..." autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <div class="date-time">
          <?php //echo date("F d, Y")?>
        </div>
        <div class="date-time addr"><!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number" />
            <label class="im-label">PO Number </label>
          </div>
        </div>
      </div>
    </div>
    <div class="gen-ins-area clearfix">
      <div class="row">
        <div class="col-sm-6">
          <input name="data[customer_id]" id="customer_id" type="hidden">
          <input name="data[service_by]" id="service_by" type="hidden" value="<?php echo $_SESSION['user']; ?>">
          <input name="data[cat_name]" id="cat_name" type="hidden" value="Generator Inspection">
		  <div class="group">
          <input name="data[generator_hours]"  id="generator_hours" type="text" >
		   <label class="im-label">Generator Hours </label>
		   </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text"  name="data[oil][qty]">
			   <label class="im-label">Qty </label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input name="data[oil][name]" id="oil" type="text" >
			   <label class="im-label">Oil</label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text" name="data[oil_filter][qty]" >
			   <label class="im-label">Qty</label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input name="data[oil_filter][name]" id="oil_filter" type="text" >
			     <label class="im-label">Oil Filter</label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
		 <div class="group">
          <input name="data[antifreeze]" id="antifreeze" type="text">
		   <label class="im-label">Antifreeze</label>
		  </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text"  name="data[air_filter_type][qty]">
			  <label class="im-label">Qty</label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input type="text"  name="data[air_filter_type][name]">
			   <label class="im-label">Air Filter Type</label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-3 col-xs-4">
			 <div class="group">
              <input type="text"  name="data[bettery_type][qty]" >
			  <label class="im-label">Qty</label>
			  </div>
            </div>
            <div class="col-sm-9 col-xs-8">
			 <div class="group">
              <input type="text"  name="data[bettery_type][name]">
			   <label class="im-label">Battery Type</label>
			  </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
		 <div class="group">
          <input name="data[travel]" id="travel" type="text">
		  <label class="im-label">Travel</label>
		  </div>
        </div>
        <div class="col-sm-6">
			<div class="group">
				<div class="n-box">
				  <select name="staff_id[]" style="width:40%">
					<option value=""></option> 
					<?php
					$statement = $db->prepare("select * from users where id!='1'");	
					$statement->execute();
					$result = $statement->fetchAll();
					foreach($result as $row)
					{
					?> 
						<option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
					<?php 
					}
					?>
				  </select>
				   <input name="hour[]" type="text" class="is-blank" placeholder="Hours" style="width:40%">
				</div> 
				<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>
			</div> 
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="box" id="customFields">
            <label class="box-title">Parts</label>
            <div class="row">
              <div class="col-sm-2 col-xs-3">
			   <div class="group">
                <input type="text" name="data[com][parts][row][qty]" >
				<label class="im-label">Qty</label>
				</div>
              </div>
              <div class="col-sm-4 col-xs-4">
                <select name="data[com][parts][row][cat]">
                  <option value="">Select Category</option>
                  <?php   echo getCategories();  ?>
                </select>
              </div>
              <div class="col-sm-5 col-xs-4">
			    <input class="inventory-list" type="text" name="data[com][parts][row][inventory]">
			   <!--<div class="group">
                <input class="inventory-list" type="text" name="data[com][parts][row][inventory]">
				<label class="im-label">Inventory</label>
				</div>-->
                <div class="inventory"></div>
              </div>
              <div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <label class="box-title"></label>
          <div class="row">
            <div class="col-sm-6">
			 <div class="group">
              <input name="equipment" id="equipment" type="text" >
			  <label class="im-label">Equipment</label>
			  </div>
            </div>
            <div class="col-sm-6">
			 <div class="group">
              <input name="hours" id="hours" type="text" >
			  <label class="im-label">Hours</label>
			  </div>
            </div>
          </div>
        </div>
      </div>
	   <div class="group">
      <textarea class="notes" name="data[notes]" id="notes" cols="" rows="" ></textarea>
	  <label class="im-label">Notes</label>
	  </div>
      <input type="hidden" value="addGeneratorInspection" name="action"/>
	   <input type="hidden" value="gen_insp" name="work_sheet"/>
      <div class="clr"></div>  
      <div class="actions"> 
        <!--  <p>Service By <a href="#">Aaron Hammond</a> --> 
        <a href="javascript:void(0);" id="inspection" class="cs-btn btn-green">Save Changes</a>
        <div id="message"> </div>
        </p>
      </div>
    </div>
  </form>
</div>
<script>
function add_timecard_staff()
{
	
	var staff_id='';
	jQuery.each(jQuery("select[name='staff_id[]']"), function() {
	staff_id += (staff_id?',':'') + jQuery(this).val();
	});
	
	/* alert(staff_id);
	return false; */
	
	var id=jQuery('.n-box').length;
	jQuery.ajax({type: "POST", 
	url: "handler.php",
	data: "id="+id+"&staff_id="+staff_id+"&action=addTimecardStaff",  
	success:function(result) 
	{
		
		jQuery(result).insertAfter('.n-box:last'); 
	},
	error:function(e){
		console.log(e);
	}	
	}); 
}

function remove_ul(e,id)
{
	if(e==1)
	{
		jQuery('.box_'+id).fadeOut(2000);
		jQuery('.box_'+id).remove();
	}
	else
	{
		
		jQuery('.box_'+id).fadeOut(3000);
		jQuery('.box_'+id).remove();
		
	}		
}

</script>