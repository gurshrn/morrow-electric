<div class="area-main">
  <?php get_navbar(); 
 // print_r($_SESSION);
  ?>
  <?php $db = get_connection();?>
  <form role="form" method="post" action="/?section=electricalWork" class="cs-form" id="electrical-work-form" autocomplete="off">
    <div class="section-hdr">
      <div class="pull-left">
        <h3>NEW WORKSHEET <span class="sepr">|</span> Electrical Work</h3>
        <p class="search-customer"><span id="username"></span>
          <input type="text" name="searchname" id="searchname" value="" Placeholder="Search Customer..." autocomplete="off"/>
          <span id="user_list"></span></p>
      </div>
      <div class="pull-right">
        <!--<div class="date-time"><?php //echo date("F d, Y")?></div>-->
        <div class="date-time addr">
          <!--PO Number-->
          <div class="group">
            <input type="text" name="po_number" id="po_number" />
            <label class="im-label">PO Number </label>
          </div>
        </div>
      </div>
    </div>
    <div class="gen-ins-area clearfix" >
    <div class="row">
      <input name="customer_id" id="customer_id" type="hidden">
      <input name="service_by" id="service_by" type="hidden" value="<?php echo $_SESSION['user']; ?>">
      <input name="user_id" id="user_id" type="hidden" value="<?php echo $_SESSION['uid']; ?>">
      <input name="cat_name" id="cat_name" type="hidden" value="Electrical Work">
      <div class="col-sm-12">
        <p class="cs-radio">Worksheet Type:
          <label>
          <input type="radio" name="worksheet_type" value="worksheet" id="RadioGroup1_0" checked="checked">
           <span class="custm-radio"></span>Worksheet</label>
          <label>
          <input type="radio" name="worksheet_type"  value="quote" id="RadioGroup1_1">
           <span class="custm-radio"></span>Quote</label>
        </p>
      </div>
      <div class="col-sm-6">
        <div class="group">
			<div class="n-box">
				<div class="row">
					<div class="col-md-8">
					  <select name="staff_id[]" style="width:100%">
						<option value="">Select Staff</option> 
						<?php
						$statement = $db->prepare("select * from users where id!='1'");	
						$statement->execute();
						$result = $statement->fetchAll();
						foreach($result as $row)
						{
						?> 
							<option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'].' '.$row['last_name']; ?></option>
						<?php 
						}
						?>
					  </select>
					</div>
					<div class="col-md-3">
						<div class="group">
							<input name="hour[]" type="text" class="is-blank validNumber"  style="width:100%">
							<label class="im-label">Hours</label>
						</div>
					</div>
				</div>
            </div> 
			
			<a href="javascript:void(0);" class="cs-btn btn-green" onclick="add_timecard_staff();">Add Staff +</a>
        </div>
		<div class="clr"></div>
      </div>
      <div class="col-sm-6">
        <div class="group">
			<div class="vehicleField">
				<div class="row">
					<div class="col-sm-8">
						<input type="hidden" class="vehicleRate">
						<input type="hidden" class="vehCount" value="0">
						<select id="vehicleRate" name="vehicle[parts][row_0][vehicle]" class="">
							<option value="">Select Vehicles</option>
							<?php   echo getVehicles();  ?>
						</select>
					</div>
					<div class="col-sm-3">
						<div class="group">
						  <input name="vehicle[parts][row_0][hours]" id="vehiclehours" type="text"  class="is-blank validNumber" disabled>
						  <label class="im-label">Travel</label>
						  <label id="uerror"></label>
						</div>
					</div>
				</div>
			</div>
			<a href="javascript:void(0);" class="cs-btn btn-green addVehicles">Add Vehicles +</a>
        </div>
      </div>
      <div class="col-sm-7">
        <div class="box" id="customFields">
          <label class="box-title">Parts</label>
          <div class="row">
            <div class="col-sm-2 col-xs-3"> 
              <div class="group">
			  <input type="hidden" value="0" class="invCount">
                <input type="text" name="data[parts][row_0][qty]"  class="is-blank qtyyy validNumber" maxlength="4" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"> 
                <label class="im-label">Qty </label>        
              </div>  
            </div>
            <div class="col-sm-4 col-xs-4">
              <select id="" name="data[parts][row_0][cat]">
                <option value="">Select Category</option>
                <?php   echo getCategories();  ?>
              </select>
            </div>
            <div class="col-sm-5 col-xs-4">
              <!-- <div class="group">
                <input class="inventory-list" type="text" name="data[parts][row][inventory]" >
				<label class="im-label">Inventory </label>
				</div>-->
              <input class="inventory-list" type="text" name="data[parts][row_0][inventory]"  placeholder="Inventory">
              <div class="inventory"></div>
            </div>
            <div class="add-more editAddmore_serv"><a href="javascript:void(0);" class=""><i class="fa fa-plus"></i></a></div>
          </div>
        </div>
      </div>
      <div class="col-sm-5">
        <label class="box-title"></label>
        <div class="equipmentFields">
			<div class="row">
				<div class="col-sm-8">
				<input type="hidden" class="eupCount" value="0">
					<input type="hidden" class="equipmentRate">
					<select id="equipmentRate" name="equip[parts][row_0][equipment]" class="">
						<option value="">Select Equipment</option>
						<?php   echo getEquipments();  ?>
					</select>
				</div>
				<div class="col-sm-3">
					<div class="group">
					  <input name="equip[parts][row_0][hours]" id="equiphours" type="text"  class="is-blank validNumber" disabled>
					  <label class="im-label">Hours </label>
					  <label id="uerror"></label>
					</div>
				</div>
			</div>
        </div>
		<a href="javascript:void(0);" class="cs-btn btn-green addEquipment">Add Equipment +</a>
      </div>
    </div>
    <div class="group">
      <textarea class="notes" name="notes" cols="" rows=""  class="is-blank">
      </textarea>
      <label class="im-label">Notes </label>
      <!--	  <input type="submit" value="submit" name="submit"/>-->
      <input type="hidden" value="addElectricalWork" name="action"/>
      <input type="hidden" value="elec_work" name="work_sheet"/>
      <div class="clr"></div>
      <div class="actions"> <a href="javascript:void(0)" class="cs-btn btn-green" id="electrical">Save Changes</a>
        <div id="message" style="color:#8EC94A"> </div>
        </p>
      </div>
    </div>
  </form>
</div>

<script>



function explode(){
  jQuery('#customFields').find('input[type=number]').addClass('qtyyy').attr('maxlength','4');
  
 
  setTimeout(explode, 500);
	jQuery(document).ready(function($)
	{ 
		jQuery('.qtyyy').keydown(function (e) {
		if (e.shiftKey || e.ctrlKey || e.altKey) {
		e.preventDefault();
		} else {
		var key = e.keyCode;
		if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
		e.preventDefault();
		} 
		}
		});
	});
}

jQuery(document).ready(function($)
{ 
	setTimeout(explode, 500);
	jQuery('.qtyyy').keydown(function (e) {
	if (e.shiftKey || e.ctrlKey || e.altKey) {
	e.preventDefault();
	} else {
	var key = e.keyCode;
	if (!((key == 8) || (key == 46) || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105))) {
	e.preventDefault();
	} 
	}
	});
	
});



function add_timecard_staff()
{
	
	var staff_id='';
	jQuery.each(jQuery("select[name='staff_id[]']"), function() {
	staff_id += (staff_id?',':'') + jQuery(this).val();
	});
	
	/* alert(staff_id);
	return false; */
	
	var id=jQuery('.n-box').length;
	jQuery.ajax({type: "POST", 
	url: "handler.php",
	data: "id="+id+"&staff_id="+staff_id+"&action=addTimecardStaff",  
	success:function(result)   
	{
		
		jQuery(result).insertAfter('.n-box:last'); 
	},
	error:function(e){
		console.log(e);
	}	
	}); 
}

function remove_ul(e,id)
{
	if(e==1)
	{
		jQuery('.box_'+id).fadeOut(2000);
		jQuery('.box_'+id).remove();
	}
	else
	{
		
		jQuery('.box_'+id).fadeOut(3000);
		jQuery('.box_'+id).remove();
		
	}		
}

</script>