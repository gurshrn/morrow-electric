<?php 
ini_set('memory_limit', '2000M');
$db = get_connection();
?>
<div class="area-main generator-area">
	<div class="top-hdr clearfix">
		
		<div class="pull-right"> 
		<a href="<?php echo SITE_URL; ?>?section=assign_service" class="cs-btn btn-blue">Assign for Service</a> 
		<a href="javascript:void(0);" onclick="show_report_section();" class="cs-btn btn-blue">New Report</a> </div>
	</div>
	<div class="user-info-area clearfix">
	
		<div id="editFromToggle" class="collapsible-area edit-wrksheet no-margin cust_gen_area" style="display:none"> </div>
	
		<div class="figures-count heading row" style="margin-top: 10px;float: left;">		 
		
			<div class="col-md-3">
			<?php echo get_total_assigned_services(); ?>  
			<small>Assigned for Service</small> </div>
			<div class="col-md-3">
			<?php echo count_services_this_week(); ?>
			<small>Serviced This Week</small></div>
			<div class="col-md-3">
			<?php echo count_gen_progress_repair_1() ?>
			
			<small>Repairs In Progress</small></div>
			<div class="col-md-3">
			<?php 
			echo count_no_service_record(); 
			
			?>
			<small>No Service Records</small></div>
			<div class="col-md-3">
			<?php echo count_expiring_warranty(); ?>
			<small>Expiring Warranties</small></div>
		</div>
		
		
		
		<div class="section-wrap in_progress_repair clearfix">
			<div class="heading small clearfix">
				<div class="pull-left">In Progress Repairs</div>
			</div>
			<div class="recently_added_gen">
			
				 
				  <div class="wrapper-cell">
					<!---div class="image"></div-->
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
				 
			
			
			
			</div>
		</div>
		<div class="section-wrap clearfix">
			<div class="heading small clearfix" id="id_recent_added"> 
				<div class="pull-left">Recently Added</div>
			</div>
			<div class="recently_added_gen">
				 <div class="wrapper-cell">
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
			</div>
		</div>
		<div class="section-wrap clearfix">
			<div class="heading small clearfix">
				<div class="pull-left">Assigned for Service</div>
			</div>
			<div class="asigned_service_gen">
				 <div class="wrapper-cell">
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
			
			
			</div>
		</div>
		<div class="section-wrap clearfix">
			<div class="heading small clearfix">
				<div class="pull-left">Recently Serviced</div>
			</div>
			<div class="recently_service_gen">
				 <div class="wrapper-cell">
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
			
			</div>
		</div>
		<div class="section-wrap clearfix">
			<div class="heading small clearfix" id="id_alert_area">
				<div class="pull-left">Alert Area</div>
			</div>
			<div class="alert_area_gen">			
				 
				  <div class="wrapper-cell">
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
				 
			</div>
		</div>
		<div class="section-wrap clearfix">
			<div class="heading small clearfix">
				<div class="pull-left">Warranty Expiring</div>
				<div class="pull-right"><a href="<?php echo SITE_URL; ?>?section=generator&days=30">30 Days</a> | <a href="<?php echo SITE_URL; ?>?section=generator&days=60">60 Days</a> | <a href="<?php echo SITE_URL; ?>?section=generator&days=90">90 Days</a></div>
			</div> 
			<div class="warranty_expiration_gen">
				 <div class="wrapper-cell">
					<div class="text">
					  <div class="text-line"> </div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					  <div class="text-line"></div>
					</div>
				  </div>
			
			</div>
		</div>
		<div class="clr"></div> 
	</div>
</div>
<script>

jQuery(document).ready(function(){
	progress_repair();
	recently_added_gen();
	asigned_service_gen();
	recently_service_gen();
	alert_area_gen();
	warranty_expiration_gen();
});

function progress_repair()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=progress_repair",      
		success:function(result)    
		{
			jQuery('.in_progress_repair').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	}); 
}
function recently_added_gen()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=recently_added_gen",      
		success:function(result)    
		{
			jQuery('.recently_added_gen').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	}); 
}
function asigned_service_gen()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=asigned_service_gen",      
		success:function(result)    
		{
			jQuery('.asigned_service_gen').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	}); 
}
function recently_service_gen()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=recently_service_gen",      
		success:function(result)    
		{
			jQuery('.recently_service_gen').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	});
}
function alert_area_gen()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=alert_area_gen",      
		success:function(result)    
		{
			jQuery('.alert_area_gen').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	});
}
function warranty_expiration_gen()
{
	jQuery.ajax({
		type: "POST",   
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=warranty_expiration_gen",      
		success:function(result)    
		{
			jQuery('.warranty_expiration_gen').html(result);
		}, 
		error:function(e){ 
			console.log(e);  
		}	
	});
}

function page_progress()  
{
	
	var total_rec=jQuery('.progress_rep_total_rec').val();
	var display_row=jQuery('.progress_rep_display_row').val();   
	var recent_index=jQuery('.progress_rep_index').val();     
	
		
	jQuery.ajax({type: "POST",   
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageProgress",      
	success:function(result)    
	{
		var curr_rows=jQuery('.progress_rep').find('tbody').find('tr').length;
		var recent=parseInt(recent_index)+1;  
		jQuery('.progress_rep_index').val(recent);     
		var content=jQuery('.progress_rep').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);      
		
		jQuery('.recent_less').empty().append('<a class="view-btn more progress_added_less" href="javascript:void(0);" onclick="page_progress_less();">Less</a>');   
		
		if(parseInt(curr_rows)>=parseInt(total_rec))  
		{
			jQuery('.progress_rep_more').hide();       
		}	
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}

function page_progress_less()  
{
	
	var total_rec=jQuery('.progress_rep_total_rec').val();
	var display_row=jQuery('.progress_rep_display_row').val();   
	var recent_index=jQuery('.progress_rep_index').val();     
	var recent=parseInt(recent_index)-1;   
		
	jQuery.ajax({type: "POST",   
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageProgress",      
	success:function(result)    
	{
		
		
		jQuery('.progress_rep_index').val(recent);     
		var content=jQuery('.progress_rep').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);      
		var curr_rows=jQuery('.progress_rep').find('tbody').find('tr').length;
		
		jQuery('.recent_less').empty().append('<a class="view-btn more progress_added_less" href="javascript:void(0);" onclick="less=1&page_progress_less();">Less</a>');   
		
		if(parseInt(curr_rows)<=parseInt(display_row))     
		{ 
			jQuery('.progress_added_less').hide();    
			 
		} 
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}


function page_recent_service()  
{ 
	
	var total_rec=jQuery('.recent_service_total_rec').val();
	var display_row=jQuery('.recent_service_display_row').val();   
	var recent_index=jQuery('.recent_service_index').val();     
	
		
	jQuery.ajax({type: "POST",   
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageRecentService",       
	success:function(result)    
	{
		var curr_rows=jQuery('.recent_service').find('tbody').find('tr').length; 
		var recent=parseInt(recent_index)+1;  
		jQuery('.recent_service_index').val(recent);     
		var content=jQuery('.recent_service').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);      
		
		if(parseInt(curr_rows)>=parseInt(total_rec))  
		{
			jQuery('.recent_service_more').hide();       
		}	 
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}

function page_recent_added()
{
	
	var total_rec=jQuery('.recent_total_rec').val();
	var display_row=jQuery('.display_row').val();   
	var recent_index=jQuery('.recent_index').val();    
	
		
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageRecentAdded",     
	success:function(result)    
	{
		var curr_rows=jQuery('.recent_added').find('tbody').find('tr').length;
		var recent=parseInt(recent_index)+1;  
		jQuery('.recent_index').val(recent);    
		var content=jQuery('.recent_added').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);  
		
		jQuery('.recent_less').empty().append('<a class="view-btn more recent_added_less" href="javascript:void(0);" onclick="page_recent_less();">Less</a>');   
		
		
		if(parseInt(curr_rows)>=parseInt(total_rec))    
		{
			jQuery('.recent_added_more').hide(); 
			jQuery('.recent_added_less').hide();  
		}	 
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}


function page_recent_less()
{
	var total_rec=jQuery('.recent_total_rec').val();
	var display_row=jQuery('.display_row').val();   
	var recent_index=jQuery('.recent_index').val();    
	var recent=parseInt(recent_index)-1;   
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "less=1&display_row="+display_row+"&offset="+recent+"&action=PageRecentAdded",     
	success:function(result)      
	{ 
		jQuery('.recent_added_less').hide(); 
		jQuery('.recent_index').val(recent);    
		jQuery('.recent_added').find('tbody').empty().append(result);
		var curr_rows=jQuery('.recent_added').find('tbody').find('tr').length; 
		
		jQuery('.recent_less').empty().append('<a class="view-btn more recent_added_less" href="javascript:void(0);" onclick="page_recent_less();">Less</a>');  
		
		jQuery("html, body").animate({
        scrollTop: jQuery('#id_recent_added').offset().top 
		}, 500); 
		
		if(parseInt(curr_rows)<=parseInt(display_row))     
		{
			jQuery('.recent_added_less').hide(); 
			 
		} 	
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
}


function page_alert_area() 
{
	
	var total_rec=jQuery('.alert_area_total_rec').val();
	var display_row=jQuery('.alert_area_display_row').val();   
	var recent_index=jQuery('.alert_area_index').val();     
	jQuery('.alert_recent_less').show();
		
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageAlertArea",      
	success:function(result)    
	{
		var curr_rows=jQuery('.alert_area').find('tbody').find('tr').length;
		var recent=parseInt(recent_index)+1;  
		jQuery('.alert_area_index').val(recent);     
		var content=jQuery('.alert_area').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);   
		jQuery('.alert_recent_less').empty().append('<a class="view-btn more alert_added_less" href="javascript:void(0);" onclick="page_alert_area_less();">Less</a>');       
		if(parseInt(curr_rows)>=parseInt(total_rec))  
		{ 
			jQuery('.alert_area_more').hide();   
		}	
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}

function page_alert_area_less()  
{
	
	var total_rec=jQuery('.alert_area_total_rec').val();
	var display_row=jQuery('.alert_area_display_row').val();   
	var recent_index=jQuery('.alert_area_index').val();     
	var recent=parseInt(recent_index)-1;  
		
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "less=1&display_row="+display_row+"&offset="+recent+"&action=PageAlertArea",      
	success:function(result)     
	{ 
		
		
		jQuery('.alert_area_index').val(recent);     
		jQuery('.alert_area').find('tbody').empty().append(result);
		var curr_rows=jQuery('.alert_area').find('tbody').find('tr').length; 
		
		jQuery('.alert_recent_less').empty().append('<a class="view-btn more alert_added_less" href="javascript:void(0);" onclick="page_alert_area_less();">Less</a>');    
		jQuery("html, body").animate({
        scrollTop: jQuery('#id_alert_area').offset().top 
		}, 500);  
		
		if(parseInt(curr_rows)<=parseInt(display_row))     
		{
			jQuery('.alert_recent_less').hide();  
			 
		} 	 
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}

function page_assign_services() 
{
	
	var total_rec=jQuery('.assgn_services_total_rec').val();
	var display_row=jQuery('.assgn_services_display_row').val();   
	var recent_index=jQuery('.assgn_services_index').val();     
	
		
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&action=PageAssignServices",      
	success:function(result)     
	{
		var curr_rows=jQuery('.assgn_services').find('tbody').find('tr').length;
		var recent=parseInt(recent_index)+1;  
		jQuery('.assgn_services_index').val(recent);     
		var content=jQuery('.assgn_services').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);   
		
		if(parseInt(curr_rows)>=parseInt(total_rec))  
		{
			jQuery('.assgn_services_more').hide();   
		}	
		
		
	}, 
	error:function(e){ 
		console.log(e);  
	}	
	}); 
	
}

function page_warranty_exp() 
{
	
	var total_rec=jQuery('.warranty_exp_total_rec').val();
	var display_row=jQuery('.warranty_exp_display_row').val();   
	var recent_index=jQuery('.warranty_exp_index').val();     
	var days=jQuery('.days').val();     
	
		
	jQuery.ajax({type: "POST",  
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "display_row="+display_row+"&offset="+recent_index+"&days="+days+"&action=PageWarrantyExp",      
	success:function(result)     
	{
		var curr_rows=jQuery('.warranty_exp').find('tbody').find('tr').length;
		var recent=parseInt(recent_index)+1;  
		jQuery('.warranty_exp_index').val(recent);      
		var content=jQuery('.warranty_exp').find('tbody').find('tr:last');
		jQuery(result).insertAfter(content);    
		
		if(parseInt(curr_rows)>=parseInt(total_rec))  
		{
			jQuery('.warranty_exp_more').hide();         
		}	
		
		
	}, 
	error:function(e){ 
		console.log(e);   
	}	
	}); 
	
}

function show_cust_gen(e,cust_id)
{
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('#editFromToggle').empty().append(loader);
	jQuery('#editFromToggle').show(); 
	
	jQuery("html, body").animate({
        scrollTop:jQuery('#editFromToggle').offset().top 
		}, 2000);  
	
	
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_new.php",
	data: "e="+e+"&cust_id="+cust_id+"&action=ShowCustomerGen",     
	success:function(result)    
	{
		
		jQuery('#editFromToggle').empty().append(result); 
		 
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 


} 

function sel_item(inv,id,item)
{
	if(inv==1)
	{
		jQuery('input[name=inventory_1]').val(item);
		jQuery('input[name=inventory_1]').addClass('is-filled');
		jQuery('#oil_filter_id').val(id); 
		jQuery('.oil_filter').empty();
	}	
	else if(inv==2)     
	{
		jQuery('input[name=inventory_2]').val(item);
		jQuery('input[name=inventory_2]').addClass('is-filled'); 
		jQuery('#air_filter_id').val(id);
		jQuery('.air_filter').empty(); 
	}	
	else if(inv==3)
	{
		jQuery('input[name=inventory_3]').val(item); 
		jQuery('input[name=inventory_3]').addClass('is-filled');  
		jQuery('#spark_plug_id').val(id);
		jQuery('.spark_plug').empty();
	}	
}

function assign_gen(e,gen_id)
{
	jQuery("html, body").animate({
        scrollTop: jQuery('#editFromToggle').offset().top 
    }, 1000); 
	var ser_on=jQuery('.gen_id_'+gen_id).find('td:eq(1)').html();	
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('#editFromToggle').empty().append(loader);
	jQuery('#editFromToggle').show();  
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "e="+e+"&gen_id="+gen_id+"&ser_on="+ser_on+"&action=ShowAssignGen",     
	success:function(result)      
	{
		jQuery('#editFromToggle').empty().append(result); 
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}

function show_report_section()
{
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('#editFromToggle').empty().append(loader);
	jQuery('#editFromToggle').show();  
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "&action=ShowGenReportt",     
	success:function(result)      
	{
		jQuery('#editFromToggle').empty().append(result); 
	}, 
	error:function(e){ 
		console.log(e); 
	}	
	}); 
}

function show_hide_reg_drop(e)  
{
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.reg_sec').empty().show().append(loader); 
	if(e==1) 
	{
		var region="";
		
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=IncludeRegionSelect",     
		success:function(result)          
		{ 
			jQuery('.reg_sec').empty().append(result);    
			//jQuery('.reg_sec').empty().append('');    
			jQuery('.report_submit').empty().append('<a class="cs-btn btn-green cust_region_rep_btn"  href="<?php echo SITE_URL; ?>reports/service-by-region.php?type=all" target="_blank">Create Report</a>');  
		},    
		error:function(e){ 
			console.log(e); 
		}	
		}); 
		
		    
	}	
	if(e==2)  
	{
		
		jQuery('.report_submit').empty().append('<a class="cs-btn btn-green" href="<?php echo SITE_URL; ?>winter_report.php" onclick="create_report();">Create Report</a>');   
		jQuery('.reg_sec').hide();
	}	
	 
	if(e==3)   
	{
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "action=IncludeServiceDatepicker",     
		success:function(result)       
		{ 
			jQuery('.reg_sec').empty().append(result);      
			jQuery('.report_submit').empty().append('<a class="cs-btn btn-green"  href="<?php echo SITE_URL; ?>serviced_report.php?date=0">Create Report</a>');  
		},    
		error:function(e){  
			console.log(e); 
		}	
		}); 
		  
	
	}	
	jQuery('.cs-btn').show(); 
}

function check_region_report_type()
{
	var region_type=jQuery('select[name=region_type]').val();
	if(region_type=='all')
	{
		jQuery('.cust_region_rep_btn').attr('href','<?php echo SITE_URL; ?>reports/service-by-region.php?type=all');
	}	
	else
	{
		jQuery('.cust_region_rep_btn').attr('href','<?php echo SITE_URL; ?>reports/service-by-region.php?type=current');
	}	 
}


function create_report()
{
	var report_type=jQuery('input[name=report_type]:checked').val();
	var region=jQuery('select[name=region]').val();
	if(report_type==1)
	{
		var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
		jQuery('.assign_listing').empty().append(loader);
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "region="+region+"&action=ReportbyRegion",       
		success:function(result)       
		{
			jQuery('.assign_listing').empty().append(result);
		}, 
		error:function(e){  
			console.log(e); 
		}	
		}); 
	}	
	else
	{
		var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
		jQuery('.assign_listing').empty().append(loader);
		jQuery.ajax({type: "POST", 
		url: "<?php echo SITE_URL; ?>/handler_gen.php",
		data: "&action=ReportbySpringWinter",       
		success:function(result)       
		{
			jQuery('.assign_listing').empty().append(result);
		}, 
		error:function(e){  
			console.log(e); 
		}	
		}); 
	}	
}

function check_region_report()
{
	var region=jQuery('select[name=region]').val();
	var loader='<center><img src="<?php echo SITE_URL; ?>/assets/images/loader.gif" /></center>';
	jQuery('.report_submit').empty().append(loader);
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "&region="+region+"&action=CheckRegionReport",       
	success:function(result)       
	{
		if(result==0)
		{
			jQuery('.report_submit').empty().append('<div class="alert static alert-danger">No Generator Data Is Available For This Region</div>'); 
		}	
		else 
		{
			
			jQuery('.report_submit').empty().append('<a class="cs-btn btn-green"  href="<?php echo SITE_URL; ?>region_report.php?region='+region+'"  style="">Create Report</a>');   
		}		
		//jQuery('.report_submit').empty().append(result); 
	}, 
	error:function(e){  
		console.log(e); 
	}	
	}); 
}
 

</script>