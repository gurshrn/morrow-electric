<?php 
$uid=$_SESSION['uid'];
$role=	userRole($uid);
if( $role['0']!='admin')
{
?>

<div class="area-main"> <?php echo "You are not authorized to this location";?> </div>
<?php }
else 
{
?>
<div class="area-main">
  <div class="top-hdr clearfix">
    <div class="pull-left"><a href="javascript:void(0)" id='addForm' class="cs-btn btn-blue">Add Staff</a></div>
    <div class="pull-right"> <a href="<?php echo SITE_URL; ?>?section=addCustomer" class="cs-btn btn-blue">New Customer</a> </div>
    <div id="fromToggle" class="collapsible-area" style="display:none">
      <div class="section-hdr">
        <h3>Staff Details</h3>
        <a href="javascript:void(0);" class="close"><i class="fa fa-plus"></i></a> </div>
      <div class="gen-ins-area clearfix">
        <form role="form" class="cs-form" id="add-staff">
          <div class="row">
            <div class="col-sm-6">
              <div class="group">
                <input name="first_name" id="first_name" type="text" >
                <label class="im-label">First Name</label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="last_name" type="text" >
                <label class="im-label">Last Name </label>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="username" id="username" type="text" >
                <label class="im-label">Username </label>
              </div>
            </div>
            <div class="col-sm-6">
              <select name="role">
                <option value="">--- Select Role ---</option>
                <option value="staff">Staff</option>
                <option value="admin">Admin</option>
              </select>
            </div>
            <div class="col-sm-6">
              <div class="group">
                <input name="email" type="text">
                <label class="im-label">Email address </label>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="group">
                <input name="password" type="password" >
                <label class="im-label">Password </label>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="group">
                <input name="billrate" class="validNumber" type="text" >
                <label class="im-label">Billable Rate </label>
              </div>
            </div>
			<div class="col-sm-6">
              <div class="group">
                <input name="payroll" class="validNumber payroll" type="text" >
                <label class="im-label">Payroll Rate </label>
              </div>
            </div>
          </div>
          <input type="hidden" value="addStaff" name="action"/>
        </form>
        <div class="clr"></div>
        <div class="text-center">
          <p><a href="javascript:void(0)" id="staff" class="cs-btn btn-green">Create</a>
          <div id="message"> </div>
          </p>
        </div>
      </div>
    </div>
    <div id="editFromToggle" class="collapsible-area" style="display:none"></div>
  </div>
  <div class="table-responsive">
    <h3 class="section-hdr">Staff List</h3>
    <?php 
	$db = get_connection();			
	$sql="SELECT  * FROM  users where id!=1";	
	if( $role['0']!='admin')
	{ 
	$sql.=" and role!='admin'";
	}
	$statement = $db->prepare($sql);		
	$statement->execute();
	$result = $statement->fetchAll();
	  ?>
    <table id="example" class="display table data-tbl" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th> </th>
          <th>First name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Role</th>
          <th align="center">Action </th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th> </th>
          <th>First name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Role</th>
          <th>Edit / Delete</th>
        </tr>
      </tfoot>
      <tbody>
        <?php 
					$i=2;
			foreach($result as $row)
			{			
			$statement1 = $db->prepare("SELECT first_name,last_name,email FROM customers where id=".$row['customer_id']);	
			 	
			$statement1->execute();
			$cus_result = $statement1->fetchAll();
						
			$first_name=$cus_result[0]['first_name'];
			$last_name=$cus_result[0]['last_name'];
			$email=$cus_result[0]['email'];
			?>
        <tr id="row_<?php echo $i?>">
          <td><img src="assets/images/square-gif.gif"></td>
          <?php 
			$originalDate = $row['created'];
			$newDate = date("d M, Y", strtotime($originalDate));				
				echo "<td class='fname'><span>".$row['first_name']."</span><input name='first_name_".$i."' type='text' value='".$row['first_name']."' style='display:none'></td>";
				
				
				echo "<td><span>".$row['last_name']."</span><input name='first_name_".$i."' type='text' value='".$row['last_name']."' style='display:none'></td>";
				echo "<td><span>".$row['email']."</span><input name='first_name_".$i."' type='text' value='".$row['email']."' style='display:none'></td>";
				echo "<td><span>".ucfirst($row['role'])."</span><select name='user-role' style='display:none'><option value='admin'>Admin</option><option value='staff'>staff</option></select></td>";
				
				$name=$row['first_name'].' ';
				$name.=trim($row['last_name']);
	
				$id= "myModal".$i;
				echo "<td><a id=".$row['id']." href='javascript:void(0)' class='view-btn'>Edit</a>";
				
				if($uid!=$row['id'])
				echo "<a class='staff-del' id=".$row['id']." u='".$name."' href='javascript:void(0)' class='delete-this' > <i class='fa fa-trash-o' aria-hidden='true'></i> </a>";
				else 
				echo "<a  href='javascript:void(0)' class='not-allowed' > <i class='fa fa-trash-o' aria-hidden='true'></i> </a>";
				
				
			$i++;
			}
		  
		  ?>
      </tbody>
    </table>
    <script type="text/javascript" language="javascript" class="init">		
		$(document).ready(function() {
				jQuery(document).on('keypress', '.validNumber', function (eve) 
	{
		if (eve.which == 0) {
			return true;
		}
		else
		{
			if (eve.which == '.') 
			{
				eve.preventDefault();
			}
			if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57)) 
			{
				if (eve.which != 8)
				{
					eve.preventDefault();
				}
			}
		}
	});
		//table = $('#example').DataTable();		
		
			table=$('#example').DataTable({
			responsive: true,
			"oLanguage": { "sSearch": "" } ,
			})		
			
			$('div.dataTables_filter input').attr('placeholder', 'Search...');	
			jQuery("#example").wrap("<div class='responsive-table'></div>");
			
			})
	
	
	$('#example tbody').on( 'click', '.staff-del', function () {
		 $(this).parent().parent().addClass("deletion ");
		 var username= $(this).attr("u");
		 
	 if(confirm ("Are you sure you want to Delete: " +username+"?"))
	 {	
			$(this).fadeOut("slow", function() {
			table.row( $(this).parents('tr') ).remove().draw();
			});	
    
		 id=$(this).attr("id");		
		$.ajax({type: "POST",
								url: "handler.php",
								data: "id="+id+"&action=staffDel",
								success:function(result){
								
								},
								error:function(e){
									console.log(e);
								}	
							  });
							  
							  }
	 else 
	 {
		  $(this).parent().parent().removeClass("deletion ");
	 return;  
	 }
	 
	
} );

$('#example tbody').on( 'click', '.view-btn', function () {

    $('html, body').animate({scrollTop:0}, 'slow');

	var recId=$(this).attr("id");	
		$.ajax({type: "POST",
								url: "handler.php",
								data: "id="+recId+"&action=staffEdit",
								success:function(result){
								$("#fromToggle").hide();	
								$("#editFromToggle").show();	
								$("#editFromToggle").html(result);
								$('#editFromToggle input').filter(function() {
												return this.value;
											}).addClass('is-filled');
											$('#editFromToggle .notes').filter(function() {
												return this.value;
											}).addClass('is-filled');
								
								},
								error:function(e){
									console.log(e);
								}	
							  });

	/*var rowId=$(this).parent().parent().attr("id")
	$("#"+rowId+" td input").show();
  $("#"+rowId+" td select").show();
	$("#"+rowId+" td span").hide();
  $(this).html('save')
  $(this).next().show()
  $(this).hide();*/
} );

	$(document).ready(function() {	
		
			
			
			$("#addForm").click(function(){
				$("#editFromToggle").hide();			
				$("#fromToggle").slideToggle('slow');  
			});
			$(".not-allowed").click(function(){
				alert("You can't delete yourself.")
			});
			$(".close").click(function(){
				$("#fromToggle").slideUp('slow');
				$("#editFromToggle").slideUp('slow');
			});
		
		} );
	
	</script>
  </div>
</div>
</div>
<?php 

}
?>
