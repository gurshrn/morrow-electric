<?php 
include('setup.php');
$db = get_connection();	 
get_mysqlconnection();	
$today=date('Y-m-d');
$file_name='Service Region.pdf';  

$sql="SELECT DISTINCT name FROM `service_region` order by name asc ";       

$statement = $db->prepare($sql);	  
$statement->execute();	    
$result = $statement->fetchAll(); 
?>
<link href="<?php echo SITE_URL; ?>assets/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet -->
<link href="<?php echo SITE_URL; ?>assets/css/style.css" rel="stylesheet">



<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://morrowelectric.pro/jquery.mosaicflow.min.js"></script>


<style>
  @import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');
 
 body{
	 
	 font-family: 'Open Sans', sans-serif; 
	 margin:0;
	 padding:0; 

  }
ul {
    margin: 0;
    padding: 0;
}
li{ 
           box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
	 list-style:none;
	 text-align:left;
	     margin: 0 0 15px;	
 } 
h4 {
    color: #000;

    font-weight: 600;
    margin: 0 0 5px;
    width: 100%;
    border-bottom: 1px solid #000;
    display: table-cell;
} 
.mosaicflow__item {
    display: table;
    width: 100%;
    padding: 0 12px;
}
.cities-check, .client-name, .date-section,
.client-label {
    display: table-cell;
    vertical-align: top;
    padding: 5px 8px;
	
	   box-sizing:border-box;
   -webkit-box-sizing:border-box;
      -moz-box-sizing:border-box;
	   -ms-box-sizing:border-box;
}
h2{ 
font-size:32px;
	margin:10px 0 20px;
	text-align:center;	
 }
.cities-check {
    width: 16px;
    padding: 10px 3px;
}
.cities-check img{
  width:10px;	
 }
.client-name {
    padding: 5px 5px;
	width:33%;
}
.client-label {
    font-size: 12px;
    width: 30%;
}
.date-section {
    width: 75px;
    font-size: 11px;
} 
.mosaicflow__column {
 float:left;
 }
.mosaicflow__item img {
 display:block;
 width:100%;
 height:auto;
 }
 
</style> 

<div style="margin:0 auto; max-width:1200px;">

  <h2>Morrow Electric Generator Service 2016</h2>
  
  <ul class="clearfix mosaicflow">
    <?php
	foreach($result as $row)
	{
	$service_reg=$row['name'];	 
	?>
	<li class="mosaicflow__item"><h4><?php echo $service_reg; ?></h4></li>
		<?php
		$sql1="SELECT * 
		FROM  `customer_generator` 
		WHERE  service_reg =  '".$service_reg."'";
		$statement1 = $db->prepare($sql1);	  
		$statement1->execute();	     
		$result1 = $statement1->fetchAll(); 
		foreach($result1 as $row1)  
		{
			$gen_id=$row1['id'];
			$label=get_gen_info($gen_id,'label');
			$cust_id=get_gen_info($gen_id,'cust_id');
			$type=get_gen_info($gen_id,'type');
			$cust_name=get_cust_info($cust_id,'company_name');
			$home_address=get_cust_info($cust_id,'home_address');
			$cottage_address=get_cust_info($cust_id,'cottage_address');
			$service_date=count_gen_progress_repair($gen_id);
			$home_address=get_cust_info($cust_id,'home_address');
			$cottage_address=get_cust_info($cust_id,'cottage_address');
			if($cottage_address!="")
			{
				$address=$cottage_address;
			}
			else
			{
				$address=$home_address;
			}	
			if($service_date!=0)
			{
				$service_date=date('M d, Y', strtotime($service_date));
			}	
			else
			{
				$service_date="";
			}	
			
			$tick=serviced_gen_worksheet_report($gen_id);
			
		?>
		<li class="mosaicflow__item">
	  
	 
			<div class="cities-check">
			    <?php
				if($tick > 0)
				{	
				?>
					<img src="<?php echo SITE_URL; ?>tick.png">
				<?php
				}
				?>
			</div>
			<div class="client-name">
			   <?php echo $cust_name; ?>
			</div>
			<div class="client-label">
			  <?php echo $label; ?>
			</div>
			<?php
			if($service_date!="")
			{	
			?>
				<div class="date-section">
				  <?php echo $service_date; ?>
				</div>     
			<?php
			}
			else
			{	
			?>
				<div class="date-section">
				 (No Date)
				</div>  
			<?php
			}
			?>

		 </li> 
		<?php
		}
	}	
	?>	
     

  </ul>
</div>



