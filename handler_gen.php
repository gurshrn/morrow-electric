<?php 
require_once   'setup.php';
date_default_timezone_set("US/Eastern"); 
  

if(isset($_REQUEST['action']) && !empty($_REQUEST['action']))
{
    $action = $_REQUEST['action'];
   call_user_func($action);
}

function PageRecentAdded()
{
	$db = get_connection();	
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$offset=$_REQUEST['offset'];  
	$less=$_REQUEST['less'];  
	
	$display_count = $_REQUEST['display_row'];
	$offset = ( $offset - 1 ) * $display_count;
	if($less==1)
	{
		$sql="select * from customer_generator order by id desc limit 0,$offset";	 
	}
	else
	{		
		$sql="select * from customer_generator order by id desc limit $offset,$display_count";	
	}
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$data = $statement->fetchAll(); 
	foreach($data as $row) 						
	{						
	$cust_name=get_cust_info($row['cust_id'],'company_name');
	?>	
		<tr u="<?php echo $sql; ?>">
			<td>
				&nbsp;
			</td>
			<td><?php echo date("F j, Y", strtotime($row['install_date'])); ?></td>
			<td><?php echo $cust_name; ?></td>
			<td><?php echo $row['type']; ?></td> 
			<td><?php echo date("F j, Y", strtotime($row['warr_exp'])); ?></td>
			<td><a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $row['id']; ?>');">View</a></td>
		</tr>
	<?php
	}	 

}	    


function PageAlertArea() 
{
	$db = get_connection();	
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$offset=$_REQUEST['offset'];  
	$display_count = $_REQUEST['display_row'];
	$less = $_REQUEST['less']; 
	$offset = ( $offset - 1 ) * $display_count;
	if($less==1)
	{
		$sql="SELECT a . * , b . * 
	FROM customer_generator AS a
	INNER JOIN worksheet_genrator AS b ON a.id != b.work_id
	ORDER BY a.install_date DESC limit 0,$offset"; 
	}	
	else
	{
		$sql="SELECT a . * , b . * 
	FROM customer_generator AS a
	INNER JOIN worksheet_genrator AS b ON a.id != b.work_id
	ORDER BY a.install_date DESC limit $offset,$display_count";
	}	
		
	$statement = $db->prepare($sql);	  
	$statement->execute();	
	$data = $statement->fetchAll(); 
	foreach($data as $row) 						
	{						
	$gen_id=$row['gen_id'];
	$cust_name=get_cust_info($row['cust_id'],'company_name');
	$label=get_gen_info($gen_id,'label');
	?>	
		<tr <?php echo $less; ?>> 
			<td>
				&nbsp;
			</td>
			<td><?php echo $cust_name; ?></td>
			<td><?php echo $label; ?></td>
			<td><?php echo $row['type']; ?></td> 
			<td><?php echo $row['service_reg']; ?></td>
			<td>
			
			<a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $row['cust_id']; ?>');">View</a> 
			</td>
		</tr>
	<?php
	} 

}


function PageRecentService() 
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$offset=$_REQUEST['offset'];  
	$display_count = $_REQUEST['display_row'];
	$offset = ( $offset - 1 ) * $display_count;
	$sql="SELECT * 
	FROM  `worksheet_genrator` 
	WHERE  `work_sheet` =  'generatorServices'
	ORDER BY DATE( cr_date ) desc limit $offset,$display_count";	
	$statement = $db->prepare($sql);	  
	$statement->execute();	
	$result = $statement->fetchAll(); 
	foreach($result as $row) 						
	{		 				
	$work_id=$row['work_id']; 
	$genrator=$row['gen_id']; 
	$cr_date=$row['cr_date'];  
	$cust_id=get_gen_info($genrator,'cust_id');
	$cust_name=get_cust_info($cust_id,'company_name'); 
	$install_date=get_gen_info($genrator,'install_date');
	$type=get_gen_info($genrator,'type');
	$service_reg=get_gen_info($genrator,'service_reg');
	$work_status=get_work_sheet_info($work_id,'generatortroubles','trouble_status');
		
		?>	
		<tr>
			<td>
				&nbsp;
			</td>
			<td><?php echo date('F j, Y', strtotime($cr_date)); ?></td>
			<td><?php echo $cust_name; ?></td> 
			<td><?php echo $type; ?></td>  
			<td><?php echo $service_reg; ?></td> 
			
			<td><a class="view-btn" target="_blank" href="<?php echo SITE_URL; ?>pdf.php?id=<?php echo $work_id; ?>&type=generatorservices&sec=generatorServices">View</a></td>
		</tr>
		<?php
		
	} 

}
 
function PageProgress()  
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$offset=$_REQUEST['offset'];  
	$less=$_REQUEST['less'];  
	$display_count = $_REQUEST['display_row'];
	$offset = ( $offset - 1 ) * $display_count;
	
	if($less==1)
	{	
	$sql="SELECT * 
	FROM  `worksheet_genrator` 
	WHERE  `work_sheet` =  'gen_trouble'
	ORDER BY DATE( cr_date ) desc limit $offset,$display_count";	
	}
	else
	{
		$sql="SELECT * 
		FROM  `worksheet_genrator` 
		WHERE  `work_sheet` =  'gen_trouble'
		ORDER BY DATE( cr_date ) desc limit 0,$offset";	  
	}	
	$statement = $db->prepare($sql);	  
	$statement->execute();	 
	$result = $statement->fetchAll(); 
	foreach($result as $row) 						
	{						
	$work_id=$row['work_id']; 
	$genrator=$row['gen_id']; 
	$cr_date=$row['cr_date'];  
	$cust_id=get_gen_info($genrator,'cust_id');
	$cust_name=get_cust_info($cust_id,'company_name'); 
	$install_date=get_gen_info($genrator,'install_date');
	$type=get_gen_info($genrator,'type');
	$service_reg=get_gen_info($genrator,'service_reg');
	$work_status=get_work_sheet_info($work_id,'generatortroubles','trouble_status');
		if($work_status=="repair")
		{
		?>	
		<tr>
			<td>
				&nbsp;
			</td>
			<td><?php echo date('F j, Y', strtotime($cr_date)); ?></td>
			<td><?php echo $cust_name; ?></td> 
			<td><?php echo $type; ?></td>  
			<td><?php echo $service_reg; ?></td> 
			
			<td><a href="javascript:void(0)" u="generatortroubles" class="edit-btn" id="<?php echo 
			$work_id; ?>">View</a></td>
		</tr>
		<?php
		}
	}

}

function PageAssignServices()  
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$offset=$_REQUEST['offset'];  
	$display_count = $_REQUEST['display_row'];
	$offset = ( $offset - 1 ) * $display_count;
	$sql="SELECT * 
	FROM  `genassignservices` 
	ORDER BY id desc limit $offset,$display_count";	 
	$statement = $db->prepare($sql);	   
	$statement->execute();	 
	$result = $statement->fetchAll(); 
	foreach($result as $row) 						
	{						
	$gen_id=$row['gen_id'];   
	$cr_date=$row['service_date'];    
	$cust_id=get_gen_info($gen_id,'cust_id'); 
	$cust_name=get_cust_info($cust_id,'company_name'); 
	$install_date=get_gen_info($gen_id,'install_date');
	$type=get_gen_info($gen_id,'type');
	$service_reg=get_gen_info($gen_id,'service_reg'); 
	?>	 
		<tr class="gen_idd_<?php echo $gen_id; ?>">
			<td> 
				&nbsp;
			</td>
			<td><?php echo date('F j, Y', strtotime($cr_date));; ?></td> 
			<td><?php echo $cust_name; ?></td>  
			<td><?php echo $type; ?></td>  
			<td><?php echo $service_reg; ?></td> 
			
			<td>
			
			<a class="view-btn"  href="<?php echo SITE_URL; ?>?section=assign_service#gen_id_<?php echo $gen_id; ?>" >View</a> 
			</td>
		</tr>
		<?php
	
		
	}

}


function PageWarrantyExp()  
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	
	$days=$_REQUEST['days'];  
	$offset=$_REQUEST['offset'];  
	$display_count = $_REQUEST['display_row'];
	$offset = ( $offset - 1 ) * $display_count;
	if($days!=0)
		{ 
			if($days==90) 
			{  
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 60 && DATEDIFF(  '".$today."', warr_exp ) <=90)
					ORDER BY days_left asc limit $offset,$display_count";	
			}
			if($days==60)
			{ 
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 30 && DATEDIFF(  '".$today."', warr_exp ) <=60)
					ORDER BY days_left asc limit $offset,$display_count";	
			}
			if($days==30)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY days_left asc limit $offset,$display_count";	
			}
		} 
		else
		{
			$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY days_left asc limit $offset,$display_count";	 
		}
	
	$statement = $db->prepare($sql);	   
	$statement->execute();	
	$data = $statement->fetchAll(); 
	foreach($data as $row) 	   					
	{						
	$today=date('Y-m-d');
	
	$genrator=$row['id'];  
	$cust_id=get_gen_info($genrator,'cust_id');
	$cust_name=get_cust_info($cust_id,'company_name'); 
	$warr_exp=get_gen_info($genrator,'warr_exp');
	$type=get_gen_info($genrator,'type');
	$service_reg=get_gen_info($genrator,'service_reg');
	$days_left=$row['days_left']; 
	?>
	<tr>
		<td>
			&nbsp;
		</td>
		<td><?php echo $days_left; ?></td>  
		<td><?php echo date("F j, Y", strtotime($warr_exp)); ?></td>
		<td><?php echo  $cust_name; ?></td>
		<td><?php echo $type; ?></td>
		<td><a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $cust_id; ?>');">View</a> </td> 
	</tr>	
	<?php						
	
	} 

}

function GetInstallDate()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	$reg=$_REQUEST['reg'];  
	$sql="SELECT distinct install_date
	FROM  `customer_generator` where service_reg='".$reg."'";	
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$data = $statement->fetchAll(); 
	?>
	<select name="install_date" class="form-control is-filled">
		<option value="">Intalling Date</option> 
		<?php
		foreach($data as $row) 						
		{						
		$cr_date=$row['install_date'];   
		?>
			<option value="<?php echo $cr_date; ?>"><?php echo date('F j, Y', strtotime($cr_date));; ?></option>
		<?php 
		}
		?>	
	</select>
	<?php
	
}

function FilterGenService()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	$ser_reg=$_REQUEST['ser_reg'];  
	$install_date=$_REQUEST['install_date'];  

	$display_row=10; 
	$sql="SELECT * 
	FROM  `customer_generator` where service_reg='".$ser_reg."' and install_date='".$install_date."'
	ORDER BY DATE( install_date ) desc ";
	$statement = $db->prepare($sql); 	
	$statement->execute();
	$count = $statement->rowCount();
	if($count > 0)
	{ 
	$sql="SELECT * 
	FROM  `customer_generator` where service_reg='".$ser_reg."' and install_date='".$install_date."'
	ORDER BY DATE( install_date ) desc ";
	$statement = $db->prepare($sql);	 
	$statement->execute();	
	$data = $statement->fetchAll(); 
	?>
	<input type="hidden" class="recent_service_total_rec" value="<?php echo $count; ?>">
	<input type="hidden" class="recent_service_display_row" value="<?php echo $display_row; ?>">  
	<input type="hidden" class="recent_service_index" value="2">    
	<div class="responsive-table recent_service">
		<table>
		<thead>
			<tr>
				<th> </th>
				<th>Last Serviced</th> 
				<th>Customer Name</th> 
				<th>Type</th>
				<th>Region</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php 
			foreach($data as $row) 						
			{						
			$gen_id=$row['id'];   
			$cr_date=$row['install_date'];   
			$cust_id=$row['cust_id']; 
			$cust_name=get_cust_info($cust_id,'company_name'); 
			$type=$row['type']; 
			$service_reg=$row['service_reg']; 
			$last_serviced=get_last_service($gen_id); 
			$last_serviceds=get_last_service($gen_id); 
			if($last_serviced==0)
			{
				$last_serviced="Nil";
			}
			else
			{
				$last_serviced=date('F j, Y', strtotime($last_serviced));
			}		
			?>	 
				<tr id="gen_id_<?php echo $gen_id; ?>"> 
					<td>
						&nbsp;
					</td>
					<td><?php echo $last_serviced; ?></td> 
					<td><?php echo $cust_name; ?></td> 
					<td><?php echo $type; ?></td>  
					<td><?php echo $service_reg; ?></td> 
					
					<td>
					<?php
					if($last_serviceds==0)
					{
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('0',<?php echo $gen_id; ?>);">Assign</a>
					<?php
					}
					else
					{	
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('1',<?php echo $gen_id; ?>);">Edit</a> 
					<?php
					}
					?>
					</td>
				</tr>
				<?php
				
			} 
			?>
			
		</tbody>
		</table>
	</div>
	<?php
	}
}

function FilterByRegion_1()
{
	
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	$ser_reg=$_REQUEST['region'];    
	$query="";
	if($ser_reg!="")
	{
		$query=" and service_reg='".$ser_reg."'";
	}		
	
	$display_row=10; 
	$sql="SELECT * 
	FROM  `customer_generator` where id!='' $query
	";
	$statement = $db->prepare($sql); 	
	$statement->execute();
	$count = $statement->rowCount();
	if($count > 0)
	{ 
	$sql="SELECT * 
	FROM  `customer_generator` where id!='' $query
	";
	$statement = $db->prepare($sql);	 
	$statement->execute();	
	$data = $statement->fetchAll(); 
	?>
	<input type="hidden" class="recent_service_total_rec" value="<?php echo $count; ?>">
	<input type="hidden" class="recent_service_display_row" value="<?php echo $display_row; ?>">  
	<input type="hidden" class="recent_service_index" value="2">    
	<div class="responsive-table recent_service"> 
		<table id="example">
		<thead> 
			<tr>
			<th></th>
			<th style="width: 18%;">Last Serviced</th> 
			<th style="width: 25%;">Customer Name</th> 
			<th>Label</th> 
			<th style="width: 10%;">Type</th>
			
			<th></th> 
			</tr> 
		</thead>
		<tbody>
			<?php 
			foreach($data as $row) 						
			{						
			$gen_id=$row['id'];   
			$label=$row['label'];   
			$cr_date=$row['install_date'];   
			$cust_id=$row['cust_id']; 
			$cust_name=get_cust_info($cust_id,'company_name'); 
			$type=$row['type']; 
			$service_reg=$row['service_reg']; 
			$last_serviced=get_last_service($gen_id); 
			$last_serviceds=get_last_service($gen_id); 
			if($last_serviced==0)
			{
				$last_serviced="(None)";
			}
			else
			{
				$last_serviced=date('F j, Y', strtotime($last_serviced));
			}
		
			?>	 
				<tr id="gen_id_<?php echo $gen_id; ?>">
					<td> 
						&nbsp;
					</td>
					<td><?php echo $last_serviced; ?></td> 
					<td><?php echo $cust_name; ?></td> 
					<td><?php echo $label; ?></td> 
					<td><?php echo $type; ?></td>  
					
					
					<td>
					<?php
					if($last_serviceds==0)
					{
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('0',<?php echo $gen_id; ?>);">Assign</a>
					<?php
					}
					else
					{	
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('1',<?php echo $gen_id; ?>);">Edit</a> 
					<?php
					}
					?>
					</td>
				</tr>
				<?php
				
			} 
			?>
			
		</tbody>
		</table>
	</div>
	<?php
	}
}


function ResetGenService() 
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	 

	$display_row=10; 
	$sql="SELECT * 
	FROM  `customer_generator` 	ORDER BY DATE( install_date ) desc ";
	$statement = $db->prepare($sql); 	 
	$statement->execute();
	$count = $statement->rowCount();
	if($count > 0)
	{  
	$sql="SELECT *  
	FROM  `customer_generator`
	ORDER BY DATE( install_date ) desc ";
	$statement = $db->prepare($sql);	  
	$statement->execute();	
	$data = $statement->fetchAll();   
	?>
	<input type="hidden" class="recent_service_total_rec" value="<?php echo $count; ?>">
	<input type="hidden" class="recent_service_display_row" value="<?php echo $display_row; ?>">  
	<input type="hidden" class="recent_service_index" value="2">     
	<div class="responsive-table recent_service"> 
		<table>
		<thead>
			<tr>
				<th> </th>
				<th>Last Serviced</th> 
				<th>Customer Name</th> 
				<th>Type</th>
				<th>Region</th>
				<th></th>
			</tr>
		</thead>
		<tbody> 
			<?php 
			foreach($data as $row) 						
			{						
			$gen_id=$row['id'];   
			$cr_date=$row['install_date'];   
			$cust_id=$row['cust_id']; 
			$cust_name=get_cust_info($cust_id,'company_name'); 
			$type=$row['type']; 
			$service_reg=$row['service_reg']; 
			$last_serviced=get_last_service($gen_id); 
			$last_serviceds=get_last_service($gen_id); 
			if($last_serviced==0)
			{
				$last_serviced="Nil";
			}
			else
			{
				$last_serviced=date('F j, Y', strtotime($last_serviced));
			}		
			?>	 
				<tr class="gen_id_<?php echo $gen_id; ?>">
					<td>
						&nbsp;
					</td>
					<td><?php echo $last_serviced; ?></td> 
					<td><?php echo $cust_name; ?></td> 
					<td><?php echo $type; ?></td>  
					<td><?php echo $service_reg; ?></td> 
					
					<td>
					<?php
					if($last_serviceds==0)
					{
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('0',<?php echo $gen_id; ?>);">Assign</a>
					<?php
					}
					else
					{	
					?>
					<a class="view-btn"  href="javascript:void(0);" onclick="assign_gen('1',<?php echo $gen_id; ?>);">Edit</a> 
					<?php
					}
					?>
					</td>
				</tr>
				<?php
				
			} 
			?>
			
		</tbody>
		</table>
	</div>
	<?php
	}
}



function ShowAssignGen()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	$gen_id=$_REQUEST['gen_id']; 
	$ser_on=$_REQUEST['ser_on'];  
	$e=$_REQUEST['e'];  
	$cust_id=get_gen_info($gen_id,'cust_id');
	$cust_name=get_cust_info($cust_id,'company_name'); 
	if($e==1)
	{
		$type=genassignservices($gen_id,'type');
		$staff_ids=genassignservices($gen_id,'staff_id');
		$service_on=genassignservices($gen_id,'service_date');
		$service_on=date('m/d/Y', strtotime($service_on)); 
	}	  
	
?> 
<script>
jQuery(document).ready(function()
{
	jQuery( ".datepicker" ).datepicker({
        changeMonth: false,
        changeYear: false,
		minDate:0 
		
    }); 
	
	jQuery('#assign_service').validate({
		
		rules: { 
			staff_id: {
				required: true  
			},
			
			assign_date: {
				required: true
			}
		},
		
		submitHandler: function(form) {
			
			var len=jQuery('input[name=service_type]:checked').length;
			if(len=="0")
			{
				alert('Select Service type');
				return false;
			}	
			jQuery('.result').empty().show();	 		
			jQuery(form).ajaxSubmit({
				type: "POST",
				data: jQuery(form).serialize(),
				url: '<?php echo SITE_URL; ?>handler_new.php',  
				success: function(data) 
				{
					jQuery('.result').empty().append('<div class="alert static alert-success">Record saved successfully</div>'); 
					jQuery('.result').fadeOut(3000); 
					var selection=jQuery('select[name=ser_reg]').val();  
					filter_by_region(1); 	
				}   
			});
		} 
		
	});
	
	
});

function close_this()
{
	jQuery('.box').hide();
	jQuery('.assign_box').slideUp();  
	jQuery('.filter_gen').slideDown();  
	
	jQuery('#editFromToggle').empty();
}

</script>
<h3 class="section-hdr">Assign for Service <a href="javascript:void(0);" onclick="close_this();" class="close"><i class="fa fa-plus"></i></a></h3> 	 
<form name="assign_service" id="assign_service" action="" method="post" class="cs-form"> 
    <input type="hidden" name="gen_id" value="<?php echo $gen_id; ?>">
    <input type="hidden" name="action" value="AddAssignedService">  
	
	<div>  
		<label style="margin-left: 20px;"> 
			<strong><?php echo $cust_name; ?></strong>
		</label>
		<label>
			<?php echo get_gen_info($gen_id,'service_reg'); ?>
		</label>
		<label>
			Serviced on <?php echo $ser_on; ?>  
		</label>
		<label>
			<select name="staff_id">
				<option value="">Assign To</option>
				<?php
				$sql="SELECT * 
				FROM  `users` 
				where  id!='1'";	 
				$statement = $db->prepare($sql);	
				$statement->execute();	
				$data = $statement->fetchAll(); 
				foreach($data as $row) 						
				{						
				$staff_id=$row['id'];    
				$first_name=$row['first_name'];   
				$last_name=$row['last_name'];   
				$name=$first_name.' '.$last_name;
				?> 
				<option value="<?php echo $staff_id; ?>" <?php if($staff_id==$staff_ids) { echo 'selected'; } ?>><?php echo $name; ?></option>
				<?php
				}
				?>
			</select>  
		</label>
		<span style="margin-left: 20px;"> 
		<label>
			<input name="service_type" value="service"  type="radio" <?php if($type=="service") { echo 'checked'; } ?>>
			<span class="custm-radio"></span>Service</label> 
		<label> 
		<label>
			<input name="service_type" value="inspection"  type="radio" <?php if($type=="inspection") { echo 'checked'; } ?>>
			<span class="custm-radio"></span>Inspection</label>     
		<label> 
		<label>
			<input name="service_type" value="repair"  type="radio" <?php if($type=="repair") { echo 'checked'; } ?>>
			<span class="custm-radio"></span>Repair</label>     
		<label>  
		</span>
		<label>
			<input type="text" name="assign_date" class="datepicker" placeholder="Service Date" value="<?php echo $service_on; ?>" readonly> 
		</label>
		<label class="rs">
			<textarea name="notes" placeholder="Notes"></textarea>  
		</label>    
		<input type="submit" class="cs-btn btn-green" name="submit" value="ASSIGN" style="margin-left:10px;">
		  
    </div>
	<div class="result"></div>   
</form>
<?php	
}

function ReportbyRegion()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
	$region=$_REQUEST['region']; 
	$region_query="";
	if($region!="") 
	{
		$region_query=" AND c.service_reg =  '".$region."'";
	}
?>
<div class="heading small clearfix">
		<div class="pull-left">In Progress Repairs</div>
	</div>
	<div class="details-table">  
		<?php
		$display_row=10;  
		$sql="SELECT a. * , b. * , c.id, c.service_reg
		FROM generatortroubles AS a
		INNER JOIN worksheet_genrator AS b ON a.id = b.work_id
		INNER JOIN customer_generator AS c ON b.gen_id = c.id
		WHERE a.trouble_status =  'complete' ".$region_query;
		$statement = $db->prepare($sql);	
		$statement->execute();
		$count = $statement->rowCount();
		if($count > 0)
		{    
		$sql="SELECT a. * , b. * , c.id, c.service_reg
		FROM generatortroubles AS a
		INNER JOIN worksheet_genrator AS b ON a.id = b.work_id
		INNER JOIN customer_generator AS c ON b.gen_id = c.id
		WHERE a.trouble_status =  'complete' ".$region_query;	    
		$statement = $db->prepare($sql);	  
		$statement->execute();	    
		$result = $statement->fetchAll(); 
		?>
		<input type="hidden" class="progress_rep_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="progress_rep_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="progress_rep_index" value="2">     
		<div class="responsive-table progress_rep display">
			<table> 
				<thead>
					<tr>
						<th> </th>
						<th>Date Started</th>
						<th>Customer Name</th>
						<th>Type</th>
						<th>Region</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach($result as $row) 						
					{						
					$genrator=$row['gen_id'];  
					$cr_date=$row['cr_date'];  
					$cust_id=get_gen_info($genrator,'cust_id');
					$cust_name=get_cust_info($cust_id,'company_name'); 
					$install_date=get_gen_info($genrator,'install_date');
					$type=get_gen_info($genrator,'type');
					$service_reg=get_gen_info($genrator,'service_reg');
					  
						
						?>	
						<tr>
							<td>
								&nbsp;
							</td>
							<td><?php echo date('F j, Y', strtotime($install_date)); ?></td>
							<td><?php echo $cust_name; ?></td> 
							<td><?php echo $type; ?></td>  
							<td><?php echo $service_reg; ?></td> 
							
							<td>
							<a href="javascript:void(0)" u="generatortroubles" class="edit-btn-1" id="<?php echo 
							$work_id; ?>" onclick="alert('Where you want to redirect');">View</a>
							</td>
						</tr>
						<?php
						
					}
					?>
					
				</tbody>
			</table>
		</div>
		<?php
		}
		?>
	</div>
<?php	
}


function ReportbySpringWinter()
{

$db = get_connection();	 
get_mysqlconnection();	
unset($_REQUEST['action']);

?>
<div class="heading small clearfix">
		<div class="pull-left">In Progress Repairs</div>
	</div>
	<div class="details-table">  
		<?php
		$display_row=10;  
		$sql="SELECT * 
		FROM  `customer_generator` 
		WHERE  `winterize` =  'yes'";
		$statement = $db->prepare($sql);	
		$statement->execute();
		$count = $statement->rowCount();
		if($count > 0)
		{    
		$sql="SELECT * 
		FROM  `customer_generator` 
		WHERE  `winterize` =  'yes' order by DATE(install_date) desc";      
		$statement = $db->prepare($sql);	   
		$statement->execute();	    
		$result = $statement->fetchAll(); 
		?>
		<input type="hidden" class="progress_rep_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="progress_rep_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="progress_rep_index" value="2">     
		<div class="responsive-table progress_rep display">
			<table> 
				<thead>
					<tr>
						<th> </th>
						<th>Date Started</th>
						<th>Customer Name</th>
						<th>Type</th>
						<th>Region</th>
						<th>Spring Startup</th>
						<th>Generator Winter<br> Storage</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					foreach($result as $row) 						
					{						
					$genrator=$row['id'];   
					$cr_date=$row['cr_date'];  
					$cust_id=get_gen_info($genrator,'cust_id');
					$cust_name=get_cust_info($cust_id,'company_name'); 
					$install_date=get_gen_info($genrator,'install_date');
					$type=get_gen_info($genrator,'type');
					$service_reg=get_gen_info($genrator,'service_reg'); 
					  
					$count=check_work_sheet($gen_id);	
						?>	
						<tr>
							<td>
								&nbsp;
							</td>
							<td><?php echo date('F j, Y', strtotime($install_date)); ?></td>
							<td><?php echo $cust_name; ?></td> 
							<td><?php echo $type; ?></td>  
							<td><?php echo $service_reg; ?></td> 
							<td>&nbsp;</td> 
							<td>
							<?php
							if($count==0)
							{	
							?>
							<i class="fa blue fa-check-circle-o"></i>
							<?php
							}
							?>
							</td>
							<td>
							<a href="javascript:void(0)" u="generatortroubles" class="edit-btn-1" id="<?php echo 
							$work_id; ?>" onclick="alert('Where you want to redirect');">View</a>
							</td>
						</tr>
						<?php
						
					}
					?>
					
				</tbody>
			</table>
		</div>
		<?php
		}
		?>
	</div>
<?php	
}

function ShowGenReportt()
{
$db = get_connection();	 
get_mysqlconnection();	
unset($_REQUEST['action']); 
?>
<div class="report_sec">
		<h3 class="section-hdr clearfix">New Report<a href="javascript:void(0);" onclick="jQuery('.cust_gen_area').empty().slideUp();" class="close"><i class="fa fa-plus"></i></a></h3>  
		 	<div class="new-report-gen clearfix cs-form">
				<label>
					<input name="report_type" value="3" type="radio" onclick="show_hide_reg_drop(3);">
					<span class="custm-radio"></span>ASSIGN FOR SERVICE</label> 
				<label>
					<input name="report_type" value="1" type="radio" onclick="show_hide_reg_drop(1);">
					<span class="custm-radio"></span>SERVICE BY REGION</label> 
				<label>
					<input name="report_type" value="2" type="radio" onclick="show_hide_reg_drop(2);">
					<span class="custm-radio"></span>SPRING START-UP & WINTERIZING</label> 
				     	
			 	
					<span class="reg_sec">
						 
					</span>
			
			
			
			<span class="report_submit" style="margin-left: 15px;">
				<a class="cs-btn btn-green" href="javascript:void(0);" onclick="create_report();" style="display:none;">Create Report</a>
			</span>	 
		 </div>
</div>
<?php	
}

function CheckRegionReport()
{
$db = get_connection();	 
get_mysqlconnection();	
unset($_REQUEST['action']);	 
$region=$_REQUEST['region'];	
$region_query="";
if($region!="") 
{
	$region_query=" AND c.service_reg =  '".$region."'";
}
$year=date('Y'); 
$sql="SELECT a. * , b. * , c.id, c.service_reg
FROM generatorservices AS a
INNER JOIN worksheet_genrator AS b ON a.id = b.work_id
INNER JOIN customer_generator AS c ON b.gen_id = c.id
WHERE YEAR( a.created ) =  '".$year."' ".$region_query;
$statement = $db->prepare($sql);	  
$statement->execute(); 
echo $statement->rowCount();

}


function IncludeRegionSelect()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);	 
	$sql="SELECT distinct service_reg
	FROM  `customer_generator`";	
	$statement = $db->prepare($sql);	
	$statement->execute();	
	$data = $statement->fetchAll();    
?>
<select name="region_type" onchange="check_region_report_type();">
	<option value="all">All</option>
	<option value="current">Current Year</option>
</select>	 
<!--
<select name="region" onchange="check_region_report();">
	<option value="">Select A Region</option>
	<?php
	foreach($data as $row) 						
	{						
	$service_reg=$row['service_reg'];    
	?>
	<option value="<?php echo $service_reg; ?>"><?php echo $service_reg; ?></option>
	<?php
	}
	?>
</select>
-->
<?php
}

function IncludeServiceDatepicker()
{
	unset($_REQUEST['action']);	 
?>
<script>
jQuery(document).ready(function()
{
	jQuery( ".datepicker" ).datepicker({
        changeMonth: false,
        changeYear: false,
		onSelect: function(year, month, inst) {
		var loader='<img src="<?php SITE_URL; ?>/assets/images/loader.gif" />';	 
		jQuery('.report_submit').empty().append(loader);   	
		var datValue=jQuery(this).val(); 
		var year= jQuery.datepicker.formatDate('yy-mm-dd', new Date(datValue));
		check_sel_date_rec(year);
		return false;
		}
	});
}); 

function check_sel_date_rec(date)
{
	jQuery.ajax({type: "POST", 
	url: "<?php echo SITE_URL; ?>/handler_gen.php",
	data: "date="+date+"&action=CheckSelDateRec",       
	success:function(result)        
	{
		 
		if(result > 0)
		{
			jQuery('.report_submit').empty().append('<a class="cs-btn btn-green" href="<?php echo SITE_URL; ?>serviced_report.php?date='+date+'">Create Report</a>'); 
		}
		else
		{
			jQuery('.report_submit').empty().append('<div class="alert static alert-danger">No record found with this date</div>'); 
		}
	}, 
	error:function(e){  
		console.log(e); 
	}	
	}); 
}

</script>
<input type="text" name="serviced_date" class="datepicker" placeholder="Select Date" readonly> 
<?php	
}

function CheckSelDateRec()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);	 
	$date=$_REQUEST['date'];
	$sql="SELECT * 
	FROM  `genassignservices` 
	WHERE service_date =  '".$date."'";	
	$statement = $db->prepare($sql);	
	$statement->execute();	 
	echo $count = $statement->rowCount();  	
		
}
function progress_repair()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	<div class="details-table">
		<?php
			$display_row=10;  
			$sql="SELECT * 
			FROM  `worksheet_genrator` 
			WHERE  `work_sheet` =  'gen_trouble'
			ORDER BY DATE( cr_date ) desc";
			$statement = $db->prepare($sql);	
			$statement->execute();
			$count = $statement->rowCount();
			if($count > 0)
			{    
			$sql="SELECT * 
			FROM  `worksheet_genrator` 
			WHERE  `work_sheet` =  'gen_trouble'
			ORDER BY DATE( cr_date ) desc limit 0,$display_row";	  
			$statement = $db->prepare($sql);	  
			$statement->execute();	    
			$result = $statement->fetchAll();  
		?>
		<input type="hidden" class="progress_rep_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="progress_rep_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="progress_rep_index" value="2">    
		<div class="responsive-table progress_rep display">
			<table> 
				<thead>
					<tr>
						<th> </th>
						<th>Date Started</th>
						<th>Customer Name</th>
						<th>Type</th>
						<th>Region</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach($result as $row) 						
						{						
							$work_id=$row['work_id']; 
							$genrator=$row['gen_id']; 
							$cr_date=$row['cr_date'];  
							$cust_id=get_gen_info($genrator,'cust_id');
							$cust_name=get_cust_info($cust_id,'company_name'); 
							$install_date=get_gen_info($genrator,'install_date');
							$type=get_gen_info($genrator,'type');
							$service_reg=get_gen_info($genrator,'service_reg');
							$work_status=get_work_sheet_info($work_id,'generatortroubles','trouble_status');
							if($work_status=="repair")
							{ 
					?>	
								<tr>
									<td>
										&nbsp;
									</td>
									<td><?php echo date('F j, Y', strtotime($cr_date)); ?></td>
									<td><?php echo $cust_name; ?></td> 
									<td><?php echo $type; ?></td>  
									<td><?php echo $service_reg; ?></td> 
									
									<td>
									<a href="javascript:void(0)" u="generatortroubles" class="edit-btn" id="<?php echo 
									$work_id; ?>">View</a>
									</td>
								</tr>
						<?php } } ?>
				</tbody>
			</table>
		</div>
<?php } ?>

	</div>
		<div class="more-footer clearfix">
			<?php
				if($count > 10)  
				{ 
			?>
					<div class="pull-right"><a class="view-btn more progress_more" href="javascript:void(0);" onclick="page_progress();">More</a></div>   
					<div class="progress_less less_page">
						<a class="view-btn more recent_added_less " href="javascript:void(0);" onclick="page_recent_less();">Less</a>  
					</div>
			<?php }	 ?>
		</div>


<?php } 

function recently_added_gen()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	<div class="details-table ">
	
		<?php
			$display_row=10; 
			$sql="select * from customer_generator order by id desc"; 
			$statement = $db->prepare($sql);	
			$statement->execute();
			$count = $statement->rowCount();
			if($count > 0)
			{
				$sql="select * from customer_generator order by id desc limit 0,$display_row";	
				$statement = $db->prepare($sql);	
				$statement->execute();	
				$data = $statement->fetchAll();   
		?>
			<input type="hidden" class="recent_total_rec" value="<?php echo $count; ?>">
			<input type="hidden" class="display_row" value="<?php echo $display_row; ?>">  
			<input type="hidden" class="recent_index" value="2">    
			<div class="responsive-table recent_added"> 
			<table>
				<thead>
					<tr>
						<th> </th>
						<th>Install Date</th>
						<th>Customer Name</th>
						<th>Type</th>
						<th>Warranty Expiring</th>
						<th></th>
					</tr>
				</thead>					
				<tbody>						
				<?php 
					foreach($data as $row) 						
					{						
						$cust_name=get_cust_info($row['cust_id'],'company_name'); 
				?>	
						<tr>
							<td>
								&nbsp;
							</td>
							<td><?php echo date("F j, Y", strtotime($row['install_date'])); ?></td>
							<td><?php echo $cust_name; ?></td>
							<td><?php echo $row['type']; ?></td> 
							<td><?php echo date("F j, Y", strtotime($row['warr_exp'])); ?></td>
							<td>
							<a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $row['id']; ?>');">View</a> 
							</td>	 
						</tr>
				<?php } ?>	
				</tbody>
			</table>
		</div>
<?php } ?>

	</div>
	<div class="more-footer clearfix">
		<?php
			if($count > 0)
			{ 
			
		?>
				<div class="pull-right"> 
				<a class="view-btn more recent_added_more less_page" href="javascript:void(0);" onclick="page_recent_added();">More</a> 
				<div class="recent_less less_page"></div>     
				</div> 
		<?php } ?>
	</div>
<?php } 

function asigned_service_gen()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	<div class="details-table">
		<?php
			$today=date('Y-m-d');
			$display_row=10; 
			$sql="SELECT * 
			FROM  `genassignservices` 
			WHERE DATE( cr_date ) >= DATE( NOW( ) )
			ORDER BY id desc"; 
			$statement = $db->prepare($sql);	
			$statement->execute();
			$count = $statement->rowCount();
			if($count > 0)
			{
			$sql="SELECT * 
			FROM  `genassignservices` 
			WHERE DATE( cr_date ) >= DATE( NOW( ) )
			ORDER BY id desc limit 0,$display_row";	
			$statement = $db->prepare($sql); 	
			$statement->execute();	 
			$data = $statement->fetchAll();  
		?>
		<input type="hidden" class="assgn_services_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="assgn_services_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="assgn_services_index" value="2">    
		<div class="responsive-table assgn_services">  
			<table>
				<thead>
					<tr>
						<th> </th>
						<th style="width:16%;">Scheduled Serviced</th> 
						<th style="width: 22%;">Customer Name</th>    
						<th style="width: 15%;">Type</th>
						<th style="width: 17%;">Region</th>
						<th style="width: 15%;">Assigned To</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody> 
					<?php 
						foreach($data as $row) 						
						{						
							$id=$row['id'];   
							$gen_id=$row['gen_id'];   
							$cr_date=$row['service_date'];    
							$cust_id=get_gen_info($gen_id,'cust_id'); 
							$cust_name=get_cust_info($cust_id,'company_name'); 
							$install_date=get_gen_info($gen_id,'install_date');
							$type=get_gen_info($gen_id,'type');
							$service_reg=get_gen_info($gen_id,'service_reg');
							$staff_id=$row['staff_id']; 
							$fname=get_staff_info($staff_id,'first_name'); 
							$lname=get_staff_info($staff_id,'last_name');   
					?>	 
							<tr class="gen_idd_<?php echo $gen_id; ?>">
								<td> 
									&nbsp;
								</td>
								<td><?php echo date('F j, Y', strtotime($cr_date));; ?></td> 
								<td><?php echo $cust_name; ?></td>  
								<td><?php echo $type; ?></td>  
								<td><?php echo $service_reg; ?></td> 
								<td><?php echo $fname. " ".$lname; ?></td> 
								<td> 
									<?php 
										$asd=count_gen_serviced_complete_1($id); 
										if($asd!=0)  
										{	 
									?>
											<i class="fa green-col fa-check-circle"></i>
									<?php } ?>
								</td>
								<td> 
								<a class="view-btn"  href="<?php echo SITE_URL; ?>?section=assign_service#gen_id_<?php echo $gen_id; ?>" >View</a> 
								</td>
							</tr>
					<?php }  ?>
							
				</tbody>
			</table>
		</div>
	</div>
	<div class="more-footer clearfix">
		<?php if($count > 10){ 	?>
				<div class="pull-right"><a class="view-btn more assgn_services_more" href="javascript:void(0);" onclick="page_assign_services();">More</a></div>
		<?php } ?>
	</div>
<?php } } 
	
function recently_service_gen()
{

	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	
	<div class="details-table">
	<?php
	
		$display_row=10; 
		$sql="SELECT * 
		FROM  `worksheet_genrator` 
		WHERE  `work_sheet` =  'generatorServices'
		ORDER BY DATE( cr_date ) desc ";
		$statement = $db->prepare($sql);	
		$statement->execute();
		$count = $statement->rowCount();
		if($count > 0)
		{ 
		$sql="SELECT * 
		FROM  `worksheet_genrator` 
		WHERE  `work_sheet` =  'generatorServices'
		ORDER BY DATE( cr_date ) desc  limit 0,$display_row";	
		$statement = $db->prepare($sql);	
		$statement->execute();	
		$data = $statement->fetchAll();  
	?>
	<input type="hidden" class="recent_service_total_rec" value="<?php echo $count; ?>">
	<input type="hidden" class="recent_service_display_row" value="<?php echo $display_row; ?>">  
	<input type="hidden" class="recent_service_index" value="2">    
	<div class="responsive-table recent_service">
		<table>
			<thead>
				<tr>
					<th> </th>
					<th>Date Completed</th> 
					<th>Customer Name</th>
					<th>Type</th>
					<th>Region</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($data as $row) 						
					{						
						$work_id=$row['work_id']; 
						$genrator=$row['gen_id']; 
						$cr_date=$row['cr_date'];  
						$cust_id=get_gen_info($genrator,'cust_id');
						$cust_name=get_cust_info($cust_id,'company_name'); 
						$install_date=get_gen_info($genrator,'install_date');
						$type=get_gen_info($genrator,'type');
						$service_reg=get_gen_info($genrator,'service_reg');
						$work_status=get_work_sheet_info($work_id,'generatortroubles','trouble_status'); 
				?>	
					<tr>
						<td>
							&nbsp;
						</td>
						<td><?php echo date('F j, Y', strtotime($cr_date)); ?></td>
						<td><?php echo $cust_name; ?></td> 
						<td><?php echo $type; ?></td>  
						<td><?php echo $service_reg; ?></td> 
						
						<td><a class="view-btn" target="_blank" href="<?php echo SITE_URL; ?>pdf.php?id=<?php echo $work_id; ?>&type=generatorservices&sec=generatorServices">View</a></td>
					</tr>
				<?php }  ?>
			</tbody>
		</table>
	</div>
		<?php } ?>
	</div>
	<div class="more-footer clearfix">
		<?php
			if($count > 10)
			{ 
		?>
			<div class="pull-right"><a class="view-btn more page_recent_service_more" href="javascript:void(0);" onclick="page_recent_service();">More</a></div>  
		<?php } ?>
	</div>
<?php }

function alert_area_gen()
{
	ini_set('memory_limit', '-1');
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	<div class="details-table">
		<?php
			$display_row=10; 
			$sql="SELECT a . * , b . * 
			FROM customer_generator AS a
			INNER JOIN worksheet_genrator AS b ON a.id != b.work_id
			ORDER BY a.install_date DESC";
			$statement = $db->prepare($sql);	
			$statement->execute();
			$count = $statement->rowCount();
			if($count > 0)
			{
			$sql="SELECT a . * , b . * 
			FROM customer_generator AS a
			INNER JOIN worksheet_genrator AS b ON a.id != b.work_id
			ORDER BY a.install_date DESC limit 0,$display_row";	 
			$statement = $db->prepare($sql);	
			$statement->execute();	
			$data = $statement->fetchAll();    
		?>
		<input type="hidden" class="alert_area_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="alert_area_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="alert_area_index" value="2">     
		<div class="responsive-table alert_area">
			<table>
				<thead>
					<tr>
						<th> </th>
						<th>Customer Name</th>
						<th>Label</th> 
						<th>Type</th>
						<th width="25%">Region</th>
						<th></th>
					</tr>
				</thead>					
				<tbody>						
					<?php 
						foreach($data as $row) 						
						{						
							$gen_id=$row['gen_id'];
							$cust_name=get_cust_info($row['cust_id'],'company_name');
							$label=get_gen_info($gen_id,'label'); 
					?>	
							<tr>
								<td>
									&nbsp;
								</td>
								<td><?php echo $cust_name; ?></td>
								<td><?php echo $label; ?></td>
								<td><?php echo $row['type']; ?></td> 
								<td><?php echo $row['service_reg']; ?></td>
								<td>
								
								<a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $row['cust_id']; ?>');">View</a> 
								</td>
							</tr>
					<?php }  ?>	
				</tbody>
			</table>
		</div>
		<?php } ?>
	</div>
	<div class="more-footer clearfix">
		<?php
			if($count > 10)  
			{ 
		?>
				<div class="pull-right"><a class="view-btn more alert_area_more" href="javascript:void(0);" onclick="page_alert_area();">More</a></div>  
				<div class="alert_recent_less less_page"></div> 
		<?php } ?>
	</div>
<?php }

function warranty_expiration_gen()
{
	$db = get_connection();	 
	get_mysqlconnection();	
	unset($_REQUEST['action']);
?>
	
	<?php
		$display_row=10; 
		$today=date('Y-m-d');
		if(isset($_GET['days']))
		{
			if($_GET['days']==90) 
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 60 && DATEDIFF(  '".$today."', warr_exp ) <=90)
					ORDER BY DATE( cr_date ) DESC ";	
			}
			if($_GET['days']==60)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF( '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 30 && DATEDIFF(  '".$today."', warr_exp ) <=60)
					ORDER BY DATE( cr_date ) DESC";	
			}
			if($_GET['days']==30)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY DATE( cr_date ) DESC";	
			}
		}
		else
		{
			$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY DATE( cr_date ) DESC";	 
		}	
		
		
		$statement = $db->prepare($sql);	
		$statement->execute();
		$count = $statement->rowCount();
		if($count > 0)
		{
		if(isset($_GET['days']))
		{
			if($_GET['days']==90)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 60 && DATEDIFF(  '".$today."', warr_exp ) <=90)
					ORDER BY days_left asc limit 0,$display_row";	
			}
			if($_GET['days']==60)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 30 && DATEDIFF(  '".$today."', warr_exp ) <=60)
					ORDER BY days_left asc limit 0,$display_row";	
			}
			if($_GET['days']==30)
			{
				$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY days_left asc limit 0,$display_row";	
			}
		}
		else
		{
			$sql="SELECT id, cust_id, label, service_reg, model, 
					TYPE , sn, install_date, warr_exp, oil_qty, oil_filter, air_filter, spark_plug, winterize, 
					STATUS , cr_date, DATEDIFF(  '".$today."', warr_exp ) AS days_left
					FROM  `customer_generator` 
					WHERE (DATEDIFF(  '".$today."', warr_exp ) > 0 && DATEDIFF(  '".$today."', warr_exp ) <=30)
					ORDER BY days_left asc limit 0,$display_row";	 
		}
		
			$statement = $db->prepare($sql);	  
			$statement->execute();	
			$data = $statement->fetchAll();      
		?>
		<input type="hidden" class="warranty_exp_total_rec" value="<?php echo $count; ?>">
		<input type="hidden" class="warranty_exp_display_row" value="<?php echo $display_row; ?>">  
		<input type="hidden" class="warranty_exp_index" value="2">   
		<?php
			if(isset($_GET['days']))
			{ 
		?>
				<input type="hidden" class="days" value="<?php echo $_GET['days']; ?>"> 
		<?php 
			}
			else
			{	 
		?>
				<input type="hidden" class="days" value="0">  
		<?php }  ?>
		<div class="details-table">   
			<div class="responsive-table warranty_exp"> 
				<table> 
					<thead>
						<tr>
							<th> </th>
							<th>Days Left</th> 
							<th>Expiry Date</th>
							<th>Customer Name</th>
							<th>Type</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
							foreach($data as $row) 						
							{						
							$today=date('Y-m-d');
							
							$genrator=$row['id'];  
							$cust_id=get_gen_info($genrator,'cust_id');
							$cust_name=get_cust_info($cust_id,'company_name'); 
							$warr_exp=get_gen_info($genrator,'warr_exp');
							$type=get_gen_info($genrator,'type');
							$service_reg=get_gen_info($genrator,'service_reg');
							$days_left=$row['days_left'];  
						?>
						<tr>
							<td>
								&nbsp;
							</td>
							<td><?php echo $days_left; ?></td>  
							<td><?php echo date("F j, Y", strtotime($warr_exp)); ?></td>
							<td><?php echo  $cust_name; ?></td>
							<td><?php echo $type; ?></td>
							<td><a href="javascript:void(0)" class="view-btn" onclick="show_cust_gen('1','<?php echo $cust_id; ?>');">View</a> </td> 
						</tr>	
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php } ?>
		<div class="more-footer clearfix"> 
			<?php
				if($count > 10)
				{	 
			?>
					<div class="pull-right warranty_exp_more"><a class="view-btn more " href="javascript:void(0);" onclick="page_warranty_exp();">More</a></div>   
			<?php } ?>
		</div> 
<?php }

?>

